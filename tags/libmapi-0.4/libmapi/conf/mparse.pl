#!/usr/bin/perl -w

###################################################
# package to parse the mapi-properties files and 
# generate code for libmapi in OpenChange
#
# Perl code based on pidl one from Andrew Tridgell and the Samba team
#
# Copyright (C) Julien Kerihuel 2005-2007
# released under the GNU GPL

use strict;
use Getopt::Long;

my $ret = "";
my $tabs = "";

sub indent() { $tabs.="\t"; }
sub deindent() { $tabs = substr($tabs, 1); }
sub mparse($) { $ret .= $tabs.(shift)."\n"; }

my($opt_outputdir) = '.';
my($opt_parser) = '';

my	%prop_types = (
    0x0		=> "PT_UNSPECIFIED",
    0x1		=> "PT_NULL",
    0x2		=> "PT_SHORT",
    0x3		=> "PT_LONG",
    0x4		=> "PT_FLOAT",
    0x5		=> "PT_DOUBLE",
    0x6		=> "PT_CURRENCY",
    0x7		=> "PT_APPTIME",
    0xa		=> "PT_ERROR",
    0xb		=> "PT_BOOLEAN",
    0xd		=> "PT_OBJECT",
    0x14	=> "PT_I8",
    0x1e	=> "PT_STRING8",
    0x1f	=> "PT_UNICODE",
    0x40	=> "PT_SYSTIME",
    0x48	=> "PT_CLSID",
    0x102	=> "PT_BINARY",
# Multi-valued property types
    0x1002	=> "PT_MV_SHORT",
    0x1003	=> "PT_MV_LONG",
    0x1004	=> "PT_MV_FLOAT",
    0x1005	=> "PT_MV_DOUBLE",
    0x1006	=> "PT_MV_CURRENCY",
    0x1007	=> "PT_MV_APPTIME",
    0x1014	=> "PT_MV_I8",
    0x101e	=> "PT_MV_STRING8",
    0x101f	=> "PT_MV_UNICODE",
    0x1040	=> "PT_MV_SYSTIME",
    0x1048	=> "PT_MV_CLSID",
    0x1102	=> "PT_MV_BINARY"
);

# main program

my $result = GetOptions (
			 'outputdir=s' => \$opt_outputdir,
			 'parser=s' => \$opt_parser
			 );

if (not $result) {
    exit(1);
}

#####################################################################
# read a file into a string
sub FileLoad($)
{
    my($filename) = shift;
    local(*INPUTFILE);
    open(INPUTFILE, $filename) || return undef;
    my($saved_delim) = $/;
    undef $/;
    my($data) = <INPUTFILE>;
    close(INPUTFILE);
    $/ = $saved_delim;
    return $data;
}

#####################################################################
# write a string into a file
sub FileSave($$)
{
    my($filename) = shift;
    my($v) = shift;
    local(*FILE);
    open(FILE, ">$filename") || die "can't open $filename";    
    print FILE $v;
    close(FILE);
}

#####################################################################
# generate mapitags.h file
sub mapitags_header($)
{
    my $contents = shift;
    my $line;
    my @lines;
    my @prop;
    my $prop_type;
    my $prop_value;

    mparse "/* parser auto-generated by mparse */";
    mparse "#ifndef __MAPITAGS_H__";
    mparse "#define __MAPITAGS_H__";
    mparse "";

    @lines = split(/\n/, $contents);
    foreach $line (@lines) {
	$line =~ s/^\#+.*$//;
	if ($line) {
	    @prop = split(/\s+/, $line);
	    $prop_type = hex $prop[0];
	    $prop_type &= 0xFFFF;
	    $prop_value = hex $prop[0];
	    $prop_value = ($prop_value >> 16) & 0xFFFF;

	    mparse sprintf "#define %-51s PROP_TAG(%-13s, 0x%.04x) /* %s */", $prop[1], $prop_types{$prop_type}, $prop_value, $prop[0] if ($prop_types{$prop_type});
	    if (($prop_type == 0x1e) || ($prop_type == 0x101e)){
		$prop_type++;
		$prop[0] = sprintf "0x%.8x", ((hex $prop[0]) & 0xFFFF0000) + $prop_type;
		mparse sprintf "#define %-51s PROP_TAG(%-13s, 0x%.04x) /* %s */", "$prop[1]_UNICODE", $prop_types{$prop_type}, $prop_value, $prop[0] if ($prop_types{$prop_type});
	    }
	    $prop_type = 0xa;
	    $prop[0] = sprintf "0x%.8x", ((hex $prop[0]) & 0xFFFF0000) + $prop_type;
	    mparse sprintf "#define %-51s PROP_TAG(%-13s, 0x%.04x) /* %s */", "$prop[1]_ERROR", $prop_types{$prop_type}, $prop_value, $prop[0] if ($prop_types{$prop_type});
	}
    }
    mparse "";
    mparse "#endif /* !__MAPITAGS_H__ */";

    return $ret;
    
}


#####################################################################
# generate mapitags.c file
sub mapitags_interface($)
{
    my $contents = shift;
    my $line;
    my @lines;
    my @prop;
    my $prop_url;
    my $prop_type;
    my $prop_value;

    mparse "/* parser auto-generated by mparse */";
    mparse "#include <libmapi/libmapi.h>";
    mparse "#include <libmapi/proto_private.h>";
    mparse "#include <gen_ndr/ndr_exchange.h>";
    mparse "#include <libmapi/mapitags.h>";
    mparse "";
    mparse "struct mapi_proptags";
    mparse "{";
    indent;
    mparse "uint32_t	proptag;";
    mparse "uint32_t	proptype;";
    mparse "const char	*propname;";
    deindent;
    mparse "};";
    mparse "";
    mparse "static struct mapi_proptags mapitags[] = {";
    indent;
    
    @lines = split(/\n/, $contents);
    foreach $line (@lines) {
	$line =~ s/^\#+.*$//;
	if ($line) {
	    @prop = split(/\s+/, $line);
	    $prop_type = hex $prop[0];
	    $prop_type &= 0xFFFF;
	    $prop_value = hex $prop[0];
	    $prop_value = ($prop_value >> 16) & 0xFFFF;
	    if ($prop_types{$prop_type}) {
		mparse sprintf "{ %-51s, %-13s, \"%s\" },", $prop[1], $prop_types{$prop_type}, $prop[1];
		if (($prop_type == 0x1e) || ($prop_type == 0x101e)) {
		    $prop_type++;
		    mparse sprintf "{ %-51s, %-13s, \"%s\" },", "$prop[1]_UNICODE", $prop_types{$prop_type}, $prop[1];
		}
		    $prop_type = 0xa;
		mparse sprintf "{ %-51s, %-13s, \"%s\" },", "$prop[1]_ERROR", $prop_types{$prop_type}, $prop[1];
	    }
	}
    }

    mparse sprintf "{ %-51s, %-13d, \"NULL\"}", 0, 0;
    deindent;
    mparse "};;";
    mparse "";
    mparse "_PUBLIC_ const char *get_proptag_name(uint32_t proptag)";
    mparse "{";
    indent;
    mparse "uint32_t idx;";
    mparse "";
    mparse "for (idx = 0; mapitags[idx].proptag; idx++) {";
    indent;
    mparse "if (mapitags[idx].proptag == proptag) { ";
    indent;
    mparse "return mapitags[idx].propname;";
    deindent;
    mparse "}";
    deindent;
    mparse "}";
    mparse "";
    mparse "return NULL;";
    deindent;
    mparse "}";
    mparse "";
    mparse "_PUBLIC_ uint32_t get_proptag_value(const char *propname)";
    mparse "{";
    indent;
    mparse "uint32_t idx;";
    mparse "";
    mparse "for (idx = 0; mapitags[idx].proptag; idx++) {";
    indent;
    mparse "if (!strcmp(mapitags[idx].propname, propname)) { ";
    indent;
    mparse "return mapitags[idx].proptag;";
    deindent;
    mparse "}";
    deindent;
    mparse "}";
    mparse "";
    mparse "return 0;";
    deindent;
    mparse "}";
    mparse "";

    return $ret;
    
}

#####################################################################
# generate mapitags_enum.idl file
sub mapitags_enum($)
{
    my $contents = shift;
    my $line;
    my @lines;
    my @prop;
    my $prop_type;
    my %hash;

    mparse "/* parser auto-generated by mparse */";
    mparse "";

    mparse "typedef [v1_enum, flag(NDR_PAHEX)] enum {";
    indent;
    
    @lines = split(/\n/, $contents);
    foreach $line (@lines) {
	$line =~ s/^\#+.*$//;
	if ($line) {
	    @prop = split(/\s+/, $line);
	    $prop_type = hex $prop[0];
	    $prop_type &= 0xFFFF;

	    mparse sprintf "%-51s = %s,", $prop[1], $prop[0];

	    if (($prop_type == 0x1e) || ($prop_type == 0x101e)) {
		$prop_type = hex $prop[0];
		$prop_type++;
		mparse sprintf "%-51s = 0x%.8x,", "$prop[1]_UNICODE", $prop_type;
	    }
	    if (!exists($hash{((hex $prop[0]) & 0xFFFF0000)})) {
		$prop_type = 0xa;
		$prop_type += ((hex $prop[0]) & 0xFFFF0000);
		mparse sprintf "%-51s = 0x%.8x,", "$prop[1]_ERROR", $prop_type;
		$hash{((hex $prop[0]) & 0xFFFF0000)} = 1;
	    }
	}
    }
    mparse sprintf "%-51s = %s", "MAPI_PROP_RESERVED", "0xFFFFFFFF";
    deindent;
    mparse "} MAPITAGS;";    
    mparse "";
    
    return $ret;
}

#####################################################################
# generate mapicode.h file
sub mapicodes_header($)
{
    my $contents = shift;
    my $line;
    my @lines;
    my @prop;
    my $error;

    mparse "/* parser auto-generated by mparse */";
    mparse "#ifndef __MAPICODE_H__";
    mparse "#define __MAPICODE_H__";
    mparse "";
    mparse "#define MAPI_RETVAL_IF(x,e,c)	\\";
    mparse "do {				\\";
    mparse "	if (x) {			\\";
    mparse "		errno = (e);		\\";
    mparse "		if (c) {		\\";
    mparse "			talloc_free(c);	\\";
    mparse "		}			\\";
    mparse "		return -1;		\\";
    mparse "	}				\\";
    mparse "} while (0)";
    mparse "";
    mparse "/* Status macros for MAPI */";
    mparse "typedef unsigned long	SCODE;";
    mparse "";
    mparse "";
    mparse "#define SEVERITY_ERROR	1";
    mparse "#define SEVERITY_WARN	0";
    mparse "";
    mparse "#define FACILITY_ITF	4";
    mparse "#define	MAKE_MAPI_CODE(sev, fac, code) \\";
    mparse "(((SCODE)(sev)<<31)|((SCODE)(fac)<<16)|((SCODE)(code)))";
    mparse "";
    mparse "#define	MAKE_MAPI_E(code) (MAKE_MAPI_CODE(SEVERITY_ERROR, FACILITY_ITF, code))";
    mparse "#define	MAKE_MAPI_S(code) (MAKE_MAPI_CODE(SEVERITY_WARN, FACILITY_ITF, code))";
    mparse "";
    mparse "#define	MAPI_STATUS_V(x) ((SCODE)x)";
    mparse "";
    mparse "#define	MAPI_STATUS_IS_OK(x) (MAPI_STATUS_V(x) == 0)";
    mparse "#define	MAPI_STATUS_IS_ERR(x) ((MAPI_STATUS_V(x) & 0xc0000000) == 0xc0000000)";
    mparse "#define	MAPI_STATUS_EQUAL(x,y) (MAPI_STATUS_V(x) == MAPI_STATUS_V(y))";
    mparse "";
    mparse "#define	MAPI_STATUS_IS_OK_RETURN(x) do { \\";
    mparse "		if (MAPI_STATUS_IS_OK(x)) {\\";
    mparse "			return x;\\";
    mparse "		}\\";
    mparse "} while (0)";
    mparse "";
    mparse "#define	MAPI_STATUS_NOT_OK_RETURN(x) do { \\";
    mparse "		if (!MAPI_STATUS_IS_OK(x)) {\\";
    mparse "			return x;\\";
    mparse "		}\\";
    mparse "} while (0)";
    mparse "";
    mparse "#define	MAPI_STATUS_IS_ERR_RETURN(x) do { \\";
    mparse "		if (MAPI_STATUS_IS_ERR(x)) {\\";
    mparse "			return x;\\";
    mparse "		}\\";
    mparse "} while (0)";
    mparse "";
    mparse "#define	MAPI_STATUS_NOT_ERR_RETURN(x) do { \\";
    mparse "		if (!MAPI_STATUS_IS_ERR(x)) {\\";
    mparse "			return x;\\";
    mparse "		}\\";
    mparse "} while (0)";
    mparse "";
    mparse "";
    mparse "#endif /* !__MAPICODE_H__ */";

    return $ret;
    
}

#####################################################################
# generate mapicode.c file

sub mapicodes_interface($)
{
    my $contents = shift;
    my $line;
    my @lines;
    my @prop;
    my $prop_url;
    my $prop_type;
    my $prop_value;

    mparse "/* parser auto-generated by mparse */";
    mparse "#include <libmapi/libmapi.h>";
    mparse "#include <libmapi/proto_private.h>";
    mparse "#include <gen_ndr/ndr_exchange.h>";
    mparse "";
    mparse "_PUBLIC_ void mapi_errstr(const char *function, uint32_t mapi_code)";
    mparse "{";
    indent;
    mparse "struct ndr_print	ndr_print;";
    mparse "";
    mparse "ndr_print.depth = 1;";
    mparse "ndr_print.print = ndr_print_debug_helper;";
    mparse "ndr_print_MAPISTATUS(&ndr_print, function, mapi_code);";
    deindent;
    mparse "}";
}

#####################################################################
# generate mapicodes_enum.idl file
sub mapicodes_enum($)
{
    my $contents = shift;
    my $line;
    my @lines;
    my @prop;
    my $prop_type;

    mparse "/* parser auto-generated by mparse */";
    mparse "";

    mparse "typedef [public, v1_enum, flag(NDR_PAHEX)] enum {";
    indent;
    
    @lines = split(/\n/, $contents);
    foreach $line (@lines) {
	$line =~ s/^\#+.*$//;
	if ($line) {
	    @prop = split(/\s+/, $line);
	    mparse sprintf "%-51s = %s,", $prop[1], $prop[0];
	}
    }
    mparse sprintf "%-51s = %s", "MAPI_E_RESERVED", "0xFFFFFFFF";
    deindent;
    mparse "} MAPISTATUS;";    
    mparse "";
    
    return $ret;
}

sub process_file($)
{
    my $mapi_file = shift;
    my $outputdir = $opt_outputdir;

    print "\nParsing $mapi_file\n";
    my $contents = FileLoad($mapi_file);
    defined $contents || return undef;

    
    if ($opt_parser eq "mapitags") {
	print "Generating $outputdir" . "mapitags.h\n";
	my $parser = ("$outputdir/mapitags.h");
	FileSave($parser, mapitags_header($contents));
	
	print "Generating $outputdir" . "mapitags.c\n";
	$ret = '';
	my $code_parser = ("$outputdir/mapitags.c");
	FileSave($code_parser, mapitags_interface($contents));
	
	print "Generating mapitags_enum.h\n";
	$ret = '';
	my $enum_parser = ("mapitags_enum.h");
	FileSave($enum_parser, mapitags_enum($contents));
    }

    if ($opt_parser eq "mapicodes") {
	print "Generating $outputdir" . "mapicode.h\n";
	my $parser = ("$outputdir/mapicode.h");
	FileSave($parser, mapicodes_header($contents));

	print "Generating $outputdir" . "mapicode.c\n";
	$ret = '';
	my $code_parser = ("$outputdir/mapicode.c");
	FileSave($code_parser, mapicodes_interface($contents));

	print "Generating mapicodes_enum.h\n";
	$ret = '';
	my $enum_parser = ("mapicodes_enum.h");
	FileSave($enum_parser, mapicodes_enum($contents));
    }
}

process_file($_) foreach (@ARGV);
