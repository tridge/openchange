#
# ms-Exch-Base-Class
# description: The base auxiliary class that is applied to all classes 
# that are extended by CDO for Exchange 2000 Server (CDOEX) and the base 
# Microsoft Windows classes that are used.
#

dn: CN=ms-Exch-Base-Class,CN=Schema,CN=Configuration,${BASEDN}
adminDescription: ms-Exch-Base-Class
adminDisplayName: ms-Exch-Base-Class
cn: ms-Exch-Base-Class
name: ms-Exch-Base-Class
objectClass: top
objectClass: classSchema
lDAPDisplayName: msExchBaseClass
defaultHidingValue: TRUE
defaultObjectCategory: CN=ms-Exch-Base-Class,CN=Schema,CN=Configuration,${BASEDN}
governsID: 1.2.840.113556.1.5.7000.62.14
instanceType: 4
mayContain: legacyExchangeDN
distinguishedName: CN=ms-Exch-Base-Class,CN=Schema,CN=Configuration,${BASEDN}
objectClassCategory: 3
objectCategory: CN=Class-Schema,CN=Schema,CN=Configuration,${BASEDN}
rDNAttID: cn
schemaIDGUID: d8782c34-46ca-11d3-aa72-00c04f8eedd8 
showInAdvancedViewOnly: TRUE
subClassOf: top
systemOnly: FALSE

#
# ms-Exch-Admin-Group-Container
# description: An administrative group container. Used for extended rights and roles.
#

dn: CN=ms-Exch-Admin-Group-Container,CN=Schema,CN=Configuration,${BASEDN}
adminDescription: ms-Exch-Admin-Group-Container
adminDisplayName: ms-Exch-Admin-Group-Container
auxiliaryClass: msExchBaseClass
cn: ms-Exch-Admin-Group-Container
name: ms-Exch-Admin-Group-Container
defaultHidingValue: TRUE
defaultObjectCategory: CN=ms-Exch-Admin-Group-Container,CN=Schema,CN=Configuration,${BASEDN}
defaultSecurityDescriptor: D:S:
governsID: 1.2.840.113556.1.5.7000.62.50019
instanceType: 4
lDAPDisplayName: msExchAdminGroupContainer
distinguishedName: CN=ms-Exch-Admin-Group-Container,CN=Schema,CN=Configuration,${BASEDN}
objectCategory: CN=Class-Schema,CN=Schema,CN=Configuration,${BASEDN}
objectClass: top
objectClass: classSchema
objectClassCategory: 1
rDNAttID: cn
schemaIDGUID:: e7a44058-a980-11d2-a9ff-00c04f8eedd8
showInAdvancedViewOnly: TRUE
subClassOf: container
systemOnly: FALSE


#
# ms-Exch-Admin-Group
# objectCategory for cn=First Administrative Group
# description: An administrative group.
#

dn: CN=ms-Exch-Admin-Group,CN=Schema,CN=Configuration,${BASEDN}
adminDescription: ms-Exch-Admin-Group
adminDisplayName: ms-Exch-Admin-Group
cn: ms-Exch-Admin-Group
name: ms-Exch-Admin-Group
objectClass: top
objectClass: classSchema
lDAPDisplayName: msExchAdminGroup
objectCategory: CN=Class-Schema,CN=Schema,CN=Configuration,${BASEDN}
objectClassCategory: 1
governsID: 1.2.840.113556.1.5.7000.62.50011
instanceType: 4
distinguishedName: CN=ms-Exch-Admin-Group,CN=Schema,CN=Configuration,${BASEDN}
defaultObjectCategory: CN=ms-Exch-Admin-Group,CN=Schema,CN=Configuration,${BASEDN}
defaultHidingValue: TRUE
defaultSecurityDescriptor: D:S:
auxiliaryClass: msExchBaseClass
possSuperiors: msExchAdminGroupContainer
rDNAttID: cn
schemaIDGUID: e768a58e-a980-11d2-a9ff-00c04f8eedd8 
showInAdvancedViewOnly: TRUE
subClassOf: top
systemOnly: FALSE

#
# ms-Exch-Organization-Container
# objectCategory for cn=First Organization (/o=MyCompany)
# description: An organization container. Used for extended rights and roles.
#

dn: CN=ms-Exch-Organization-Container,CN=Schema,CN=Configuration,${BASEDN}
adminDescription: ms-Exch-Organization-Container
adminDisplayName: ms-Exch-Organization-Container
cn: ms-Exch-Organization-Container
name: ms-Exch-Organization-Container
objectClass: top
objectClass: classSchema
lDAPDisplayName: msExchOrganizationContainer
objectCategory: CN=Class-Schema,CN=Schema,CN=Configuration,${BASEDN}
objectClassCategory: 1
governsID: 1.2.840.113556.1.5.7000.62.50020
instanceType: 4
distinguishedName: CN=ms-Exch-Organization-Container,CN=Schema,CN=Configuration,${BASEDN}
defaultObjectCategory: CN=ms-Exch-Organization-Container,CN=Schema,CN=Configuration,${BASEDN}
defaultHidingValue: TRUE
defaultSecurityDescriptor: D:S:
auxiliaryClass: ms-Exch-Base-Class
rDNAttID: cn
schemaIDGUID: 366a319c-a982-11d2-a9ff-00c04f8eedd8 
showInAdvancedViewOnly: TRUE
subClassOf: container
systemOnly: FALSE

#
# ms-Exch-Configuration-Container
# description: This container stores configuration information for the Exchange server.
#

dn: CN=ms-Exch-Configuration-Container,CN=Schema,CN=Configuration,${BASEDN}
adminDescription: ms-Exch-Configuration-Container
adminDisplayName: ms-Exch-Configuration-Container
cn: ms-Exch-Configuration-Container
name: ms-Exch-Configuration-Container
auxiliaryClass: msExchBaseClass
defaultHidingValue: TRUE
defaultObjectCategory: CN=ms-Exch-Configuration-Container,CN=Schema,CN=Configuration,${BASEDN}
defaultSecurityDescriptor: D:S:
governsID: 1.2.840.113556.1.5.176
instanceType: 4
lDAPDisplayName: msExchConfigurationContainer
distinguishedName: CN=ms-Exch-Configuration-Container,CN=Schema,CN=Configuration,${BASEDN}
objectCategory: CN=Class-Schema,CN=Schema,CN=Configuration,${BASEDN}
objectClass: top
objectClass: classSchema
objectClassCategory: 1
rDNAttID: cn
schemaIDGUID: d03d6858-06f4-11d2-aa53-00c04fd7d83a
showInAdvancedViewOnly: TRUE
subClassOf: container
systemFlags: 16
systemOnly: FALSE

#
# ms-Exch-Servers-Container
# description: A container used to hold servers. Used for extended
# rights and roles.
#

dn: CN=ms-Exch-Servers-Container,CN=Schema,CN=Configuration,${BASEDN}
adminDescription: ms-Exch-Servers-Container
adminDisplayName: ms-Exch-Servers-Container
cn: ms-Exch-Servers-Container
name: ms-Exch-Servers-Container
objectClass: top
objectClass: classSchema
lDAPDisplayName: msExchServersContainer
objectCategory: CN=Class-Schema,CN=Schema,CN=Configuration,${BASEDN}
objectClassCategory: 1
governsID: 1.2.840.113556.1.5.7000.62.50013
instanceType: 4
distinguishedName: CN=ms-Exch-Servers-Container,CN=Schema,CN=Configuration,${BASEDN}
defaultObjectCategory: CN=ms-Exch-Servers-Container,CN=Schema,CN=Configuration,${BASEDN}
defaultHidingValue: TRUE
defaultSecurityDescriptor: D:(A;;LC;;;AU)
auxiliaryClass: msExchBaseClass
possSuperiors: msExchAdminGroup
rDNAttID: cn
schemaIDGUID: 346e5cba-a982-11d2-a9ff-00c04f8eedd8
showInAdvancedViewOnly: TRUE
subClassOf: container
systemOnly: FALSE

#
# ms-Exch-Exchange-Server
# description: A representation of an Exchange server object
#

dn: CN=ms-Exch-Exchange-Server,CN=Schema,CN=Configuration,${BASEDN}
adminDescription: ms-Exch-Exchange-Server
adminDisplayName: ms-Exch-Exchange-Server
cn: ms-Exch-Exchange-Server
name: ms-Exch-Exchange-Server
objectClass: top
objectClass: classSchema
lDAPDisplayName: msExchExchangeServer
objectCategory: CN=Class-Schema,CN=Schema,CN=Configuration,${BASEDN}
objectClassCategory: 1
governsID: 1.2.840.113556.1.5.7000.62.50009
instanceType: 4
distinguishedName: CN=ms-Exch-Exchange-Server,CN=Schema,CN=Configuration,${BASEDN}
defaultObjectCategory: CN=ms-Exch-Exchange-Server,CN=Schema,CN=Configuration,${BASEDN}
defaultHidingValue: TRUE
defaultSecurityDescriptor: D:(A;;RP;;;AU)
auxiliaryClass: msExchBaseClass
possSuperiors: msExchServersContainer
rDNAttID: cn
schemaIDGUID: 01a9aa9c-a981-11d2-a9ff-00c04f8eedd8
showInAdvancedViewOnly: TRUE
subClassOf: server
systemOnly: FALSE

#
# ms-Exch-Mail-Nickname
# description: Exchange mailbox nickname for users
#

dn: CN=ms-Exch-Mail-Nickname,CN=Schema,CN=Configuration,${BASEDN}
adminDescription: ms-Exch-Mail-Nickname
adminDisplayName: ms-Exch-Mail-Nickname
attributeID: 1.2.840.113556.1.2.447
attributeSyntax: 2.5.5.12
cn: ms-Exch-Mail-Nickname
name: ms-Exch-Mail-Nickname
instanceType: 4
isSingleValued: TRUE
lDAPDisplayName: mailNickname
distinguishedName: CN=ms-Exch-Mail-Nickname,CN=Schema,CN=Configuration,${BASEDN}
objectCategory: CN=Attribute-Schema,CN=Schema,CN=Configuration,${BASEDN}
objectClass: top
objectClass: attributeSchema
schemaIDGUID:: bf9679b3-0de6-11d0-a285-00aa003049e2
showInAdvancedViewOnly: TRUE

#
# ms-Exch-Storage-Group
# description: The list of stores for this Microsoft Jet instance
#

dn: CN=ms-Exch-Storage-Group,CN=Schema,CN=Configuration,${BASEDN}
adminDescription: ms-Exch-Storage-Group
adminDisplayName: ms-Exch-Storage-Group
cn: ms-Exch-Storage-Group
name: ms-Exch-Storage-Group
auxiliaryClass: msExchBaseClass
defaultHidingValue: TRUE
defaultObjectCategory: CN=ms-Exch-Storage-Group,CN=Schema,CN=Configuration,${BASEDN}
defaultSecurityDescriptor: D:S:
governsID: 1.2.840.113556.1.5.7000.62.11006
instanceType: 4
lDAPDisplayName: msExchStorageGroup
mayContain: displayName
mustContain: cn
distinguishedName: CN=ms-Exch-Storage-Group,CN=Schema,CN=Configuration,${BASEDN}
objectCategory: CN=Class-Schema,CN=Schema,CN=Configuration,${BASEDN}
objectClass: top
objectClass: classSchema
objectClassCategory: 1
possSuperiors: computer
rDNAttID: cn
schemaIDGUID: 3435244a-a982-11d2-a9ff-00c04f8eedd8
showInAdvancedViewOnly: TRUE
subClassOf: container
systemOnly: FALSE

#
# ms-Exch-Delivery-Mechanism
# description: 	
#

dn: CN=ms-Exch-Delivery-Mechanism,CN=Schema,CN=Configuration,${BASEDN}
adminDescription: ms-Exch-Delivery-Mechanism
adminDisplayName: ms-Exch-Delivery-Mechanism
cn: ms-Exch-Delivery-Mechanism
name: ms-Exch-Delivery-Mechanism
attributeID: 1.2.840.113556.1.2.241
attributeSyntax: 2.5.5.9
instanceType: 4
isSingleValued: TRUE
lDAPDisplayName: deliveryMechanism
distinguishedName: CN=ms-Exch-Delivery-Mechanism,CN=Schema,CN=Configuration,${BASEDN}
objectCategory: CN=Attribute-Schema,CN=Schema,CN=Configuration,${BASEDN}
objectClass: top
objectClass: attributeSchema
schemaIDGUID: bf96794e-0de6-11d0-a285-00aa003049e2
showInAdvancedViewOnly: TRUE

#
# ms-Exch-MDB
# description: Used for generic database configuration.
#

dn: CN=ms-Exch-MDB,CN=Schema,CN=Configuration,${BASEDN}
adminDescription: ms-Exch-MDB
adminDisplayName: ms-Exch-MDB
cn: ms-Exch-MDB
name: ms-Exch-MDB
auxiliaryClass: msExchBaseClass
defaultHidingValue: TRUE
defaultObjectCategory: CN=ms-Exch-MDB,CN=Schema,CN=Configuration,${BASEDN}
defaultSecurityDescriptor: D:S:
governsID: 1.2.840.113556.1.5.7000.62.11002
instanceType: 4
lDAPDisplayName: msExchMDB
mayContain: displayName
mustContain: deliveryMechanism
mustContain: cn
distinguishedName: CN=ms-Exch-MDB,CN=Schema,CN=Configuration,${BASEDN}
objectCategory: CN=Class-Schema,CN=Schema,CN=Configuration,${BASEDN}
objectClass: top
objectClass: classSchema
objectClassCategory: 1
possSuperiors: msExchStorageGroup
possSuperiors: msExchExchangeServer
possSuperiors: container
possSuperiors: computer
rDNAttID: cn
schemaIDGUID: 03d069d2-a981-11d2-a9ff-00c04f8eedd8
showInAdvancedViewOnly: TRUE
subClassOf: top
systemOnly: FALSE

#
# ms-Exch-Private-MDB
# description: A private database configuration
#

dn: CN=ms-Exch-Private-MDB,CN=Schema,CN=Configuration,${BASEDN}
adminDescription: ms-Exch-Private-MDB
adminDisplayName: ms-Exch-Private-MDB
cn: ms-Exch-Private-MDB
name: ms-Exch-Private-MDB
auxiliaryClass: msExchBaseClass
defaultHidingValue: TRUE
defaultObjectCategory: CN=ms-Exch-Private-MDB,CN=Schema,CN=Configuration,${BASEDN}
defaultSecurityDescriptor: D:S:
governsID: 1.2.840.113556.1.5.7000.62.11004
instanceType: 4
lDAPDisplayName: msExchPrivateMDB
distinguishedName: CN=ms-Exch-Private-MDB,CN=Schema,CN=Configuration,${BASEDN}
objectCategory: CN=Class-Schema,CN=Schema,CN=Configuration,${BASEDN}
objectClass: top
objectClass: classSchema
objectClassCategory: 1
possSuperiors: container
possSuperiors: computer
rDNAttID: cn
schemaIDGUID: 36145cf4-a982-11d2-a9ff-00c04f8eedd8
showInAdvancedViewOnly: TRUE
subClassOf: msExchMDB
systemOnly: FALSE

#
# ms-Exch-Mail-Storage
# description: The auxiliary class for objects that require store-specific information.
#

dn: CN=ms-Exch-Mail-Storage,CN=Schema,CN=Configuration,${BASEDN}
adminDescription: ms-Exch-Mail-Storage
adminDisplayName: ms-Exch-Mail-Storage
cn: ms-Exch-Mail-Storage
defaultHidingValue: TRUE
defaultObjectCategory: CN=ms-Exch-Mail-Storage,CN=Schema,CN=Configuration,${BASEDN}
governsID: 1.2.840.113556.1.5.7000.62.5
instanceType: 4
lDAPDisplayName: msExchMailStorage
mayContain: homeMDB
distinguishedName: CN=ms-Exch-Mail-Storage,CN=Schema,CN=Configuration,${BASEDN}
objectCategory: CN=Class-Schema,CN=Schema,CN=Configuration,${BASEDN}
objectClass: top
objectClass: classSchema
objectClassCategory: 3
rDNAttID: cn
schemaIDGUID: 03652000-a981-11d2-a9ff-00c04f8eedd8
showInAdvancedViewOnly: TRUE
subClassOf: top
systemOnly: FALSE

#
# ms-Exch-Information-Store
# description: The Information Store configuration
#

dn: CN=ms-Exch-Information-Store,CN=Schema,CN=Configuration,${BASEDN}
adminDescription: ms-Exch-Information-Store
adminDisplayName: ms-Exch-Information-Store
cn: ms-Exch-Information-Store
name: ms-Exch-Information-Store
auxiliaryClass: msExchBaseClass
defaultObjectCategory: CN=ms-Exch-Information-Store,CN=Schema,CN=Configuration,${BASEDN}
defaultSecurityDescriptor: D:S:
governsID: 1.2.840.113556.1.5.7000.62.11001
instanceType: 4
lDAPDisplayName: msExchInformationStore
mustContain: cn
distinguishedName: CN=ms-Exch-Information-Store,CN=Schema,CN=Configuration,${BASEDN}
objectCategory: CN=Class-Schema,CN=Schema,CN=Configuration,${BASEDN}
objectClass: top
objectClass: classSchema
objectClassCategory: 1
possSuperiors: msExchExchangeServer
rDNAttID: cn
schemaIDGUID: 031b371a-a981-11d2-a9ff-00c04f8eedd8
showInAdvancedViewOnly: TRUE
subClassOf: container
systemOnly: FALSE

#
# PurportedSearch
# description: search argument for an address book view
#

#dn: CN=PurportedSearch,CN=Schema,CN=Configuration,${BASEDN}
#adminDescription: PurportedSearch
#adminDisplayName: PurportedSearch
#attributeID: 1.2.840.113556.1.4.886
#attributeSyntax: 2.5.5.12
#cn: PurportedSearch
#instanceType: 4
#lDAPDisplayName: purportedSearch
#distinguishedName: CN=PurportedSearch,CN=Schema,CN=Configuration,${BASEDN}
#objectCategory: CN=AttributeSchema,CN=Schema,CN=Configuration,${BASEDN}
#objectClass: top
#objectClass: attributeSchema
#schemaIDGUID: b4b54e50-943a-11d1-aebd-0000f80367c1
#showInAdvancedViewOnly: TRUE
#systemOnly: FALSE

#
# Address-Book-Container
# description: holding members of an address book view
#

dn: CN=Address-Book-Container,CN=Schema,CN=Configuration,${BASEDN}
adminDescription: Address-Book-Container
adminDisplayName: Address-Book-Container
cn: Address-Book-Container
name: Address-Book-Container
defaultHidingValue: TRUE
defaultObjectCategory: CN=Address-Book-Container,CN=Schema,CN=Configuration,${BASEDN}
governsID: 1.2.840.113556.1.5.125
instanceType: 4
lDAPDisplayName: addressBookContainer
distinguishedName: CN=Address-Book-Container,CN=Schema,CN=Configuration,${BASEDN}
objectCategory: CN=Class-Schema,CN=Schema,CN=Configuration,${BASEDN}
objectClass: top
objectClass: classSchema
objectClassCategory: 1
possSuperiors: msExchConfigurationContainer
possSuperiors: container
rDNAttID: cn
schemaIDGUID: 3e74f60f-3e73-11d1-a9c0-0000f80367c1
showInAdvancedViewOnly: TRUE
subClassOf: top
#systemMayContain: purportedSearch
systemMustContain: displayName
systemOnly: FALSE
systemPossSuperiors: addressBookContainer
systemPossSuperiors: configuration
