#include "idl_types.h"

/*
   http://support.microsoft.com/default.aspx?scid=KB;en-us;q159298
   Any UUID starting with:
   A4 - store
   F5 - directory
 */

[
  uuid("99e64010-b032-11d0-97a4-00c04fd6551d"),
  pointer_default(unique),
  version(3.0)
] interface exchange_store_admin3
{
	void ec_store_admin3_dummy();
}


[
  uuid("89742ace-a9ed-11cf-9c0c-08002be7ae86"),
  pointer_default(unique),
  version(2.0)
] interface exchange_store_admin2
{
	void ec_store_admin2_dummy();
}

[
  uuid("a4f1db00-ca47-1067-b31e-00dd010662da"),
  pointer_default(unique),
  version(1.0)
] interface exchange_store_admin1
{
	void ec_store_admin1_dummy();
}


[
  uuid("1544f5e0-613c-11d1-93df-00c04fd7bd09"),
  pointer_default(unique),
  version(1.0),
  helpstring("Exchange 2003 Directory Request For Response")
] interface exchange_ds_rfr
{
	/*****************/
	/* Function 0x00 */
	void RfrGetNewDSA();

	/*****************/
	/* Function 0x01 */
	void RfrGetFQDNFromLegacyDN();
}

[
  uuid("f930c514-1215-11d3-99a5-00a0c9b61b04"),
  helpstring("System Attendant Cluster Interface"),
  pointer_default(unique),
  version(1.0)
] interface exchange_sysatt_cluster
{
	void sysatt_cluster_dummy();
}

/*
[83d72bf0-0d89-11ce-b13f-00aa003bac6c] MS Exchange
System Attendant Private Interface
*/

[
  uuid("469d6ec0-0d87-11ce-b13f-00aa003bac6c"),
  pointer_default(unique),
  helpstring("Exchange 5.5 System Attendant Request for Response")
] interface exchange_system_attendant
{
	void sysatt_dummy();
}

[
  uuid("9e8ee830-4559-11ce-979b-00aa005ffebe"),
  pointer_default(unique),
  version(2.0),
  helpstring("Exchange 5.5 MTA")
] interface exchange_mta
{
	/*****************/
	/* Function 0x00 */
	void MtaBind();

	/*****************/
	/* Function 0x01 */
	void MtaBindAck();
}

[
  uuid("f5cc59b4-4264-101a-8c59-08002b2f8426"),
  pointer_default(unique),
  version(21.0),
  helpstring("Exchange 5.5 DRS")
] interface exchange_drs
{
	/*****************/
	/* Function 0x00 */
	void ds_abandon();

	/*****************/
	/* Function 0x01 */
	void ds_add_entry();

	void ds_bind();
	void ds_compare();
	void ds_list();
	void ds_modify_entry();
	void ds_modify_rdn();
	void ds_read();
	void ds_receive_result();
	void ds_remove_entry();
	void ds_search();
	void ds_unbind();
	void ds_wait();
	void dra_replica_add();
	void dra_replica_delete();
	void dra_replica_synchronize();
	void dra_reference_update();
	void dra_authorize_replica();
	void dra_unauthorize_replica();
	void dra_adopt();
	void dra_set_status();
	void dra_modify_entry();
	void dra_delete_subref();
}

[
  uuid("f5cc5a7c-4264-101a-8c59-08002b2f8426"),
  version(21.0),
  pointer_default(unique),
  helpstring("Exchange 5.5 XDS")
] interface exchange_xds
{
	void xds_dummy();
}

[
  uuid("38a94e72-a9bc-11d2-8faf-00c04fa378ff"),
  pointer_default(unique),
  version(1.0)
] interface exchange_mta_qadmin
{
	void exchange_mta_qadmin();
}


[
  uuid("0e4a0156-dd5d-11d2-8c2f-00c04fb6bcde"),
  pointer_default(unique),
  version(1.0)
] interface exchange_store_information
{
	void exchange_store_information_dummy();
}

[
  uuid("f5cc5a18-4264-101a-8c59-08002b2f8426"),
  endpoint("ncacn_np:[\\pipe\\lsass]","ncacn_np:[\\pipe\\protected_storage]","ncacn_ip_tcp:[]"),
  pointer_default(unique),
  version(56.0),
  helpstring("Exchange 5.5 Name Service Provider")
] interface exchange_nsp
{

#include "mapitags_enum.h"
#include "mapicodes_enum.h"

	/*****************/
	/* Function 0x00 */

	/*
	** MAPIUID explanation:
	** http://msdn.microsoft.com/library/default.asp?url=/library/en-us/mapi/html/c42065c2-b1f5-4930-84a6-6ef90c6162d0.asp
	*/

	typedef struct _MAPIUID{
		uint8 ab[16];
	} MAPIUID;

	/* 
	** input locale combination: 
	** http://www.microsoft.com/globaldev/reference/win2k/setup/lcid.mspx
	*/

	typedef struct {
		uint32 language;
		uint32 method;
	} input_locale;

	typedef [flag(NDR_NOALIGN)] struct {
		uint32	        handle;
		uint32		flag;
		MAPIUID		service_provider;
		uint32		codepage;	/* CPID */
		input_locale	input_locale;	/* LCID */
	} MAPI_SETTINGS;

	typedef struct {
		[unique, length_is(cValues - 1), size_is(cValues - 1)] MAPITAGS *aulPropTag;
		uint32 cValues;
	} SPropTagArray;
	
	typedef struct {
		[length_is(cValues - 1), size_is(cValues - 1)] uint32 *value;
		uint32 cValues;
	} instance_key;



	/*****************/
	/* Function 0x00 */
	MAPISTATUS NspiBind(
		[in] uint32			unknown,
		[in] MAPI_SETTINGS		*settings,
		[in,out,unique] GUID		*mapiuid,
		[out] policy_handle		*handle
		);

	/*****************/
	/* Function 0x01 */
	MAPISTATUS NspiUnbind(
		[in, out] policy_handle	*handle,
		[in] uint32	       	status
		);


	/*****************/
	/* Function 0x02 */
	MAPISTATUS NspiUpdateStat(
		[in] policy_handle	*handle,
		[in] uint32		dwAlignPad,
		[in,out] MAPI_SETTINGS	settings,
		[in,out] uint32		unknown
		);

	/*****************/
	/* Function 0x03 */
	MAPISTATUS NspiQueryRows(
		[in]				policy_handle	*handle,
		[in]				uint32		flag,
		[in,out]			MAPI_SETTINGS	*settings,
		[in]				uint32		lRows,
		[in][size_is(lRows)][unique]	uint32		*instance_key,
		[in]				uint32		unknown,
		[in]				SPropTagArray	*REQ_properties,
		[out]				SRowSet		**RowSet
		);

	void NspiSeekEntries();

	/*****************/
	/* Function 0x05 */

	

	typedef struct {
		[unique] MAPIUID *lpguid;
		uint32 ulKind;
		uint32 lID; /* this is actually a union in mapidefs.h */
	} MAPINAMEID;

/* Restriction types */
#define RES_AND            0
#define RES_OR             1
#define RES_NOT            2
#define RES_CONTENT        3
#define RES_PROPERTY       4
#define RES_COMPAREPROPS   5
#define RES_BITMASK        6
#define RES_SIZE           7
#define RES_EXIST          8
#define RES_SUBRESTRICTION 9
#define RES_COMMENT        10

	typedef struct {
		uint32 relop;
		MAPITAGS ulPropTag;
		SPropValue *lpProp;
	} SPropertyRestriction;

	typedef struct {
		uint32	cRes;
		[size_is(cRes)][unique]SRestriction *lpRes;
	} SAndRestriction;

	typedef [switch_type(uint32)] union {
		[case(RES_AND)           ] SAndRestriction resAnd;
//     [case(RES_OR)            ] SOrRestriction resOr;
//     [case(RES_NOT)           ] SNotRestriction resNot;
//     [case(RES_CONTENT)       ] SContentRestriction resContent;
		[case(RES_PROPERTY)      ] SPropertyRestriction resProperty;
//     [case(RES_COMPAREPROPS)  ] SComparePropsRestriction resCompareProps;
//     [case(RES_BITMASK)       ] SBitMaskRestriction resBitMask;
//     [case(RES_SUBRESTRICTION)] SSubRestriction resSub;
//     [case(RES_SIZE)          ] SSizeRestriction resSize;
//     [case(RES_EXIST)         ] SExistRestriction resExist;
  } SRestriction_CTR;

	typedef [public] struct _SRestriction{
		uint32 rt;
		[switch_is(rt)] SRestriction_CTR res;
	} SRestriction;

/* Sort type */
#define TABLE_SORT_ASCEND	0
#define TABLE_SORT_DESCEND	0

	typedef [public] struct _SSortOrder{
		uint32 ulPropTag;
		uint32 ulOrder;
	} SSortOrder;

	typedef [public] struct _SSortOrderSet{
		uint32 cSorts;
		uint32 cCategories;
		uint32 cExpanded;
		[size_is(cSorts)][unique]SSortOrder *aSort;
	} SSortOrderSet;

	MAPISTATUS NspiGetMatches(
		[in]		policy_handle	*handle,
		[in]		uint32		unknown1,
		[in,out]	MAPI_SETTINGS	*settings,
		[in][unique]	SPropTagArray	*PropTagArray,
		[in]		uint32	       	unknown2,
		[in][unique]	SRestriction	*restrictions,
		[in]		uint32		unknown3,
		[out]		instance_key	*instance_key,
		[in][unique]	SPropTagArray	*REQ_properties,
		[out]		SRowSet		**RowSet
		);


	void NspiResortRestriction();

	/*****************/
	/* Function 0x07 */

	typedef struct {
 		[string,charset(DOS)] uint8	*str;
	} NAME_STRING;

	MAPISTATUS NspiDNToEph(
		[in] policy_handle	*handle,
		[in] uint32		flag,
		[in] uint32		size,
 		[in][size_is(size)] NAME_STRING	server_dn[],
		[out] instance_key	*instance_key
		);

	void NspiGetPropList();

	/*****************/
	/* Function 0x09 */
	MAPISTATUS NspiGetProps(
		[in]   	policy_handle		*handle,
		[in]   	uint32			flag,
		[in]	MAPI_SETTINGS		*settings,
		[in]	SPropTagArray		*REQ_properties,
		[out]	SRow			**REPL_values
		);


	void NspiCompareDNTs();
	void NspiModProps();

#define	PT_UNSPECIFIED	0x0000
#define	PT_NULL		0x0001
#define	PT_I2		0x0002
#define	PT_LONG		0x0003
#define	PT_R4		0x0004
#define	PT_DOUBLE	0x0005
#define	PT_CURRENCY	0x0006
#define	PT_APPTIME	0x0007
#define	PT_ERROR	0x000a /* means the given attr contains no value */
#define	PT_BOOLEAN	0x000b
#define	PT_OBJECT	0x000d
#define	PT_I8		0x0014
#define	PT_STRING8	0x001e
#define	PT_UNICODE	0x001f
#define	PT_SYSTIME	0x0040
#define	PT_CLSID       	0x0048
#define	PT_BINARY	0x0102

/* Multi-valued properties */

#define	PT_MV_I2       	0x1002
#define	PT_MV_LONG     	0x1003
#define	PT_MV_R4       	0x1004
#define	PT_MV_DOUBLE   	0x1005
#define	PT_MV_CURRENCY 	0x1006
#define	PT_MV_APPTIME  	0x1007
#define	PT_MV_I8       	0x1014
#define	PT_MV_STRING8	0x101e
#define	PT_MV_TSTRING	0x101e
#define	PT_MV_UNICODE	0x101f
#define	PT_MV_SYSTIME	0x1040
#define	PT_MV_CLSID	0x1048
#define	PT_MV_BINARY	0x1102

	/*****************/
	/* Function 0x0c */

	typedef struct {
		uint32				cb;
		[size_is(cb)][unique] uint8    	*lpb;
	} SBinary;

	typedef struct {
		uint32 dwLowDateTime;
		uint32 dwHighDateTime;
	} FILETIME;

	typedef struct {
		uint32 cValues;
		[size_is(cValues)] uint16 *lpi;
	} SShortArray;

	typedef struct {
		uint32 cValues;
		[size_is(cValues)] uint32 *lpl; 
	} MV_LONG_STRUCT;

	typedef struct {
		[flag(STR_ASCII|STR_SIZE4|STR_LEN4)] string lppszA;
	} LPSTR;

	typedef struct {
		uint32 cValues;
		[size_is(cValues)] LPSTR **strings; 
	} SLPSTRArray;

	typedef struct {
		uint32 cValues;
		[size_is(cValues)] SBinary *lpbin;
	} SBinaryArray;

	typedef [flag(NDR_NOALIGN)] struct {
		uint32 cValues;
		[size_is(cValues)] uint32 *lpguid; 
	} SGuidArray;

	typedef struct {
		uint32 cValues;
		[size_is(cValues)] uint32 *lpi;
	} MV_UNICODE_STRUCT;
	
	typedef struct {
		uint32 cValues;
		[size_is(cValues)] FILETIME *lpft;
	} SDateTimeArray;

	typedef [switch_type(uint32)] union {
		[case(PT_I2)]			uint16			i;
		[case(PT_LONG)]			uint32			l;
		[case(PT_BOOLEAN)]		uint16			b;
		[case(PT_I8)]			dlong			d;
		[case(PT_STRING8)][unique][string,charset(DOS)] uint8	*lpszA;
		[case(PT_BINARY)]		SBinary			bin;
		[case(PT_UNICODE)]		[string,charset(UTF16)] uint16	*lpszW;
		[case(PT_CLSID)]		MAPIUID			*lpguid;
		[case(PT_SYSTIME)]		FILETIME		ft;
		[case(PT_ERROR)]		MAPISTATUS		err;
		[case(PT_MV_I2)]		SShortArray		MVi;
		[case(PT_MV_LONG)]		MV_LONG_STRUCT		MVl;
		[case(PT_MV_STRING8)]		SLPSTRArray		MVszA;
		[case(PT_MV_BINARY)]		SBinaryArray		MVbin;
		[case(PT_MV_CLSID)]		SGuidArray		MVguid;
		[case(PT_MV_UNICODE)]		MV_UNICODE_STRUCT	MVszW;
		[case(PT_MV_SYSTIME)]		SDateTimeArray		MVft;
		[case(PT_NULL)]			uint32			null;
		[case(PT_OBJECT)]		uint32			object;
	} SPropValue_CTR;

	typedef [public]struct {
		MAPITAGS ulPropTag;
		uint32 dwAlignPad;
		[switch_is(ulPropTag & 0xFFFF)] SPropValue_CTR value; 
	} SPropValue;
	
	typedef struct {
		uint32 ulAdrEntryPad;
		uint32 cValues;
		[size_is(cValues)][unique] SPropValue *lpProps;
	} SRow;


	typedef [public] struct {
		uint32 cRows;
		[size_is(cRows)] SRow aRow[*];
	} SRowSet;
	
	MAPISTATUS NspiGetHierarchyInfo(
		[in] policy_handle	*handle,
		[in] uint32		unknown1,
		[in] MAPI_SETTINGS	*settings,
 		[in,out] uint32		*unknown2,
 		[out] SRowSet		**RowSet
		);

	void NspiGetTemplateInfo();
	void NspiModLInkAtt();
	void NspiDeleteEntries();
	void NspiQueryColumns();
	void NspiGetNamesFromIDs();
	void NspiGetIDsFromNames();

	/*****************/
	/* Function 0x13 */

	typedef [v1_enum, flag(NDR_PAHEX)] enum {
		MAPI_UNRESOLVED	= 0x0,
		MAPI_AMBIGUOUS	= 0x1,
		MAPI_RESOLVED	= 0x2
	} flaglist_flags;

	typedef [flag(NDR_NOALIGN)]struct {
 		uint32		dwAlignPad;
		uint32		cFlags;
		uint32		offset;
		uint32		length;
		flaglist_flags 	ulFlags[cFlags];
	} FlagList;

 	typedef struct {
		uint32		cEntries;
 		uint32		count;
 		[flag(STR_ASCII|STR_SIZE4|STR_LEN4)]string *recipient[count];
 	} names;

	MAPISTATUS NspiResolveNames(
		[in] policy_handle	*handle,
		[in] uint32		dwAlignPad,
		[in,ref] MAPI_SETTINGS	*settings,
		[in,ref] SPropTagArray *SPropTagArray,
 		[in,ref] names		*recipients,
		[out,ref]FlagList	**flags,
		[out,ptr]SRowSet        *RowSet
	);

	typedef struct {
		uint32		cEntries;
		uint32		count;
		[string,charset(UTF16)] uint16	*recipient[count];
	} namesW;

	MAPISTATUS NspiResolveNamesW(
		[in] policy_handle	*handle,
		[in] uint32		dwAlignPad,
		[in,ref] MAPI_SETTINGS	*settings,
		[in,ref] SPropTagArray	*SPropTagArray,
		[in,ref] namesW		*recipients,
		[out,ref]FlagList	**flags,
		[out,ptr]SRowSet	*RowSet
		);
}

[
  uuid("a4f1db00-ca47-1067-b31f-00dd010662da"),
  pointer_default(unique),
  endpoint("ncacn_np:[\\pipe\\lsass]","ncacn_np:[\\pipe\\protected_storage]","ncacn_ip_tcp:"),
  version(0.82),
  helpstring("Exchange 5.5 EMSMDB")
] interface exchange_emsmdb
{
	/*****************/
	/* Function 0x00 */

	MAPISTATUS EcDoConnect(
 		[out]				policy_handle	*handle,
                [in,string,charset(DOS)]	uint8		name[],
		[in]				uint32		unknown1[3],
                [in]				uint32		code_page,
		[in]				input_locale	input_locale,
                [in]				uint32          unknown2,
                [in]				uint16          unknown3,
		[out]				uint32		unknown4[3],
                [out]				uint16          *session_nb,
                [out,unique,string,charset(DOS)]uint8		*org_group,
                [out,unique,string,charset(DOS)]uint8		*user,
                [out]				uint16          store_version[3],
		[in,out]       			uint16          emsmdb_client_version[3],
                [in,out]			uint32		*alloc_space
		);

	/*****************/
	/* Function 0x01 */
	MAPISTATUS EcDoDisconnect(
		[in,out]   policy_handle   *handle
		);

	/*****************/
	/* Function 0x02 */

	/*
	  EcDoRpc opnums
	*/

	typedef [public, enum8bit, flag(NDR_PAHEX)] enum 
		{
			MAPI_STORE	= 0x1,
			MAPI_ADDRBOOK	= 0x2,
			MAPI_FOLDER	= 0x3,
			MAPI_ABCONT	= 0x4,
			MAPI_MESSAGE	= 0x5,
			MAPI_MAILUSER	= 0x6, /* Individual Recipient */
			MAPI_ATTACH	= 0x7,
			MAPI_DISTLIST	= 0x8,
			MAPI_PROFSECT	= 0x9,
			MAPI_STATUS	= 0xA,
			MAPI_SESSION	= 0xB,
			MAPI_FORMINFO	= 0xC
		} MAPI_OBJTYPE;


	typedef [public, enum8bit, flag(NDR_PAHEX)] enum
		{
			op_MAPI_Release			= 0x1,
			op_MAPI_OpenFolder		= 0x2,
			op_MAPI_OpenMessage		= 0x3,
			op_MAPI_GetHierarchyTable	= 0x4,
			op_MAPI_GetContentsTable	= 0x5,
			op_MAPI_CreateMessage		= 0x6,
			op_MAPI_GetProps		= 0x7,
			op_MAPI_GetPropsAll		= 0x8,
			op_MAPI_GetPropList		= 0x9,
			op_MAPI_SetProps		= 0xa,
			op_MAPI_DeleteProps		= 0xb,
			op_MAPI_ModifyRecipients	= 0xe,
			op_MAPI_SetColumns		= 0x12,
			op_MAPI_QueryRows		= 0x15,
			op_MAPI_GetRowCount		= 0x17,
			op_MAPI_SeekRow			= 0x18,
			op_MAPI_CreateFolder		= 0x1c,
			op_MAPI_DeleteFolder		= 0x1d,
			op_MAPI_DeleteMessages		= 0x1e,
			op_MAPI_GetAttachmentTable     	= 0x21,
			op_MAPI_OpenAttach		= 0x22,
			op_MAPI_CreateAttach		= 0x23,
			op_MAPI_SaveChanges		= 0x25,
			op_MAPI_GetReceiveFolder	= 0x27,
			op_MAPI_OpenStream		= 0x2b,
			op_MAPI_ReadStream		= 0x2c,
			op_MAPI_WriteStream		= 0x2d,
			op_MAPI_SubmitMessage		= 0x32,
			op_MAPI_QueryColumns		= 0x37,
			op_MAPI_EmptyFolder		= 0x58,
			op_MAPI_GetReceiveFolderTable	= 0x68,
			op_MAPI_OpenMsgStore		= 0xFE
		} MAPI_OPNUM;


	typedef [public,flag(NDR_NOALIGN)] struct {
		uint16				cb;
		[flag(NDR_BUFFERS)]uint8	lpb[cb];
	} SBinary_short;
	
	typedef [public,nodiscriminant,flag(NDR_NOALIGN)] union {
		[case(PT_BOOLEAN)]	uint8		b;
		[case(PT_I2)]		uint16		i;
		[case(PT_LONG)]     	uint32	       	l;
		[case(PT_SYSTIME)]	FILETIME	ft;
		[case(PT_ERROR)]       	uint32		err;
		[case(PT_STRING8)]	astring	       	lpszA;
		[case(PT_UNICODE)][flag(STR_NULLTERM)] string lpszW;
  		[case(PT_BINARY)]	SBinary_short 	bin;
	} mapi_SPropValue_CTR;

	typedef [public,flag(NDR_NOALIGN)] struct {
		MAPITAGS ulPropTag;
		[switch_is(ulPropTag & 0xFFFF)] mapi_SPropValue_CTR value; 
	} mapi_SPropValue;

	typedef [flag(NDR_NOALIGN)] struct {
		uint16					cValues;
		[flag(NDR_REMAINING)]mapi_SPropValue	lpProps[cValues];
	} mapi_SPropValue_array;

	/**************************/
	/* EcDoRpc Function 0x1   */
	typedef [flag(NDR_NOALIGN)] struct {
	} Release_req;

	typedef [flag(NDR_NOALIGN)] struct {
	} Release_repl;

	/**************************/
	/* EcDoRpc Function 0x2   */
	typedef [flag(NDR_NOALIGN)] struct {
		uint8		handle_idx;
		hyper		folder_id;
 		uint8		unknown;
	} OpenFolder_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint16		unknown;
	} OpenFolder_repl;
	
	/**************************/
	/* EcDoRpc Function 0x3   */
	typedef [flag(NDR_NOALIGN)] struct {
		uint8		folder_handle_idx;
		uint16		max_data;
		hyper		folder_id;
		uint8		message_handle_idx;
		hyper		message_id;
	} OpenMessage_req;

	typedef [v1_enum, flag(NDR_PAHEX)] enum {
		CP_USASCII	= 0x04E4,
		CP_UNICODE	= 0x04B0,
		CP_JAUTODETECT	= 0xC6F4,
		CP_KAUTODETECT	= 0xC705,
		CP_ISO2022JPESC = 0xC42D,
		CP_ISO2022JPSIO	= 0xC42E
	} CODEPAGEID;

	typedef [enum8bit, flag(NDR_PAHEX)] enum {
		MAPI_ORIG		= 0x0,
		MAPI_TO			= 0x1,
		MAPI_CC			= 0x2,
		MAPI_BCC		= 0x3
	} ulRecipClass;

	/**************** EXPERIMENTAL ******************/

	typedef [enum8bit, flag(NDR_PAHEX)] enum {
		SINGLE_RECIPIENT	= 0x0,
		DISTRIBUTION_LIST	= 0x1
	} addr_type;

	typedef  [flag(NDR_NOALIGN)]struct {
		uint8		organization_length;
		addr_type	addr_type;
	} RecipExchange;

	typedef [flag(NDR_NOALIGN)] struct {
	} RecipSMTP;

	typedef [nodiscriminant, flag(NDR_NOALIGN)] union {
		[case(0x0)] RecipExchange	EXCHANGE;
		[case(0xA)] RecipSMTP		SMTP;
		[default];
	} recipient_type;

	/* Bit 5: always UTF8 encoded */
	typedef [nodiscriminant, flag(NDR_NOALIGN)] union {
		[case(0x400)] astring		lpszA;
		[default];
	} recipient_displayname_7bit;

	/* Bit 10 */
	typedef [nodiscriminant, flag(NDR_NOALIGN)] union {
		[case(0x20)] astring				lpszA;
		[case(0x220)][flag(STR_NULLTERM)] string	lpszW;
		[default];
	} recipient_email;

	/* Bit 11 */
	/* First Name = PR_GIVEN_NAME */
	typedef [nodiscriminant, flag(NDR_NOALIGN)] union {
		[case(0x10)] astring				lpszA;
		[case(0x210)][flag(STR_NULLTERM)] string	lpszW;
		[default];
	} recipient_firstname;

	/* Bit 15 */
	typedef [nodiscriminant, flag(NDR_NOALIGN)] union {
		[case(0x1)] astring				lpszA;
		[case(0x201)][flag(STR_NULLTERM)] string	lpszW;
		[default];
	} recipient_displayname;

	typedef [flag(NDR_NOALIGN)] struct {
		uint16							bitmask;
		[switch_is(bitmask & 0xA)]   recipient_type		type;
		[switch_is(bitmask & 0x400)] recipient_displayname_7bit	username;
		[switch_is(bitmask & 0x210)] recipient_firstname       	firstname;
		[switch_is(bitmask & 0x201)] recipient_displayname	displayname;
		[switch_is(bitmask & 0x220)] recipient_email   		email_address;
		uint16							prop_count;
		uint8							layout;
		[flag(NDR_REMAINING)] DATA_BLOB				prop_values;
	} recipients_headers;
	
	/**************** EXPERIMENTAL *****************/

	typedef [flag(NDR_NOALIGN)] struct {
		ulRecipClass	RecipClass;
		CODEPAGEID	codepage;
		[subcontext(2)] recipients_headers recipients_headers;
	} OpenMessage_recipients;

	typedef [public, nodiscriminant]union {
		[case(0x1)];
		[case(0x3)] astring subject;
	} OpenMessage_repl_UNION;

	typedef [flag(NDR_NOALIGN)] struct {
		uint8		unknown;
		uint8		type_prefix;
		[switch_is(type_prefix)] OpenMessage_repl_UNION prefix;
		uint8		type_subject;
		[switch_is(type_subject)] OpenMessage_repl_UNION subject;
		uint16		recipient_count;
		uint16		prop_count;
		MAPITAGS	properties[prop_count];
		uint8		recipient_count_again;
		OpenMessage_recipients	recipients[recipient_count];
	} OpenMessage_repl;

	/**************************/
	/* EcDoRpc Function 0x4   */
	typedef [flag(NDR_NOALIGN)] struct {
		uint8		handle_idx;
		uint8		unknown;
	} GetHierarchyTable_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint16		cn_rows;
		uint16		unknown;
	} GetHierarchyTable_repl;

	/**************************/
	/* EcDoRpc Function 0x5   */
	typedef [flag(NDR_NOALIGN)] struct {
		uint8		handle_idx;
		uint8		unknown;
	} GetContentsTable_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint16          cn_rows;
		uint16          unknown;
	} GetContentsTable_repl;

	/**************************/
	/* EcDoRpc Function 0x6   */
	typedef [flag(NDR_NOALIGN)] struct {
		uint8		handle_idx;
		uint16		max_data;
		hyper		folder_id;
		uint8		padding;
	} CreateMessage_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint8		padding;
	} CreateMessage_repl;

	/*************************/
	/* EcDoRpc Function 0x7  */
	typedef [flag(NDR_NOALIGN)] struct {
		uint32		unknown;
		uint16		prop_count;
		MAPITAGS	properties[prop_count];
	} GetProps_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint8		layout;
		[flag(NDR_REMAINING)] DATA_BLOB prop_data;
	} GetProps_repl;

	/*************************/
	/* EcDoRpc Function 0x8  */
	typedef [flag(NDR_NOALIGN)] struct {
		uint16			dwAlignPad;
		uint16			unknown;
	} GetPropsAll_req;

	typedef [flag(NDR_NOALIGN)] struct {
		mapi_SPropValue_array	properties;
	} GetPropsAll_repl;

	/*************************/
	/* EcDoRpc Function 0x9  */
	typedef [flag(NDR_NOALIGN)] struct {
	} GetPropList_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint16		count;
		MAPITAGS	tags[count];
	} GetPropList_repl;

	/*************************/
	/* EcDoRpc Function 0xa  */

	typedef [flag(NDR_NOALIGN)] struct {
 		[subcontext(2)] mapi_SPropValue_array values;
	} SetProps_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint16		unknown;
	} SetProps_repl;

	/*************************/
	/* EcDoRpc Function 0xb  */
	typedef [flag(NDR_NOALIGN)] struct {
		uint16		count;
		MAPITAGS	tags[count];
	} DeleteProps_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint16		unknown;
	} DeleteProps_repl;

	/*************************/
	/* EcDoRpc Function 0xe  */

	/* 
	 * MODRECIP_NULL and INVALID are not part of the msdn flags
	 * but are added for printing support 
	 */
	typedef [enum8bit,flag(NDR_PAHEX)] enum {
		MODRECIP_NULL		= 0x0,
		MODRECIP_INVALID	= 0x1,
		MODRECIP_ADD		= 0x2,
		MODRECIP_MODIFY		= 0x4,
		MODRECIP_REMOVE		= 0x8
	} modrecip;

	typedef [flag(NDR_NOALIGN)]struct {
		uint32		idx;
		ulRecipClass	RecipClass;		
		[subcontext(2),flag(NDR_REMAINING)] recipients_headers headers;
	} recipients;

	typedef [flag(NDR_NOALIGN)] struct {
 		uint16				prop_count;
 		MAPITAGS			properties[prop_count];
		uint16				cValues;
		recipients			recipient[cValues];
	} ModifyRecipients_req;

	typedef [flag(NDR_NOALIGN)] struct {

	} ModifyRecipients_repl;


	/*************************/
	/* Ecdorpc Function 0x12 */
	typedef [flag(NDR_NOALIGN)] struct {
		uint8		unknown;
		uint16		prop_count;
		MAPITAGS	properties[prop_count];
	} SetColumns_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint8		unknown;
	} SetColumns_repl;

	/**************************/
	/* EcDoRpc Function 0x15  */

	typedef [enum8bit] enum {
		TBL_ADVANCE		= 0x0,
		TBL_NOADVANCE		= 0x1
	} tbl_advance;

	typedef [flag(NDR_NOALIGN)] struct {
		tbl_advance	flag_advance;
		uint8		unknown;
		uint16		row_count;
	} QueryRows_req;

	typedef [noprint,nopull,flag(NDR_NOALIGN)] struct {
		uint8		unknown;
		uint16		results_count;
		uint8		unknown2;
		[flag(NDR_REMAINING)]DATA_BLOB	inbox;
	} QueryRows_repl;

	/**************************/
	/* EcDoRpc Function 0x17  */
	typedef [flag(NDR_NOALIGN)] struct {
	} GetRowCount_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint32	unknown;
		uint32	count;
	} GetRowCount_repl;

	/**************************/
	/* EcDoRpc Function 0x18  */
	typedef [enum8bit] enum {
		BOOKMARK_BEGINNING	= 0x0,
		BOOKMARK_CURRENT	= 0x1,
		BOOKMARK_END		= 0x2
	} BOOKMARK;

	typedef [flag(NDR_NOALIGN)] struct {
		BOOKMARK	origin;
		int32		offset;
		uint8		unknown_1;
	} SeekRow_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint32		row;
		uint8		unknown;
	} SeekRow_repl;

	/**************************/
	/* EcDoRpc Function 0x1c  */
	typedef [flag(NDR_NOALIGN)] struct {
		uint8		handle_idx;
		uint16		flags_type;
		uint16		flags_open;
		astring		name;
		astring		comment;
	} CreateFolder_req;

	typedef [flag(NDR_NOALIGN)] struct {
		hyper		folder_id;
		uint8		unknown;
	} CreateFolder_repl;

	/**************************/
	/* EcDoRpc Function 0x1d  */
	typedef [flag(NDR_NOALIGN)] struct {
		uint8		flags;
		hyper		folder_id;
	} DeleteFolder_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint8		unknown;
	} DeleteFolder_repl;

	/**************************/
	/* EcDoRpc Function 0x1e  */
	typedef [flag(NDR_NOALIGN)] struct {
		uint16		flags;
		uint16		cn_ids;
		hyper		message_ids[cn_ids];
	} DeleteMessages_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint16		unknown;
		[flag(NDR_REMAINING)]DATA_BLOB entries;
	} DeleteMessages_repl;

	/**************************/
	/* EcDoRpc Function 0x21  */
	typedef [flag(NDR_NOALIGN)] struct {
		uint8		handle_idx;
		uint8		unknown;
	} GetAttachmentTable_req;

	typedef [flag(NDR_NOALIGN)] struct {		
	} GetAttachmentTable_repl;

	/*************************/
	/* EcDoRpc Function 0x22 */
  	typedef [flag(NDR_NOALIGN)] struct {
		uint8		handle_idx;
		uint8		unknown;
		uint32		num_attach;
	} OpenAttach_req;

	typedef [flag(NDR_NOALIGN)] struct {
	} OpenAttach_repl;

	/*************************/
	/* EcDoRpc Function 0x23 */
	typedef [flag(NDR_NOALIGN)] struct {
		uint8	handle_idx;
	} CreateAttach_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint32	unknown;
	} CreateAttach_repl;

	/*************************/
	/* EcDoRpc Function 0x25 */
	typedef [flag(NDR_NOALIGN)] struct {
		uint8 handle_idx;
		uint8 unknown;
	} SaveChanges_req;

	typedef [flag(NDR_NOALIGN)] struct {
	} SaveChanges_repl;

	/*************************/
	/* EcDoRpc Function 0x27 */
	typedef [flag(NDR_NOALIGN)] struct {
		uint8		unknown;
	} GetReceiveFolder_req;

	typedef [flag(NDR_NOALIGN)] struct {
		hyper		folder_id;
	} GetReceiveFolder_repl;

	/*************************/
	/* EcDoRpc Function 0x2b */
	typedef [flag(NDR_NOALIGN)] struct {
		uint8		stream_handle_idx;
		MAPITAGS	prop;
		uint8		access_flags;
	} OpenStream_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint32		length;
	} OpenStream_repl;

	/*************************/
	/* EcDoRpc Function 0x2c */
	typedef [flag(NDR_NOALIGN)] struct {
		uint16		size;
	} ReadStream_req;

	typedef [flag(NDR_ALIGN2)] struct {
		[subcontext(2), flag(NDR_REMAINING)] DATA_BLOB data;
	} ReadStream_repl;

	/*************************/
	/* EcDoRpc Function 0x2d */
	typedef [flag(NDR_NOALIGN)] struct {
		[subcontext(2), flag(NDR_REMAINING)] DATA_BLOB	data;
	} WriteStream_req;


	typedef [flag(NDR_NOALIGN)] struct {
		uint16		size;
	} WriteStream_repl;

	/*************************/
	/* EcDoRpc Function 0x32 */
	typedef [flag(NDR_NOALIGN)] struct {
		uint8		unknown;
	} SubmitMessage_req;

	typedef [flag(NDR_NOALIGN)] struct {
//		uint8		unknown[3];
	} SubmitMessage_repl;


	/*************************/
	/* EcDoRpc Function 0x37 */
	typedef [flag(NDR_NOALIGN)] struct {
	} QueryColumns_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint16		count;
		MAPITAGS	tags[count];
	} QueryColumns_repl;

	/*************************/
	/* EcDoRpc Function 0x58 */
	typedef [flag(NDR_NOALIGN)] struct {
		uint16		unknown;
	} EmptyFolder_req;

	typedef [flag(NDR_NOALIGN)] struct {
		uint8		unknown;
	} EmptyFolder_repl;
  
	/*************************/
	/* EcDoRpc Function 0xFE */
	typedef [flag(NDR_NOALIGN)] struct {
		uint32	codepage;
		uint32	padding;
		uint8	row;
		ascstr3	mailbox_path;
	} OpenMsgStore_req;

	typedef [flag(NDR_NOALIGN)] struct {
		MAPI_OBJTYPE	PR_OBJECT_TYPE;
 		hyper		folder_id[13];
		uint8		unknown3;
		MAPIUID		entryid;
		uint16		default_store;
		MAPIUID		msgstore;
		FILETIME	ts_access;
		astring		creation_date;
		uint8		terminator;
		uint16		unknown4;
	} OpenMsgStore_repl;

	typedef [public, nodiscriminant] union {
		[case(op_MAPI_Release)] Release_req mapi_Release;
		[case(op_MAPI_OpenFolder)] OpenFolder_req mapi_OpenFolder;
		[case(op_MAPI_OpenMessage)] OpenMessage_req mapi_OpenMessage;
		[case(op_MAPI_GetHierarchyTable)] GetHierarchyTable_req mapi_GetHierarchyTable;
		[case(op_MAPI_GetContentsTable)] GetContentsTable_req mapi_GetContentsTable;
		[case(op_MAPI_CreateMessage)] CreateMessage_req mapi_CreateMessage;
		[case(op_MAPI_GetProps)] GetProps_req mapi_GetProps;
		[case(op_MAPI_GetPropsAll)] GetPropsAll_req mapi_GetPropsAll;
		[case(op_MAPI_GetPropList)] GetPropList_req mapi_GetPropList;
		[case(op_MAPI_SetProps)] SetProps_req mapi_SetProps;
		[case(op_MAPI_DeleteProps)] DeleteProps_req mapi_DeleteProps;
		[case(op_MAPI_SetColumns)] SetColumns_req mapi_SetColumns;
		[case(op_MAPI_ModifyRecipients)] ModifyRecipients_req mapi_ModifyRecipients;
		[case(op_MAPI_QueryRows)] QueryRows_req mapi_QueryRows;
		[case(op_MAPI_GetRowCount)] GetRowCount_req mapi_GetRowCount;
		[case(op_MAPI_SeekRow)] SeekRow_req mapi_SeekRow;
		[case(op_MAPI_CreateFolder)] CreateFolder_req mapi_CreateFolder;
		[case(op_MAPI_DeleteFolder)] DeleteFolder_req mapi_DeleteFolder;
		[case(op_MAPI_DeleteMessages)] DeleteMessages_req mapi_DeleteMessages;
		[case(op_MAPI_GetAttachmentTable)] GetAttachmentTable_req mapi_GetAttachmentTable;
		[case(op_MAPI_OpenAttach)] OpenAttach_req mapi_OpenAttach;
		[case(op_MAPI_CreateAttach)] CreateAttach_req mapi_CreateAttach;
		[case(op_MAPI_SaveChanges)] SaveChanges_req mapi_SaveChanges;
		[case(op_MAPI_GetReceiveFolder)] GetReceiveFolder_req mapi_GetReceiveFolder;
		[case(op_MAPI_OpenStream)] OpenStream_req mapi_OpenStream;
		[case(op_MAPI_ReadStream)] ReadStream_req mapi_ReadStream;
		[case(op_MAPI_WriteStream)] WriteStream_req mapi_WriteStream;
		[case(op_MAPI_SubmitMessage)] SubmitMessage_req mapi_SubmitMessage;
		[case(op_MAPI_QueryColumns)] QueryColumns_req mapi_QueryColumns;
		[case(op_MAPI_EmptyFolder)] EmptyFolder_req mapi_EmptyFolder;
		[case(op_MAPI_OpenMsgStore)] OpenMsgStore_req mapi_OpenMsgStore;
	} EcDoRpc_MAPI_REQ_UNION;

	typedef [public, nodiscriminant] union {
 		[case(op_MAPI_Release)] Release_repl mapi_Release;
		[case(op_MAPI_OpenFolder)] OpenFolder_repl mapi_OpenFolder;
		[case(op_MAPI_OpenMessage)] OpenMessage_repl mapi_OpenMessage;
		[case(op_MAPI_GetHierarchyTable)] GetHierarchyTable_repl mapi_GetHierarchyTable;
		[case(op_MAPI_GetContentsTable)] GetContentsTable_repl mapi_GetContentsTable;
		[case(op_MAPI_CreateMessage)] CreateMessage_repl mapi_CreateMessage;
		[case(op_MAPI_GetProps)] GetProps_repl mapi_GetProps;
		[case(op_MAPI_GetPropsAll)] GetPropsAll_repl mapi_GetPropsAll;
		[case(op_MAPI_GetPropList)] GetPropList_repl mapi_GetPropList;
		[case(op_MAPI_SetProps)] SetProps_repl mapi_SetProps;
		[case(op_MAPI_DeleteProps)] DeleteProps_repl mapi_DeleteProps;
		[case(op_MAPI_ModifyRecipients)] ModifyRecipients_repl mapi_ModifyRecipients;
		[case(op_MAPI_SetColumns)] SetColumns_repl mapi_SetColumns;
		[case(op_MAPI_QueryRows)] QueryRows_repl mapi_QueryRows;
		[case(op_MAPI_GetRowCount)] GetRowCount_repl mapi_GetRowCount;
		[case(op_MAPI_SeekRow)] SeekRow_repl mapi_SeekRow;
		[case(op_MAPI_CreateFolder)] CreateFolder_repl mapi_CreateFolder;
		[case(op_MAPI_DeleteFolder)] DeleteFolder_repl mapi_DeleteFolder;
		[case(op_MAPI_DeleteMessages)] DeleteMessages_repl mapi_DeleteMessages;
		[case(op_MAPI_GetAttachmentTable)] GetAttachmentTable_repl mapi_GetAttachmentTable;
		[case(op_MAPI_OpenAttach)] OpenAttach_repl mapi_OpenAttach;
		[case(op_MAPI_CreateAttach)] CreateAttach_repl mapi_CreateAttach;
		[case(op_MAPI_SaveChanges)] SaveChanges_repl mapi_SaveChanges;
		[case(op_MAPI_GetReceiveFolder)] GetReceiveFolder_repl mapi_GetReceiveFolder;
		[case(op_MAPI_OpenStream)] OpenStream_repl mapi_OpenStream;
		[case(op_MAPI_ReadStream)] ReadStream_repl mapi_ReadStream;
		[case(op_MAPI_WriteStream)] WriteStream_repl mapi_WriteStream;
		[case(op_MAPI_SubmitMessage)] SubmitMessage_repl mapi_SubmitMessage;
		[case(op_MAPI_QueryColumns)] QueryColumns_repl mapi_QueryColumns;
		[case(op_MAPI_EmptyFolder)] EmptyFolder_repl mapi_EmptyFolder;
		[case(op_MAPI_OpenMsgStore)] OpenMsgStore_repl mapi_OpenMsgStore;

	} EcDoRpc_MAPI_REPL_UNION;

	typedef [public,flag(NDR_NOALIGN)] struct {
		uint8		opnum;
		uint8		mapi_flags;
		uint8		handle_idx;
		[switch_is(opnum)] EcDoRpc_MAPI_REQ_UNION u;
	} EcDoRpc_MAPI_REQ;

	typedef [public,nopull,noprint,flag(NDR_NOALIGN)] struct {
		uint8     	opnum;
		uint8		handle_idx;
		MAPISTATUS	error_code;
		[switch_is(opnum)] EcDoRpc_MAPI_REPL_UNION u;
	} EcDoRpc_MAPI_REPL;


	/*
	  Abstract way to represent MAPI content
	*/

  	typedef [public,nopull,nopush,noprint] struct {
		uint32			mapi_len;	/* whole mapi_data length */
		uint16			length;		/* content length */
		EcDoRpc_MAPI_REQ	*mapi_req;
		uint32			*handles;	/* handles id array */
	} mapi_request;

	typedef [public,nopull,nopush,noprint] struct {
		uint32			mapi_len;
		uint16			length;
		EcDoRpc_MAPI_REPL	*mapi_repl;
		uint32			*handles;
	} mapi_response;
	

	[public,flag(NDR_NOALIGN)] MAPISTATUS  EcDoRpc(
		[in,out]	policy_handle	*handle,
		[in,out]	uint32 size,
		[in,out]	uint32 offset,
	  	[in][subcontext(4),flag(NDR_REMAINING|NDR_NOALIGN)] mapi_request  *mapi_request,
		[out][subcontext(4),flag(NDR_REMAINING|NDR_NOALIGN)] mapi_response *mapi_response,
		[in,out]	uint16		*length,
		[in]		uint16		max_data
		);

	/*****************/
	/* Function 0x03 */
	void EcGetMoreRpc();

	/*****************/
	/* Function 0x04 */
	MAPISTATUS EcRRegisterPushNotification(
		[in,out]	policy_handle		*handle,
		[in]		uint32			unknown,
		[in]		DATA_BLOB		blob1,
		[in]		uint16			unknown1,
		[in]		uint32			unknown2,
		[in]		DATA_BLOB		blob2,
		[in]		uint16			unknown3,
		[out]		uint32			unknown4
		);

	/*****************/
	/* Function 0x05 */
	MAPISTATUS EcRUnregisterPushNotification(
		[in,out]	policy_handle		*handle,
		[in]		uint32			unknown[2]
		);

	/*****************/
	/* Function 0x06 */
	void EcDummyRpc();

	/*****************/
	/* Function 0x07 */
	void EcRGetDCName();

	/*****************/
	/* Function 0x08 */
	void EcRNetGetDCName();

	/*****************/
	/* Function 0x09 */
	void EcDoRpcExt();

}

[
  uuid("c840a7dc-42c0-1a10-b4b9-08002b2fe182"),
  pointer_default(unique),
  helpstring("Unknown")
] interface exchange_unknown
{
	void unknown_dummy();
}

