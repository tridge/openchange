/*
    Openchange Tools

    openchangepfadmin Graphical Front End

    Copyright (C) Billy Okal 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mapiprofileadmin.h"
#include "ui_mapiprofileadmin.h"

extern "C"
{
#include <libmapi/libmapi.h>
#include <libmapiadmin/libmapiadmin.h>
#include <libmapiadmin/proto.h>
#include <talloc.h>
}

/**
	\file mapiprofileadmin.cpp

	\brief MAPI Admin gui implementation
*/

/**
 *	  Constructor
 */
MapiprofileAdmin::MapiprofileAdmin(QWidget *parent)
    : QWidget(parent), ui(new Ui::MapiprofileAdmin)
{
    // Initializing 
    ui->setupUi(this);
    connect(ui->folderManagement->createFolderButton, SIGNAL(clicked()), this, SLOT(createFolderDialog()) );
    connect(ui->folderManagement->removeFolderButton, SIGNAL(clicked()), this, SLOT(removeFolder()) );
    connect(ui->userManagement->addUserButton, SIGNAL(clicked()), this, SLOT(addUserDialog()) );
    connect(ui->userManagement->removeUserButton, SIGNAL(clicked()), this, SLOT(removeUser()) );
    connect(ui->userManagement->addPermissionButton, SIGNAL(clicked()), this, SLOT(listAllPermission()) );

    connect(ui->userManagement->modifyPermissionButton, SIGNAL(clicked()), this, SLOT(editUserPermission()) );
    connect(ui->userManagement->removePermissionButton, SIGNAL(clicked()), this, SLOT(listUserPermission()) );

    setUp();	/*initiate gui with correct state*/
}

/**
 *	Destructor
 */
MapiprofileAdmin::~MapiprofileAdmin()
{
    delete ui;
    MAPIUninitialize();	// release resources
}

/**
 *	Sets up GUI for usage
 */
void MapiprofileAdmin::setUp()
{
	ui->WelcomePage->folderSelectOption->setEnabled(true);
	ui->WelcomePage->usersSelectOption->setEnabled(true);
	ui->WelcomePage->permissionSelectOption->setEnabled(true);
	ui->WelcomePage->continueSelect->setEnabled(false);
	ui->folderManagement->setEnabled(false);
	ui->usersManagement->setEnabled(false);

	handleSelection();
}

/**
 *	Handle options selected by user on start up
 */
void MapriprofileAdmin::handleSelection()
{
	if (ui->WelcomePage->folderSelectOption->isChecked())
	{
		ui->folderManagement->setEnabled(true);
		ui->folderManagement->foldersList->setEnabled(true);
		ui->folderManagement->createFolderButton->setEnabled(true);
		ui->folderManagement->removeFolderButton->setEnabled(true);
	}
	else if (ui->WelcomePage->usersSelectOption->isChecked() || ui->WelcomePage->permissionSelectOption->isChecked())
	{

		ui->userManagement->setEnabled(true);
		ui->userManagement->usersList->setEnabled(true);
		ui->userManagement->addUserButton->setEnabled(true);
		ui->userManagement->removeUserButton->setEnabled(true);
		ui->userManagement->addPermissionButton->setEnabled(true);
		ui->userManagement->removePermissonButton->setEnabled(true);
		ui->userManagement->modifyPermissionButton->setEnabled(true);
	}
	else {}
}

/**
	\details Create a New Folder/Directory

	The function creates a folder/directory (defined with its name, comment and type)
	within a specified folder/directory.

	\param obj_parent the folder to create the new folder in
	\param ulFolderType the type of the folder
	\param name the name of the new folder
	\param comment the comment associated with the new folder
	\param ulFlags flags associated with folder creation
	\param obj_child pointer to the newly created folder

	ulFlags possible values:
	- MAPI_UNICODE: use UNICODE folder name and comment
	- OPEN_IF_EXISTS: open the folder if it already exists

	ulFolderType possible values:
	- FOLDER_GENERIC
	- FOLDER_SEARCH

	\note 	The return MAPI_E_SUCCESS or Mapi Error are all handled internally in the function hence the void type
	\note 	A debug log is recorded on both success and failure of this method

	\sa OpenFolder, DeleteFolder, EmptyFolder, GetLastError
*/
void MapiprofileAdmin::createFolder()
{
	enum MAPISTATUS 	retval;
	mapi_object_t		obj_folder;
	mapi_object_t		obj_container;
	QString				folderName;
	QString				folderComment;

	folderName	= folderNameEntry->text();
	folderComment	= folderCommentEntry->text();

	mapi_object_init(&obj_folder);		// initialize mapi object
	qDebug() << "Initializing mapi_object";
	retval = CreateFolder(obj_container, FOLDER_GENERIC, folderName.toUtf8().toConstData(), folderComment.toUtf8().toConstData(), OPEN_IF_EXISTS, &obj_folder);

	if (retval != MAPI_E_SUCCESS)
	{
		mapi_errstr("CreateFolderFail", GetLastError());
		qDebug() << "Aborting Create Folder";
		exit(1);
	}

	qDebug() << "Created a folder";

	new QListWidgetItem(folderNameEntry->text(), ui->folderManagement->foldersList);

}

/**
	\details Remove folder

	The function removes a given folder.

	\param obj_parent the folder containing the folder to be deleted
	\param FolderId the ID of the folder to delete
	\param DeleteFolderFlags control DeleteFolder operation behavior
	\param PartialCompletion pointer on a boolean value which specify
	whether the operation was partially completed or not

	Possible values for DeleteFolderFlags are:
	-# DEL_MESSAGES Delete all the messages in the folder
	-# DEL_FOLDERS Delete the subfolder and all of its subfolders
	-# DELETE_HARD_DELETE Hard delete the folder

	\note 	The return MAPI_E_SUCCESS or Mapi Error are all handled internally in the function hence the void type
	\note 	A debug log is recorded on both success and failure of this method

	\sa OpenFolder, CreateFolder, EmptyFolder, GetLastError
 */
void MapiprofileAdmin::removeFolder()
{
	enum MAPISTATUS		retval;
	mapi_object_t		obj_folder;
	QString			folderName = ui->folderList->currentItem();
	mapi_object_t		folderName;

	mapi_object_init(&obj_folder);		// initialize mapi object

	// first empty the folder
	retval = EmptyFolder(&folderName);
	if (retval != MAPI_E_SUCCESS)
	{
		mapi_errstr("EmptyFolder", GetLastError());
		exit(1);
	}

	// then continue to remove folder
	retval = DeleteFolder(folderName, mapi_object_get_id(&obj_folder));
	qDebug() << "Deleting Folder" << folderName;

	if (retval != MAPI_E_SUCCESS)
	{
	        mapi_errstr("DeleteFolder", GetLastError());
	        exit(1);
	}

	qDebug() << "Deleted Folder";
}


/**
	\details Add User

	This function adds a user

	\param username	Username
	\param password Password
	\param description A short description of the user e.g Linux Owner, Administrator
	\param fullname	Full name of user

	\note 	The return MAPI_E_SUCCESS or Mapi Error are all handled internally in the function hence the void type
	\note 	A debug log is recorded on both success and failure of this method

	\sa  GetLastError, mapiadmin_user_add
 */
void MapiprofileAdmin::addUser()
{
	struct mapiadmin_ctx			*mapiadmin_newUser;
	enum MAPISTATUS					retval;
	mapiadmin_newUser.username		= userEntry->text().toUtf8().constData();
	mapiadmin_newUser.password		= passwordEntry->text().toUtf8().constData();
	mapiadmin_newUser.description	= descEntry->text().toUtf8().constData();
	mapiadmin_newUser.fullname		= fnameEntry->().toUft8().constData();

	retval = mapiadmin_user_add(mapiadmin_newUser);

	if (retval != MAPI_E_SUCCESS)
	{
		mapi_errstr("Adding user to directory", GetLastError());
		mapiadmin_release(mapiadmin_newUser);
		qDebug() << "Aborting add user";
		exit(1);
	}

	qDebug() << "Add User successful";

	new QListWidgetItem(userEntry->text(), ui->UserManagement->usersList);
}

/**
	\details Remove User

	This function deletes a user

	\param username	Username

	\note 	The return MAPI_E_SUCCESS or Mapi Error are all handled internally in the function hence the void type
	\note 	A debug log is recorded on both success and failure of this method

	\sa  GetLastError, mapiadmin_user_del
 */
void MapiprofileAdmin::removeUser()
{
	struct mapiadmin_ctx		*mapiadmin_delUser
	enum MAPISTATUS			retval;
	
	mapiadmin_delUser.username	= ui->usersList->currentItem();

	retval = mapiadmin_user_del(mapiadmin_delUser);

	if (retval != MAPI_E_SUCCESS)
	{
		mapi_errstr("Remove User", GetLastError());
		mapiadmin_release(mapiadmin_newUser);
		qDebug() << "Aborting delete user";
		exit(1);
	}

	qDebug() << "Delete User success";

}

/**
	\details Add User Permission

	This function adds user permission to a given folder
	
	\param obj_folder the folder we add permission for
	\param username the Exchange username we add permissions for
	\param role the permission mask value

	The following permissions and rights are supported:
	- RightsNone
	- RightsReadItems
	- RightsCreateItems
	- RightsEditOwn
	- RightsDeleteOwn
	- RightsEditAll
	- RightsDeleteAll
	- RightsCreateSubfolders
	- RightsFolderOwner
	- RightsFolderContact
	- RoleNone
	- RoleReviewer
	- RoleContributor
	- RoleNoneditingAuthor
	- RoleAuthor
	- RoleEditor
	- RolePublishAuthor
	- RolePublishEditor
	- RightsAll
	- RoleOwner

	\note 	The return MAPI_E_SUCCESS or Mapi Error are all handled internally in the function hence the void type
	\note 	A debug log is recorded on both success and failure of this method

	\sa  GetLastError, AddUserPermission
 */
void MappiprofileAdmin::addUserPermssion()
{
	enum MAPISTATUS		retval;
	mapi_object_t		obj_folder;
	const char			*user;
	const char			*permission;
	
	user 				= ui->usersList->currentItem().toUtf8().constData(); 
	selectedPermission	= allPermissions->currentItem();
	permission 			= selectedPermission.toUtf8().constData();
	mapi_object_init(&obj_folder);

	retval = AddUserPermission(&obj_folder, user, permission);

	if (retval != MAPI_E_STATUS)
	{
		mapi_errstr("Add user Permission", GetLastError());
		mapi_object_release(&obj_folder);
		qDebug() << "Aborting add user permission";
		exit(1);
	}

	qDebug() << "Added user permission" << user << permission;
}

/**
	\details Modify User Permissions 
	
	\param obj_folder the folder we add permission for
	\param username the Exchange username we modify permissions for
	\param role the permission mask value (see AddUserPermission)

	\note 	The return MAPI_E_SUCCESS or Mapi Error are all handled internally in the function hence the void type
	\note 	A debug log is recorded on both success and failure of this method
	
	\sa ModifyUserPermission, GetLastError
 */
void MapiprofileAdmin::editUserPermission()
{
	enum MAPISTATUS		retval;
	const char			*user;
	const char			*permission;
	mapi_object_t		obj_folder;

	mapi_object_init(&obj_folder);
	user					= ui->usersList->currentItem().toUtf8().constData();
	selectedPermission		= userPermissions->currentItem();
	permission				= selectedPermission.toUtf8().constData();

	retval = ModifyUserPermission(obj_folder, user, permission);	// editing user permissions defined by aclrights

	if (retval != MAPI_E_SUCCESS)
	{
		mapi_errstr("edit User permission", GetLastError());
		qDebug() << "Aborting edit user permission";
		mapi_object_release(&obj_folder);
		exit(1);
	}

	qDebug() << "edited user permission";
}

/**
	\details Remove User Permission 
	
	\param obj_folder the folder we add permission for
	\param username the Exchange username we remove permissions for
	
	\note 	The return MAPI_E_SUCCESS or Mapi Error are all handled internally in the function hence the void type
	\note 	A debug log is recorded on both success and failure of this method
	
	\sa RemoveUserPermission, GetLastError
 */

void MapiprofileAdmin::removeUserPermission()
{
	enum MAPISTATUS		retval;
	mapi_object_t		obj_user;
	const char			*permission;
	
	selectedPermission	= userPermissions->currentItem();
	permission			= selectedPermission.toUtf8().constData();

	mapi_object_init(&obj_user);
	retval = RemoveUserPermission(&obj_user, permission);

	if (retval != MAPI_E_SUCCESS)
	{
		mapi_errstr("RemoveUserPermission", GetLastError());
		qDebud() << "Aborting remove user permission";
		mapi_object_release(&obj_folder);
		exit(1);
	}

	qDebug() << "Removed User Permission";
}

/**
 *	Dialog for Create Folder
 */
void MapiprofileAdmin::createFolderDialog()
{
	QLabel		*folderNameLabel 	= new QLabel(tr("Name:"));
	QLabel		*folderCommentLabel	= new QLabel(tr("Comment:"));
	folderNameEntry					= new QLineEdit;
	folderCommentEntry				= new QLineEdit;
	QPushButton	*createButton		= new QPushButton(tr("Create"));	
	QPushButton	*cancelButton		= new QPushButton(tr("Cancel"));
	QGridLayout	*layout				= new QGridLayout;
	QDialog		*dialog				= new QDialog(this);

	layout->addWidget(folderNameLabel, 0, 0);
	layout->addWidget(folderCommentLabel, 1, 0);
	layout->addWidget(folderNameEntry, 0, 1, 1, 2);
	layout->addWidget(folderCommentEntry, 1, 1, 1, 2);
	layout->addWidget(createButton, 2, 2);
	layout->addWidget(cancelButton, 2, 1);

	dialog->setLayout(layout);
	dialog->setWindowTitle(tr("Create a New Folder"));
	dialog->raise();
	dialog->activateWindow();

	connect(createButton, SIGNAL(clicked()), this, SLOT(createFolder()) );
	connect(calcelButton, SIGNAL(clicked()), this, SLOT(close()) );
}

/**
 *	Dialog for Add User
 */
void MapiprofileAdmin::addUserDialog()
{
	QDialog	*Add		= new QDialog(this);
	QLabel	*userLabel	= new QLabel(tr("Username:"));
	QLabel	*passwordLabel	= new QLabel(tr("Password:"));
	QLabel	*descLabel	= new QLabel(tr("Description:"));
	QLabel	*fnameLabel	= new QLabel(tr("Full Name:"));	
	userEntry		= new QLineEdit;
	passwordEntry		= new QLineEdit;
	descEntry		= new QLineEdit;
	fnameEntry		= new QLineEdit;
	QPushButton *cancel	= new QPushButton(tr("Cancel"));
	QPushButton *add	= new QpushButton(tr("Add"));
	QGridLayout *layout	= new QGridLayout;

	layout->addWidget(userLabel, 0, 0);
	layout->addWidget(userEntry, 0, 1);
	layout->addWidget(passwordLabel, 1, 0);
	layout->addWidget(passwordEntry, 1, 1);
	layout->addWidget(descLabel, 2, 0);
	layout->addWidget(descEntry, 2, 1);
	layout->addWidget(fnameLabel, 3, 0);
	layout->addWidget(fnameEntry, 3, 1);
	layout->addWidget(cancel, 4, 0);
	layout->addWidget(add, 4, 1);

	Add->setLayout(layout);
	Add->setWindowTitle(tr("Add User"));
	Add->raise();
	Add->activateWindow();

	connect(cancel, SIGNAL(clicked()), this, SLOT(close()) );
	connect(add, SIGNAL(clicked()), this, SLOT(addUser()) );
}

/**
	\details List Permissions of selected user 
	
	\param obj_folder the folder we list permission for
	\param username the Exchange username we remove permissions for
	
	\note 	The return MAPI_E_SUCCESS or Mapi Error are all handled internally in the function hence the void type
	\note 	A debug log is recorded on both success and failure of this method
	
	\sa GetTable, GetLastError
 */
void MapiprofileAdmin::listUserPermission()
{
	QListWidget	*permissionList	= new QListWidget;
	QDialog		*permList		= new QDialog(this);
	QPushButton	*cancel			= new QPushButton(tr("Cancel"));
	QPushButton	*remove			= new QPushButton(tr("Remove"));
	QGridLayout	*layout			= new QGridLayout;	
	enum MAPISTATUS				retval;
	struct SRowSet				permTable;
	uint32_t					permCount;
	QString 					userTable = ui->userList->currentItem();
	mapi_object_t				userTable;

	memset(&permTable, 0, sizeof(struct SRowSet));

	retval = GetTable(userTable, permTable);

	if (retval != MAPI_E SUCCESS)
	{
		mapi_errstr("Get User Permission Table", GetLastError());
		qDebug() << "Cannot open user permission table";
		exit(1);
	}

	for (permCount = 0; permCount != permTable.cRows; permCount++)
	{
		const char 	*permRole;

		permRole = permTable.aRow[permCount].lpProps[0].value.lpszA;

		QString 	role;
		role = (QString(permRole));
		
		new QListWidgetItem(role, permissionList);
	}

	qDebug() << "Opening user permission folder success, now showing permissions";

	layout->addWidget(permissionList, 0, 0, 1, 2);
	layout->addWidget(cancel, 1, 1);
	layout->addWidget(remove, 1, 2);

	permList->setLayout(layout);
	permList->setWindowTitle(tr("List of User permissions"));
	permList->raise();
	permList->activateWindow();

	connect(cancel, SIGNAL(clicked()), this, SLOT(close()));
	connect(remove, SIGNAL(clicked()), this, SLOT(removeUserPermission()));

}

/**
 *	Display a list of all permissions
 */
void MapiprofileAdmin::listAllPermission()
{
	QDialog		*Perm 	= new QDialog(this);
	allPermissions		= new QListWidget;
	QPushButton	*select	= new QPushButton(tr("Select"));
	QPushButton	*cancel	= new QPushButton(tr("Cancel"));
	QGridLayout	*layout	= new QGridLayout;
	QStringList rightsList	= (QStringList() <<"RightsNone" << "RightsReadItems" << "RightsCreateItems" << "RightsEditOwn" << "RightsDeleteOwn" << "RightsEditAll" << "RightsDeleteAll" << "RightsCreateSubfolders" << "RightsFolderOwner" << "RightsFolderContact" << "RoleNone" << "RoleReviewer" << "RoleContributor" << "RoleNoneditingAuthor" << "RoleAuthor" << "RoleEditor" << "RolePublishAuthor" << "RolePublishEditor" << "RightsAll" << "RoleOwner");
	allPermissions->addItems(rightsList);

	layout->addWidget(allPermissions, 0, 0, 1, 2);
	layout->addWidget(cancel, 1, 0);
	layout->addWidget(select, 1, 1);

	connect(cancel, SIGNAL(clicked()), this, SLOT(close()) );
	connect(select, SIGNAL(clicked()), this, SLOT(addUserPermission()) );
}
