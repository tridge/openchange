/*
    Openchange Tools

    openchangepfadmin Graphical Front End

    Copyright (C) Billy Okal 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtGui/QApplication>
#include "mapiprofileadmin.h"

/**
	\file main.cpp

	\brief MAPI Admin gui main file
*/

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MapiprofileAdmin w;
    w.show();
    return a.exec();
}
