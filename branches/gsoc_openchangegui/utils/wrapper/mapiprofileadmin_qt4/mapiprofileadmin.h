/*
    Openchange Tools

    openchangepfadmin Graphical Front End

    Copyright (C) Billy Okal 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAPIPROFILEADMIN_H
#define MAPIPROFILEADMIN_H

#include <QtGui/QWidget>

/**
	\file mapiprofileadmin.h

	\brief Declarations for MAPI admin gui implementation
*/

class QLineEdit;
class QListWidget;

namespace Ui
{
    class MapiprofileAdmin;
}

class MapiprofileAdmin : public QWidget
{
    Q_OBJECT

public:
    MapiprofileAdmin(QWidget *parent = 0);
    ~MapiprofileAdmin();

    void createFolder();
    void removeFolder();
    void listFolders();
    void setUp();
    void handleSelection();
    void removeUser();
    void addUserPermssion();
    void editUserPermission();
    void removeUserPermission();
    void createFolderDialog();
    void addUserDialog();
    void listUserPermission();
    void listAllPermission();

private:
    Ui::MapiprofileAdmin *ui;
    QString 			selectedPermission = NULL;
    QLineEdit			*folderNameEntry;
    QLineEdit			*folderCommentEntry;
    QLineEdit			*userEntry;
    QLineEdit			*passwordEntry;
    QLineEdit			*descEntry;
    QLineEdit			*fnameEntry;
    QListWidget			*allPermissions;
    QListWidget			*userPermissions;

};

#endif // MAPIPROFILEADMIN_H
