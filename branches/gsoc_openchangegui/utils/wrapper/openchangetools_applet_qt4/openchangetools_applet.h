/*
    Openchange Tools

	desktop applets for openchange tools gui front ends

    Copyright (C) Billy Okal 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPENCHANGETOOLS_APPLET
#define OPENCHANGETOOLS_APPLET

#include <kpanelapplet.h>

/**
	\file openchangetools_applet.h

	\brief Declaration of openchangetools applet
*/
class KAboutData;

class OpenchangetoolsApplet : public KPanelApplet
{
	Q_OBJECT

public:
	OpenchangetoolsApplet(const QString& configFile, Type t = Stretch, int actions = 0, QWidget *parent = 0, const char *name = 0);

	int widthForHeight( int height ) const;
	int heightForWidth( int width ) const;
	void aboutApplet();
	void initMapiprofile();
	void initMapiadmin();
private:
	KAboutData	_aboutData;
};

#endif

