/*
    Openchange Tools

    desktop applets for openchange tools gui front ends

    Copyright (C) Billy Okal 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <klocale.h>
#include <kglobal.h>
#include <qlayout.h>
#include <kpushbutton.h>
#include <mapiprofile.h>
#include <mapiprofileadmin.h>
#include "openchangetools_applet.h"
#include "openchangetools_applet.moc"

/**
	\file openchangetools_applet.cpp

	\brief implementation of applet for openchangetools
*/
extern "C"
{
	KPanelApplet*	init(QWidget *parent, const QString& configFile)
	{
		KGlobal::locale()->insertCatalogue("openchangetools_applet");
		return new OpenchangetoolsApplet(configFile, KPanelApplet::Normal, KPanelApplet::About, parent, "OpenchangetoolsApplet");
	}
}

/**
  *	Constructor
  */
OpenchangetoolsApplet::OpenchangetoolsApplet(const QString& configFile, Type type, int actions, QWidget *parent, const char *name) : KPanelApplet(configFile, type, actions, parent, name), _AboutData(0)
{
	setBackgroundMode(NoBackground);
	setFrameStyle(StyledPanel | Sunken);

	QVBoxLayout	*_layout				= new QVBoxLayout(this);
	KPushBUtton *_launchProfileBtn		= new KPushButton("Mapiprofile",this);
	KPushBUtton *_launchAdminBtn		= new KPushButton("MapiAdmin",this);

	_layout->add(_lauchProfileBtn);
	_layout->add(_lauchAdminBtn);

	connect(_launchProfileBtn, SIGNAL(clicked()), this, SLOT(initMapiprofile()));
	connect(_launchAdminBtn, SIGNAL(clicked()), this, SLOT(initMapiAdmin()));
}

/**
	  Indicate the height of applet.

	  - inherited from KPanel applet
  */
int OpenchangetoolsApplet::widthForHeight( int height ) const
{
	return height;
}

/**
	  Indicate the width of applet.

	  - inherited from KPanel applet
  */
int OpenchangetoolsApplet::heightForWidth( int width ) const
{
	return width;
}

/**
  	About GUI
	
	  - Shows about info
  */
void OpenchangetoolsApplet::aboutApplet() 
{
  if(!_aboutData) {

      _aboutData = new KAboutData("OpenchangetoolsApplet", I18N_NOOP("OpenchangetoolsApplet"), 
              "1.0", I18N_NOOP("Openchange tools applet.\n\n"
              "This applet provides access to Openchange utilities like mapiprofile and mapiadmin.\n"
              "Select an option to continue."),
              KAboutData::License_GPL, "(c) 2009, Okal Billy");

      _aboutData->addAuthor("Okal Billy", 0, "bokal@acm.org");
  }

  KAboutApplication dialog(_aboutData);

  dialog.show();
}

/**
	Initiate Mapiprofile gui

	  - Start up mapiprofile tool
  */
void OpenchangetoolsApplet::initMapiprofile()
{
	Mapiprofile	_initMapi;

	_initMapi.show();
}

/**
  	Initiate Mapiadmin gui
	
	  - Start uo mapiprofileadmin tool
  */
void OpenchangetoolsApplet::initMapiAdmin()
{
	MapprofileAdmin	_initAdmin;

	_initAdmin.show();
}
