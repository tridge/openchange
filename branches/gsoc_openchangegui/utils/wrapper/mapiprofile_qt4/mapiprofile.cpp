/*
    Openchange Tools

    mapiprofile Graphical Front End

    Copyright (C) Billy Okal 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mapiprofile.h"
#include "ui_mapiprofile.h"
#include <iostream>
#include <qmessagebox.h>
#include <qdialog.h>
#include <qstring.h>
#include <QtGui>

extern "C"
{
#include <libmapi/libmapi.h>
#include <talloc.h>
}

/**
	\file mapiprofile.cpp

	\brief MAPI Profiles GUI Implementation
*/

/*   ---------   GLOBALS  ----------   */

QString LDIF            = "/usr/local/samba/share/setup";	// default ldif 
QString database        = "$HOME/.openchange/profiles.ldb";	// default database
int selectD             = 0;								// select database tag
int createD             = 0;								// create database tag

/**
    Constructor

	- Sets initial configurations of the gui elements
 */
Mapiprofile::Mapiprofile(QWidget *parent)
    : QWidget(parent), ui(new Ui::Mapiprofile)
{
    ui->setupUi(this);
    ui->profileManager->setEnabled(false);
    ui->proceed->setEnabled(false);
    ui->addProfBtn->setEnabled(false);
    ui->deleteProfBtn->setEnabled(false);
    ui->dumpProfBtn->setEnabled(false);
    ui->setDefProfBtn->setEnabled(false);
    ui->profilesList->setEnabled(false);
    ui->addProfBtn->connect(ui->addProfBtn, SIGNAL(clicked()), this, SLOT(addDialog()) );
    ui->deleteProfBtn->connect(ui->deleteProfBtn, SIGNAL(clicked()), this, SLOT(deleteProfile()) );
    ui->setDefProfBtn->connect(ui->setDefProfBtn, SIGNAL(clicked()), this, SLOT(setDefaultProfile()) );
    ui->dumpProfBtn->connect(ui->dumpProfBtn, SIGNAL(clicked()), this, SLOT(dumpProfile()) );
}

/**
	Destructor
  
	- Cleanup
*/
Mapiprofile::~Mapiprofile()
{
    // releasing resources
	MAPIUninitialize();
    delete ui;
}

/**
	\details Create New Database Dialog

	- Provides an interface for user to enter parameters of the new database
	- Provides buttons to pick user signals for relevant options
 */
void Mapiprofile::createNew()
{
    newDatabase         = new QDialog(this);
    newDatabaseEntry    = new QLineEdit;
    newCreateBtn        = new QPushButton(tr("Create"));
    cancelBtn           = new QPushButton(tr("Cancel"));
    newTopLabel         = new QLabel(tr("Enter name of database to create"));
    newDatabaseLabel    = new QLabel(tr("Database Name:"));
    QGridLayout *layout = new QGridLayout;

    layout->addWidget(newTopLabel, 0, 0, 1, 2);
    layout->addWidget(newDatabaseLabel, 1, 0);
    layout->addWidget(newDatabaseEntry, 1, 1);
    layout->addWidget(cancelBtn, 2, 0);
    layout->addWidget(newCreateBtn, 2, 1);

    newDatabase->setLayout(layout);
    newDatabase->setWindowTitle(tr("Create new database"));
    newDatabase->show();
    newDatabase->raise();
    newDatabase->activateWindow();

	/*	pass various signals to relevant actions	*/
    connect(newCreateBtn, SIGNAL(clicked()), this, SLOT(createNewDatabase()));
    connect(cancelBtn, SIGNAL(clicked()), this, SLOT(close()));
}

/**
 	\details Select an existing database
	
	- Pops a file dialog with existing databases for user to select
	- The selected database is returned for processing
 */
void Mapiprofile::selectDatabase()
{
    QString select = QFileDialog::getOpenFileName(this, "Select a database", QString::null, QString::null);

    // reconfigure database to selected one.
    database = select;

    ui->profileManager->setEnabled(true);
    enableProfileMan();
}

/**
	\details Create New Database

	\param database The name of the database to be created as const char*
	\param ldif 	The ldif directory

	\note 	The return MAPI_E_SUCCESS or Mapi Error are all handled internally in the function hence the void type
	\note 	A debug log is recorded on both success and failure of this method

	\sa 	CreateProfileStore, GetLastError
*/
void Mapiprofile::createNewDatabase()
{
    enum MAPISTATUS 	  retval;
    database.append( newDatabaseEntry->text() );

    retval = CreateProfileStore(database.toUtf8().constData(), LDIF.toUtf8().constData());

    if (retval != MAPI_E_SUCCESS)  
    {  
	mapi_errstr("Creating Database", GetLastError());
	exit(1);
    }
    // then enable profile admin but with no profiles listed yet
    ui->addProfBtn->setEnabled(true);
    ui->deleteProfBtn->setEnabled(false);
    ui->profilesList->setEnabled(true);
    ui->profileManager->setEnabled(true);
    listProfiles();
    // reconfigue working database to newly created one.
    database = "$HOME/.openchange/" + newDatabaseEntry->text() + ".ldb";
}

/**
	\details Handle option 'select existing database'
*/
void Mapiprofile::on_selectExistingDatabase_toggled(bool checked)
{
    ui->proceed->setEnabled(true);
    selectD = 1;
}

/**
	\details Handle option 'create new database'
*/
void Mapiprofile::on_createNewDatabase_toggled(bool checked)
{
    ui->proceed->setEnabled(true);
    createD = 1;
}

/**
	\details Utility for handling user options on startup
*/
void Mapiprofile::on_proceed_clicked()
{
    if (selectD == 1)
        selectDatabase();
    else
        createNew();	
    // reset markers
    selectD = 0;
    createD = 0;
}

/**
	\details Utility for setting up Profile Management
*/
void Mapiprofile::enableProfileMan()
{
    ui->addProfBtn->setEnabled(true);
    ui->deleteProfBtn->setEnabled(true);
    ui->dumpProfBtn->setEnabled(true);
    ui->setDefProfBtn->setEnabled(true);
    ui->profilesList->setEnabled(true);
}

/**
	\details Add new Profile Dialog

	- Provides an interface for user to enter parameters of the new profile
	- Provides buttons to pick user signals for relevant options
 */
void Mapiprofile::addDialog()
{
    Add 					= new QDialog(this);
    QLabel *topLabel		= new QLabel(tr("Enter the profile name and details below"));
    QLabel *usernameLabel	= new QLabel(tr("Username:"));
    QLabel *passwordLabel	= new QLabel(tr("Password:"));
    QLabel *profileLabel	= new QLabel(tr("Profile:"));
    profileName				= new QLineEdit;
    usernameEntry			= new QLineEdit;
    passwordEntry			= new QLineEdit;
    cancelBtn				= new QPushButton(tr("Cancel"));
    addBtn					= new QPushButton(tr("Add"));
    QGridLayout *layout		= new QGridLayout;

    layout->addWidget(topLabel, 0, 0, 1, 2);
    layout->addWidget(profileLabel, 1, 0);
    layout->addWidget(profileName, 1, 1);
    layout->addWidget(usernameLabel, 2, 0);
    layout->addWidget(usernameEntry, 2, 1);
    layout->addWidget(passwordLabel, 3, 0);
    layout->addWidget(passwordEntry, 3, 1);
    layout->addWidget(cancelBtn, 4, 0);
    layout->addWidget(addBtn, 4, 1);

    Add->setLayout(layout);
    Add->setWindowTitle(tr("Add a new Profile"));
    Add->show();
    Add->raise();
    Add->activateWindow();

    connect(cancelBtn, SIGNAL(clicked()), this, SLOT(close()) );
    connect(addBtn, SIGNAL(clicked()), this, SLOT(addProfile()) );
}

/**
	\details Add New Profile

	\param profile 		The name of the profile to be created as const char*
	\param username 	The username for the profile in const char*
	\param password		Password

	\note 	The return MAPI_E_SUCCESS or Mapi Error are all handled internally in the function hence the void type
	\note 	A debug log is recorded on both success and failure of this method

	\sa 	OpenProfile, CreateProfile, GetLastError
*/
void Mapiprofile::addProfile()
{
    qDebug() << "creating profile: " << profileName->text();

    enum MAPISTATUS         retval;
    struct mapi_profile     test;

    // if the profile already exists, we overwrite it....
    retval = OpenProfile( &test, profileName->text().toUtf8().constData(), 0 );
    qDebug() << "checking profile result: " << retval;
    if ( GetLastError() != MAPI_E_NOT_FOUND ) {
    // then continue and destroy it
    // no need to confirm deletion here, its internal
        if ((retval = DeleteProfile(profileName->text().toUtf8().constData()) ) != MAPI_E_SUCCESS) {
            mapi_errstr("DeleteProfile", GetLastError());
            return;
        }
    }
    // no copy exists so we create a new one
    retval = CreateProfile(profileName->text().toUtf8().constData(),
                         usernameEntry->text().toUtf8().constData(),
                         passwordEntry->text().toUtf8().constData(), 0);
    if (retval != MAPI_E_SUCCESS) {
        mapi_errstr("CreateProfile", GetLastError());
        return;
    }
}

/**
	\details Delete Profile

	\param profile The name of the profile to be deleted, given by selected item on profiles list

	\note 	The return MAPI_E_SUCCESS or Mapi Error are all handled internally in the function hence the void type
	\note 	A debug log is recorded on both success and failure of this method

	\sa 	DeleteProfile, GetLastError
*/
void Mapiprofile::deleteProfile()
{
    enum MAPISTATUS retval;
    QString deleteSelect = (ui->profilesList->currentItem()->text());

    //no need to check if profile exists because we only act on the list of
    //profiles
    
    QMessageBox confirm;
    confirm.setText("Warning!");
    confirm.setIcon(QMessageBox::Warning);
    confirm.setInformativeText("This will completely delete the profile from the database. Click OK to continue or Cancel to abort");
    confirm.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    confirm.setDefaultButton(QMessageBox::Cancel);

    int choice = confirm.exec();

    switch (choice)
    {
    	case QMessageBox::Ok:	// we have a go
	{
	    if ((retval = DeleteProfile(deleteSelect.toUtf8().constData()) ) != MAPI_E_SUCCESS) 
	    {
 		mapi_errstr("DeleteProfile", GetLastError());
	        exit (1);
	    }
	    qDebug() << deleteSelect << "deleted";
	    break;
	}
	case QMessageBox::Cancel:	// we abort
	{
	    qDebug() << "Aborting deletion";
	    break;
	}
	default:
	    break;
    }
}

/**
	\details Populate profile list from database

	\note 	The return MAPI_E_SUCCESS or Mapi Error are all handled internally in the function hence the void type
	\note 	A debug log is recorded on both success and failure of this method

	\sa 	GetProfileTable, GetLastError
*/
void Mapiprofile::listProfiles()
{
    enum MAPISTATUS retval;
    struct SRowSet profileTable;
    uint32_t profCount;       // profile count

    memset(&profileTable, 0, sizeof (struct SRowSet));
    if ((retval = GetProfileTable(&profileTable)) != MAPI_E_SUCCESS) {
        mapi_errstr("GetProfileTable", GetLastError());
        exit (1);
    }
    
    // fill up the profiles list
    for (profCount = 0; profCount != profileTable.cRows; profCount++)
    {
        const char *profName = NULL;
		uint32_t   check     = 0;	// check if 1 then default profile, 0 else

        profName = profileTable.aRow[profCount].lpProps[0].value.lpszA;
		check    = profileTable.aRow[profCount].lpProps[1].value.l;

	if (check == 1)
	{
	    QString defProfile (QString (profName) + QString(" [default]"));
	    QListWidgetItem *def = new QListWidgetItem(defProfile, ui->profilesList);
	    ui->profilesList->setCurrentItem(def);
	}
	else
	    new QListWidgetItem(QString(profName), ui->profilesList);

    }

}

/**
	\details Set Default Profile

	\param profile The name of the profile to set default, given by selected item on profiles list

	\note 	The return MAPI_E_SUCCESS or Mapi Error are all handled internally in the function hence the void type
	\note 	A debug log is recorded on both success and failure of this method

	\sa 	SetDefaultProfile, GetLastError
*/
void Mapiprofile::setDefaultProfile()
{
    enum MAPISTATUS retval;
    QString defaultselect  = ui->profilesList->currentItem()->text();
    qDebug() << "selected Profile to set default" << defaultselect;

    if (defaultselect.endsWith(" [default]"))
	{
		qDebug() << "profile already default";
        return;         // its already the default profile
	}
    if ((retval = SetDefaultProfile(defaultselect.toUtf8().constData()) ) != MAPI_E_SUCCESS)
	{
        mapi_errstr("SetDefaultProfile", GetLastError());
		qDebug() << "aborting set default profile";
		exit(1);
	}
	qDebug() << "profile" << defaultselect << "set to default";
}

/**
	\details Dump Profile, shows a dialog listing profile details

	\param profile The name of the profile to set default, given by selected item on profiles list

	\note 	The return MAPI_E_SUCCESS or Mapi Error are all handled internally in the function hence the void type
	\note 	A debug log is recorded on both success and failure of this method

	\sa 	OpenProfile, GetLastError
*/
void Mapiprofile::dumpProfile()
{
	enum MAPISTATUS 	  	retval;
	struct mapi_profile 	prof;
	dumpUsername			= new QLabel;
	dumpusm					= new QLabel(tr("Username: "));
	dumpPassword			= new QLabel;
	dumppwd					= new QLabel(tr("Password: "));
	dumpWorkstation			= new QLabel;
	dumpws					= new QLabel(tr("Workstation: "));
	dumpRealm				= new QLabel;
	dumprlm					= new QLabel(tr("Realm: "));
	dumpDomain				= new QLabel;
	dumpdom					= new QLabel(tr("Domain: "));
	QGridLayout *layout		= new QGridLayout;
	View					= new QDialog(this);

	retval = OpenProfile( &prof, ui->profilesList->currentItem()->text().toUtf8().constData(), 0 );

	if (retval != MAPI_E_SUCCESS)
	{
		mapi_errstr("OpenProfile for dump", GetLastError());
		qDebug() << "aborting dump profile";
		exit(1);
	}

	/* retrieving profile details from the struct mapi_profile */
	const char* usernm     	= prof.username;
	const char* passwd     	= prof.password;
	const char* wstation  	= prof.workstation;
	const char* rlm        	= prof.realm;
	const char* dom       	= prof.domain;

	dumpUsername->setText(usernm);
	dumpPassword->setText(passwd);
	dumpWorkstation->setText(wstation);
	dumpRealm->setText(rlm);
	dumpDomain->setText(dom);

	layout->addWidget(dumpusm, 0, 0);
	layout->addWidget(dumpUsername, 0, 1);
	layout->addWidget(dumppwd, 1, 0);
	layout->addWidget(dumpPassword, 1, 1);
	layout->addWidget(dumpws, 2, 0);
	layout->addWidget(dumpWorkstation, 2, 1);
	layout->addWidget(dumprlm, 3, 0);
	layout->addWidget(dumpRealm, 3 ,1);
	layout->addWidget(dumpdom, 4, 0);
	layout->addWidget(dumpDomain, 4, 1);

	View->setLayout(layout);
	View->setWindowTitle(tr("Profile Details"));
	View->show();
	View->raise();
	View->activateWindow();
	
}


