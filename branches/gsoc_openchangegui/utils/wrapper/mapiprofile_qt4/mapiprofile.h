/*
    Openchange Tools

    mapiprofile Graphical Front End

    Copyright (C) Billy Okal 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAPIPROFILE_H
#define MAPIPROFILE_H

#include <QtGui/QWidget>
#include <qdialog.h>

/**
	\file mapiprofile.h

	\brief Declarations for mapiprofle gui implementation
*/

class QDialog;
class QLabel;
class QLineEdit;
class QPushButton;

namespace Ui
{
    class Mapiprofile;
}

class Mapiprofile : public QWidget
{
    Q_OBJECT

public:
    Mapiprofile(QWidget *parent = 0);
    ~Mapiprofile();
    void createNew();
    void selectDatabase();
    void enableProfileMan();
    void addProfile();
    void dumpProfile();
    void deleteProfile();
    void listProfiles();
    void setDefaultProfile();
    void addDialog();

public slots:
    void createNewDatabase();

private:
    Ui::Mapiprofile *ui;
    QPushButton     *cancelBtn;
    QDialog         *newDatabase;
    QDialog	    *Add;
    QDialog	    *View;
    QLineEdit       *newDatabaseEntry;
    QLineEdit	    *usernameEntry;
    QLineEdit       *passwordEntry;
    QLineEdit       *profileName;
    QLabel	    *dumpUsername;
    QLabel	    *dumpusm;
    QLabel	    *dumpPassword;
    QLabel	    *dumppwd;
    QLabel          *dumpWorkstation;
    QLabel 	    *dumpws;
    QLabel	    *dumpRealm;
    QLabel	    *dumprlm;
    QLabel          *dumpDomain;
    QLabel    	    *dumpdom;
    QPushButton     *newCreateBtn;
    QPushButton	    *addBtn;
    QLabel          *newTopLabel;
    QLabel          *newDatabaseLabel;

private slots:
    void on_proceed_clicked();
    void on_createNewDatabase_toggled(bool checked);
    void on_selectExistingDatabase_toggled(bool checked);
};

#endif // MAPIPROFILE_H
