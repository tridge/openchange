/*
   Transalte MAPI into POP3

   OpenChange Project

   Copyright (C) Yangyan Li 2008

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <libmapi/libmapi.h>
#include <samba/popt.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <param.h>
#include <magic.h>
#include <ctype.h>
#include <assert.h>

#include "openchange-tools.h"
#include "mapi2pop3.h"

static int      opt_pop3io = POP3IO_STDIO;
static int      opt_all = false;
static int      opt_timeout = 10;
static const char *opt_profdb = NULL;

static int      successful_user_cmd = false;
static int      pop3io_session_fd;
static uint32_t count;
static int      deleted_count;
static int      size;
static int      *position = NULL;
static bool     *deleted = NULL;
static char     profname[MAX_ARG_LEN + 1];

static enum pop3_state current_state;
static struct cmd_table pop3_commands[] = {
    {"user", AUTHORIZATION, &user_cmd},
    {"pass", AUTHORIZATION, &pass_cmd},
//  {"apop", AUTHORIZATION, &apop_cmd},

    {"stat", TRANSACTION, &stat_cmd},
    {"list", TRANSACTION, &list_cmd},
    {"retr", TRANSACTION, &retr_cmd},
    {"dele", TRANSACTION, &dele_cmd},
    {"noop", TRANSACTION, &noop_cmd},
    {"rset", TRANSACTION, &rset_cmd},
//    {"top", TRANSACTION, &top_cmd}, 
    {"uidl", TRANSACTION, &uidl_cmd},

    {"quit", (AUTHORIZATION | TRANSACTION), &quit_cmd}
};

static TALLOC_CTX    *mem_ctx;
static mapi_object_t obj_store;
static mapi_object_t obj_inbox;
static mapi_object_t obj_table;
static struct SRowSet rowset;

static bool pop3_response(const char *format, ...)
{
    va_list         ap;
    int             len;
    va_list         ap_test;
    char            len_test;
    char            *data = NULL;
    va_start(ap, format);

    switch (opt_pop3io) {
    case POP3IO_STDIO:
	vprintf(format, ap);
	break;
    case POP3IO_SOCKET:
	va_copy(ap_test, ap);
	len = vsnprintf(&len_test, 1, format, ap_test);
	va_end(ap_test);
	if (len < 0) {
	    fprintf(stderr, "Error: error response length\n");
	    va_end(ap);
	    return false;
	}

	data = (char *) malloc(sizeof(char) * (len + 1));
	if (data == NULL) {
	    fprintf(stderr, "Error: memory allocation failed for response data\n");
	    va_end(ap);
	    return false;
	}

	vsnprintf(data, len + 1, format, ap);
	write(pop3io_session_fd, data, len + 1);
	free(data);

	break;
    default:
	fprintf(stderr, "Error: undefined pop3io type\n");
	va_end(ap);
	return false;
    }				/* ----- end switch ----- */
    va_end(ap);

    return true;
}

static bool pop3_read_cmd_line(char line[])
{
    int             i;
    struct timeval  tv;
    fd_set          readfds;

    memset(line, 0, sizeof(line));

    tv.tv_sec = opt_timeout;
    tv.tv_usec = 0;

    FD_ZERO(&readfds);

    switch (opt_pop3io) {
    case POP3IO_STDIO:
        FD_SET(STDIN_FILENO, &readfds);
	select(STDIN_FILENO+1, &readfds, NULL, NULL, &tv);
	if (!(FD_ISSET(STDIN_FILENO, &readfds))){
		fprintf(stderr, "Timed out.\n");
		return false;
	}

	for (i = 0; i < MAX_CMD_LINE_LEN; i++) {
	    line[i] = getchar();
	    if (line[i] == '\n') {
		line[i] = '\0';
		break;
	    }
	}
	break;
    case POP3IO_SOCKET:
	FD_SET(pop3io_session_fd, &readfds);
	select(pop3io_session_fd+1, &readfds, NULL, NULL, &tv);
	if (!(FD_ISSET(pop3io_session_fd, &readfds))){
		fprintf(stderr, "Timed out.\n");
		return false;
	}
	read(pop3io_session_fd, line, MAX_CMD_LINE_LEN);
	line[strlen(line) - 2] = '\0';
	break;
    default:
	fprintf(stderr, "Error: undefined pop3io type\n");
	break;
    }
    return true;
}

static int init_pop3io(int pop3io_port)
{
    int             pop3io_fd = -1;
    struct sockaddr_in server_addr;

    switch (opt_pop3io) {
    case POP3IO_STDIO:
	return 1;
    case POP3IO_SOCKET:
	pop3io_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (pop3io_fd < 0) {
	    fprintf(stderr, "Error: failed to open socket\n");
	    return -1;
	}
	bzero((char *) &server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = INADDR_ANY;
	server_addr.sin_port = htons(pop3io_port);
	if (bind(pop3io_fd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
	    fprintf(stderr, "Error: failed on binding\n");
	    return -1;
	}
	break;
    default:
	fprintf(stderr, "Error: undefined pop3io type\n");
	return -1;
    }				/* ----- end switch ----- */

    return pop3io_fd;
}

static bool init_pop3io_session(int pop3io_fd)
{
    socklen_t             client_len;
    struct sockaddr_in client_addr;

    switch (opt_pop3io) {
    case POP3IO_STDIO:
	return true;
    case POP3IO_SOCKET:
	listen(pop3io_fd, 5);
	client_len = sizeof(client_addr);
	pop3io_session_fd = accept(pop3io_fd, (struct sockaddr *) &client_addr, &client_len);
	if (pop3io_session_fd < 0) {
	    fprintf(stderr, "Error: failed on accept\n");
	    return false;
	}
	return true;
    default:
	fprintf(stderr, "Error: undefined pop3io type\n");
	return false;
    }				/* ----- end switch ----- */

    return true;
}

static bool close_pop3io(int pop3io_fd)
{
    switch (opt_pop3io) {
    case POP3IO_STDIO:
	return true;
    case POP3IO_SOCKET:
	close(pop3io_fd);
	return true;
    default:
	fprintf(stderr, "Error: undefined pop3io type\n");
	return false;
    }				/* ----- end switch ----- */

    return true;
}

static bool close_pop3io_session()
{
    switch (opt_pop3io) {
    case POP3IO_STDIO:
	break;
    case POP3IO_SOCKET:
	close(pop3io_session_fd);
	break;
    default:
	fprintf(stderr, "Error: undefined pop3io type\n");
	return false;
    }				/* ----- end switch ----- */

    return true;
}

static void pop3_server(int pop3io_fd)
{
    int             valid_cmd;
    char            line[MAX_CMD_LINE_LEN] = { 0 };
    int             argc;
    char           *argv[MAX_ARGC];
    struct cmd_table *pop3_cmd;
    int             pop3io_session_fd;

    pop3io_session_fd = init_pop3io_session(pop3io_fd);

    if (pop3io_session_fd >= 0) {
	pop3_response("+OK Pop3 proxy server ready\015\012");
    } else {
	pop3_response("-ERR Failed to open a new session\015\012");
	return;
    }

    while (current_state != UPDATE) {
	valid_cmd = false;
	/* get the input from the client */
	if (!pop3_read_cmd_line(line)) {
	    /* time out */
	    if (current_state == TRANSACTION) {
		mapi_object_release(&obj_table);
		mapi_object_release(&obj_inbox);
		mapi_object_release(&obj_store);
		MAPIUninitialize();
		talloc_free(mem_ctx);
	    }

	    if(position){
		free(position);
		position = NULL;
	    }
	    if(deleted){
	        free(deleted);
	        deleted = NULL;
	    }
	    return;
	}
	/* init argc, argv */
	for (argc = MAX_ARGC; argc; argv[--argc] = NULL);

	/* parse command */
	char            delims[] = " ";
	argc = 0;
	argv[argc] = strtok(line, delims);
	while (argv[argc] != NULL && argc < MAX_ARGC - 1) {
	    argc++;
	    argv[argc] = strtok(NULL, delims);
	}

	if (!argc) {
	    pop3_response("-ERR Command not implemented\015\012");
	    continue;
	}

	/* cycle through jumptable */
	for (pop3_cmd = pop3_commands; pop3_cmd->handler != NULL; pop3_cmd++) {
	    if (!strcasecmp(pop3_cmd->name, argv[0])) {
		if (pop3_cmd->valid_state == current_state || !strcasecmp(pop3_cmd->name, "QUIT")) {
		    (pop3_cmd->handler) (argc, argv);
		} else {
		    pop3_response("-ERR That command is not available right now.\015\012");
		}

		/* valid command is found */
		valid_cmd = true;

		/* PASS may only be given immediately after a successful USER command, so we have to record this state */
		if (strcasecmp("user", argv[0]) || (!successful_user_cmd)) {
		    successful_user_cmd = false;
		}
		break;
	    }
	}

	if (!valid_cmd) {
	    pop3_response("-ERR Invalid command.\015\012");
	}
    }

    close_pop3io_session();
    if (current_state == TRANSACTION) {
	mapi_object_release(&obj_table);
	mapi_object_release(&obj_inbox);
	mapi_object_release(&obj_store);
	MAPIUninitialize();
	talloc_free(mem_ctx);
    }
    if(position != NULL) {
	free(position);
    }
    if(deleted != NULL) {
	free(deleted);
    }
}


static void user_cmd(int argc, char *argv[])
{
    if (argc < 2) {
	pop3_response("-ERR Missing username(profile name) argument\015\012");
	return;
    }

    strncpy(profname, argv[1], MAX_ARG_LEN);
    profname[MAX_ARG_LEN] = '\0';

    if (is_valid_user()) {
	pop3_response("+OK User(profile) name accepted, password please.\015\012");
	successful_user_cmd = true;
    } else {
	pop3_response("-ERR Failed to check the user(profile)\015\012");
	successful_user_cmd = false;
    }

    return;
}

static void pass_cmd(int argc, char *argv[])
{
    int             i;
    if (!successful_user_cmd) {
	pop3_response("-ERR PASS may only be given immediately after a successful USER command\015\012");
	return;
    }

    if (argc < 2) {
	pop3_response("-ERR Missing password argument\015\012");
	return;
    }

    if (logon(argv[1])) {
	size = 0;
	for (i = 1; i <= count; i++)
	    size += get_size(i);
	pop3_response("+OK Maildrop ready, %d messages (%d octets)\015\012", count, size);
	current_state = TRANSACTION;
    } else {
	pop3_response("-ERR Failed to logon exchange server\015\012");
    }

    return;
}

static void quit_cmd(int argc, char *argv[])
{
    if (expunge_deleted()) {
	pop3_response("+OK See you later!\015\012");
    } else {
	pop3_response("-ERR Error occurrs when quiting\015\012");
    }

    if (current_state == TRANSACTION) {
	mapi_object_release(&obj_table);
	mapi_object_release(&obj_inbox);
	mapi_object_release(&obj_store);
	MAPIUninitialize();
	talloc_free(mem_ctx);
    }

    free(position);
    position = NULL;
    free(deleted);
    deleted = NULL;

    current_state = UPDATE;
    return;
}

static void stat_cmd(int argc, char *argv[])
{
    pop3_response("+OK %d %d\015\012", count-deleted_count, size);
}

static void list_cmd(int argc, char *argv[])
{
    int             msg_num;
    if (argc == 1) {
	pop3_response("+OK %d message(s) %d octets.\015\012", count - deleted_count, size);

	for (msg_num = 1; msg_num <= count; msg_num++) {
	    if (!is_deleted(msg_num)) {
		pop3_response("%d %d\015\012", msg_num, get_size(msg_num));
	    }
	}
	pop3_response(".\015\012");
	return;
    }
    if (argc == 2) {
	msg_num = atoi(argv[1]);
	if (msg_num <= 0) {
	    pop3_response("-ERR Invalid message number.\015\012");
	    return;
	}

	if (msg_num <= count) {
	    if (!is_deleted(msg_num)) {
		pop3_response("+OK %d %d\015\012", msg_num, get_size(msg_num));
	    } else {
		pop3_response("-ERR Message deleted.\015\012");
	    }
	} else {
	    pop3_response("-ERR Invalid message number.\015\012");
	}
    }
}

static void retr_cmd(int argc, char *argv[])
{
    if (argc < 2) {
	pop3_response("-ERR Missing message number argument\015\012");
	return;
    }

    int             msg_num = atoi(argv[1]);
    if (msg_num <= 0) {
	pop3_response("-ERR Invalid message number.\015\012");
	return;
    }
    if (msg_num <= count) {
	if (!is_deleted(msg_num)) {
	    pop3_response("+OK %d octets\015\012", get_size(msg_num));
	    fetch_headers(msg_num);
	    fetch_body(msg_num);
	    pop3_response(".\015\012");
	} else {
	    pop3_response("-ERR Message already deleted\015\012");
	}
    } else {
	pop3_response("-ERR Invalid message number.\015\012");
    }
}

static void dele_cmd(int argc, char *argv[])
{
    if (argc < 2) {
	pop3_response("-ERR Missing message number argument\015\012");
	return;
    }

    int             msg_num = atoi(argv[1]);
    if (msg_num <= 0) {
	pop3_response("-ERR Invalid message number.\015\012");
	return;
    }

    if (msg_num <= count) {
	if (is_deleted(msg_num)) {
	    pop3_response("-ERR Message already deleted\015\012");
	} else {
	    mark_deleted(msg_num);
	    pop3_response("+OK Message %d deleted.\015\012", msg_num);
	}
    } else {
	pop3_response("-ERR Invalid message number.\015\012");
    }
}

static void noop_cmd(int argc, char *argv[])
{
    pop3_response("+OK Let us wait.\015\012");
}

static void rset_cmd(int argc, char *argv[])
{
    reset_deleted();
    pop3_response("+OK Maildrop has %d message(s) %d octets.\015\012", count, size);
}

static void uidl_cmd(int argc, char *argv[])
{
    int             msg_num;
    char            uidl[MAX_UIDL_LEN];

    if (argc == 1) {
	pop3_response("+OK %d message(s) %d octets.\015\012", count - deleted_count, size);

	for (msg_num = 1; msg_num <= count; msg_num++) {
	    if (!is_deleted(msg_num)) {
		get_uidl(msg_num, uidl);
		pop3_response("%d %s\015\012", msg_num, uidl);
	    }
	}
	pop3_response(".\015\012");
	return;
    }
    if (argc == 2) {
	msg_num = atoi(argv[1]);
	if (msg_num <= 0) {
	    pop3_response("-ERR Invalid message number.\015\012");
	    return;
	}

	if (msg_num <= count) {
	    if (is_deleted(msg_num)) {
		pop3_response("-ERR Message deleted.\015\012");
	    } else {
		get_uidl(msg_num, uidl);
		pop3_response("+OK %d %s\015\012", msg_num, uidl);
	    }
	} else {
	    pop3_response("-ERR Invalid message number.\015\012");
	}
    }
}

/**
   \details Logon Exchange Server and open Inbox with the profile set in is_valid_user
 
   \param password password to use for the profile

   \return true on success, otherwise false.

   \note 

   \sa is_valid_user
 */

static bool logon(const char *password)
{
    enum MAPISTATUS retval;
    struct mapi_session *session = NULL;
    struct SPropTagArray *SPropTagArray = NULL;
    struct SPropValue *lpProps;
    struct SRow     aRow;
    mapi_id_t       id_inbox;
    mapi_id_t      *fid;
    mapi_id_t      *mid;
    mapi_object_t   obj_message;
    uint32_t        props_count;
    uint32_t        unread_count = 0;
    uint64_t        msg_flags;
    int             i;

    mem_ctx = talloc_init("mapi2pop3");

    /* Initialize MAPI subsystem */
    retval = MAPIInitialize(opt_profdb);
    if (retval != MAPI_E_SUCCESS) {
	fprintf(stderr, "Error: MAPIInitialize failed\n");
	MAPIUninitialize();
	talloc_free(mem_ctx);
	return false;
    }

    retval = MapiLogonEx(&session, profname, password);
    if (retval != MAPI_E_SUCCESS) {
	fprintf(stderr, "Error: MapiLogonEx failed\n");
	MAPIUninitialize();
	talloc_free(mem_ctx);
	return false;
    }

    /* Open the default message store */
    mapi_object_init(&obj_store);
    retval = OpenMsgStore(&obj_store);
    if (retval != MAPI_E_SUCCESS) {
	fprintf(stderr, "Error: OpenMsgStore failed\n");
	mapi_object_release(&obj_store);
	MAPIUninitialize();
	talloc_free(mem_ctx);
	return false;
    }

     /*  open default Inbox */
    retval = GetDefaultFolder(&obj_store, &id_inbox, olFolderInbox);
    if (retval != MAPI_E_SUCCESS) {
	fprintf(stderr, "Error: GetDefaultFolder failed\n");
	mapi_object_release(&obj_store);
	MAPIUninitialize();
	talloc_free(mem_ctx);
	return false;
    }

    mapi_object_init(&obj_inbox);
    retval = OpenFolder(&obj_store, id_inbox, &obj_inbox);
    if (retval != MAPI_E_SUCCESS) {
	fprintf(stderr, "Error: OpenFolder failed\n");
	mapi_object_release(&obj_inbox);
	mapi_object_release(&obj_store);
	MAPIUninitialize();
	talloc_free(mem_ctx);
	return false;
    }

    if (!opt_all) {
	SPropTagArray = set_SPropTagArray(mem_ctx, 0x1, PR_CONTENT_UNREAD);
	retval = GetProps(&obj_inbox, SPropTagArray, &lpProps, &props_count);
	MAPIFreeBuffer(SPropTagArray);
	if (retval == MAPI_E_SUCCESS) {
	    /* Build a SRow structure */
	    aRow.ulAdrEntryPad = 0;
	    aRow.cValues = props_count;
	    aRow.lpProps = lpProps;

	    unread_count = *(const uint64_t *) find_SPropValue_data(&aRow, PR_CONTENT_UNREAD);
	} else {
	    fprintf(stderr, "Error: Failed to read PR_CONTENT_UNREAD\n");
	    talloc_free(lpProps);
	    mapi_object_release(&obj_table);
	    mapi_object_release(&obj_inbox);
	    mapi_object_release(&obj_store);
	    MAPIUninitialize();
	    talloc_free(mem_ctx);
	    return false;
	}
	talloc_free(lpProps);
    }

    mapi_object_init(&obj_table);
    retval = GetContentsTable(&obj_inbox, &obj_table, 0, &count);
    if (retval != MAPI_E_SUCCESS) {
	fprintf(stderr, "Error: GetContentsTable failed\n");
	mapi_object_release(&obj_table);
	mapi_object_release(&obj_inbox);
	mapi_object_release(&obj_store);
	MAPIUninitialize();
	talloc_free(mem_ctx);
	return false;
    }

    SPropTagArray = set_SPropTagArray(mem_ctx, 0x2,
		                      PR_FID,
				      PR_MID);
    retval = SetColumns(&obj_table, SPropTagArray);
    MAPIFreeBuffer(SPropTagArray);
    if (retval != MAPI_E_SUCCESS) {
	fprintf(stderr, "Error: SetColumns failed\n");
	mapi_object_release(&obj_table);
	mapi_object_release(&obj_inbox);
	mapi_object_release(&obj_store);
	MAPIUninitialize();
	talloc_free(mem_ctx);
	return false;
    }

    retval = QueryRows(&obj_table, count, TBL_ADVANCE, &rowset);
    if (retval != MAPI_E_SUCCESS) {
	fprintf(stderr, "Error: QueryRows failed\n");
	mapi_object_release(&obj_table);
	mapi_object_release(&obj_inbox);
	mapi_object_release(&obj_store);
	MAPIUninitialize();
	talloc_free(mem_ctx);
	return false;
    }

    if (opt_all) {
	unread_count = count;
    }
    position = (int *) malloc(sizeof(int) * unread_count);
    deleted = (bool *) malloc(sizeof(bool) * unread_count);
    memset(deleted, false, sizeof(deleted));
    deleted_count = 0;

    if (opt_all) {
	for (i = 0; i < count; i++)
	    position[i] = i;
    } else {
	unread_count = 0;
	for (i = 0; i < count; i++) {
	    fid = (mapi_id_t *) find_SPropValue_data(&(rowset.aRow[i]), PR_FID);
	    mid = (mapi_id_t *) find_SPropValue_data(&(rowset.aRow[i]), PR_MID);
	    mapi_object_init(&obj_message);

	    retval = OpenMessage(&obj_store, *fid, *mid, &obj_message, 0x0);
	    if (retval == !MAPI_E_SUCCESS) {
		fprintf(stderr, "Error: OpenMessage failed\n");
		mapi_object_release(&obj_message);
		mapi_object_release(&obj_table);
		mapi_object_release(&obj_inbox);
		mapi_object_release(&obj_store);
		MAPIUninitialize();
		talloc_free(mem_ctx);
		return false;
	    }
	    SPropTagArray = set_SPropTagArray(mem_ctx, 0x1, PR_MESSAGE_FLAGS);
	    lpProps = talloc_zero(mem_ctx, struct SPropValue);
	    retval = GetProps(&obj_message, SPropTagArray, &lpProps, &props_count);
	    MAPIFreeBuffer(SPropTagArray);
	    if (retval == !MAPI_E_SUCCESS) {
		fprintf(stderr, "Error: GetProps failed\n");
		talloc_free(lpProps);
		mapi_object_release(&obj_message);
		mapi_object_release(&obj_table);
		mapi_object_release(&obj_inbox);
		mapi_object_release(&obj_store);
		MAPIUninitialize();
		talloc_free(mem_ctx);
		return false;
	    }

	    /* Build a SRow structure */
	    aRow.ulAdrEntryPad = 0;
	    aRow.cValues = props_count;
	    aRow.lpProps = lpProps;

	    msg_flags = *(const uint64_t *) find_SPropValue_data(&aRow, PR_MESSAGE_FLAGS);
	    if (!(msg_flags & MSGFLAG_READ)) {
		position[unread_count++] = i;
	    }

	    talloc_free(lpProps);
	    mapi_object_release(&obj_message);
	}
	count = unread_count;
    }

    return true;
}


/**
   \details Mark the given message "deleted"
 
   \param number the number of the to-be-deleted message

   \return true on success, otherwise false.

   \note 

   \sa
 */
static bool mark_deleted(int number)
{
    assert(number > 0 && number <= count);

    deleted[number - 1] = true;
    size -= get_size(number);
    deleted_count++;
    return true;
}


/**
   \details If --all is set, delete the messages which are marked "deleted", else
   mark them "read"
 
   \param

   \return true on success, otherwise false.

   \note 

   \sa
 */
static bool expunge_deleted()
{
    enum MAPISTATUS retval;
    mapi_id_t      *fid;
    mapi_id_t      *mid;
    mapi_id_t      *deleted_ids;
    mapi_object_t   obj_message;
    int             i;

    deleted_ids = talloc_array(mem_ctx, uint64_t, deleted_count);
    deleted_count = 0;
    for (i = 0; i < count; i++) {
	if (!deleted[i]) {
	    continue;
	}

	if (opt_all) {
	    deleted_ids[deleted_count++] = rowset.aRow[position[i]].lpProps[1].value.d;
	} else {
	    fid = (mapi_id_t *) find_SPropValue_data(&(rowset.aRow[position[i]]), PR_FID);
	    mid = (mapi_id_t *) find_SPropValue_data(&(rowset.aRow[position[i]]), PR_MID);
	    mapi_object_init(&obj_message);

	    retval = OpenMessage(&obj_store, *fid, *mid, &obj_message, 0x0);
	    if (retval == !MAPI_E_SUCCESS) {
		fprintf(stderr, "Error: OpenMessage failed\n");
		mapi_object_release(&obj_message);
		return false;
	    }

	    retval = SetMessageReadFlag(&obj_inbox, &obj_message, MSGFLAG_READ);
	    if (retval == !MAPI_E_SUCCESS) {
		fprintf(stderr, "Error: SetMessageReadFlag failed\n");
		mapi_object_release(&obj_message);
		return false;
	    }
	    mapi_object_release(&obj_message);
	}
    }
    if (opt_all) {
	retval = DeleteMessage(&obj_inbox, deleted_ids, deleted_count);
	if (retval == !MAPI_E_SUCCESS) {
	    fprintf(stderr, "Error: DeleteMessage failed\n");
	    mapi_object_release(&obj_message);
	    return false;
	}
    }

    return true;
}


/**
   \details reset the status of all messages to "not to be deleted" 
 
   \param

   \return true on success, otherwise false.

   \note 

   \sa
 */
static bool reset_deleted()
{
    int             i;
    if (deleted == NULL) {
	return false;
    }

    for (i = 0; i < count; i++) {
	if (deleted[i]) {
	    size += get_size(i+1);
	}
    }

    memset(deleted, false, sizeof(deleted));
    deleted_count = 0;
    return true;
}

/**
   \details Get the size of the given message
 
   \param number the number of the message

   \return the size of the given message on success, otherwise 0

   \note message size in MAPI indicates the approximate number of bytes
   transferred when the message is moved from one message store to
   another. Being the sum of the sizes of all properties on the message
   object, it is usually considerably greater than the message text alone

   \sa
 */
static int get_size(int number)
{
    enum MAPISTATUS retval;
    struct SPropTagArray *SPropTagArray = NULL;
    struct SPropValue *lpProps;
    struct SRow     aRow;
    mapi_object_t   obj_message;
    mapi_id_t      *fid;
    mapi_id_t      *mid;
    uint32_t        props_count;
    uint32_t        msg_size = 0;

    assert(number > 0 && number <= count);

    fid = (mapi_id_t *) find_SPropValue_data(&(rowset.aRow[position[number - 1]]), PR_FID);
    mid = (mapi_id_t *) find_SPropValue_data(&(rowset.aRow[position[number - 1]]), PR_MID);
    mapi_object_init(&obj_message);

    retval = OpenMessage(&obj_store, *fid, *mid, &obj_message, 0x0);
    if (retval == MAPI_E_SUCCESS) {
	SPropTagArray = set_SPropTagArray(mem_ctx, 0x1, PR_MESSAGE_SIZE);
	retval = GetProps(&obj_message, SPropTagArray, &lpProps, &props_count);
	MAPIFreeBuffer(SPropTagArray);
	if (retval == MAPI_E_SUCCESS) {
	    /* Build a SRow structure */
	    aRow.ulAdrEntryPad = 0;
	    aRow.cValues = props_count;
	    aRow.lpProps = lpProps;

	    msg_size = *(const uint32_t *) find_SPropValue_data(&aRow, PR_MESSAGE_SIZE);
	}
	talloc_free(lpProps);
    }

    mapi_object_release(&obj_message);

    return msg_size;
}


/**
   \details Get the uidl(PR_ENTRYID) of the message
 
   \param number the number of the message
   \param uidl the resulting uidl

   \return true on success, otherwise false

   \note 

   \sa
 */
static bool get_uidl(int number, char uidl[])
{
    enum MAPISTATUS retval;
    struct SPropTagArray *SPropTagArray = NULL;
    struct SPropValue *lpProps;
    struct SRow     aRow;
    mapi_object_t   obj_message;
    mapi_id_t       *fid;
    mapi_id_t       *mid;
    const struct SBinary_short *entryid;
    uint32_t        props_count;
    int             low;
    int             high;
    int             i;

    assert(number > 0 && number <= count);

    fid = (mapi_id_t *) find_SPropValue_data(&(rowset.aRow[position[number - 1]]), PR_FID);
    mid = (mapi_id_t *) find_SPropValue_data(&(rowset.aRow[position[number - 1]]), PR_MID);
    mapi_object_init(&obj_message);

    retval = OpenMessage(&obj_store, *fid, *mid, &obj_message, 0x0);
    if (retval == !MAPI_E_SUCCESS) {
	fprintf(stderr, "Error: OpenMessage failed\n");
	mapi_object_release(&obj_message);
	return false;
    }

    SPropTagArray = set_SPropTagArray(mem_ctx, 0x1, PR_ENTRYID);
    retval = GetProps(&obj_message, SPropTagArray, &lpProps, &props_count);
    MAPIFreeBuffer(SPropTagArray);
    if (retval == !MAPI_E_SUCCESS) {
	fprintf(stderr, "Error: GetProps failed\n");
	talloc_free(lpProps);
	mapi_object_release(&obj_message);
	return false;
    }

    /* Build a SRow structure */
    aRow.ulAdrEntryPad = 0;
    aRow.cValues = props_count;
    aRow.lpProps = lpProps;

    memset(uidl, 0, sizeof(uidl));
    entryid = (const struct SBinary_short *) find_SPropValue_data(&aRow, PR_ENTRYID);
    for (i = 0; i < entryid->cb && i < MAX_UIDL_LEN / 2 - 1; i++) {
	low = entryid->lpb[i] & 0xf;
	high = entryid->lpb[i] & 0xf0;
	high = high >> 4;
	sprintf(uidl + 2 * i, "%X%X", high, low);
    }
    talloc_free(lpProps);

    mapi_object_release(&obj_message);

    return true;
}


/**
   \details Check if the given message was marked as deleted
 
   \param number the number of the message

   \return true on success, otherwise false

   \note 

   \sa
 */
static bool is_deleted(int number)
{
    assert(number > 0 && number <= count);
    return (deleted[number - 1]);
}


/**
   \details Check if this profile is in the profile database
 
   \param

   \return true on success, otherwise false

   \note 

   \sa
 */
static bool is_valid_user()
{
    enum MAPISTATUS retval;
    struct SRowSet  proftable;
    int             profcount;
    const char     *name_in_proftable = NULL;
    const char     *default_profname = NULL;

    retval = MAPIInitialize(opt_profdb);
    if (retval != MAPI_E_SUCCESS) {
	MAPIUninitialize();
	return false;
    }

    memset(&proftable, 0, sizeof(struct SRowSet));
    retval = GetProfileTable(&proftable);
    if (retval != MAPI_E_SUCCESS) {
	MAPIUninitialize();
	return false;
    }

    for (profcount = 0; profcount != proftable.cRows; profcount++) {
	name_in_proftable = proftable.aRow[profcount].lpProps[0].value.lpszA;
	if (strcmp(name_in_proftable, profname) == 0) {
	    break;
	}
    }

    if (profcount == proftable.cRows) {
	retval = GetDefaultProfile(&default_profname);
	if (retval != MAPI_E_SUCCESS) {
	    MAPIUninitialize();
	    return false;
	}
	strncpy(profname, default_profname, MAX_ARG_LEN);
	fprintf(stderr, "Warning: no such user(profile) found, use the default one\n");
	successful_user_cmd = true;
    }

    MAPIUninitialize();

    return true;
}

/**
   \details response with the smtp address which is crosponding to the given display name

   \param name the display name of the user

   \return

   \note 

   \sa
 */
static void smtp_address(const char *name)
{
    assert(name != NULL);

    struct SPropTagArray *SPropTagArray;
    struct SRowSet *SRowSet;
    enum MAPISTATUS retval;
    const char     *display_name = NULL;
    uint32_t        i;
    uint32_t        count;
    uint8_t         ulFlags;

    SPropTagArray = set_SPropTagArray(mem_ctx, 0x02,
		                      PR_DISPLAY_NAME_UNICODE,
				      PR_SMTP_ADDRESS_UNICODE);
    count = 0x7;
    ulFlags = TABLE_START;
    do {
	count += 0x2;
	retval = GetGALTable(SPropTagArray, &SRowSet, count, ulFlags);
	if (retval != MAPI_E_SUCCESS) {
	    pop3_response("\n");
	    fprintf(stderr, "MAPI: Error when translate display name into smtp address\n");
	    MAPIFreeBuffer(SRowSet);
	    MAPIFreeBuffer(SPropTagArray);
	    return;
	}
	if (SRowSet->cRows) {
	    for (i = 0; i < SRowSet->cRows; i++) {
		display_name = (const char *) find_SPropValue_data(&SRowSet->aRow[i], PR_DISPLAY_NAME_UNICODE);
		if (strcmp(display_name, name) == 0) {
		    pop3_response(" <%s>\n", (const char *) find_SPropValue_data(&SRowSet->aRow[i], PR_SMTP_ADDRESS_UNICODE));
		    MAPIFreeBuffer(SRowSet);
		    MAPIFreeBuffer(SPropTagArray);
		    return;
		}
	    }
	}
	ulFlags = TABLE_CUR;
	MAPIFreeBuffer(SRowSet);
    } while (SRowSet->cRows == count);
    MAPIFreeBuffer(SPropTagArray);

    pop3_response("\n");
    return;
}

/**
   \details Fetch header lines of the given message
 
   \param number the number of the message

   \return true on success, otherwise false

   \note 

   \sa
 */
static bool fetch_headers(int number)
{
    enum MAPISTATUS retval;
    struct SPropTagArray *SPropTagArray = NULL;
    struct SPropValue *lpProps;
    struct SRow     aRow;
    mapi_object_t   obj_message;
    const char     *msgid;
    mapi_id_t      *fid;
    mapi_id_t      *mid;
    const uint64_t *delivery_date;
    const char     *date = NULL;
    const char     *from = NULL;
    const char     *to = NULL;
    const char     *cc = NULL;
    const char     *bcc = NULL;
    const char     *subject = NULL;
    const uint8_t  *has_attach = NULL;
    uint8_t         format;
    uint32_t        props_count;

    assert(number > 0 && number <= count);

    fid = (mapi_id_t *) find_SPropValue_data(&(rowset.aRow[number - 1]), PR_FID);
    mid = (mapi_id_t *) find_SPropValue_data(&(rowset.aRow[number - 1]), PR_MID);
    mapi_object_init(&obj_message);

    retval = OpenMessage(&obj_store, *fid, *mid, &obj_message, 0x0);
    if (retval == !MAPI_E_SUCCESS) {
	fprintf(stderr, "Error: OpenMessage failed\n");
	mapi_object_release(&obj_message);
	return false;
    }

    SPropTagArray = set_SPropTagArray(mem_ctx,
				      0x09,
				      PR_INTERNET_MESSAGE_ID,
				      PR_CONVERSATION_TOPIC,
				      PR_MESSAGE_DELIVERY_TIME,
				      PR_SENT_REPRESENTING_NAME,
				      PR_DISPLAY_TO,
				      PR_DISPLAY_CC,
				      PR_DISPLAY_BCC,
				      PR_HASATTACH,
				      PR_MSG_EDITOR_FORMAT);
    retval = GetProps(&obj_message, SPropTagArray, &lpProps, &props_count);
    MAPIFreeBuffer(SPropTagArray);
    if (retval == !MAPI_E_SUCCESS) {
	fprintf(stderr, "Error: GetProps failed\n");
	talloc_free(lpProps);
	mapi_object_release(&obj_message);
	return false;
    }
    /* Build a SRow structure */
    aRow.ulAdrEntryPad = 0;
    aRow.cValues = props_count;
    aRow.lpProps = lpProps;

    msgid = (const char *) find_SPropValue_data(&aRow, PR_INTERNET_MESSAGE_ID);
    if (!msgid) {
	fprintf(stderr, "Error: msgid is NULL\n");
	talloc_free(lpProps);
	mapi_object_release(&obj_message);
	return false;
    }


    has_attach = (const uint8_t *) octool_get_propval(&aRow, PR_HASATTACH);
    from = (const char *) octool_get_propval(&aRow, PR_SENT_REPRESENTING_NAME);
    to = (const char *) octool_get_propval(&aRow, PR_DISPLAY_TO);
    cc = (const char *) octool_get_propval(&aRow, PR_DISPLAY_CC);
    bcc = (const char *) octool_get_propval(&aRow, PR_DISPLAY_BCC);

    /* octool_get_propval() will not return NULL even if PR_DISPLAY_TO,
     * PR_DISPLAY_CC or PR_DISPLAY_TO is null, so have a test on their lengths.
     */
    if (strlen(to) * strlen(cc) * strlen(bcc)) {
	talloc_free(lpProps);
	mapi_object_release(&obj_message);
	return false;
    }

    delivery_date = (const uint64_t *) octool_get_propval(&aRow, PR_MESSAGE_DELIVERY_TIME);
    if (delivery_date) {
	date = nt_time_string(mem_ctx, *delivery_date);
    } else {
	date = "None";
    }
    subject = (const char *) octool_get_propval(&aRow, PR_CONVERSATION_TOPIC);


    pop3_response("Date: %s\n", date);

    pop3_response("From: %s", from);
    smtp_address(from);

    if (strlen(to)) {
	pop3_response("To: %s", to);
	smtp_address(to);
    }

    if (strlen(cc)) {
	pop3_response("Cc: %s", cc);
	smtp_address(cc);
    }

    if (strlen(bcc)) {
	pop3_response("Bcc: %s", bcc);
	smtp_address(bcc);
    }

    if (subject) {
	pop3_response("Subject: %s\n", subject);
    }

    pop3_response("Message-ID: %s\n", msgid);
    pop3_response("MIME-Version: 1.0\n");

    if (has_attach && *has_attach) {
	/* simple structure */
	pop3_response("Content-Type: multipart/mixed; boundary=\"%s\"\n", BOUNDARY);
    } else {
	/* complex structure */
	retval = GetBestBody(&obj_message, &format);
	switch (format) {
	case olEditorText:
	    pop3_response("Content-Type: text/plain; charset=us-ascii\n");
	    pop3_response("Content-Transfer-Encoding: quoted-printable\n");
	    break;
	case olEditorHTML:
	    pop3_response("Content-Type: text/html\n");
	    pop3_response("Content-Transfer-Encoding: quoted-printable\n");
	    break;
	case olEditorRTF:
	    pop3_response("Content-Type: text/rtf\n");
	    pop3_response("Content-Transfer-Encoding: quoted-printable\n");
	    break;
	}
    }
    pop3_response("\n");

    talloc_free(lpProps);
    mapi_object_release(&obj_message);

    return true;
}

static const char *get_filename(const char *filename)
{
    const char     *substr;

    if (!filename) {
	return NULL;
    }

    substr = rindex(filename, '/');
    if (substr) {
	return substr;
    }

    return filename;
}

/*
  encode as base64
  Samba4 code
  caller frees
*/ 
static char *ldb_base64_encode(void *mem_ctx, const char *buf, int len)
{
    const char     *b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    int             bit_offset;
    int             byte_offset;
    int             idx;
    int             i;
    const uint8_t  *d = (const uint8_t *) buf;
    int             bytes = (len * 8 + 5) / 6;
    int             pad_bytes = (bytes % 4) ? 4 - (bytes % 4) : 0;
    char           *out;

    out = talloc_array(mem_ctx, char, bytes + pad_bytes + 1);
    if (!out) {
	return NULL;
    }

    for (i = 0; i < bytes; i++) {
	byte_offset = (i * 6) / 8;
	bit_offset = (i * 6) % 8;
	if (bit_offset < 3) {
	    idx = (d[byte_offset] >> (2 - bit_offset)) & 0x3F;
	} else {
	    idx = (d[byte_offset] << (bit_offset - 2)) & 0x3F;
	    if (byte_offset + 1 < len) {
		idx |= (d[byte_offset + 1] >> (8 - (bit_offset - 2)));
	    }
	}
	out[i] = b64[idx];
    }

    for (; i < bytes + pad_bytes; i++)
	out[i] = '=';
    out[i] = 0;

    return out;
}


static char *get_base64_attachment(TALLOC_CTX * mem_ctx, mapi_object_t obj_attach, const uint32_t size, char **magic)
{
    enum MAPISTATUS retval;
    const char     *tmp;
    mapi_object_t   obj_stream;
    uint32_t        stream_size;
    uint32_t        read_size;
    unsigned char   buf[MAX_READ_SIZE];
    uint32_t        max_read_size = MAX_READ_SIZE;
    DATA_BLOB       data;
    magic_t         cookie = NULL;

    retval = OpenStream(&obj_attach, PR_ATTACH_DATA_BIN, 0, &obj_stream);
    if (retval != MAPI_E_SUCCESS) {
	return false;
    }

    retval = GetStreamSize(&obj_stream, &data.length);
    if (retval != MAPI_E_SUCCESS) {
	return false;
    }
    data.data = talloc_size(mem_ctx, data.length);

    read_size = size;
    for (stream_size = 0; stream_size < data.length && read_size != 0; stream_size += MAX_READ_SIZE) {
	retval = ReadStream(&obj_stream, buf, max_read_size, &read_size);
	if (retval != MAPI_E_SUCCESS) {
	    return NULL;
	}
	memcpy(data.data, buf, read_size);
    }

    cookie = magic_open(MAGIC_MIME);
    if (cookie == NULL) {
	printf("%s\n", magic_error(cookie));
	return NULL;
    }
    if (magic_load(cookie, NULL) == -1) {
	printf("%s\n", magic_error(cookie));
	return NULL;
    }
    tmp = magic_buffer(cookie, (void *) data.data, data.length);
    *magic = talloc_strdup(mem_ctx, tmp);
    magic_close(cookie);

    /* convert attachment to base64 */
    return (ldb_base64_encode(mem_ctx, (const char *) data.data, data.length));
}


/**
   \details Check if a character is safe to be represented as the ASCII character
 
   \param ch the character to be checked

   \return true on success, otherwise false

   \note 

   \sa
 */
static bool is_safe_char(char ch)
{
    /* For total robustness, it is better to quote every character except for the
     * 73-character set known to be invariant across all gateways, that is the
     * letters anddigits (A-Z, a-z and 0-9) and the following 11 characters:
     * ' ( ) + , - . / : = ?
     */
    return isalnum(ch) || ch == '\'' || ch == '(' || ch == ')' || ch == '+' || ch == ','
	|| ch == '-' || ch == '.' || ch == '/' || ch == ':' || ch == '=' || ch == '?';
}


/**
   \details Encode the body and response with the encoded data
   
   \param body the data to be encoded and responsed with

   \return

   \note 

   \sa
 */
static void quoted_printable_encode(const DATA_BLOB * body)
{
    int             line_count = 0;
    int             body_count = 0;
    char            hex[16] = "0123456789ABCDEF";
    char            ch;
    char            line[78];

    while (body_count < body->length) {
	ch = *(body->data + body_count);
	body_count++;

	if (is_safe_char(ch)){
	    line[line_count++] = ch;
	} else {
	    line[line_count++] = '=';
	    line[line_count++] = hex[(ch >> 4) & 15];
	    line[line_count++] = hex[ch & 15];
	}

	if (line_count >= 73 || ch == '\n') {
	    if (ch != '\n') {
		line[line_count++] = '=';
	    }
	    line[line_count++] = '\r';
	    line[line_count] = '\n';

	    pop3_response("%s", line);

	    line_count = 0;
	}
    }
    if (line_count != 0) {
	line[line_count++] = '\r';
	line[line_count] = '\n';
	pop3_response("%s", line);
    }
}


/**
   \details Fetch body data of the given message
   
   \param number the number of the message

   \return true on success, otherwise false

   \note 

   \sa
 */
static bool fetch_body(int number)
{
    enum MAPISTATUS retval;
    struct SPropTagArray *SPropTagArray = NULL;
    struct SPropValue *lpProps;
    struct SPropValue *attach_lpProps;
    struct SRow     aRow;
    struct SRow     aRow2;
    struct SRowSet  rowset_attach;
    mapi_object_t   obj_message;
    mapi_object_t   obj_tb_attach;
    mapi_object_t   obj_attach;
    const char     *msgid;
    mapi_id_t      *fid;
    mapi_id_t      *mid;
    const uint8_t  *has_attach = NULL;
    const uint32_t *attach_num = NULL;
    DATA_BLOB       body;
    const char     *attach_filename;
    const uint32_t *attach_size;
    char           *attachment_data;
    char           *magic;
    uint32_t        props_count;
    int             attach_count;
    uint8_t         format;
    
    assert(number > 0 && number <= count);

    fid = (mapi_id_t *) find_SPropValue_data(&(rowset.aRow[position[number - 1]]), PR_FID);
    mid = (mapi_id_t *) find_SPropValue_data(&(rowset.aRow[position[number - 1]]), PR_MID);
    mapi_object_init(&obj_message);

    retval = OpenMessage(&obj_store, *fid, *mid, &obj_message, 0x0);
    if (retval == !MAPI_E_SUCCESS) {
	fprintf(stderr, "Error: OpenMessage failed\n");
	mapi_object_release(&obj_message);
	return false;
    }
    SPropTagArray = set_SPropTagArray(mem_ctx,
				      0x07,
				      PR_INTERNET_MESSAGE_ID,
				      PR_MSG_EDITOR_FORMAT,
				      PR_BODY,
				      PR_BODY_UNICODE,
				      PR_HTML,
				      PR_RTF_COMPRESSED,
				      PR_HASATTACH);
    retval = GetProps(&obj_message, SPropTagArray, &lpProps, &props_count);
    MAPIFreeBuffer(SPropTagArray);
    if (retval == !MAPI_E_SUCCESS) {
	fprintf(stderr, "Error: GetProps failed\n");
	talloc_free(lpProps);
	mapi_object_release(&obj_message);
	return false;
    }

    /*  build a SRow structure */
    aRow.ulAdrEntryPad = 0;
    aRow.cValues = props_count;
    aRow.lpProps = lpProps;

    msgid = (const char *) find_SPropValue_data(&aRow, PR_INTERNET_MESSAGE_ID);
    if (!msgid) {
	fprintf(stderr, "Error: msgid is NULL\n");
	talloc_free(lpProps);
	mapi_object_release(&obj_message);
	return false;
    }
    has_attach = (const uint8_t *) find_SPropValue_data(&aRow, PR_HASATTACH);
    retval = octool_get_body(mem_ctx, &obj_message, &aRow, &body);

    /* body */
    if (body.length) {
	if (has_attach && *has_attach) {
	    pop3_response("--%s\n", BOUNDARY);

	    /* complex structure */
	    retval = GetBestBody(&obj_message, &format);
	    switch (format) {
	    case olEditorText:
		pop3_response("Content-Type: text/plain; charset=us-ascii\n");
		pop3_response("Content-Transfer-Encoding: quoted-printable\n");
		/* Just display UTF8 content inline */
		pop3_response("Content-Disposition: inline\n");
		break;
	    case olEditorHTML:
		pop3_response("Content-Type: text/html\n");
		pop3_response("Content-Transfer-Encoding: quoted-printable\n");
		break;
	    case olEditorRTF:
		pop3_response("Content-Type: text/rtf\n");
		pop3_response("Content-Transfer-Encoding: quoted-printable\n");
		pop3_response("--%s\n", BOUNDARY);
		break;
	    }
	}

	/* encode body.data into quoted printable */
	quoted_printable_encode(&body);
	talloc_free(body.data);

	/* fetch attachments */
	if (has_attach && *has_attach) {
	    mapi_object_init(&obj_tb_attach);
	    retval = GetAttachmentTable(&obj_message, &obj_tb_attach);
	    if (retval == MAPI_E_SUCCESS) {
		SPropTagArray = set_SPropTagArray(mem_ctx, 0x1, PR_ATTACH_NUM);
		retval = SetColumns(&obj_tb_attach, SPropTagArray);
		MAPIFreeBuffer(SPropTagArray);
		if (retval == !MAPI_E_SUCCESS) {
		    fprintf(stderr, "Error: GetProps failed\n");
		    talloc_free(lpProps);
		    mapi_object_release(&obj_message);
		    return false;
		}

		retval = QueryRows(&obj_tb_attach, 0xa, TBL_ADVANCE, &rowset_attach);
		if (retval == !MAPI_E_SUCCESS) {
		    fprintf(stderr, "Error: QueryRows failed\n");
		    talloc_free(lpProps);
		    mapi_object_release(&obj_message);
		    return false;
		}

		for (attach_count = 0; attach_count < rowset_attach.cRows; attach_count++) {
		    attach_num = (const uint32_t *) find_SPropValue_data(&(rowset_attach.aRow[attach_count]), PR_ATTACH_NUM);
		    retval = OpenAttach(&obj_message, *attach_num, &obj_attach);
		    if (retval == MAPI_E_SUCCESS) {
			SPropTagArray = set_SPropTagArray(mem_ctx, 0x3,
							  PR_ATTACH_FILENAME,
							  PR_ATTACH_LONG_FILENAME,
							  PR_ATTACH_SIZE);
			retval = GetProps(&obj_attach, SPropTagArray, &attach_lpProps, &props_count);
			MAPIFreeBuffer(SPropTagArray);
			if (retval == MAPI_E_SUCCESS) {
			    aRow2.ulAdrEntryPad = 0;
			    aRow2.cValues = props_count;
			    aRow2.lpProps = attach_lpProps;

			    attach_filename = get_filename(octool_get_propval(&aRow2, PR_ATTACH_LONG_FILENAME));
			    if (!attach_filename || (attach_filename && !strcmp(attach_filename, ""))) {
				attach_filename = get_filename(octool_get_propval(&aRow2, PR_ATTACH_FILENAME));
			    }
			    attach_size = (const uint32_t *) octool_get_propval(&aRow2, PR_ATTACH_SIZE);
			    attachment_data = get_base64_attachment(mem_ctx, obj_attach, *attach_size, &magic);
			    if (attachment_data) {
				pop3_response("\n\n--%s\n", BOUNDARY);
				pop3_response("Content-Disposition: attachment; filename=\"%s\"\n", attach_filename);
				pop3_response("Content-Type: \"%s\"\n", magic);
				pop3_response("Content-Transfer-Encoding: base64\n\n");
				pop3_response("%s", attachment_data);
			    }
			}
			talloc_free(attach_lpProps);
		    }
		}
		pop3_response("\n--%s--\n", BOUNDARY);
	    }			/* if GetAttachmentTable returns success */
	}			/* if (has_attach && *has_attach) */
    }

    talloc_free(lpProps);
    mapi_object_release(&obj_message);

    return true;
}


int main(int argc, const char *argv[])
{
    poptContext     pc;
    int             opt;
    int             opt_pop3port = -1;
    const char     *temp_opt_pop3io = NULL;
    int             pop3io_fd;

    enum { OPT_PROFILE_DB = 1000, OPT_PROFILE, OPT_PASSWORD, OPT_POP3IO, OPT_POP3PORT, OPT_ALL, OPT_TIMEOUT };

    struct poptOption long_options[] = {
	POPT_AUTOHELP
	{"database", 'f', POPT_ARG_STRING, NULL, OPT_PROFILE_DB, "set the profile database path"},
	{"pop3io", 'i', POPT_ARG_STRING, NULL, OPT_POP3IO, "set where the tool reads from and writes to, stdio(default) or socket"},
	{"pop3port", 'p', POPT_ARG_INT, NULL, OPT_POP3PORT, "set the port to use for the socket, when pop3io is set to socket"},
	{"all", 'a', POPT_ARG_NONE, NULL, OPT_ALL, "fetch all mails from exchange server (if not specified, only fetch the unread ones)"},
	{"timeout", 't', POPT_ARG_INT, NULL, OPT_TIMEOUT, "fet how long the tool should wait before time out (by default, 10 seconds)"},
	{NULL}
    };

    pc = poptGetContext("mapi2pop3", argc, argv, long_options, 0);

    while ((opt = poptGetNextOpt(pc)) != -1) {
	switch (opt) {
	case OPT_PROFILE_DB:
	    opt_profdb = poptGetOptArg(pc);
	    break;
	case OPT_POP3IO:
	    temp_opt_pop3io = poptGetOptArg(pc);
	    if (strncasecmp(temp_opt_pop3io, "Socket", 6) == 0) {
		opt_pop3io = POP3IO_SOCKET;
	    } else if (strncasecmp(temp_opt_pop3io, "Stdio", 5) == 0) {
		opt_pop3io = POP3IO_STDIO;
	    } else {
		opt_pop3io = POP3IO_UNDEFINED;
	    }
	    break;
	case OPT_POP3PORT:
	    opt_pop3port = *poptGetOptArg(pc);
	    break;
	case OPT_ALL:
	    opt_all = true;
	case OPT_TIMEOUT:
	    opt_timeout = *poptGetOptArg(pc);
	    break;
	default:
	    fprintf(stderr, "Error: Invalid option(s) detected\n");
	    break;
	}
    }

    /* Sanity checks */

    if (!opt_profdb) {
	opt_profdb = talloc_asprintf(mem_ctx, DEFAULT_PROFDB, getenv("HOME"));
    }

    if (opt_pop3io == POP3IO_UNDEFINED) {
	fprintf(stderr, "Warning: pop3io type undefined, use the default value: stdio\n");
	opt_pop3io = POP3IO_STDIO;
    }

    if (opt_pop3io == POP3IO_SOCKET) {
	if (opt_pop3port < 0) {
	    fprintf(stderr, "Error: pop3port number unspecified or less than 0, try the default port number: 65110\n");
	    opt_pop3port = 65110;
	} else if (opt_pop3port < 1024) {
	    fprintf(stderr, "Warning: port number < 1024 is reserved to processes having an effective UID == root\n");
	} else if (opt_pop3port > 65535) {
	    fprintf(stderr, "Error: pop3port number is out of range, try the default port number: 65110\n");
	    opt_pop3port = 65110;
	}
    }

    /* Initialize pop3io */
    pop3io_fd = init_pop3io(opt_pop3port);
    if (pop3io_fd < 0) {
	fprintf(stderr, "Error: failed to initialize pop3io\n");
	exit(1);
    }

    /* run the pop3 proxy server */
    pop3_server(pop3io_fd);


    close_pop3io(pop3io_fd);

    return 0;
}
