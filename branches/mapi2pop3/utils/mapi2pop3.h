/*
   Transalte MAPI into POP3

   OpenChange Project

   Copyright (C) Yangyan Li 2008

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef	__MAPI2POP3_H_
#define	__MAPI2POP3_H_

#ifndef __BEGIN_DECLS
#ifdef __cplusplus
#define __BEGIN_DECLS		extern "C" {
#define __END_DECLS		}
#else
#define __BEGIN_DECLS
#define __END_DECLS
#endif
#endif

#define POP3IO_UNDEFINED	0
#define POP3IO_STDIO		1
#define POP3IO_SOCKET		2

#define MAX_ARG_LEN		40
#define MAX_CMD_LINE_LEN	120	/* max length of the pop3 client command */
#define MAX_ARGC		3	/* max  of arguments of the pop3 command */
#define MAX_UIDL_LEN		512	/* max length of the uidl, in fact, the PR_ENTRYID */

#define BOUNDARY	"=_DocE+STaALJfprDB"
#define	MAX_READ_SIZE	0x4000


enum pop3_state
{
	AUTHORIZATION,
	TRANSACTION,
	UPDATE
};

struct cmd_table
{
	char name[10];
	enum pop3_state valid_state;
	void ( *handler ) ( int, char *[] );
};

__BEGIN_DECLS

/* POP3 server functions */
/* Commands valid in the AUTHORIZATION state */
static void user_cmd ( int , char *[] );
static void pass_cmd ( int , char *[] );
static void quit_cmd ( int , char *[] );
/* Optional */
//void apop_cmd ( int , char *[] );

/* Commands valid in the TRANSACTION state */
static void stat_cmd ( int , char *[] );
static void list_cmd ( int , char *[] );
static void retr_cmd ( int , char *[] );
static void dele_cmd ( int , char *[] );
static void noop_cmd ( int , char *[] );
static void rset_cmd ( int , char *[] );
/* Optional */
//static void top_cmd ( int , char *[] );
static void uidl_cmd ( int , char *[] );

/* Commands valid in the UPDATE state */
//static void quit_cmd ( int , char *[] );

static void pop3_server(int pop3io_fd);


/* POP3 server IO */
static bool pop3_response(const char *, ...);
static bool pop3_read_cmd_line(char []);
static int init_pop3io(int );
static bool init_pop3io_session(int );
static bool close_pop3io(int );
static bool close_pop3io_session( void );


/* MAPI Client */
static bool logon(const char *);
static bool mark_deleted(int );
static bool expunge_deleted(void);
static bool reset_deleted(void);
static int get_size(int );
static bool get_uidl(int , char []);
static bool is_deleted(int );
static bool is_valid_user(void);
static bool fetch_headers(int );
static bool fetch_body(int );

__END_DECLS

#endif /* __MAPI2POP3_H_ */
