#ifndef	__EMSMDB_DORPC_H__
#define	__EMSMDB_DORPC_H__

#include <ndr.h>
#include <ldb.h>
#include <ldb_errors.h>
#include <talloc.h>
#include <param.h>

#include "dcesrv_exchange_emsmdb.h"

#undef _PRINTF_ATTRIBUTE
#define _PRINTF_ATTRIBUTE(a1, a2) PRINTF_ATTRIBUTE(a1, a2)

#ifndef __BEGIN_DECLS
#ifdef __cplusplus
#define __BEGIN_DECLS		extern "C" {
#define __END_DECLS		}
#else
#define __BEGIN_DECLS
#define __END_DECLS
#endif
#endif


/*struct emsmdb_ctx {
	TALLOC_CTX			*mem_ctx;
	void				*store_ctx;
	uint32_t			timeStamp;
	uint32_t			nextServerObjectHandleId;
	struct loadparm_context *lp_ctx;
	struct ServerObjectHandle	*serverObjects;
};*/

struct ServerObjectHandle {
	uint32_t			handle;
	void				*serverObject;
	char				*name;
	char				*owner;
	char				*filename;
	struct ServerObjectHandle	*next;
};

struct LogonObject {
	uint8_t	logonId;
	char	*essdn;
	struct GUID mailboxUid;
};

struct FolderObject {
	char	*name;
};

struct TableObject {
	uint32_t	*columns;
	uint16_t	columnsCount;
};


NTSTATUS emsmdb_doRpc(TALLOC_CTX *mem_ctx, struct emsmdbp_context *emsmdb_ctx , struct mapi_request *request, struct mapi_response *response,uint32_t i);


#endif /* __EMSMDB_H__ */
