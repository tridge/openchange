#
# Address-Book-Container
# description: holding members of an address book view
#
dn: CN=Address-Book-Container,${SCHEMADN}
objectClass: top
objectClass: classSchema
cn: Address-Book-Container
distinguishedName: CN=Address-Book-Container,${SCHEMADN}
possSuperiors: msExchConfigurationContainer
possSuperiors: container
subClassOf: top
governsID: 1.2.840.113556.1.5.125
mayContain: msExchMinAdminVersion
mayContain: msExchAddressListOU
mayContain: msExchSearchScope
mayContain: msExchSearchBase
mayContain: msExchEnableInternalEvaluator
mayContain: msExchPurportedSearchUI
rDNAttID: cn
showInAdvancedViewOnly: TRUE
adminDisplayName: Address-Book-Container
adminDescription: Address-Book-Container
objectClassCategory: 1
lDAPDisplayName: addressBookContainer
name: Address-Book-Container
schemaIDGUID: 3e74f60f-3e73-11d1-a9c0-0000f80367c1
systemOnly: FALSE
systemPossSuperiors: addressBookContainer
systemPossSuperiors: configuration
systemMayContain: purportedSearch
systemMustContain: displayName
defaultSecurityDescriptor: D:(A;;RPWPCRCCDCLCLORCWOWDSDDTSW;;;DA)(A;;RPWPCRCCD
systemFlags: 16
defaultHidingValue: TRUE
objectCategory: CN=Class-Schema,${SCHEMADN}
defaultObjectCategory: CN=Address-Book-Container,${SCHEMADN}



#
# Contact
# description: contains information about a person or company that
# users may often contact
#
dn: CN=Contact,${SCHEMADN}
objectClass: top
objectClass: classSchema
cn: Contact
distinguishedName: CN=Contact,${SCHEMADN}
subClassOf: organizationalPerson
governsID: 1.2.840.113556.1.5.15
mayContain: msExchOriginatingForest
rDNAttID: cn
showInAdvancedViewOnly: TRUE
adminDisplayName: Contact
adminDescription: Contact
auxiliaryClass: msExchMultiMediaUser
auxiliaryClass: msExchCertificateInformation
auxiliaryClass: msExchBaseClass
auxiliaryClass: msExchCustomAttributes
objectClassCategory: 1
lDAPDisplayName: contact
name: Contact
schemaIDGUID: 5cb41ed0-0e4c-11d0-a286-00aa003049e2
systemOnly: FALSE
systemPossSuperiors: organizationalUnit
systemPossSuperiors: domainDNS
systemMayContain: notes
systemMustContain: cn
systemAuxiliaryClass: mailRecipient
defaultSecurityDescriptor: D:(A;;RPWPCRCCDCLCLORCWOWDSDDTSW;;;DA)(A;;RPWPCRCCD
systemFlags: 16
defaultHidingValue: FALSE
objectCategory: CN=Class-Schema,${SCHEMADN}
defaultObjectCategory: CN=Person,${SCHEMADN}



#
# DisplayTemplate
# description: specifies information for an address template.
#
dn: CN=Display-Template,${SCHEMADN}
objectClass: top
objectClass: classSchema
cn: Display-Template
distinguishedName: CN=Display-Template,${SCHEMADN}
subClassOf: top
governsID: 1.2.840.113556.1.3.59
rDNAttID: cn
showInAdvancedViewOnly: TRUE
adminDisplayName: Display-Template
adminDescription: Display-Template
auxiliaryClass: msExchBaseClass
objectClassCategory: 1
lDAPDisplayName: displayTemplate
name: Display-Template
schemaIDGUID: 5fd4250c-1262-11d0-a060-00aa006c33ed
systemOnly: FALSE
systemPossSuperiors: container
systemMayContain: originalDisplayTableMSDOS
systemMayContain: originalDisplayTable
systemMayContain: helpFileName
systemMayContain: helpData32
systemMayContain: helpData16
systemMayContain: addressEntryDisplayTableMSDOS
systemMayContain: addressEntryDisplayTable
systemMustContain: cn
defaultSecurityDescriptor: D:(A;;RPWPCRCCDCLCLORCWOWDSDDTSW;;;DA)(A;;RPWPCRCCD
defaultHidingValue: TRUE
objectCategory: CN=Class-Schema,${SCHEMADN}
defaultObjectCategory: CN=Display-Template,${SCHEMADN}



#
# DSA 
# description: Directory service only uses the subclass MSFT-DSA
#
dn: CN=DSA,${SCHEMADN}
objectClass: top
objectClass: classSchema
cn: DSA
distinguishedName: CN=DSA,${SCHEMADN}
subClassOf: applicationEntity
governsID: 2.5.6.13
mayContain: fileVersion
rDNAttID: cn
showInAdvancedViewOnly: TRUE
adminDisplayName: DSA
adminDescription: DSA
objectClassCategory: 1
lDAPDisplayName: dSA
name: DSA
schemaIDGUID: 3fdfee52-47f4-11d1-a9c3-0000f80367c1
systemOnly: FALSE
systemPossSuperiors: server
systemPossSuperiors: computer
systemMayContain: knowledgeInformation
defaultSecurityDescriptor: D:(A;;RPWPCRCCDCLCLORCWOWDSDDTSW;;;DA)(A;;RPWPCRCCD
systemFlags: 16
defaultHidingValue: TRUE
objectCategory: CN=Class-Schema,${SCHEMADN}
defaultObjectCategory: CN=DSA,${SCHEMADN}



#
# Group-Of-Names
# description: Used to define entries representing an unordered set of
# names that represent individual objects or other groups of names.
#
dn: CN=Group-Of-Names,${SCHEMADN}
objectClass: top
objectClass: classSchema
cn: Group-Of-Names
distinguishedName: CN=Group-Of-Names,${SCHEMADN}
subClassOf: top
governsID: 2.5.6.9
mayContain: reportToOwner
mayContain: reportToOriginator
mayContain: oOFReplyToOriginator
mayContain: hideDLMembership
mayContain: dLMemberRule
rDNAttID: cn
showInAdvancedViewOnly: TRUE
adminDisplayName: Group-Of-Names
adminDescription: Group-Of-Names
objectClassCategory: 0
lDAPDisplayName: groupOfNames
name: Group-Of-Names
schemaIDGUID: bf967a9d-0de6-11d0-a285-00aa003049e2
systemOnly: FALSE
systemPossSuperiors: organizationalUnit
systemPossSuperiors: locality
systemPossSuperiors: organization
systemPossSuperiors: container
systemMayContain: seeAlso
systemMayContain: owner
systemMayContain: ou
systemMayContain: o
systemMayContain: businessCategory
systemMustContain: member
systemMustContain: cn
defaultSecurityDescriptor: D:(A;;RPWPCRCCDCLCLORCWOWDSDDTSW;;;DA)(A;;RPWPCRCCD
systemFlags: 16
defaultHidingValue: TRUE
objectCategory: CN=Class-Schema,${SCHEMADN}
defaultObjectCategory: CN=Group-Of-Names,${SCHEMADN}



#
# PurportedSearch
# description: This attribute specifies the search argument for an
# address book view.
#
dn: CN=Purported-Search,${SCHEMADN}
objectClass: top
objectClass: attributeSchema
cn: Purported-Search
distinguishedName: CN=Purported-Search,${SCHEMADN}
attributeID: 1.2.840.113556.1.4.886
attributeSyntax: 2.5.5.12
isSingleValued: TRUE
rangeLower: 0
rangeUpper: 2048
showInAdvancedViewOnly: TRUE
adminDisplayName: Purported-Search
adminDescription: Purported-Search
oMSyntax: 64
searchFlags: 8
lDAPDisplayName: purportedSearch
name: Purported-Search
schemaIDGUID: b4b54e50-943a-11d1-aebd-0000f80367c1
systemOnly: FALSE
systemFlags: 16
objectCategory: CN=Attribute-Schema,${SCHEMADN}



#
# Owner
# description: The distinguished name of an object that has ownership
# of an object.
#
dn: CN=Owner,${SCHEMADN}
objectClass: top
objectClass: attributeSchema
cn: Owner
distinguishedName: CN=Owner,${SCHEMADN}
attributeID: 2.5.4.32
attributeSyntax: 2.5.5.1
isSingleValued: TRUE
linkID: 44
showInAdvancedViewOnly: TRUE
adminDisplayName: Owner
oMObjectClass:: KwwCh3McAIVK
adminDescription: Owner
oMSyntax: 127
searchFlags: 0
lDAPDisplayName: owner
name: Owner
schemaIDGUID: bf9679f3-0de6-11d0-a285-00aa003049e2
systemOnly: FALSE
systemFlags: 16
isMemberOfPartialAttributeSet: TRUE
objectCategory: CN=Attribute-Schema,${SCHEMADN}
