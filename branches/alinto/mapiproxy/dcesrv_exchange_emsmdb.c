/*
   EMSMDB Server

   OpenChange Project

   Copyright (C) Julien Kerihuel 2008

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <mapiproxy/dcesrv_mapiproxy.h>
#include <mapiproxy/dcesrv_mapiproxy_proto.h>
#include <mapiproxy/providers/emsmdb/emsmdb.h>

#include <util/debug.h>

#include "dcesrv_exchange.h"


/* 
  EcDoConnect 
*/
enum MAPISTATUS dcesrv_EcDoConnect(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct EcDoConnect *r)
{
	struct dcesrv_handle * handle;
	struct emsmdb_ctx * emsmdb_context;
	char * user = "";
	
	DEBUG(3,("Dans dcesrv_EcDoConnect\n"));
	
	DEBUG(4,("__________\n"));
	DEBUG(4,("szUserDN : %s\n",r->in.szUserDN));
	DEBUG(4,("ulFlags : %.8x\n",r->in.ulFlags));
	DEBUG(4,("ulConMod : %.8x\n",r->in.ulConMod));
	DEBUG(4,("cbLimit : %.8x\n",r->in.cbLimit));
	DEBUG(4,("ulCpid : %.8x\n",r->in.ulCpid));
	DEBUG(4,("ulLcidString : %.8x\n",r->in.ulLcidString));
	DEBUG(4,("ulLcidSort : %.8x\n",r->in.ulLcidSort));
	DEBUG(4,("ulIcxrLink : %.8x\n",r->in.ulIcxrLink));
	DEBUG(4,("usFCanConvertCodePages : %.4x\n",r->in.usFCanConvertCodePages));
	DEBUG(4,("rgwClientVersion : %.4x %.4x %.4x\n",r->in.rgwClientVersion[0],r->in.rgwClientVersion[1],r->in.rgwClientVersion[2]));
	DEBUG(4,("pullTimeStamp : %.8x\n",*(r->in.pullTimeStamp)));
	DEBUG(4,("_____\n"));
	
	handle = dcesrv_handle_new(dce_call->context, EXCHANGE_HANDLE_EMSMDB);
	if (!handle) {
		/* replaces NT_STATUS_NO_MEMORY */
		return MAPI_E_NOT_ENOUGH_RESOURCES;
	}
	
	/* Connexion a EMSMDB */
	emsmdb_context = emsmdb_init(dce_call);
	if (!emsmdb_context) {
		return MAPI_E_FAILONEPROVIDER;
	}
	handle->data = (void *) emsmdb_context;
	
	*(r->out.handle) = handle->wire_handle;
	
	r->out.pcmsPollsMax = talloc(mem_ctx,uint32_t);
	*(r->out.pcmsPollsMax) = 0x0000ea60; /* 60 secondes */
	r->out.pcRetry = talloc(mem_ctx,uint32_t);
	*(r->out.pcRetry) = 0x0000003c; /* 60 essais */
	r->out.pcmsRetryDelay = talloc(mem_ctx,uint32_t);
	*(r->out.pcmsRetryDelay) = 0x000003e8; /* 1 seconde */
	
	r->out.picxr = talloc(mem_ctx,uint32_t);
	*(r->out.picxr) = 0x0001;
	
	user = &(strrchr(r->in.szUserDN,'=')[1]);
	
	r->out.szDNPrefix = talloc_strndup(mem_ctx, r->in.szUserDN, strlen(r->in.szUserDN) - strlen(user));
	/*r->out.szDNPrefix = talloc_strdup(mem_ctx, "/O=FIRST ORGANIZATION/OU=FIRST ADMINISTRATIVE GROUP/CN=RECIPIENTS/CN=");*/
	r->out.szDisplayName = talloc_strdup(mem_ctx, user);
	
	r->out.rgwServerVersion[0] = 0x0005;
	r->out.rgwServerVersion[1] = 0x1b20;
	r->out.rgwServerVersion[2] = 0x0003;
	/*r->out.rgwServerVersion[0] = 0x0801;
	r->out.rgwServerVersion[1] = 0x80f0;
	r->out.rgwServerVersion[2] = 0x0005;*/
	
	r->out.rgwClientVersion[0] = r->in.rgwClientVersion[0];
	r->out.rgwClientVersion[1] = r->in.rgwClientVersion[1];
	r->out.rgwClientVersion[2] = r->in.rgwClientVersion[2];
	/*r->out.rgwClientVersion[0] = 0x000a;
	r->out.rgwClientVersion[1] = 0x0000;
	r->out.rgwClientVersion[2] = 0x1013;*/
	
	r->out.pullTimeStamp = r->in.pullTimeStamp;
	/**(r->out.pullTimeStamp) = 0xd23230ee;*/
	*(r->out.pullTimeStamp) = 0xf6cfd1dd;
	
	r->out.result = MAPI_E_SUCCESS;
	
	DEBUG(4,("pcmsPollsMax : %.8x\n",*(r->out.pcmsPollsMax)));
	DEBUG(4,("pcRetry : %.8x\n",*(r->out.pcRetry)));
	DEBUG(4,("pcmsRetryDelay : %.8x\n",*(r->out.pcmsRetryDelay)));
	DEBUG(4,("picxr : %.4x\n",*(r->out.picxr)));
	DEBUG(4,("szDNPrefix : %s\n",r->out.szDNPrefix));
	DEBUG(4,("szDisplayName : %s\n",r->out.szDisplayName));
	DEBUG(4,("rgwServerVersion : %.4x %.4x %.4x\n",r->out.rgwServerVersion[0],r->out.rgwServerVersion[1],r->out.rgwServerVersion[2]));
	DEBUG(4,("rgwClientVersion : %.4x %.4x %.4x\n",r->out.rgwClientVersion[0],r->out.rgwClientVersion[1],r->out.rgwClientVersion[2]));
	DEBUG(4,("pullTimeStamp : %.8x\n",*(r->out.pullTimeStamp)));
	DEBUG(4,("result : %x\n",r->out.result));
	DEBUG(4,("__________\n"));
	
	return MAPI_E_SUCCESS;
	
	/*DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);*/
}


/* 
  EcDoDisconnect 
*/
enum MAPISTATUS dcesrv_EcDoDisconnect(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct EcDoDisconnect *r)
{
	struct dcesrv_handle	*h;
	struct emsmdb_ctx	*emsmdb_context;
	
	DEBUG(3,("Dans dcesrv_EcDoDisconnect\n"));
	
	if (!NTLM_AUTH_IS_OK(dce_call)) {
		DEBUG(1, ("No challenge requested by client, cannot authenticate\n"));
		return MAPI_E_LOGON_FAILED;
	}

	h = dcesrv_handle_fetch(dce_call->context, r->in.handle, DCESRV_HANDLE_ANY);
	if (h) {
		emsmdb_context = (struct emsmdb_ctx *) h->data;

		if (emsmdb_context)
			talloc_free(emsmdb_context->mem_ctx);
	}
	return MAPI_E_SUCCESS;
	/*DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);*/
}


/* 
  EcDoRpc 
*/
enum MAPISTATUS dcesrv_EcDoRpc(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct EcDoRpc *r)
{
	struct dcesrv_handle *h;
	struct emsmdb_ctx *emsmdb_context;
	//int i;
	struct mapi_response *mapi_response;
	
	DEBUG(3,("\n***** Dans dcesrv_EcDoRpc *****\n"));
	/*DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);*/
	
	DEBUG(4,("Entree : \n"));
	NDR_PRINT_DEBUG(uint32,&r->in.size);
	NDR_PRINT_DEBUG(uint32,&r->in.offset);
	
	NDR_PRINT_DEBUG(mapi_request,r->in.mapi_request);
	
	/* Preparation de la reponse */
	h = dcesrv_handle_fetch(dce_call->context, r->in.handle, DCESRV_HANDLE_ANY);
	if ( h == NULL )
	{
		return MAPI_E_END_OF_SESSION;
	}
	emsmdb_context = (struct emsmdb_ctx *) h->data;
	
	r->out.handle = r->in.handle;
	r->out.size = r->in.size;
	r->out.offset = r->in.offset;
	
	mapi_response = talloc(mem_ctx, struct mapi_response);
	emsmdb_doRpc(mem_ctx, emsmdb_context , r->in.mapi_request, mapi_response);
	
	r->out.mapi_response = mapi_response;
	r->out.length = r->in.length;
	*(r->out.length) = mapi_response->mapi_len;
	
	DEBUG(4,("_____\nSortie : \n"));
	NDR_PRINT_DEBUG(uint32,&r->out.size);
	NDR_PRINT_DEBUG(uint32,&r->out.offset);
	
	NDR_PRINT_DEBUG(mapi_response,r->out.mapi_response);
	DEBUG(4,("***** Fin EcDoRpc *****\n"));
	
	return MAPI_E_SUCCESS;
	/*DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);*/
}


/* 
  EcGetMoreRpc 
*/
enum MAPISTATUS dcesrv_EcGetMoreRpc(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct EcGetMoreRpc *r)
{
	DEBUG(3,("Unimplemented function dcesrv_EcGetMoreRpc\n"));
	// TODO 
	//DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
	return MAPI_E_NO_SUPPORT;
}


/* 
  EcRRegisterPushNotification 
*/
enum MAPISTATUS dcesrv_EcRRegisterPushNotification(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct EcRRegisterPushNotification *r)
{
	DEBUG(3,("In dcesrv_EcRRegisterPushNotification\n"));
	
	r->out.handle = r->in.handle;
	r->out.hNotification = talloc(mem_ctx, uint32_t);
	*(r->out.hNotification) = 0x00000000;
	
	return MAPI_E_NO_SUPPORT;
	
	/*DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);*/
}


/* 
  EcRUnregisterPushNotification 
*/
enum MAPISTATUS dcesrv_EcRUnregisterPushNotification(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct EcRUnregisterPushNotification *r)
{
	DEBUG(3,("Unimplemented function dcesrv_EcRUnregisterPushNotification\n"));
	// TODO
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}


/* 
  EcDummyRpc 
*/
enum MAPISTATUS dcesrv_EcDummyRpc(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct EcDummyRpc *r)
{
	DEBUG(3,("Unimplemented function dcesrv_EcDummyRpc\n"));
	// TODO
	//DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}


/* 
  EcRGetDCName 
*/
enum MAPISTATUS dcesrv_EcRGetDCName(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct EcRGetDCName *r)
{
	DEBUG(3,("Unimplemented function dcesrv_EcRGetDCName\n"));
	// TODO
	//DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}


/* 
  EcRNetGetDCName 
*/
enum MAPISTATUS dcesrv_EcRNetGetDCName(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct EcRNetGetDCName *r)
{
	DEBUG(3,("Unimplemented function dcesrv_EcRNetGetDCName\n"));
	// TODO
	//DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}


/* 
  EcDoRpcExt 
*/
enum MAPISTATUS dcesrv_EcDoRpcExt(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct EcDoRpcExt *r)
{
	DEBUG(3,("Unimplemented function dcesrv_EcDoRpcExt\n"));
	// TODO
	//DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/*
  EcDoConnectEx
 */
enum MAPISTATUS dcesrv_EcDoConnectEx(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
				     struct EcDoConnectEx *r)
{
	DEBUG(3,("Unimplemented function dcesrv_EcDoConnectEx\n"));
	// TODO
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/**
   \details Dispatch incoming EMSMDB request to the correct function

   EMSMDB provider functions are called within the switch

   \return NTSTATUS_OK
 */
NTSTATUS dcesrv_exchange_emsmdb_dispatch(struct dcesrv_call_state *dce_call, 
					 TALLOC_CTX *mem_ctx,
					 struct mapiproxy *mapiproxy,
					 void *r)
{
	enum MAPISTATUS				retval = MAPI_E_CALL_FAILED;
	const struct ndr_interface_table	*table;
	uint16_t				opnum;

	table = (const struct ndr_interface_table *) dce_call->context->iface->private;
	opnum = dce_call->pkt.u.request.opnum;

	/* Sanity checks */
	if (!table) return NT_STATUS_OK;
	if (table->name && strcmp(table->name, NDR_EXCHANGE_EMSMDB_NAME)) return NT_STATUS_OK;

	switch (opnum) {
	case NDR_ECDOCONNECT:
		retval = dcesrv_EcDoConnect(dce_call, mem_ctx, (struct EcDoConnect *)r);
		break;
	case NDR_ECDODISCONNECT:
		retval = dcesrv_EcDoDisconnect(dce_call, mem_ctx, (struct EcDoDisconnect *)r);
		break;
	case NDR_ECDORPC:
		retval = dcesrv_EcDoRpc(dce_call, mem_ctx, (struct EcDoRpc *)r);
		break;
	case NDR_ECRREGISTERPUSHNOTIFICATION:
		retval = dcesrv_EcRRegisterPushNotification(dce_call, mem_ctx, (struct EcRRegisterPushNotification *)r);
		break;
	case NDR_ECRUNREGISTERPUSHNOTIFICATION:
		retval = dcesrv_EcRUnregisterPushNotification(dce_call, mem_ctx, (struct EcRUnregisterPushNotification *)r);
		break;
	case NDR_ECDUMMYRPC:
		retval = dcesrv_EcDummyRpc(dce_call, mem_ctx, (struct EcDummyRpc *)r);
		break;
	case NDR_ECRGETDCNAME:
		retval = dcesrv_EcRGetDCName(dce_call, mem_ctx, (struct EcRGetDCName *)r);
		break;
	case NDR_ECRNETGETDCNAME:
		retval = dcesrv_EcRNetGetDCName(dce_call, mem_ctx, (struct EcRNetGetDCName *)r);
		break;
	case NDR_ECDORPCEXT:
		retval = dcesrv_EcDoRpcExt(dce_call, mem_ctx, (struct EcDoRpcExt *)r);
		break;
	case NDR_ECDOCONNECTEX:
		retval = dcesrv_EcDoConnectEx(dce_call, mem_ctx, (struct EcDoConnectEx *)r);
		break;
	}

	/* don't relay the traffic to remove server */
	mapiproxy->norelay = true;

	return NT_STATUS_OK;
}
