/* 
   dcesrv_exchange_nsp.h

   Copyright (C) Julien Kerihuel 2005
   Copyright (C) Arthur Vuillard 2008
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _DCESRV_EXCHANGE_H_
#define _DCESRV_EXCHANGE_H_

struct auth_serversupplied_info 
{
	struct dom_sid *account_sid;
	struct dom_sid *primary_group_sid;

	size_t n_domain_groups;
	struct dom_sid **domain_groups;

	DATA_BLOB user_session_key;
	DATA_BLOB lm_session_key;

	const char *account_name;
	const char *domain_name;

	const char *full_name;
	const char *logon_script;
	const char *profile_path;
	const char *home_directory;
	const char *home_drive;
	const char *logon_server;
	
	NTTIME last_logon;
	NTTIME last_logoff;
	NTTIME acct_expiry;
	NTTIME last_password_change;
	NTTIME allow_password_change;
	NTTIME force_password_change;

	uint16_t logon_count;
	uint16_t bad_password_count;

	uint32_t acct_flags;

	bool authenticated;
};

#define	NTLM_AUTH_IS_OK(dce_call) \
(dce_call->conn->auth_state.session_info->server_info->authenticated == true)

#define	NTLM_AUTH_USERNAME(dce_call) \
(dce_call->conn->auth_state.session_info->credentials->username)

#define	NTLM_AUTH_SESSION_INFO(dce_call) \
(dce_call->conn->auth_state.session_info)

#define	NTLM_AUTH_IS_OK_RETURN(dce_call) do { \
	if (!NTLM_AUTH_IS_OK(dce_call)) {\
		return true;\
	}\
} while (0)

#define LAST_INT_FROM_BINARY(bin, size, pi) \
        if ((bin) != NULL) { \
                uint32_t        counter = 1; \
\
                *(pi) = 0; \
                do { \
                        *(pi) |= (bin)[(size) - counter]; \
                        *(pi) <<= (8 * (sizeof(uint32_t) - counter)); \
                } while (++counter < sizeof(uint32_t)); \
        }

#define FIRST_INT_FROM_BINARY(bin, pi) \
        if ((bin) != NULL) { \
                uint32_t        counter = 1; \
\
                *(pi) = 0; \
                do { \
                        *(pi) |= (bin)[sizeof(uint32_t) - counter]; \
                        *(pi) <<= (8 * (sizeof(uint32_t) - counter)); \
                } while (++counter < sizeof(uint32_t)); \
        }


#endif /* !_DCESRV_EXCHANGE_H_ */
