/*
   OpenChange Server Provider implementation.

   EMSMDB: Exchange Transport Provider implementation

   Copyright (C) Julien Kerihuel 2008.
   Copyright (C) Mickael Jeannot 2008.
   Copyright (C) Arthur Vuillard 2008.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef	__EMSMDB_H__
#define	__EMSMDB_H__

#include <ndr.h>
#include <ldb.h>
#include <ldb_errors.h>
#include <talloc.h>
#include <param.h>

#undef _PRINTF_ATTRIBUTE
#define _PRINTF_ATTRIBUTE(a1, a2) PRINTF_ATTRIBUTE(a1, a2)

#ifndef __BEGIN_DECLS
#ifdef __cplusplus
#define __BEGIN_DECLS		extern "C" {
#define __END_DECLS		}
#else
#define __BEGIN_DECLS
#define __END_DECLS
#endif
#endif


struct emsmdb_ctx {
	TALLOC_CTX			*mem_ctx;
	void				*store_ctx;
	uint32_t			timeStamp;
	uint32_t			nextServerObjectHandleId;
	struct loadparm_context *lp_ctx;
	struct ServerObjectHandle	*serverObjects;
};

struct ServerObjectHandle {
	uint32_t			handle;
	void				*serverObject;
	char				*name;
	char				*owner;
	char				*filename;
	struct ServerObjectHandle	*next;
};

struct LogonObject {
	uint8_t	logonId;
	char	*essdn;
};

struct FolderObject {
	char	*name;
};

struct TableObject {
	uint32_t	*columns;
	uint16_t	columnsCount;
};

#define	EMSMDB_CTX	"emsmdb context"

struct emsmdb_ctx *emsmdb_init(struct dcesrv_call_state *dce_call);
NTSTATUS emsmdb_doRpc(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct mapi_request *request, struct mapi_response *response);


#endif /* __EMSMDB_H__ */
