/*
   OpenChange Server Provider implementation.

   EMSMDB: Exchange Transport Provider implementation

   Copyright (C) Julien Kerihuel 2008.
   Copyright (C) Mickael Jeannot 2008.
   Copyright (C) Arthur Vuillard 2008.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <mapiproxy/dcesrv_mapiproxy.h>
#include <mapiproxy/providers/emsmdb/emsmdb.h>

#include <util/debug.h>

#include <time.h>
#include <libmapi/libmapi.h>

NTSTATUS getProps ( TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx, uint32_t *props, uint16_t propCount, char *name, char *owner, uint8_t **replData, uint32_t *replSize );
uint32_t getHandleIdx (TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx, void *object, char *name, char *owner);
void freeHandleIdx (TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx, uint32_t index);
struct ServerObjectHandle * getServerObjectHandle ( TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx, uint32_t handleIdx );
NTSTATUS emsmdb_doRpc_Release(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_OpenFolder(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_GetHierarchyTable(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_GetContentsTable(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_CreateMessage(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_GetProps(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_SetProps(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_SaveChangesMessage(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_SetColumns(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_SortTable(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_Restrict(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_QueryRows(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_SeekRow(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_CreateFolder(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_GetReceiveFolder(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_RegisterNotification(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_GetRulesTable(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_LongTermIdFromId(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_FindRow(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_GetIDsFromNames(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_GetLocalReplicaIds(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);
NTSTATUS emsmdb_doRpc_Logon(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size);


/* Retourne les properties de l'objet passe en parametre
 * props : liste des properties a recuperer
 * propCount : nb de props
 * name : nom de l'objet
 * repl : valeurs retournees */
NTSTATUS getProps ( TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx, uint32_t *props, uint16_t propCount, char *name, char *owner, uint8_t **replData, uint32_t *replSize )
{
	int i;
	int ret;
	int size;
	uint8_t *data;
	struct ldb_result *result = NULL;
	const char * const attrs[] = { "*", NULL };
	//char *filter;
	
	DEBUG(4,("\tdans getProps\n"));
	
	size = *replSize;
	data = *replData;
	
	//filter = talloc_asprintf(mem_ctx, "(&(name=%s)(owner=%s))",name,owner);
	//DEBUG(4,("\t\tfilter : %s\n",filter));
//	ret = ldb_search(emsmdb_ctx->store_ctx, NULL, LDB_SCOPE_SUBTREE, filter, attrs,	&result );		
	ret = ldb_search(emsmdb_ctx->store_ctx, mem_ctx, &result, ldb_dn_new(mem_ctx, emsmdb_ctx->store_ctx, ""), LDB_SCOPE_SUBTREE, attrs,  "(&(name=%s)(owner=%s))",name,owner) ;
	if ( ret != LDB_SUCCESS || result->count != 1 )
	{
		if ( ret != LDB_SUCCESS )
		{
			DEBUG(0,("getProps: ERROR: object not found\n"));
		}
		else
		{
			DEBUG(4,("getProps: ERROR: found %d objects\n",result->count));
		}
		return NT_STATUS_OBJECT_NAME_NOT_FOUND;
	}
	
	for ( i = 0 ; i < propCount ; i++ )
	{
		char * prop = talloc_asprintf(mem_ctx,"x%.8x",props[i]);
		const struct ldb_val *val;
		
		val = ldb_msg_find_ldb_val(result->msgs[0],prop);
		if ( val != NULL )
		{
			int j;
			int k;
			uint32_t unicodeValue;
			DEBUG(4,("property %.8x (size %d) : ",props[i],val->length));
			switch ( props[i] & 0xffff )
			{
				case PT_BINARY :
					data = talloc_realloc(mem_ctx,data,uint8_t,size + val->length + 4);
					data[size++] = 0x00;
					data[size++] = val->length & 0xff;
					data[size++] = val->length >> 8;
					for ( j = 0 ; j < val->length ; j++ )
					{
						DEBUG(4,("%x ",val->data[j]));
						data[size++] = val->data[j];
					}
					DEBUG(4,("\tsize=%d\n",size));
					break;
				case PT_UNICODE: 
					data = talloc_realloc(mem_ctx,data,uint8_t,size + (val->length * 2) + 4);
					data[size++] = 0x00;
					for ( j = 0 ; j < val->length ; j++ )
					{
						if ( (val->data[j] & 0x80) == 0x00 )
						{
							k = 0;
							unicodeValue = val->data[j];
						}
						else if ( (val->data[j] & 0xe0) == 0xc0 )
						{
							k = 1;
							unicodeValue = val->data[j] & 0x1f;
						}
						else if ( (val->data[j] & 0xf0) == 0xe0 )
						{
							k = 2;
							unicodeValue = val->data[j] & 0x0f;
						}
						else
						{
							/* en theorie inutile */
							DEBUG(5,("ATTENTION: emsmdb: getProp, passage dans le else\n"));
							k = 0;
							unicodeValue = val->data[j];
						}
						while ( k > 0 )
						{
							unicodeValue = unicodeValue << 6;
							unicodeValue += val->data[++j] & 0x3f;
							k--;
						}
						DEBUG(4,("%x ",unicodeValue));
						data[size++] = unicodeValue & 0x00ff;
						data[size++] = unicodeValue >> 8;
					}
					data[size++] = 0x00;
					data[size++] = 0x00;
					DEBUG(4,("\tsize=%d\n",size));
					break;
				default :
					data = talloc_realloc(mem_ctx,data,uint8_t,size + val->length + 2);
					data[size++] = 0x00;
					for ( j = 0 ; j < val->length ; j++ )
					{
						DEBUG(4,("%x ",val->data[j]));
						data[size++] = val->data[j];
					}
					DEBUG(4,("\tsize=%d\n",size));
					break;
			}
		}
		else
		{
			DEBUG(4,("property non trouvee, data_size=%d\n",size));
			data = talloc_realloc(mem_ctx,data,uint8_t,size + 7);
			data[size++] = 0x0a;
			data[size++] = 0x0f;
			data[size++] = 0x01;
			data[size++] = 0x04;
			data[size++] = 0x80;
			DEBUG(4,("\tsize=%d\n",size));
		}
	}
	
	*replSize = size;
	*replData = data;
	
	return NT_STATUS_OK;
}


uint32_t getHandleIdx (TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx, void *object, char *name, char *owner)
{
	struct ServerObjectHandle	*serverObj;
	char valeurIncorrecte = 1;
	
	DEBUG(4,("dans getHandleIdx\n"));
	while ( valeurIncorrecte )
	{
		emsmdb_ctx->nextServerObjectHandleId++;
		if ( emsmdb_ctx->nextServerObjectHandleId == 0xffffffff )
		{
			emsmdb_ctx->nextServerObjectHandleId = 1;
		}
		valeurIncorrecte = 0;
		/* On verifie si l'index est deja utilise */
		DEBUG(4,("\t"));
		for ( serverObj = emsmdb_ctx->serverObjects ; serverObj != NULL && ! valeurIncorrecte ; serverObj = serverObj->next )
		{
			DEBUG(4,("%x ",serverObj->handle));
			valeurIncorrecte = ( serverObj->handle == emsmdb_ctx->nextServerObjectHandleId );
		}
		DEBUG(4,("\n"));
	}
	
	/* On cree l'objet */
	serverObj = talloc(emsmdb_ctx->mem_ctx,struct ServerObjectHandle);
	serverObj->handle = emsmdb_ctx->nextServerObjectHandleId;
	serverObj->serverObject = object;
	serverObj->name = talloc_strdup(serverObj,name);
	serverObj->owner = talloc_strdup(serverObj,owner);
	serverObj->filename = NULL;
	serverObj->next = emsmdb_ctx->serverObjects;
	emsmdb_ctx->serverObjects = serverObj;
	
	DEBUG(4,("fin getHandleIdx, retour : %x\n",emsmdb_ctx->nextServerObjectHandleId));
	return emsmdb_ctx->nextServerObjectHandleId;
}

void freeHandleIdx (TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx, uint32_t index)
{
	DEBUG(4,("dans freeHandleIdx\n"));
	DEBUG(4,("index=%x\n",index));
	if ( emsmdb_ctx->serverObjects != NULL )
	{
		struct ServerObjectHandle	*serverObj;
		struct ServerObjectHandle	*serverObj2;
		
		/* On cherche l'objet correspondant a l'index */
		DEBUG(4,("\t%x ",emsmdb_ctx->serverObjects->handle));
		if ( emsmdb_ctx->serverObjects->handle == index )
		{
			DEBUG(4,("\tindex trouve\n"));
			serverObj = emsmdb_ctx->serverObjects;
			emsmdb_ctx->serverObjects = serverObj->next;
			serverObj->next = NULL;
			talloc_free(serverObj);
		}
		else
		{
			for ( serverObj = emsmdb_ctx->serverObjects ; serverObj->next != NULL && ( serverObj->next->handle != index ) ; serverObj = serverObj->next )
			{
				DEBUG(4,("%x ",serverObj->next->handle));
			}
			DEBUG(4,("\n"));
		
			if ( serverObj->next )
			{
				DEBUG(4,("\tindex trouve\n"));
				/* On supprime l'objet */
				serverObj2 = serverObj->next;
				serverObj->next = serverObj2->next;
				serverObj2->next = NULL;
				talloc_free(serverObj2);
			}
		}
	}
	DEBUG(4,("fin freeHandleIdx\n"));
}

struct ServerObjectHandle * getServerObjectHandle ( TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx, uint32_t handleIdx )
{
	struct ServerObjectHandle *s;
	
	DEBUG(4,("dans getServerObjectHandle: "));
	for ( s = emsmdb_ctx->serverObjects ; s != NULL && s->handle != handleIdx ; s = s->next )
	{
		DEBUG(4,("%x ",s->handle));
	}
	DEBUG(4,("| %x\n",s->handle));
	if ( s == NULL )
	{
		DEBUG(4,("ATTENTION : handle_idx non trouve\n"));
	}
	DEBUG(5,("\tfin getServerObjectHandle, retour handle : %x\n",s->handle));
	return s;
}



/*
  Initialize the context data structure and open a connection on samba
  databases
*/
struct emsmdb_ctx *emsmdb_init(struct dcesrv_call_state *dce_call)
{
	TALLOC_CTX		*mem_ctx;
	struct emsmdb_ctx	*emsmdb_ctx;
	struct event_context	*ev;
	time_t temps = time(NULL);
	
	DEBUG(4,("dans emsmdb_init\n"));
	
	mem_ctx = talloc_init(EMSMDB_CTX);
	emsmdb_ctx = talloc(mem_ctx, struct emsmdb_ctx);
	if (!emsmdb_ctx) return NULL;
	emsmdb_ctx->mem_ctx = mem_ctx;
	emsmdb_ctx->timeStamp = temps;
	
	emsmdb_ctx->nextServerObjectHandleId = 0;
	emsmdb_ctx->serverObjects = NULL;
	
	ev = event_context_init(mem_ctx);
	if (!ev) return NULL;
	
	emsmdb_ctx->store_ctx = ldb_init(emsmdb_ctx->mem_ctx, ev);
	
	emsmdb_ctx->lp_ctx = dce_call->conn->dce_ctx->lp_ctx;
	/*if (ldb_connect(emsmdb_ctx->store_ctx, 
			private_path(emsmdb_ctx->mem_ctx, global_loadparm, 
						 "mailstore.ldb"), 
			0, NULL) != LDB_SUCCESS) {
		DEBUG(0, ("Connection to the mailstore database failed\n"));
		exit (-1);
	}*/
	if (ldb_connect(emsmdb_ctx->store_ctx, 
			private_path(emsmdb_ctx->mem_ctx, emsmdb_ctx->lp_ctx, 
						 "mailstore.ldb"), 
			0, NULL) != LDB_SUCCESS) {
		DEBUG(0, ("Connection to the mailstore database failed\n"));
		exit (-1);
	}
	
	return emsmdb_ctx;
}

NTSTATUS emsmdb_doRpc_Release(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	repl->opnum = req->opnum;
	repl->handle_idx = req->handle_idx;
	
	freeHandleIdx(mem_ctx,emsmdb_ctx,handles[req->handle_idx]);
	
	repl->error_code = MAPI_E_SUCCESS;
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_OpenFolder(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	struct ServerObjectHandle *serverObject;
	char *name;
	
	repl->opnum = req->opnum;
	repl->handle_idx = req->u.mapi_OpenFolder.handle_idx;
	
	repl->u.mapi_OpenFolder.HasRules = 0x00;
	repl->u.mapi_OpenFolder.IsGhosted = 0x00;
	/*repl->u.OpenFolder.Ghost*/
	
	name = talloc_asprintf(mem_ctx,"f%.8x%.8x",(uint32_t)(req->u.mapi_OpenFolder.folder_id >> 32),(uint32_t)(req->u.mapi_OpenFolder.folder_id & 0xffffffff));
	
	serverObject = getServerObjectHandle(mem_ctx,emsmdb_ctx,handles[req->handle_idx]);
	if ( serverObject == NULL )
	{
		DEBUG(4,("serverObject NULL\n"));
	}
	handles[repl->handle_idx] = getHandleIdx(mem_ctx,emsmdb_ctx,NULL,name,serverObject->owner);
	talloc_free(name);
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 8;
	if ( repl->u.mapi_OpenFolder.IsGhosted )
	{
		/* Indiquer ici la nouvelle taille */
		*size += 0;
	}
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_GetHierarchyTable(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	char *name;
	struct ServerObjectHandle *serverObject;
	struct TableObject *handleTable = talloc(emsmdb_ctx->mem_ctx,struct TableObject);
	
	repl->opnum = req->opnum;
	repl->handle_idx = req->u.mapi_GetHierarchyTable.handle_idx;
	
	handleTable->columns = NULL;
	handleTable->columnsCount = 0;
	
	serverObject = getServerObjectHandle(mem_ctx,emsmdb_ctx,handles[req->handle_idx]);
	name = talloc_asprintf(mem_ctx,"t_%s",serverObject->name);
	handles[repl->handle_idx] = getHandleIdx(mem_ctx,emsmdb_ctx,(void *)handleTable,name,serverObject->owner);
	
	if ( !strcmp(serverObject->name,"fb3eeddcc00000001") )
	{
		repl->u.mapi_GetHierarchyTable.RowCount = 0x0a;
	}
	else
	{
		repl->u.mapi_GetHierarchyTable.RowCount = 0;
	}
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 10;
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_GetContentsTable(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	char *name;
	struct ServerObjectHandle *serverObject;
	struct TableObject *handleTable = talloc(emsmdb_ctx->mem_ctx,struct TableObject);
	
	repl->opnum = req->opnum;
	repl->handle_idx = req->u.mapi_GetContentsTable.handle_idx;
	
	handleTable->columns = NULL;
	handleTable->columnsCount = 0;
	
	serverObject = getServerObjectHandle(mem_ctx,emsmdb_ctx,handles[req->handle_idx]);
	name = talloc_asprintf(mem_ctx,"t_%s",serverObject->name);
	handles[repl->handle_idx] = getHandleIdx(mem_ctx,emsmdb_ctx,(void *)handleTable,name,serverObject->owner);
	
	repl->u.mapi_GetContentsTable.RowCount = 0;
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 10;
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_CreateMessage(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	struct ServerObjectHandle *serverObject;
	char *name;
	
	repl->opnum = req->opnum;
	repl->handle_idx = req->u.mapi_CreateMessage.handle_idx;
	
	repl->u.mapi_CreateMessage.HasMessageId = 0x01;
	repl->u.mapi_CreateMessage.MessageId.MessageId = 0xd1eeddcc00000001;
	
	name = talloc_asprintf(mem_ctx,"m%.8x%.8x",(uint32_t)(repl->u.mapi_CreateMessage.MessageId.MessageId >> 32),(uint32_t)(repl->u.mapi_CreateMessage.MessageId.MessageId & 0xffffffff));
	
	serverObject = getServerObjectHandle(mem_ctx,emsmdb_ctx,handles[req->handle_idx]);
	handles[repl->handle_idx] = getHandleIdx(mem_ctx,emsmdb_ctx,NULL,name,serverObject->owner);
	
	talloc_free(name);
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 15;
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_GetProps(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	//int i;
	NTSTATUS ret;
	uint8_t *data;
	uint32_t data_size;
	struct ServerObjectHandle *serverObjectHandle;
	
	DEBUG(4,("\tdans doRpc_GetProps\n"));
	
	repl->opnum = req->opnum;
	repl->handle_idx = req->handle_idx;
	repl->u.mapi_GetProps.layout = 0x01;
	
	data = talloc_zero(mem_ctx,uint8_t);
	data_size = 0;
	
	serverObjectHandle = getServerObjectHandle(mem_ctx,emsmdb_ctx,handles[req->handle_idx]);
	
	ret = getProps(mem_ctx,emsmdb_ctx,&(req->u.mapi_GetProps.properties[0]),req->u.mapi_GetProps.prop_count,serverObjectHandle->name,serverObjectHandle->owner,&data,&data_size);
	
	if ( !NT_STATUS_IS_OK(ret) )
	{
		repl->error_code = MAPI_E_INVALID_OBJECT;
		*size += data_size + 6;
	
		return NT_STATUS_OK;
	}
	
	repl->u.mapi_GetProps.prop_data.data = data;
	repl->u.mapi_GetProps.prop_data.length = data_size;
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += data_size + 7;
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_SetProps(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	/*int i;
	int ret;
	uint8_t *data;
	uint32_t data_size;
	struct ServerObjectHandle *serverObjectHandle;
	struct ldb_result *result = NULL;
	struct ldb_message *message;
	const char * const attrs[] = { "*", NULL };
	char *filter;
	int cpt_props;*/
	
	DEBUG(4,("\tdans doRpc_SetProps\n"));
	
	repl->opnum = req->opnum;
	repl->handle_idx = req->handle_idx;
	
	repl->u.mapi_SetProps.PropertyProblemCount = 0x0000;
	
	repl->error_code = MAPI_E_SUCCESS;
	/**size += 6;*/
	*size += 8;
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_SaveChangesMessage(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	repl->opnum = req->opnum;
	repl->handle_idx = req->handle_idx;
	
	repl->u.mapi_SaveChangesMessage.handle_idx = req->u.mapi_SaveChangesMessage.handle_idx;
	repl->u.mapi_SaveChangesMessage.MessageId = 0xd1eeddcc00000001;
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 15;
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_SetColumns(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	struct ServerObjectHandle *serverObject;
	struct TableObject *table;
	int i;
	
	repl->opnum = req->opnum;
	repl->handle_idx = req->handle_idx;
	
	serverObject = getServerObjectHandle(mem_ctx,emsmdb_ctx,handles[req->handle_idx]);
	table = (struct TableObject *)(serverObject->serverObject);
	
	if ( table->columnsCount != 0 )
	{
		talloc_free(table->columns);
	}
	
	table->columnsCount = req->u.mapi_SetColumns.prop_count;
	table->columns = talloc_array(table,uint32_t,table->columnsCount);
	for ( i = 0 ; i < table->columnsCount ; i++ )
	{
		table->columns[i] = req->u.mapi_SetColumns.properties[i];
	}
	
	repl->u.mapi_SetColumns.TableStatus = TBLSTAT_COMPLETE;
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 7;
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_SortTable(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	repl->opnum = req->opnum;
	repl->handle_idx = req->handle_idx;
	
	repl->u.mapi_SortTable.TableStatus = TBLSTAT_COMPLETE;
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 7;
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_Restrict(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	repl->opnum = req->opnum;
	repl->handle_idx = req->handle_idx;
	
	repl->u.mapi_Restrict.TableStatus = TBLSTAT_COMPLETE;
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 7;
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_QueryRows(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	struct ServerObjectHandle *serverObject;
	struct TableObject *table;
	
	repl->opnum = req->opnum;
	repl->handle_idx = req->handle_idx;
	
	serverObject = getServerObjectHandle(mem_ctx,emsmdb_ctx,handles[req->handle_idx]);
	table = (struct TableObject *)(serverObject->serverObject);
	
	if ( !strcmp(serverObject->name,"t_fb3eeddcc00000001") )
	{
		size_t data_size;
		uint8_t *data;
		
		repl->u.mapi_QueryRows.Origin = 0x02;
		repl->u.mapi_QueryRows.RowCount = 0x000a;
		data_size = 0;
		data = talloc_zero(mem_ctx,uint8_t);
		
		data = talloc_realloc(mem_ctx,data,uint8_t,data_size + 3);
		data[data_size++] = 0x01;
		getProps(mem_ctx,emsmdb_ctx,table->columns,table->columnsCount,"fb4eeddcc00000001",serverObject->owner,&data,&data_size);
		
		data = talloc_realloc(mem_ctx,data,uint8_t,data_size + 3);
		data[data_size++] = 0x01;
		getProps(mem_ctx,emsmdb_ctx,table->columns,table->columnsCount,"fb5eeddcc00000001",serverObject->owner,&data,&data_size);
		
		data = talloc_realloc(mem_ctx,data,uint8_t,data_size + 3);
		data[data_size++] = 0x01;
		getProps(mem_ctx,emsmdb_ctx,table->columns,table->columnsCount,"fa2eeddcc00000001",serverObject->owner,&data,&data_size);
		
		data = talloc_realloc(mem_ctx,data,uint8_t,data_size + 3);
		data[data_size++] = 0x01;
		getProps(mem_ctx,emsmdb_ctx,table->columns,table->columnsCount,"fa0eeddcc00000001",serverObject->owner,&data,&data_size);
		
		data = talloc_realloc(mem_ctx,data,uint8_t,data_size + 3);
		data[data_size++] = 0x01;
		getProps(mem_ctx,emsmdb_ctx,table->columns,table->columnsCount,"fa1eeddcc00000001",serverObject->owner,&data,&data_size);
		
		data = talloc_realloc(mem_ctx,data,uint8_t,data_size + 3);
		data[data_size++] = 0x01;
		getProps(mem_ctx,emsmdb_ctx,table->columns,table->columnsCount,"fb6eeddcc00000001",serverObject->owner,&data,&data_size);
		
		data = talloc_realloc(mem_ctx,data,uint8_t,data_size + 3);
		data[data_size++] = 0x01;
		getProps(mem_ctx,emsmdb_ctx,table->columns,table->columnsCount,"fb7eeddcc00000001",serverObject->owner,&data,&data_size);
		
		data = talloc_realloc(mem_ctx,data,uint8_t,data_size + 3);
		data[data_size++] = 0x01;
		getProps(mem_ctx,emsmdb_ctx,table->columns,table->columnsCount,"fa3eeddcc00000001",serverObject->owner,&data,&data_size);
		
		data = talloc_realloc(mem_ctx,data,uint8_t,data_size + 3);
		data[data_size++] = 0x01;
		getProps(mem_ctx,emsmdb_ctx,table->columns,table->columnsCount,"fa4eeddcc00000001",serverObject->owner,&data,&data_size);
		
		data = talloc_realloc(mem_ctx,data,uint8_t,data_size + 3);
		data[data_size++] = 0x01;
		getProps(mem_ctx,emsmdb_ctx,table->columns,table->columnsCount,"fa5eeddcc00000001",serverObject->owner,&data,&data_size);
		
		repl->u.mapi_QueryRows.RowData.length = data_size;
		repl->u.mapi_QueryRows.RowData.data = data;
		*size += data_size;
	}
	else
	{
		/*repl->u.mapi_QueryRows.Origin = 0x00;*/
		repl->u.mapi_QueryRows.Origin = 0x02;
		repl->u.mapi_QueryRows.RowCount = 0x0000;
		repl->u.mapi_QueryRows.RowData.length = 0;
		repl->u.mapi_QueryRows.RowData.data = 0;
	}
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 9;
	
	DEBUG(5,("fin queryRows, size=%d\n",*size));
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_SeekRow(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	repl->opnum = req->opnum;
	repl->handle_idx = req->handle_idx;
	
	repl->u.mapi_SeekRow.HasSoughtLess = 0x00;
	repl->u.mapi_SeekRow.RowsSought = 0;
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 11;
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_CreateFolder(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	struct ServerObjectHandle *serverObject;
	
	repl->opnum = req->opnum;
	repl->handle_idx = req->u.mapi_CreateFolder.handle_idx;
	
	repl->u.mapi_CreateFolder.folder_id = 0xa7eeddcc00000001;
	repl->u.mapi_CreateFolder.IsExistingFolder = 0x00;
	
	serverObject = getServerObjectHandle(mem_ctx,emsmdb_ctx,handles[req->handle_idx]);
	handles[repl->handle_idx] = getHandleIdx(mem_ctx,emsmdb_ctx,NULL,"fa7eeddcc00000001",serverObject->owner);
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 15;
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_GetReceiveFolder(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	repl->opnum = req->opnum;
	repl->handle_idx = req->handle_idx;
	
	repl->u.mapi_GetReceiveFolder.MessageClass = req->u.mapi_GetReceiveFolder.MessageClass;
	/*repl->u.mapi_GetReceiveFolder.MessageClass = talloc_strdup(mem_ctx,"IPM.Note");*/
	
	repl->u.mapi_GetReceiveFolder.folder_id = 0xb4eeddcc00000001;
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 15 + strlen(repl->u.mapi_GetReceiveFolder.MessageClass);
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_RegisterNotification(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	struct ServerObjectHandle *serverObject;
	
	repl->opnum = req->opnum;
	repl->handle_idx = req->u.mapi_RegisterNotification.handle_idx;
	
	serverObject = getServerObjectHandle(mem_ctx,emsmdb_ctx,handles[req->handle_idx]);
	handles[repl->handle_idx] = getHandleIdx(mem_ctx,emsmdb_ctx,NULL,"advise",serverObject->owner);
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 6;
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_GetRulesTable(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	struct ServerObjectHandle *serverObject;
	struct TableObject *handleTable = talloc(emsmdb_ctx->mem_ctx,struct TableObject);
	
	repl->opnum = req->opnum;
	repl->handle_idx = req->u.mapi_GetRulesTable.handle_idx;
	
	handleTable->columns = NULL;
	handleTable->columnsCount = 0;
	
	serverObject = getServerObjectHandle(mem_ctx,emsmdb_ctx,handles[req->handle_idx]);
	handles[repl->handle_idx] = getHandleIdx(mem_ctx,emsmdb_ctx,(void *)handleTable,"t_rules",serverObject->owner);
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 6;
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_LongTermIdFromId(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	repl->opnum = req->opnum;
	repl->handle_idx = req->handle_idx;
	
	repl->u.mapi_LongTermIdFromId.LongTermId.DatabaseGuid.time_low = 0x64d4774d;
	repl->u.mapi_LongTermIdFromId.LongTermId.DatabaseGuid.time_mid = 0x4983;
	repl->u.mapi_LongTermIdFromId.LongTermId.DatabaseGuid.time_hi_and_version = 0x4f70;
	repl->u.mapi_LongTermIdFromId.LongTermId.DatabaseGuid.clock_seq[0] = 0x9b;
	repl->u.mapi_LongTermIdFromId.LongTermId.DatabaseGuid.clock_seq[1] = 0x8b;
	repl->u.mapi_LongTermIdFromId.LongTermId.DatabaseGuid.node[0] = 0x46;
	repl->u.mapi_LongTermIdFromId.LongTermId.DatabaseGuid.node[1] = 0xe6;
	repl->u.mapi_LongTermIdFromId.LongTermId.DatabaseGuid.node[2] = 0x35;
	repl->u.mapi_LongTermIdFromId.LongTermId.DatabaseGuid.node[3] = 0xbb;
	repl->u.mapi_LongTermIdFromId.LongTermId.DatabaseGuid.node[4] = 0x78;
	repl->u.mapi_LongTermIdFromId.LongTermId.DatabaseGuid.node[5] = 0xab;
	/*repl->u.mapi_LongTermIdFromId.LongTermId.GlobalCounter[0] = (uint8_t)(req->u.mapi_LongTermIdFromId.Id >> 56);
	repl->u.mapi_LongTermIdFromId.LongTermId.GlobalCounter[1] = (uint8_t)((req->u.mapi_LongTermIdFromId.Id >> 48) & 0xff);
	repl->u.mapi_LongTermIdFromId.LongTermId.GlobalCounter[2] = (uint8_t)((req->u.mapi_LongTermIdFromId.Id >> 40) & 0xff);
	repl->u.mapi_LongTermIdFromId.LongTermId.GlobalCounter[3] = (uint8_t)((req->u.mapi_LongTermIdFromId.Id >> 32) & 0xff);
	repl->u.mapi_LongTermIdFromId.LongTermId.GlobalCounter[4] = (uint8_t)((req->u.mapi_LongTermIdFromId.Id >> 24) & 0xff);
	repl->u.mapi_LongTermIdFromId.LongTermId.GlobalCounter[5] = (uint8_t)((req->u.mapi_LongTermIdFromId.Id >> 16) & 0xff);*/
	repl->u.mapi_LongTermIdFromId.LongTermId.GlobalCounter[0] = (uint8_t)(req->u.mapi_LongTermIdFromId.Id & 0xff);
	repl->u.mapi_LongTermIdFromId.LongTermId.GlobalCounter[1] = (uint8_t)((req->u.mapi_LongTermIdFromId.Id >> 8) & 0xff);
	repl->u.mapi_LongTermIdFromId.LongTermId.GlobalCounter[2] = (uint8_t)((req->u.mapi_LongTermIdFromId.Id >> 16) & 0xff);
	repl->u.mapi_LongTermIdFromId.LongTermId.GlobalCounter[3] = (uint8_t)((req->u.mapi_LongTermIdFromId.Id >> 24) & 0xff);
	repl->u.mapi_LongTermIdFromId.LongTermId.GlobalCounter[4] = (uint8_t)((req->u.mapi_LongTermIdFromId.Id >> 32) & 0xff);
	repl->u.mapi_LongTermIdFromId.LongTermId.GlobalCounter[5] = (uint8_t)((req->u.mapi_LongTermIdFromId.Id >> 40) & 0xff);
	repl->u.mapi_LongTermIdFromId.LongTermId.padding = 0x0000;
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 30;
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_FindRow(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	repl->opnum = req->opnum;
	repl->handle_idx = req->handle_idx;
	
	repl->error_code = MAPI_E_NOT_FOUND;
	*size += 6;
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_GetIDsFromNames(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	int i;
	
	repl->opnum = req->opnum;
	repl->handle_idx = req->handle_idx;
	
	repl->u.mapi_GetIDsFromNames.count = req->u.mapi_GetIDsFromNames.count;
	repl->u.mapi_GetIDsFromNames.propID = talloc_array(mem_ctx,uint16_t,repl->u.mapi_GetIDsFromNames.count + 2);
	for ( i = 0 ; i < repl->u.mapi_GetIDsFromNames.count ; i++ )
	{
		repl->u.mapi_GetIDsFromNames.propID[i] = 0x8100 + i;
	}
	
	repl->error_code = MAPI_E_SUCCESS;
	*size += 8 + (2 * repl->u.mapi_GetIDsFromNames.count);
	
	return NT_STATUS_OK;
}

NTSTATUS emsmdb_doRpc_GetLocalReplicaIds(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	repl->opnum = req->opnum;
	repl->handle_idx = req->handle_idx;
	repl->u.mapi_GetLocalReplicaIds.ReplGuid.time_low = 0x64d4774d;
	repl->u.mapi_GetLocalReplicaIds.ReplGuid.time_mid = 0x4983;
	repl->u.mapi_GetLocalReplicaIds.ReplGuid.time_hi_and_version = 0x4f70;
	repl->u.mapi_GetLocalReplicaIds.ReplGuid.clock_seq[0] = 0x9b;
	repl->u.mapi_GetLocalReplicaIds.ReplGuid.clock_seq[1] = 0x8b;
	repl->u.mapi_GetLocalReplicaIds.ReplGuid.node[0] = 0x46;
	repl->u.mapi_GetLocalReplicaIds.ReplGuid.node[1] = 0xe6;
	repl->u.mapi_GetLocalReplicaIds.ReplGuid.node[2] = 0x35;
	repl->u.mapi_GetLocalReplicaIds.ReplGuid.node[3] = 0xbb;
	repl->u.mapi_GetLocalReplicaIds.ReplGuid.node[4] = 0x78;
	repl->u.mapi_GetLocalReplicaIds.ReplGuid.node[5] = 0xab;
	
	repl->u.mapi_GetLocalReplicaIds.GlobalCount[0] = 0x00;
	repl->u.mapi_GetLocalReplicaIds.GlobalCount[1] = 0x00;
	repl->u.mapi_GetLocalReplicaIds.GlobalCount[2] = 0x00;
	repl->u.mapi_GetLocalReplicaIds.GlobalCount[3] = 0x00;
	repl->u.mapi_GetLocalReplicaIds.GlobalCount[4] = 0x00;
	repl->u.mapi_GetLocalReplicaIds.GlobalCount[5] = 0x00;
	
	repl->error_code = MAPI_E_SUCCESS;
	/**size += 6;*/
	*size += 28;
	
	return NT_STATUS_OK;
}

/*
This remote operation logs on to a mailbox or public folder.

See [MS-OXCRPC].pdf section 2.2.2
See [MS-OXCSTOR].pdf section 2.2.1.1
*/
NTSTATUS emsmdb_doRpc_Logon(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct EcDoRpc_MAPI_REQ *req, struct EcDoRpc_MAPI_REPL *repl, uint32_t *handles, uint16_t *size)
{
	/*int i;*/
	char * owner;
	struct tm *time_struct;
	time_t temps = time(NULL);
	NTTIME temps2;
	
	unix_to_nt_time(&temps2,temps);
	time_struct = localtime(&temps);
	
	repl->opnum = req->opnum;
	repl->handle_idx = req->handle_idx;
	
	if ( req->u.mapi_Logon.LogonFlags & LogonPrivate )
	{
		struct LogonObject *handleLogon;
		
		repl->u.mapi_Logon.LogonFlags = req->u.mapi_Logon.LogonFlags & LogonPrivate;
	
		/*for ( i = 0 ; i < 13 ; i++ )
		{
			repl->u.mapi_Logon.type.store_mailbox.folder_id[i] = 0x0000000000000000;
		}*/
		repl->u.mapi_Logon.LogonType.store_mailbox.FolderIds[0] = 0xb0eeddcc00000001;
		repl->u.mapi_Logon.LogonType.store_mailbox.FolderIds[1] = 0xb1eeddcc00000001;
		repl->u.mapi_Logon.LogonType.store_mailbox.FolderIds[2] = 0xb2eeddcc00000001;
		repl->u.mapi_Logon.LogonType.store_mailbox.FolderIds[3] = 0xb3eeddcc00000001;
		repl->u.mapi_Logon.LogonType.store_mailbox.FolderIds[4] = 0xb4eeddcc00000001;
		repl->u.mapi_Logon.LogonType.store_mailbox.FolderIds[5] = 0xb5eeddcc00000001;
		repl->u.mapi_Logon.LogonType.store_mailbox.FolderIds[6] = 0xb6eeddcc00000001;
		repl->u.mapi_Logon.LogonType.store_mailbox.FolderIds[7] = 0xb7eeddcc00000001;
		repl->u.mapi_Logon.LogonType.store_mailbox.FolderIds[8] = 0xb8eeddcc00000001;
		repl->u.mapi_Logon.LogonType.store_mailbox.FolderIds[9] = 0xb9eeddcc00000001;
		repl->u.mapi_Logon.LogonType.store_mailbox.FolderIds[10] = 0xbaeeddcc00000001;
		repl->u.mapi_Logon.LogonType.store_mailbox.FolderIds[11] = 0xbbeeddcc00000001;
		repl->u.mapi_Logon.LogonType.store_mailbox.FolderIds[12] = 0xbceeddcc00000001;
	
		repl->u.mapi_Logon.LogonType.store_mailbox.ResponseFlags = 0x07;
	
		repl->u.mapi_Logon.LogonType.store_mailbox.MailboxGuid.time_low = 0xa591f8f7;
		repl->u.mapi_Logon.LogonType.store_mailbox.MailboxGuid.time_mid = 0x341c;
		repl->u.mapi_Logon.LogonType.store_mailbox.MailboxGuid.time_hi_and_version = 0x4116;
		repl->u.mapi_Logon.LogonType.store_mailbox.MailboxGuid.clock_seq[0] = 0x8c;
		repl->u.mapi_Logon.LogonType.store_mailbox.MailboxGuid.clock_seq[1] = 0x48;
		repl->u.mapi_Logon.LogonType.store_mailbox.MailboxGuid.node[0] = 0x9d;
		repl->u.mapi_Logon.LogonType.store_mailbox.MailboxGuid.node[1] = 0xb0;
		repl->u.mapi_Logon.LogonType.store_mailbox.MailboxGuid.node[2] = 0x1a;
		repl->u.mapi_Logon.LogonType.store_mailbox.MailboxGuid.node[3] = 0x86;
		repl->u.mapi_Logon.LogonType.store_mailbox.MailboxGuid.node[4] = 0xf5;
		repl->u.mapi_Logon.LogonType.store_mailbox.MailboxGuid.node[5] = 0x0b;
	
		repl->u.mapi_Logon.LogonType.store_mailbox.ReplId = 0x0001;
		
		repl->u.mapi_Logon.LogonType.store_mailbox.ReplGUID.time_low = 0x64d4774d;
		repl->u.mapi_Logon.LogonType.store_mailbox.ReplGUID.time_mid = 0x4983;
		repl->u.mapi_Logon.LogonType.store_mailbox.ReplGUID.time_hi_and_version = 0x4f70;
		repl->u.mapi_Logon.LogonType.store_mailbox.ReplGUID.clock_seq[0] = 0x9b;
		repl->u.mapi_Logon.LogonType.store_mailbox.ReplGUID.clock_seq[1] = 0x8b;
		repl->u.mapi_Logon.LogonType.store_mailbox.ReplGUID.node[0] = 0x46;
		repl->u.mapi_Logon.LogonType.store_mailbox.ReplGUID.node[1] = 0xe6;
		repl->u.mapi_Logon.LogonType.store_mailbox.ReplGUID.node[2] = 0x35;
		repl->u.mapi_Logon.LogonType.store_mailbox.ReplGUID.node[3] = 0xbb;
		repl->u.mapi_Logon.LogonType.store_mailbox.ReplGUID.node[4] = 0x78;
		repl->u.mapi_Logon.LogonType.store_mailbox.ReplGUID.node[5] = 0xab;
		
		repl->u.mapi_Logon.LogonType.store_mailbox.LogonTime.Seconds = time_struct->tm_sec;
		repl->u.mapi_Logon.LogonType.store_mailbox.LogonTime.Minutes = time_struct->tm_min;
		repl->u.mapi_Logon.LogonType.store_mailbox.LogonTime.Hour = time_struct->tm_hour;
		repl->u.mapi_Logon.LogonType.store_mailbox.LogonTime.DayOfWeek = time_struct->tm_wday;
		repl->u.mapi_Logon.LogonType.store_mailbox.LogonTime.Day = time_struct->tm_mday;
		repl->u.mapi_Logon.LogonType.store_mailbox.LogonTime.Month = time_struct->tm_mon + 1;
		repl->u.mapi_Logon.LogonType.store_mailbox.LogonTime.Year = time_struct->tm_year + 1900;
		
		repl->u.mapi_Logon.LogonType.store_mailbox.GwartTime = temps2 - 1000000;
		
		repl->u.mapi_Logon.LogonType.store_mailbox.StoreState = 0;
		
		handleLogon = talloc(emsmdb_ctx->mem_ctx,struct LogonObject);
		handleLogon->logonId = req->logon_id;
		handleLogon->essdn = talloc_strdup(emsmdb_ctx->mem_ctx,req->u.mapi_Logon.EssDN);
		owner = &(strrchr(handleLogon->essdn,'=')[1]);
		handles[repl->handle_idx] = getHandleIdx(mem_ctx,emsmdb_ctx,(void *)handleLogon,"logon",owner);
		
		repl->error_code = MAPI_E_SUCCESS;
		*size += 166;
	}
	else
	{
		repl->error_code = MAPI_E_NOT_FOUND;
		*size += 6;
	}
	return NT_STATUS_OK;
}


NTSTATUS emsmdb_doRpc(TALLOC_CTX *mem_ctx, struct emsmdb_ctx *emsmdb_ctx , struct mapi_request *request, struct mapi_response *response)
{
	int i;
	uint16_t size = 0;
	uint16_t handles_len = 0;
	NTSTATUS retour = NT_STATUS_OK;
	
	response->handles = request->handles;
	DEBUG(4,("dans emsmdb_DoRpc\n"));
	
	/* Si aucune operation n'est passee */
	if ( request->mapi_len <= 2 )
	{
		response->mapi_repl = NULL;
		handles_len = request->mapi_len - request->length;
	
		response->length = sizeof(response->length);
		response->mapi_len = response->length + handles_len;
		return NT_STATUS_OK;
	}
	
	response->mapi_repl = talloc_zero(mem_ctx, struct EcDoRpc_MAPI_REPL);
	for ( i = 0 ; request->mapi_req[i].opnum != 0 ; i++ )
	{
		DEBUG(4,("i=%d ; opnum = %x\n",i,request->mapi_req[i].opnum));
		response->mapi_repl = talloc_realloc(mem_ctx, response->mapi_repl, struct EcDoRpc_MAPI_REPL, i + 2);
		switch ( request->mapi_req[i].opnum )
		{
			case op_MAPI_Release: /* 0x1 */
				retour = emsmdb_doRpc_Release(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			case op_MAPI_OpenFolder: /* 0x2 */
				DEBUG(4,("op_MAPI_OpenFolder\n"));
				retour = emsmdb_doRpc_OpenFolder(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			//case op_MAPI_OpenMessage: /* 0x3 */
			
			case op_MAPI_GetHierarchyTable: /* 0x4 */
				retour = emsmdb_doRpc_GetHierarchyTable(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			case op_MAPI_GetContentsTable: /* 0x5 */
				retour = emsmdb_doRpc_GetContentsTable(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			case op_MAPI_CreateMessage: /* 0x6 */
				retour = emsmdb_doRpc_CreateMessage(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			case op_MAPI_GetProps: /* 0x7 */
				retour = emsmdb_doRpc_GetProps(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			//case op_MAPI_GetPropsAll: /* 0x8 */
			
			//case op_MAPI_GetPropList: /* 0x9 */
			
			case op_MAPI_SetProps: /* 0xa */
				retour = emsmdb_doRpc_SetProps(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			//case op_MAPI_DeleteProps: /* 0xb */
			
			case op_MAPI_SaveChangesMessage: /* 0xc */
				retour = emsmdb_doRpc_SaveChangesMessage(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			//case op_MAPI_RemoveAllRecipients: /* 0xd */
			
			//case op_MAPI_ModifyRecipients: /* 0xe */
			
			//case op_MAPI_ReadRecipients: /* 0xf */
			
			//case op_MAPI_ReloadCachedInformation: /* 0x10 */
			
			//case op_MAPI_SetMessageReadFlag: /* 0x11 */
			
			case op_MAPI_SetColumns: /* 0x12 */
				retour = emsmdb_doRpc_SetColumns(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			case op_MAPI_SortTable: /* 0x13 */
				retour = emsmdb_doRpc_SortTable(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			case op_MAPI_Restrict: /* 0x14 */
				retour = emsmdb_doRpc_Restrict(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			case op_MAPI_QueryRows: /* 0x15 */
				retour = emsmdb_doRpc_QueryRows(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			//case op_MAPI_GetStatus: /* 0x16 */
			
			//case op_MAPI_GetRowCount: /* 0x17 */
			
			case op_MAPI_SeekRow: /* 0x18 */
			retour = emsmdb_doRpc_SeekRow(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			//case op_MAPI_SeekRowBookmark: /* 0x19 */
			
			//case op_MAPI_SeekRowApprox: /* 0x1a */
			
			//case op_MAPI_CreateBookmark: /* 0x1b */
			
			case op_MAPI_CreateFolder: /* 0x1c */
				retour = emsmdb_doRpc_CreateFolder(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			//case op_MAPI_DeleteFolder: /* 0x1d */
			
			//case op_MAPI_DeleteMessages: /* 0x1e */
			
			//case op_MAPI_GetMessageStatus: /* 0x1f */
			
			//case op_MAPI_SetMessageStatus: /* 0x20 */
			
			//case op_MAPI_GetAttachmentTable: /* 0x21 */
			
			//case op_MAPI_OpenAttach: /* 0x22 */
			
			//case op_MAPI_CreateAttach: /* 0x23 */
			
			//case op_MAPI_DeleteAttach: /* 0x24 */
			
			//case op_MAPI_SaveChangesAttachment  : /* 0x25 */
			
			//case op_MAPI_SetReceiveFolder: /* 0x26 */
			
			case op_MAPI_GetReceiveFolder: /* 0x27 */
				retour = emsmdb_doRpc_GetReceiveFolder(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			case op_MAPI_RegisterNotification: /* 0x29 */
				retour = emsmdb_doRpc_RegisterNotification(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			//case op_MAPI_Notify: /* 0x2a */
			
			//case op_MAPI_OpenStream: /* 0x2b */
			
			//case op_MAPI_ReadStream: /* 0x2c */
			
			//case op_MAPI_WriteStream: /* 0x2d */
			
			//case op_MAPI_SeekStream: /* 0x2e */
			
			//case op_MAPI_SetStreamSize: /* 0x2f */
			
			//case op_MAPI_SetSearchCriteria: /* 0x30 */
			
			//case op_MAPI_GetSearchCriteria: /* 0x31 */
			
			//case op_MAPI_SubmitMessage: /* 0x32 */
			
			//case op_MAPI_MoveCopyMessages: /* 0x33 */
			
			//case op_MAPI_AbortSubmit: /* 0x34 */
			
			//case op_MAPI_MoveFolder: /* 0x35 */
			
			//case op_MAPI_CopyFolder: /* 0x36 */
			
			//case op_MAPI_QueryColumnsAll: /* 0x37 */
			
			//case op_MAPI_Abort: /* 0x38 */
			
			//case op_MAPI_CopyTo: /* 0x39 */
			
			//case op_MAPI_CopyToStream: /* 0x3a */
			
			//case op_MAPI_GetTable: /* 0x3e */
			
			case op_MAPI_GetRulesTable: /* 0x3f */
				retour = emsmdb_doRpc_GetRulesTable(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			//case op_MAPI_ModifyTable: /* 0x40 */
			
			//case op_MAPI_ModifyRules: /* 0x41 */
			
			//case op_MAPI_GetOwningServers: /* 0x42 */
			
			case op_MAPI_LongTermIdFromId: /* 0x43 */
				retour = emsmdb_doRpc_LongTermIdFromId(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			//case op_MAPI_IdFromLongTermId: /* 0x44 */
			
			//case op_MAPI_PublicFolderIsGhosted: /* 0x45 */
			
			//case op_MAPI_OpenEmbeddedMessage: /* 0x46 */
			
			//case op_MAPI_SetSpooler: /* 0x47 */
			
			//case op_MAPI_SpoolerLockMessage: /* 0x48 */
			
			//case op_MAPI_AddressTypes: /* 0x49 */
			
			//case op_MAPI_TransportSend: /* 0x4a */
			
			//case op_MAPI_FastTransferSourceGetBuffer: /* 0x4e */
			
			case op_MAPI_FindRow: /* 0x4f */
				retour = emsmdb_doRpc_FindRow(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			//case op_MAPI_Progress: /* 0x50 */
			
			//case op_MAPI_GetNamesFromIDs: /* 0x55 */
			
			case op_MAPI_GetIDsFromNames: /* 0x56 */
				retour = emsmdb_doRpc_GetIDsFromNames(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			//case op_MAPI_EmptyFolder: /* 0x58 */
			
			//case op_MAPI_ExpandRow: /* 0x59 */
			
			//case op_MAPI_CollapseRow: /* 0x5a */
			
			//case op_MAPI_CommitStream: /* 0x5d */
			
			//case op_MAPI_GetStreamSize: /* 0x5e */
			
			//case op_MAPI_QueryNamedProperties: /* 0x5f */
			
			//case op_MAPI_GetPerUserLongTermIds: /* 0x60 */
			
			//case op_MAPI_GetPerUserGuid: /* 0x61 */
			
			//case op_MAPI_ReadPerUserInformation: /* 0x63 */
			
			//case op_MAPI_SetReadFlags: /* 0x66 */
			
			//case op_MAPI_CopyProperties: /* 0x67 */
			
			//case op_MAPI_GetReceiveFolderTable: /* 0x68 */
			
			//case op_MAPI_GetCollapseState: /* 0x6b */
			
			//case op_MAPI_SetCollapseState: /* 0x6c */
			
			//case op_MAPI_GetTransportFolder: /* 0x6d */
			
			//case op_MAPI_Pending: /* 0x6e */
			
			//case op_MAPI_RegisterOptions: /* 0x6f */
			
			//case op_MAPI_SyncConfigure: /* 0x70 */
			
			//case op_MAPI_SyncImportMessageChange: /* 0x72 */
			
			//case op_MAPI_SyncImportHierarchyChange: /* 0x73 */
			
			//case op_MAPI_SyncImportDeletes: /* 0x74 */
			
			//case op_MAPI_SyncUploadStateStreamBegin: /* 0x75 */
			
			//case op_MAPI_SyncUploadStateStreamContinue: /* 0x76 */
			
			//case op_MAPI_SyncUploadStateStreamEnd: /* 0x77 */
			
			//case op_MAPI_SyncImportMessageMove: /* 0x78 */
			
			//case op_MAPI_SetPropertiesNoReplicate: /* 0x79 */
			
			//case op_MAPI_DeletePropertiesNoReplicate: /* 0x7a */
			
			//case op_MAPI_SyncOpenCollector: /* 0x7e */
			
			case op_MAPI_GetLocalReplicaIds: /* 0x7f */
				retour = emsmdb_doRpc_GetLocalReplicaIds(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			//case op_MAPI_SyncImportReadStateChanges: /* 0x80 */
			
			//case op_MAPI_ResetTable: /* 0x81 */
			
			//case op_MAPI_SyncGetTransferState: /* 0x82 */
			
			//case op_MAPI_OpenPublicFolderByName: /* 0x87 */
			
			//case op_MAPI_SetSyncNotificationGuid: /* 0x88 */
			
			//case op_MAPI_FreeBookmark: /* 0x89 */
			
			case op_MAPI_Logon: /* 0xfe */
				retour = emsmdb_doRpc_Logon(mem_ctx, emsmdb_ctx , &(request->mapi_req[i]), &(response->mapi_repl[i]), response->handles, &size);
				break;
			
			//case op_MAPI_proxypack: /* 0xa5*/
			
			default:
				DEBUG(4,("opnum inconnu\n"));
				retour = NT_STATUS_NOT_IMPLEMENTED;
				break;
		}
	}
	
	response->mapi_repl[i].opnum = 0;
	
	handles_len = request->mapi_len - request->length;
	
	response->length = size + sizeof(response->length);
	response->mapi_len = response->length + handles_len;
	return retour;
}
