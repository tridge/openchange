/*
   NSPI Server

   OpenChange Project

   Copyright (C) Julien Kerihuel 2008
   Copyright (C) Arthur Vuillard 2008

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <mapiproxy/dcesrv_mapiproxy.h>
#include <mapiproxy/dcesrv_mapiproxy_proto.h>
#include <mapiproxy/providers/emsabp/emsabp.h>

#include <util/debug.h>
#include <libmapi/libmapi.h>
#include "dcesrv_exchange.h"

/* 
  NspiBind 
*/
enum MAPISTATUS dcesrv_NspiBind(struct dcesrv_call_state *dce_call, 
                                TALLOC_CTX *mem_ctx,
		                        struct NspiBind *r)
{
	struct GUID		*guid = (struct GUID *) NULL;
	const char		*exchange_GUID = lp_parm_string(
		dce_call->conn->dce_ctx->lp_ctx, NULL, "exchange", "GUID");
	struct emsabp_ctx	*emsabp_context;
	struct dcesrv_handle	*handle;
	struct policy_handle	wire_handle;

	DEBUG(0, ("##### in NspiBind ####\n"));

	if (!NTLM_AUTH_IS_OK(dce_call)) {
		DEBUG(1, ("No challenge requested by client, cannot authenticate\n"));

		/* Create an empty policy handle */
		wire_handle.handle_type = EXCHANGE_HANDLE_NSP;
		wire_handle.uuid = GUID_zero();
		*r->out.handle = wire_handle;

		r->out.mapiuid = r->in.mapiuid;
		r->out.result = MAPI_E_LOGON_FAILED;
		return MAPI_E_LOGON_FAILED;
	}

	emsabp_context = emsabp_init(dce_call);
	if (!emsabp_context) {
		return MAPI_E_FAILONEPROVIDER;
	}

	/* check if a valid CPID has been provided */
	if (valid_codepage(r->in.pStat->CodePage) == false) {
		DEBUG(1, ("Invalid CPID\n"));
		r->out.mapiuid = r->in.mapiuid;
		r->out.result = MAPI_E_UNKNOWN_CPID;
		return MAPI_E_UNKNOWN_CPID;
	}
	
	guid = talloc(mem_ctx, struct GUID);
	if (!NT_STATUS_IS_OK(GUID_from_string(exchange_GUID, guid))) {
		DEBUG(1, ("No Exchange default GUID specified"));
		r->out.mapiuid = r->in.mapiuid;
		r->out.result = MAPI_E_LOGON_FAILED;
		return MAPI_E_LOGON_FAILED;
	}

	handle = dcesrv_handle_new(dce_call->context, EXCHANGE_HANDLE_NSP);
	if (!handle) {
		/* replaces NT_STATUS_NO_MEMORY */
		return MAPI_E_NOT_ENOUGH_RESOURCES;
	}

	handle->data = (void *) emsabp_context;
	*r->out.handle = handle->wire_handle;
	r->out.mapiuid = guid;
	r->out.result = MAPI_E_SUCCESS;

	DEBUG(0, ("NspiBind : Success\n"));

	return MAPI_E_SUCCESS;
}

/* 
  NspiUnbind 
*/
enum MAPISTATUS dcesrv_NspiUnbind(struct dcesrv_call_state *dce_call, 
                                  TALLOC_CTX *mem_ctx,
		                          struct NspiUnbind *r)
{
	struct dcesrv_handle	*h;
	struct emsabp_ctx	*emsabp_context;

	DEBUG(0, ("##### in NspiUnbind ####\n"));

	if (!NTLM_AUTH_IS_OK(dce_call)) {
		DEBUG(1, ("No challenge requested by client, cannot authenticate\n"));
		return MAPI_E_LOGON_FAILED;
	}

	h = dcesrv_handle_fetch(dce_call->context, r->in.handle, DCESRV_HANDLE_ANY);
	if (h) {
		emsabp_context = (struct emsabp_ctx *) h->data;

		if (emsabp_context)
			talloc_free(emsabp_context->mem_ctx);
	}
	return MAPI_E_SUCCESS;
}

/* 
  NspiUpdateStat 
*/
enum MAPISTATUS dcesrv_NspiUpdateStat(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct NspiUpdateStat *r)
{
	DEBUG(3,("Unimplemented function dcesrv_NspiUpdateStat\n"));
	/* TODO */
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/* 
  NspiQueryRows 
*/
enum MAPISTATUS dcesrv_NspiQueryRows(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct NspiQueryRows *r)
{
	struct emsabp_ctx	*emsabp_context;
	struct dcesrv_handle	*h;
	NTSTATUS		status;
	int			row_nb, i = 0;

	DEBUG(0, ("##### in NspiQueryRows ####\n"));

	h = dcesrv_handle_fetch(dce_call->context, r->in.handle, DCESRV_HANDLE_ANY);
	emsabp_context = (struct emsabp_ctx *) h->data;

	r->out.pStat = r->in.pStat;
	r->out.pStat->CurrentRec = *r->in.lpETable;

	row_nb = r->in.Count;

	/* Row Set */
	r->out.ppRows = talloc(mem_ctx, struct SRowSet *);
	r->out.ppRows[0] = talloc(mem_ctx, struct SRowSet);
	r->out.ppRows[0]->cRows = row_nb;
	r->out.ppRows[0]->aRow = talloc_size(mem_ctx, sizeof(struct SRow) * row_nb);
	while (i < row_nb) {
		status = emsabp_fetch_attrs(mem_ctx, emsabp_context, &(r->out.ppRows[0]->aRow[i]), r->in.lpETable[i], r->in.pPropTags);
		if (!NT_STATUS_IS_OK(status))  /* FIXME */
			return MAPI_E_LOGON_FAILED;
		i++;
	}

        r->out.result = MAPI_E_SUCCESS;

	DEBUG(0, ("NspiQueryRows : Success\n"));

        return MAPI_E_SUCCESS;
}

/* 
  NspiSeekEntries 
*/
enum MAPISTATUS dcesrv_NspiSeekEntries(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
				       struct NspiSeekEntries *r)
{
	DEBUG(3,("Unimplemented function dcesrv_NspiSeekEntries\n"));
	/* TODO */
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/* 
  NspiGetMatches 
*/
enum MAPISTATUS dcesrv_NspiGetMatches(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
				      struct NspiGetMatches *r)
{
	struct dcesrv_handle	*h;
	struct emsabp_ctx	*emsabp_context;
	struct SPropTagArray	*instance_keys;
	NTSTATUS		status;
	int			nbrows = 0;

	DEBUG(0, ("##### in NspiGetMatches ####\n"));

	h = dcesrv_handle_fetch(dce_call->context, r->in.handle, DCESRV_HANDLE_ANY);
	emsabp_context = (struct emsabp_ctx *) h->data;

        /* Settings */
        r->out.pStat = r->in.pStat;

	/* Search the provider for the requested recipient */
	instance_keys = talloc(mem_ctx, struct SPropTagArray);
	status = emsabp_search(emsabp_context, instance_keys, r->in.Filter);
	if (!NT_STATUS_IS_OK(status)) {
		return MAPI_E_LOGON_FAILED;
	}
	
        /* Row Set */
        r->out.ppRows = talloc(mem_ctx, struct SRowSet *);
	r->out.ppRows[0] = talloc(mem_ctx, struct SRowSet);
	r->out.ppRows[0]->cRows = instance_keys->cValues;
	r->out.ppRows[0]->aRow = talloc_size(mem_ctx, sizeof(struct SRow) * (instance_keys->cValues));
	/* Instance keys */
	r->out.ppOutMIds = &instance_keys;

	DEBUG(0,("All NspiGetMatches instance_keys(%d)\n", instance_keys->cValues));
	nbrows = 0;
	while (nbrows < (instance_keys->cValues)) {
		DEBUG(0,("instance_keys[%d] = 0x%x\n", nbrows, instance_keys->aulPropTag[nbrows]));
		status = emsabp_fetch_attrs(mem_ctx, emsabp_context, &(r->out.ppRows[0]->aRow[nbrows]), instance_keys->aulPropTag[nbrows], r->in.pPropTags);
		if (!NT_STATUS_IS_OK(status))	/* FIXME */
			return MAPI_E_LOGON_FAILED;

		DEBUG(0,("NspiGetMatches after set: instance_keys[%d] = 0x%x\n", nbrows, r->out.ppOutMIds[0]->aulPropTag[nbrows]));

		nbrows++;
	}

        r->out.result = MAPI_E_SUCCESS;
	
	DEBUG(0, ("NspiGetMatches : Success\n"));
	
        return MAPI_E_SUCCESS;
}

/* 
  NspiResortRestriction 
*/
enum MAPISTATUS dcesrv_NspiResortRestriction(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
					     struct NspiResortRestriction *r)
{
	DEBUG(3,("Unimplemented function dcesrv_NspiResortRestriction\n"));
	/* TODO */
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/* 
  NspiDNToMId
*/
enum MAPISTATUS dcesrv_NspiDNToMId(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
				   struct NspiDNToMId *r)
{
	uint32_t		instance_key;
	struct dcesrv_handle	*h;
	struct emsabp_ctx	*emsabp_context;
	NTSTATUS		status;

 	DEBUG(0, ("##### in NspiDNToMid ####\n"));

	h = dcesrv_handle_fetch(dce_call->context, r->in.handle, DCESRV_HANDLE_ANY);
	emsabp_context = (struct emsabp_ctx *) h->data;

	/* Search the server identifier according to the given legacyExchangeDN */

	/* Instance key */
/*         r->out.instance_key = talloc(mem_ctx, struct instance_key); */
/*         r->out.instance_key->value = talloc_size(mem_ctx, sizeof (uint32_t)); */
	/*r->out.ppMIds = talloc_zero(mem_ctx, struct SPropTagArray *);*/

	status = emsabp_search_dn(emsabp_context, NULL, &(instance_key), r->in.pNames->Strings[0]);
	if (!NT_STATUS_IS_OK(status)) {
		/* Microsoft Exchange returns success even when the research failed */
		/*r->out.ppMIds[0]->cValues = 0;*/
		r->out.ppMIds = NULL;
		
/* 		memset(r->out.instance_key->value, 0, sizeof(uint32_t)); */
/* 		r->out.instance_key->cValues = 0x2; */
		return MAPI_E_SUCCESS;
	}
	
	r->out.ppMIds = talloc_array(mem_ctx, struct SPropTagArray *, 1);
	r->out.ppMIds[0] = talloc(mem_ctx, struct SPropTagArray);
	r->out.ppMIds[0]->cValues = 0x1;
	r->out.ppMIds[0]->aulPropTag = talloc_array(mem_ctx, uint32_t, r->out.ppMIds[0]->cValues);
	r->out.ppMIds[0]->aulPropTag[0] = instance_key;
/* 	r->out.instance_key->value[0] = instance_key; */

/*         r->out.instance_key->cValues = 0x2; */

        r->out.result = MAPI_E_SUCCESS;

	DEBUG(0, ("NspiDNToMid : Success\n"));	

        return MAPI_E_SUCCESS;
}

/* 
  NspiGetPropList 
*/
enum MAPISTATUS dcesrv_NspiGetPropList(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
				       struct NspiGetPropList *r)
{
	DEBUG(3,("Unimplemented function dcesrv_NspiGetPropList\n"));
	/* TODO */
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/* 
  NspiGetProps 
*/
enum MAPISTATUS dcesrv_NspiGetProps(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
				    struct NspiGetProps *r)
{
	uint32_t		instance_key;
	struct dcesrv_handle	*h;
	struct emsabp_ctx	*emsabp_context;
	NTSTATUS		status;

	DEBUG(0, ("##### in NspiGetProps ####\n"));

	h = dcesrv_handle_fetch(dce_call->context, r->in.handle, DCESRV_HANDLE_ANY);
	emsabp_context = (struct emsabp_ctx *) h->data;

	/* Convert instance_key */
/* 	instance_key = r->in.settings->service_provider.ab[1]; */
/* 	instance_key <<= 8; */
/* 	instance_key |= r->in.settings->service_provider.ab[0]; */
	instance_key = r->in.pStat->CurrentRec;

	r->out.ppRows = talloc_size(mem_ctx, sizeof(struct SRow *));
	r->out.ppRows[0] = talloc_size(mem_ctx, sizeof(struct SRow));

	status = emsabp_fetch_attrs(mem_ctx, emsabp_context, &(r->out.ppRows[0][0]), instance_key, r->in.pPropTags);
	if (!NT_STATUS_IS_OK(status)) {
		r->out.result = MAPI_W_ERRORS_RETURNED;
		return MAPI_W_ERRORS_RETURNED;
	}

        r->out.result = MAPI_E_SUCCESS;

	DEBUG(0, ("NspiGetProps : Success\n"));	
	
	return MAPI_E_SUCCESS;
}

/* 
  NspiCompareMIds 
*/
enum MAPISTATUS dcesrv_NspiCompareMIds(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
				       struct NspiCompareMIds *r)
{
	DEBUG(3,("Unimplemented function dcesrv_NspiCompareMIds\n"));
	/* TODO */
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/* 
  NspiModProps 
*/enum MAPISTATUS dcesrv_NspiModProps(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct NspiModProps *r)
{
	DEBUG(3,("Unimplemented function dcesrv_NspiModProps\n"));
	/* TODO */
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/* 
  NspiGetSpecialTable 
*/
enum MAPISTATUS dcesrv_NspiGetSpecialTable(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
					   struct NspiGetSpecialTable *r)
{
	struct dcesrv_handle    *h;
	struct emsabp_ctx       *emsabp_context;

	DEBUG(0, ("##### in NspiGetSpecialTable ####\n"));

	h = dcesrv_handle_fetch(dce_call->context, r->in.handle, DCESRV_HANDLE_ANY);
        emsabp_context = (struct emsabp_ctx *) h->data;

	r->out.lpVersion = talloc(mem_ctx, uint32_t);
	*(r->out.lpVersion) = 0x1;
	
	r->out.ppRows = talloc(mem_ctx, struct SRowSet *);
	r->out.ppRows[0] = talloc(mem_ctx, struct SRowSet);
	emsabp_get_hierarchytable(mem_ctx, emsabp_context, r->in.dwFlags, r->out.ppRows);

	DEBUG(0, ("NspiGetSpecialTable : success\n"));

	return MAPI_E_SUCCESS;
}

/* 
  NspiGetTemplateInfo 
*/
enum MAPISTATUS dcesrv_NspiGetTemplateInfo(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct NspiGetTemplateInfo *r)
{
	DEBUG(3,("Unimplemented function dcesrv_NspiGetTemplateInfo\n"));
	/* TODO */
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/* 
  NspiModLInkAtt 
*/
enum MAPISTATUS dcesrv_NspiModLinkAtt(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
				      struct NspiModLinkAtt *r)
{
	DEBUG(3,("Unimplemented function dcesrv_NspiModLinkAtt\n"));
	/* TODO */
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/* 
  NspiDeleteEntries 
*/
enum MAPISTATUS dcesrv_NspiDeleteEntries(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
					 struct NspiDeleteEntries *r)
{
	DEBUG(3,("Unimplemented function dcesrv_NspiDeleteEntries\n"));
	/* TODO */
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/* 
  NspiQueryColumns 
*/
enum MAPISTATUS dcesrv_NspiQueryColumns(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
					struct NspiQueryColumns *r)
{
	DEBUG(3,("Unimplemented function dcesrv_NspiQueryColumns\n"));
	/* TODO */
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/* 
  NspiGetNamesFromIDs 
*/
enum MAPISTATUS dcesrv_NspiGetNamesFromIDs(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
					   struct NspiGetNamesFromIDs *r)
{
	DEBUG(3,("Unimplemented function dcesrv_NspiGetNamesFromIDs\n"));
	/* TODO */
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/* 
  NspiGetIDsFromNames 
*/
enum MAPISTATUS dcesrv_NspiGetIDsFromNames(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
					   struct NspiGetIDsFromNames *r)
{
	DEBUG(3,("Unimplemented function dcesrv_NspiGetIDsFromNames\n"));
	/* TODO */
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/* 
  NspiResolveNames 
*/
enum MAPISTATUS dcesrv_NspiResolveNames(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct NspiResolveNames *r)
{
	DEBUG(3,("Unimplemented function dcesrv_NspiResolveNames\n"));
	/* TODO */
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/* 
  NspiResolveNamesW 
*/
enum MAPISTATUS dcesrv_NspiResolveNamesW(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx,
		       struct NspiResolveNamesW *r)
{
	DEBUG(3,("Unimplemented function dcesrv_NspiResolveNamesW\n"));
	/* TODO */
	DCESRV_FAULT(DCERPC_FAULT_OP_RNG_ERROR);
}

/**
   \details Dispatch incoming NSPI request to the correct function

   EMSABP provider functions are called within the switch

   \return NTSTATUS_OK
 */
NTSTATUS dcesrv_exchange_nsp_dispatch(struct dcesrv_call_state *dce_call, 
				      TALLOC_CTX *mem_ctx,
				      struct mapiproxy *mapiproxy,
				      void *r)
{
	enum MAPISTATUS				retval = MAPI_E_CALL_FAILED;
	const struct ndr_interface_table	*table;
	uint16_t				opnum;

	table = (const struct ndr_interface_table *) dce_call->context->iface->private;
	opnum = dce_call->pkt.u.request.opnum;

	/* Sanity checks */
	if (!table) return NT_STATUS_OK;
	if (table->name && strcmp(table->name, NDR_EXCHANGE_NSP_NAME)) return NT_STATUS_OK;

	switch (opnum) {
	case NDR_NSPIBIND:
		retval = dcesrv_NspiBind(dce_call, mem_ctx, (struct NspiBind *)r);
		break;
	case NDR_NSPIUNBIND:
		retval = dcesrv_NspiUnbind(dce_call, mem_ctx, (struct NspiUnbind *)r);
		break;
	case NDR_NSPIUPDATESTAT:
		retval = dcesrv_NspiUpdateStat(dce_call, mem_ctx, (struct NspiUpdateStat *)r);
		break;
	case NDR_NSPIQUERYROWS:
		retval = dcesrv_NspiQueryRows(dce_call, mem_ctx, (struct NspiQueryRows *)r);
		break;
	case NDR_NSPISEEKENTRIES:
		retval = dcesrv_NspiSeekEntries(dce_call, mem_ctx, (struct NspiSeekEntries *)r);
		break;
	case NDR_NSPIGETMATCHES:
		retval = dcesrv_NspiGetMatches(dce_call, mem_ctx, (struct NspiGetMatches *)r);
		break;
	case NDR_NSPIRESORTRESTRICTION:
		retval = dcesrv_NspiResortRestriction(dce_call, mem_ctx, (struct NspiResortRestriction *)r);
		break;
	case NDR_NSPIDNTOMID:
		retval = dcesrv_NspiDNToMId(dce_call, mem_ctx, (struct NspiDNToMId *)r);
		break;
	case NDR_NSPIGETPROPLIST:
		retval = dcesrv_NspiGetPropList(dce_call, mem_ctx, (struct NspiGetPropList *)r);
		break;
	case NDR_NSPIGETPROPS:
		retval = dcesrv_NspiGetProps(dce_call, mem_ctx, (struct NspiGetProps *)r);
		break;
	case NDR_NSPICOMPAREMIDS:
		retval = dcesrv_NspiCompareMIds(dce_call, mem_ctx, (struct NspiCompareMIds *)r);
		break;
	case NDR_NSPIMODPROPS:
		retval = dcesrv_NspiModProps(dce_call, mem_ctx, (struct NspiModProps *)r);
		break;
	case NDR_NSPIGETSPECIALTABLE:
		retval = dcesrv_NspiGetSpecialTable(dce_call, mem_ctx, (struct NspiGetSpecialTable *)r);
		break;
	case NDR_NSPIGETTEMPLATEINFO:
		retval = dcesrv_NspiGetTemplateInfo(dce_call, mem_ctx, (struct NspiGetTemplateInfo *)r);
		break;
	case NDR_NSPIMODLINKATT:
		retval = dcesrv_NspiModLinkAtt(dce_call, mem_ctx, (struct NspiModLinkAtt *)r);
		break;
	case NDR_NSPIDELETEENTRIES:
		retval = dcesrv_NspiDeleteEntries(dce_call, mem_ctx, (struct NspiDeleteEntries *)r);
		break;
	case NDR_NSPIQUERYCOLUMNS:
		retval = dcesrv_NspiQueryColumns(dce_call, mem_ctx, (struct NspiQueryColumns *)r);
		break;
	case NDR_NSPIGETNAMESFROMIDS:
		retval = dcesrv_NspiGetNamesFromIDs(dce_call, mem_ctx, (struct NspiGetNamesFromIDs *)r);
		break;
	case NDR_NSPIGETIDSFROMNAMES:
		retval = dcesrv_NspiGetIDsFromNames(dce_call, mem_ctx, (struct NspiGetIDsFromNames *)r);
		break;
	case NDR_NSPIRESOLVENAMES:
		retval = dcesrv_NspiResolveNames(dce_call, mem_ctx, (struct NspiResolveNames *)r);
		break;
	case NDR_NSPIRESOLVENAMESW:
		retval = dcesrv_NspiResolveNamesW(dce_call, mem_ctx, (struct NspiResolveNamesW *)r);
		break;
	}

	/* don't relay the traffic to remove server */
	mapiproxy->norelay = true;

	return NT_STATUS_OK;
}
