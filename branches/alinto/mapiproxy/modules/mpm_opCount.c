/*
   MAPI Proxy - OpCount Module

   OpenChange Project

   Copyright (C) Mickael Jeannot 2008

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mapiproxy/dcesrv_mapiproxy.h"
#include "mapiproxy/dcesrv_mapiproxy_proto.h"

#include <stdio.h>
#include <util/debug.h>

/* NOTE : The following directories must exist */
#define DIR_NAME_IN "/tmp/opCount/in/"
#define DIR_NAME_OUT "/tmp/opCount/out/"

NTSTATUS samba_init_module(void);

static void opCount_countOpCall(uint8_t isRequest, uint8_t opnum)
{
	char filename[30];
	uint32_t count;
	FILE *file;
	
	if ( isRequest )
	{
		sprintf(filename,"%s%x",DIR_NAME_IN,opnum);
	}
	else
	{
		sprintf(filename,"%s%x",DIR_NAME_OUT,opnum);
	}
	file = fopen(filename,"r");
	if ( file )
	{
		fscanf(file,"%d",&count);
		fclose(file);
	}
	else
	{
		count = 0;
	}
	count++;
	file = fopen(filename,"w");
	if ( file )
	{
		fprintf(file,"%d",count);
		fclose(file);
	}
}

static NTSTATUS opCount_init(struct dcesrv_context *dce_ctx)
{
	char filename[30];
	uint32_t i;
	
	DEBUG(5,("mapiproxy : opCount_init\n"));
	for ( i = 0 ; i <= 0xff ; i++ )
	{
		sprintf(filename,"%s%x",DIR_NAME_IN,i);
		remove(filename);
		
		sprintf(filename,"%s%x",DIR_NAME_OUT,i);
		remove(filename);
	}
	
	return NT_STATUS_OK;
}

static NTSTATUS opCount_push(struct dcesrv_call_state *dce_call, 
			   TALLOC_CTX *mem_ctx,  void *r)
{
	const struct ndr_interface_table	*table;
	uint16_t				opnum;
	
	DEBUG(5,("mapiproxy : opCount_push\n"));
	table = (const struct ndr_interface_table *)dce_call->context->iface->private;
	opnum = dce_call->pkt.u.request.opnum;
	
	if ((opnum == 0x2) && (table->name && !strcmp(table->name, "exchange_emsmdb"))
		&& (((struct EcDoRpc *)r)->out.mapi_response->length > 2))
	{
		uint32_t i;
		struct EcDoRpc_MAPI_REPL *mapi_repl = ((struct EcDoRpc *)r)->out.mapi_response->mapi_repl;
		
		for ( i = 0 ; mapi_repl && mapi_repl[i].opnum != 0x00 ; i++ )
		{
			opCount_countOpCall(0,mapi_repl[i].opnum);
		}
	}
	
	return NT_STATUS_OK;
}

static NTSTATUS opCount_ndr_pull(struct dcesrv_call_state *dce_call,
			       TALLOC_CTX *mem_ctx, struct ndr_pull *ndr)
{
	return NT_STATUS_OK;
}

static NTSTATUS opCount_pull(struct dcesrv_call_state *dce_call,
			   TALLOC_CTX *mem_ctx, void *r)
{
	const struct ndr_interface_table	*table;
	uint16_t				opnum;
	
	DEBUG(5,("mapiproxy : opCount_pull\n"));
	table = (const struct ndr_interface_table *)dce_call->context->iface->private;
	opnum = dce_call->pkt.u.request.opnum;
	
	if ((opnum == 0x2) && (table->name && !strcmp(table->name, "exchange_emsmdb"))
		&& (((struct EcDoRpc *)r)->in.mapi_request->length > 2))
	{
		uint32_t i;
		struct EcDoRpc_MAPI_REQ *mapi_req = ((struct EcDoRpc *)r)->in.mapi_request->mapi_req;
		
		for ( i = 0 ; mapi_req && mapi_req[i].opnum != 0x00 ; i++ )
		{
			opCount_countOpCall(1,mapi_req[i].opnum);
		}
	}
	
	return NT_STATUS_OK;
}

static NTSTATUS opCount_dispatch(struct dcesrv_call_state *dce_call,
			       TALLOC_CTX *mem_ctx, void *r,
			       struct mapiproxy *mapiproxy)
{
	const struct ndr_interface_table	*table;
	uint16_t				opnum;
	
	DEBUG(5,("mapiproxy : opCount_dispatch\n"));
	table = (const struct ndr_interface_table *)dce_call->context->iface->private;
	opnum = dce_call->pkt.u.request.opnum;
	
	/*if ((opnum == 0x0) && (table->name && !strcmp(table->name, "exchange_emsmdb")))
	{
		/* EcDoConnect */
	/*	char filename[30];
		uint32_t i;
		
		DEBUG(5,("\tEcDoConnect\n"));
		for ( i = 0 ; i <= 0xff ; i++ )
		{
			sprintf(filename,"%s%x",DIR_NAME_IN,i);
			remove(filename);
			
			sprintf(filename,"%s%x",DIR_NAME_OUT,i);
			remove(filename);
		}
	}
	else*/ 
	if ((opnum == 0x1) && (table->name && !strcmp(table->name, "exchange_emsmdb")))
	{
		/* EcDoDisconnect */
		FILE *file;
		char filename[30];
		uint32_t i;
		
		DEBUG(5,("\tEcDoDisconnect\n"));
		DEBUG(0,("opnum\treq\trepl\n"));
		for ( i = 0 ; i <= 0xff ; i++ )
		{
			uint32_t count_in;
			uint32_t count_out;
			
			sprintf(filename,"%s%x",DIR_NAME_IN,i);
			file = fopen(filename,"r");
			if ( file )
			{
				fscanf(file,"%d",&count_in);
				fclose(file);
			}
			else
			{
				count_in = 0;
			}
			
			sprintf(filename,"%s%x",DIR_NAME_OUT,i);
			file = fopen(filename,"r");
			if ( file )
			{
				fscanf(file,"%d",&count_out);
				fclose(file);
			}
			else
			{
				count_out = 0;
			}
			if ( count_in || count_out )
			{
				DEBUG(0,("0x%.2X : %d\t%d\n",i,count_in,count_out));
			}
		}
	}
	
	return NT_STATUS_OK;
}


NTSTATUS samba_init_module(void)
{
	struct mapiproxy_module	module;
	NTSTATUS		ret;

	/* Fill in our name */
	module.name = "opCount";
	module.description = "opCount MAPIPROXY module";
	module.endpoint = "exchange_emsmdb";

	/* Fill in all the operations */
	module.init = opCount_init;
	module.push = opCount_push;
	module.ndr_pull = opCount_ndr_pull;
	module.pull = opCount_pull;
	module.dispatch = opCount_dispatch;

	/* Register ourselves with the MAPIPROXY subsytem */
	ret = mapiproxy_module_register(&module);
	if (!NT_STATUS_IS_OK(ret)) {
		DEBUG(0, ("Failed to register 'opCount' mapiproxy module!\n"));
		return ret;
	}

	return ret;
}
