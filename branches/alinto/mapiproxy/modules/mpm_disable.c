/*
   MAPI Proxy - Downgrade Module

   OpenChange Project
   
   Copyright (C) Julien Kerihuel 2008
   Copyright (C) Mickael Jeannot 2008

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mapiproxy/dcesrv_mapiproxy.h"
#include "mapiproxy/libmapiproxy.h"

#include <util/debug.h>

#define MPM_DISABLE_ERROR "[Error] mpm_disable: "

NTSTATUS samba_init_module(void);

static struct Opnums {
	uint8_t count;
	uint8_t *ops;
} *opnums = NULL;



static void rebuild_req(TALLOC_CTX *mem_ctx, struct EcDoRpc *r)
{
	uint32_t i, j;
	struct mapi_request *req = r->in.mapi_request;
	struct EcDoRpc_MAPI_REQ mapi_req;
	struct EcDoRpc_MAPI_REQ *new_req = NULL;
	struct ndr_push *ndr_push = talloc_zero(mem_ctx,struct ndr_push);
	struct ndr_pull *ndr_pull = talloc_zero(mem_ctx,struct ndr_pull);
	
	DEBUG(5,("\tin rebuild_req\n"));
	ndr_push->iconv_convenience = smb_iconv_convenience_init(mem_ctx, "CP850", "UTF8", true);
	ndr_pull->iconv_convenience = smb_iconv_convenience_init(mem_ctx, "CP850", "UTF8", true);
	
	mapi_req.opnum = 0x07;
	mapi_req.u.mapi_GetProps.PropertySizeLimit = 0x0020;
	mapi_req.u.mapi_GetProps.WantUnicode = 1;
	mapi_req.u.mapi_GetProps.prop_count = 1;
	mapi_req.u.mapi_GetProps.properties = talloc(mem_ctx,enum MAPITAGS);
	
	for ( i = 0 ; req->mapi_req[i].opnum != 0x00 ; i++ )
	{
		uint8_t found = 0;
		
		for ( j = 0 ; j < opnums->count && !found ; j++ )
		{
			found = (opnums->ops[j] == req->mapi_req[i].opnum);
		}
		if ( found )
		{
			DEBUG(5,("\topnum 0x%.2X, disabled\n",req->mapi_req[i].opnum));
			mapi_req.logon_id = req->mapi_req[i].logon_id;
			mapi_req.handle_idx = req->mapi_req[i].handle_idx;
			mapi_req.u.mapi_GetProps.properties[0] = 0xffffff00 + req->mapi_req[i].opnum;
			ndr_push_EcDoRpc_MAPI_REQ(ndr_push, NDR_SCALARS, &mapi_req);
		}
		else
		{
			DEBUG(5,("\topnum 0x%.2X, enabled\n",req->mapi_req[i].opnum));
			ndr_push_EcDoRpc_MAPI_REQ(ndr_push, NDR_SCALARS, &(req->mapi_req[i]));
		}
	}
	
	if ( ndr_push->offset > 0 )
	{
		uint32_t handle_size;
		
		new_req = talloc_array(mem_ctx, struct EcDoRpc_MAPI_REQ, i+2);
		/*new_req = talloc_memdup(mem_ctx, ndr->data, ndr->offset);*/
		ndr_pull->data_size = ndr_push->offset;
		ndr_pull->data = ndr_push->data;
		for ( i = 0 ; ndr_pull->offset < ndr_pull->data_size ; i++ )
		{
			ndr_pull_EcDoRpc_MAPI_REQ(ndr_pull, NDR_SCALARS, &new_req[i]);
		}
		new_req[i].opnum = 0x00;
		
		handle_size = req->mapi_len - req->length;
		req->length = ndr_push->offset + 2;
		req->mapi_len = req->length + handle_size;
		*(r->in.length) = req->mapi_len;
		talloc_free(req->mapi_req);
		req->mapi_req = new_req;
	}
	
	talloc_free(ndr_push);
	/*talloc_free(ndr_pull);*/
	DEBUG(5,("\tend rebuild_req\n"));
}

static void rebuild_repl(TALLOC_CTX *mem_ctx, struct EcDoRpc *r)
{
	uint32_t i,j;
	struct mapi_response *repl = r->out.mapi_response;
	struct mapi_request *req = r->in.mapi_request;
	struct EcDoRpc_MAPI_REPL mapi_repl;
	struct EcDoRpc_MAPI_REPL *new_repl = NULL;
	struct ndr_push *ndr_push = talloc_zero(mem_ctx,struct ndr_push);
	struct ndr_pull *ndr_pull = talloc_zero(mem_ctx,struct ndr_pull);
	
	mapi_repl.error_code = MAPI_E_NO_SUPPORT;
	
	DEBUG(5,("\tin rebuild_repl\n"));
	ndr_push->iconv_convenience = smb_iconv_convenience_init(mem_ctx, "CP850", "UTF8", true);
	ndr_pull->iconv_convenience = smb_iconv_convenience_init(mem_ctx, "CP850", "UTF8", true);
	
	for ( i = 0, j = 0 ; repl->mapi_repl[i].opnum != 0x00 ; i++, j++ )
	{
		while ( req->length > 2 && req->mapi_req[j].opnum != 0x00 && req->mapi_req[j].opnum != repl->mapi_repl[i].opnum )
		{
			j++;
		}
		if ( repl->mapi_repl[i].opnum == 0x07 && req->length > 2 && req->mapi_req[j].opnum != 0x00 && 
			((req->mapi_req[j].u.mapi_GetProps.properties[0] & 0xffffff00) == 0xffffff00) )
		{
			DEBUG(5,("\topnum 0x%.2X, disabled\n",repl->mapi_repl[i].opnum));
			mapi_repl.opnum = req->mapi_req[j].u.mapi_GetProps.properties[0] & 0x000000ff;
			mapi_repl.handle_idx = repl->mapi_repl[i].handle_idx;
			ndr_push_EcDoRpc_MAPI_REPL(ndr_push, NDR_SCALARS, &mapi_repl);
		}
		else
		{
			DEBUG(5,("\topnum 0x%.2X, enabled\n",repl->mapi_repl[i].opnum));
			ndr_push_EcDoRpc_MAPI_REPL(ndr_push, NDR_SCALARS, &(repl->mapi_repl[i]));
		}
	}
	
	/*DEBUG(3, ("============ ndr =============\n"));
	dump_data(3, ndr_push->data, ndr_push->offset);
	DEBUG(3, ("=====================================\n"));*/
	
	if ( ndr_push->offset > 0 )
	{
		uint32_t handle_size;
		
		new_repl = talloc_array(mem_ctx, struct EcDoRpc_MAPI_REPL, i+2);
		/*new_repl = talloc_memdup(mem_ctx, ndr->data, ndr->offset);*/
		ndr_pull->data_size = ndr_push->offset;
		ndr_pull->data = ndr_push->data;
		for ( i = 0 ; ndr_pull->offset < ndr_pull->data_size ; i++ )
		{
			ndr_pull_EcDoRpc_MAPI_REPL(ndr_pull, NDR_SCALARS, &new_repl[i]);
		}
		new_repl[i].opnum = 0x00;
		
		handle_size = repl->mapi_len - repl->length;
		repl->length = ndr_push->offset + 2;
		repl->mapi_len = repl->length + handle_size;
		*(r->out.length) = repl->mapi_len;
		talloc_free(repl->mapi_repl);
		repl->mapi_repl = new_repl;
	}
	
	talloc_free(ndr_push);
	/*talloc_free(ndr_pull);*/
	DEBUG(5,("\tend rebuild_repl\n"));
}

static NTSTATUS disable_push(struct dcesrv_call_state *dce_call,
			       TALLOC_CTX *mem_ctx, void *r)
{
	const struct ndr_interface_table	*table;
	uint16_t				opnum;
	
	DEBUG(5,("in disable_push\n"));
	table = (const struct ndr_interface_table *)dce_call->context->iface->private;
	opnum = dce_call->pkt.u.request.opnum;
	
	if ((opnum == 0x2) && (table->name && !strcmp(table->name, "exchange_emsmdb")))
	{
		struct mapi_response *repl = ((struct EcDoRpc *)r)->out.mapi_response;
		
		if ( repl->mapi_repl && (repl->length > 2) )
		{
			rebuild_repl(mem_ctx,(struct EcDoRpc *)r);
			
			/*DEBUG(5,("***************** modified struct ********\n"));
			ndr_print_function_debug(table->calls[opnum].ndr_print, table->name, NDR_OUT | NDR_SET_VALUES, r);
			DEBUG(5,("******************************\n"));*/
		}
	}
	
	DEBUG(5,("end disable_push\n"));
	return NT_STATUS_OK;
}


static NTSTATUS disable_ndr_pull(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx, struct ndr_pull *pull)
{
	return NT_STATUS_OK;
}


static NTSTATUS disable_pull(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx, void *r)
{
	const struct ndr_interface_table	*table;
	uint16_t				opnum;
	
	DEBUG(5,("in disable_pull\n"));
	table = (const struct ndr_interface_table *)dce_call->context->iface->private;
	opnum = dce_call->pkt.u.request.opnum;
	
	if ((opnum == 0x2) && (table->name && !strcmp(table->name, "exchange_emsmdb")))
	{
		struct mapi_request *req = ((struct EcDoRpc *)r)->in.mapi_request;
	
		/*DEBUG(5,("***************** initial struct ********\n"));
		ndr_print_function_debug(table->calls[opnum].ndr_print, table->name, NDR_IN | NDR_SET_VALUES, r);
		DEBUG(5,("******************************\n"));*/
	
		if ( req->mapi_req && (req->length > 2) )
		{
			rebuild_req(mem_ctx,(struct EcDoRpc *)r);
		}
	}
	
	DEBUG(5,("end disable_pull\n"));
	return NT_STATUS_OK;
}

static NTSTATUS disable_dispatch(struct dcesrv_call_state *dce_call, TALLOC_CTX *mem_ctx, void *r,
				   struct mapiproxy *mapiproxy)
{
	return NT_STATUS_OK;
}

/**
   \details Initialize the pack module and retrieve configuration from
   smb.conf.

   Possible parameters:
   * mpm_disable:opnums = 0x1, 0x2, 0x3
   
 */
static NTSTATUS disable_init(struct dcesrv_context *dce_ctx)
{
	const char		**calls;
	unsigned long		opnum;
	int			i;
	int			j;
	struct loadparm_context	*lp_ctx;

	/* Fetch the mapi call list from smb.conf */
	calls = str_list_make(dce_ctx, lp_parm_string(dce_ctx->lp_ctx, NULL, "mpm_disable", "opnums"), NULL);

	opnums = talloc_zero(dce_ctx, struct Opnums);
	opnums->count = 0;
	opnums->ops = talloc_zero(opnums, uint8_t);

	for (i = 0; calls[i]; i++) {
		opnum = strtol(calls[i], NULL, 16);
		if (opnum <= 0 || opnum >= 0xFF) {
			DEBUG(0, ("%s: invalid MAPI opnum 0x%.2x\n", MPM_DISABLE_ERROR, (uint32_t)opnum));
			talloc_free(opnums);
			return NT_STATUS_INVALID_PARAMETER;
		}
		/* avoid duplicated opnums */
		for (j = 0; j < i; j++) {
			if (opnum == opnums->ops[j]) {
				DEBUG(0, ("%s: duplicated opnum: 0x%.2x\n", MPM_DISABLE_ERROR, (uint32_t)opnum));
				talloc_free(opnums);
				return NT_STATUS_INVALID_PARAMETER;
			}
		}
		opnums->ops = talloc_realloc(opnums, opnums->ops, uint8_t, i + 2);
		opnums->ops[i] = (uint8_t) opnum;
	}
	opnums->count = i;
	opnums->ops[i] = 0;

	lp_ctx = loadparm_init(dce_ctx);
	lp_load_default(lp_ctx);
	dcerpc_init(lp_ctx);

	return NT_STATUS_OK;
}

/**
   \details Entry point for the downgrade mapiproxy module
   
   \return NT_STATUS_OK on success, otherwise NTSTATUS error
 */
NTSTATUS samba_init_module(void)
{
	struct mapiproxy_module	module;
	NTSTATUS		ret;

	/* Fill in our name */
	module.name = "disable";
	module.description = "Disable EcDoRpc ROPS";
	module.endpoint = "exchange_emsmdb";

	/* Fill in all the operations */
	module.init = disable_init;
	module.push = disable_push;
	module.ndr_pull = disable_ndr_pull;
	module.pull = disable_pull;
	module.dispatch = disable_dispatch;

	/* Register ourselves with the MAPIPROXY subsystem */
	ret = mapiproxy_module_register(&module);
	if (!NT_STATUS_IS_OK(ret)) {
		DEBUG(0, ("Failed to register the 'disable' mapiproxy module!\n"));
		return ret;
	}
	
	return ret;
}
