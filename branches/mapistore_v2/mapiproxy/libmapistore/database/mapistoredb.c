/*
   OpenChange Storage Abstraction Layer library

   OpenChange Project

   Copyright (C) Julien Kerihuel 2010

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mapiproxy/libmapistore/mapistore.h"
#include "mapiproxy/libmapistore/mapistore_errors.h"
#include "mapiproxy/libmapistore/mapistore_private.h"

/**
   \file mapistoredb.c

   \brief Mapistore provisioning API interface
 */

int lpcfg_server_role(struct loadparm_context *lp_ctx);

// TODO: remove this code
#if 0
struct mailbox_system_folders {
	uint32_t	parent_systemindex;
	uint32_t	systemindex;
	const char	*name;
};

static const struct mailbox_system_folders mailbox_system_folders[] = {
	{ 0,	2,	"Deferred Actions"	},
	{ 0,	3,	"Spooler Queue"		},
	{ 0,	4,	"To-Do Search"		},
	{ 0,	5,	"IPM Subtree"		},
	{ 5,	6,	"Inbox"			},
	{ 5,	7,	"Outbox"		},
	{ 5,	8,	"Sent Items"		},
	{ 5,	9,	"Deleted Items"		},
	{ 0,	10,	"Common Views"		},
	{ 0,	11,	"Schedule"		},
	{ 0,	12,	"Search"		},
	{ 0,	13,	"Views"			},
	{ 0,	14,	"Shortcuts"		},
	{ 0,	15,	"Reminders"		},
	{ 0,	0,	NULL			}
};

struct mailbox_special_folders {
	uint32_t	parent_systemindex;
	uint32_t	index;
	char		*name;
	char		*PidType;
	char		*PidTagIpm;
};

static const struct mailbox_special_folders mailbox_special_folders[] = {
	{ MDB_IPM_SUBTREE,	MDB_CALENDAR,	"Calendar",	"IPF.Appointment",	"PidTagIpmAppointmentEntryId"	},
	{ MDB_IPM_SUBTREE,	MDB_CONTACTS,	"Contacts",	"IPF.Contact",		"PidTagIpmContactEntryId"	},
	{ MDB_IPM_SUBTREE,	MDB_JOURNAL,	"Journal",	"IPF.Journal",		"PidTagIpmJournalEntryId"	},
	{ MDB_IPM_SUBTREE,	MDB_NOTES,	"Notes",	"IPF.StickyNote",	"PidTagIpmNoteEntryId"		},
	{ MDB_IPM_SUBTREE,	MDB_TASKS,	"Tasks",	"IPF.Task",		"PidTagIpmTaskEntryId"		},
	{ MDB_IPM_SUBTREE,	MDB_DRAFTS,	"Drafts",	"IPF.Note",		"PidTagIpmDraftsEntryId"	},
	{ 0,			0,		NULL,		NULL,			NULL				}
};
#endif

/**
   \details Initialize the mapistore database context

   \param mem_ctx pointer to the memory context
   \param path string pointer to the mapistore database location

   If path is NULL use the default mapistore db path instead.

   \return allocated mapistore database on success, otherwise NULL
 */
struct mapistoredb_context *mapistoredb_init(TALLOC_CTX *mem_ctx,
					     const char *path)
{
	struct mapistoredb_context	*mdb_ctx;
	char				**domaindn;
	char				*full_path;
	struct stat			sb;
	int				i;
	int				ret;

	/* Sanity checks */
	if (path == NULL) {
		path = MAPISTORE_DBPATH;
	}

	/* Ensure the path is valid */
	if (stat(path, &sb) == -1) {
		perror(path);
		return NULL;
	}

	/* Step 1. Initialize mapistoredb context */
	mdb_ctx = talloc_zero(mem_ctx, struct mapistoredb_context);
	mdb_ctx->param = talloc_zero(mem_ctx, struct mapistoredb_conf);

	/* Step 2. Initialize Samba loadparm context and load default values */
	mdb_ctx->lp_ctx = loadparm_init(mdb_ctx);
	lpcfg_load_default(mdb_ctx->lp_ctx);

	/* Step 3. Initialize tevent structure */
	mdb_ctx->ev = tevent_context_init(mdb_ctx);

	/* Step 4. Open a wrapped connection on the mapistore database */
	full_path = talloc_asprintf(mem_ctx, "%s/mapistore.ldb", path);
	mdb_ctx->ldb_ctx = mapistore_ldb_wrap_connect(mdb_ctx, mdb_ctx->ev, full_path, 0);
	talloc_free(full_path);
	if (!mdb_ctx->ldb_ctx) {
		talloc_free(mdb_ctx);
		return NULL;
	}

	/* Step 5. Retrieve default values from smb.conf */
	mdb_ctx->param->netbiosname = strlower_talloc(mdb_ctx->param, lpcfg_netbios_name(mdb_ctx->lp_ctx));
	mdb_ctx->param->dnsdomain = strlower_talloc(mdb_ctx->param, lpcfg_realm(mdb_ctx->lp_ctx));
	mdb_ctx->param->domain = strlower_talloc(mdb_ctx->param, lpcfg_sam_name(mdb_ctx->lp_ctx));

	switch (lpcfg_server_role(mdb_ctx->lp_ctx)) {
	case ROLE_DOMAIN_CONTROLLER:
		domaindn = str_list_make(mdb_ctx->param, mdb_ctx->param->dnsdomain, ".");
		strlower_m(domaindn[0]);
		mdb_ctx->param->domaindn = talloc_asprintf(mdb_ctx->param, "DC=%s", domaindn[0]);
		for (i = 1; domaindn[i]; i++) {
			strlower_m(domaindn[i]);
			mdb_ctx->param->domaindn = talloc_asprintf_append_buffer(mdb_ctx->param->domaindn, ",DC=%s", domaindn[i]);
		}
		talloc_free(domaindn);
		break;
	default:
		mdb_ctx->param->domaindn = talloc_asprintf(mdb_ctx->param, "CN=%s", mdb_ctx->param->domain);
		break;
	}

	mdb_ctx->param->serverdn = talloc_asprintf(mdb_ctx->param, TMPL_MDB_SERVERDN, 
						   mdb_ctx->param->netbiosname,
						   mdb_ctx->param->domaindn);
	mdb_ctx->param->firstorg = talloc_strdup(mdb_ctx->param, DFLT_MDB_FIRSTORG);
	mdb_ctx->param->firstou = talloc_strdup(mdb_ctx->param, DFLT_MDB_FIRSTOU);
	mdb_ctx->param->firstorgdn = talloc_asprintf(mdb_ctx->param, TMPL_MDB_FIRSTORGDN,
						     mdb_ctx->param->firstou, 
						     mdb_ctx->param->firstorg,
						     mdb_ctx->param->serverdn);
	mdb_ctx->param->db_path = talloc_asprintf(mdb_ctx->param, "%s/mapistore.ldb", path);
	mdb_ctx->param->mstore_path = talloc_asprintf(mdb_ctx->param, "%s/mapistore", path);

	/* Step 6. Initialize mapistore */
	if (stat(mdb_ctx->param->mstore_path, &sb) == -1) {
		ret = mkdir(mdb_ctx->param->mstore_path, 0700);
		if (ret == -1) {
			perror(mdb_ctx->param->mstore_path);
			talloc_free(mdb_ctx);
			return NULL;
		}
	}

	mapistore_set_database_path(mdb_ctx->param->db_path);
	mapistore_set_mapping_path(mdb_ctx->param->mstore_path);
	mdb_ctx->mstore_ctx = mapistore_init(mdb_ctx, NULL);

	return mdb_ctx;
}


/**
 \detail Free a mapistoredb_context
 
 \param mdb_ctx the context to free (from mapistoredb_init())
*/
void mapistoredb_release(struct mapistoredb_context *mdb_ctx)
{
	talloc_free(mdb_ctx->lp_ctx);
	talloc_free(mdb_ctx->param);
	talloc_free(mdb_ctx);
}

/* TODO: this is a copy of code in mapistore_mstoredb.c */
static bool write_ldif_string_to_store(struct ldb_context *ldb_ctx, const char *ldif_string)
{
	struct ldb_ldif	*ldif;
	while ((ldif = ldb_ldif_read_string(ldb_ctx, (const char **)&ldif_string))) {
		int ret;
		ret = ldb_msg_normalize(ldb_ctx, ldif, ldif->msg, &ldif->msg);
		if (ret != LDB_SUCCESS) {
			ldb_ldif_read_free(ldb_ctx, ldif);
			return false;
		}
		ret = ldb_add(ldb_ctx, ldif->msg);
		if (ret != LDB_SUCCESS) {
			ldb_ldif_read_free(ldb_ctx, ldif);
			return false;
		}
		ldb_ldif_read_free(ldb_ctx, ldif);
	}
	return true;
}

/**
   \details Default provisioning for mapistore.ldb database

   \param mdb_ctx pointer to the mapistore database context
   
   \return MAPISTORE_SUCCESS on success, otherwise a non-zero MAPISTORE_ERROR
 */
enum MAPISTORE_ERROR mapistoredb_provision(struct mapistoredb_context *mdb_ctx)
{
	char		*ldif_str;

	/* Sanity checks */
	if (!mdb_ctx || !mdb_ctx->ldb_ctx) return MAPISTORE_ERR_NOT_INITIALIZED;
	
	/* Step 1. Add database schema */
	if ( ! write_ldif_string_to_store(mdb_ctx->ldb_ctx, MDB_INIT_LDIF_TMPL)) {
		return MAPISTORE_ERR_DATABASE_OPS;
	}

	/* Step 2. Add RootDSE schema */
	ldif_str = talloc_asprintf(mdb_ctx, MDB_ROOTDSE_LDIF_TMPL, 
				   mdb_ctx->param->firstou,
				   mdb_ctx->param->firstorg,
				   mdb_ctx->param->serverdn,
				   mdb_ctx->param->serverdn);
	if ( ! write_ldif_string_to_store(mdb_ctx->ldb_ctx, ldif_str)) {
		talloc_free(ldif_str);
		return MAPISTORE_ERR_DATABASE_OPS;
	}
	talloc_free(ldif_str);

	/* Step 3. Provision Server object responsible for
	 * maintaining the Replica identifier */
	ldif_str = talloc_asprintf(mdb_ctx, MDB_SERVER_LDIF_TMPL,
				   mdb_ctx->param->serverdn,
				   mdb_ctx->param->netbiosname,
				   mdb_ctx->param->firstorg,
				   mdb_ctx->param->serverdn,
				   mdb_ctx->param->firstorg,
				   mdb_ctx->param->firstou,
				   mdb_ctx->param->firstorg,
				   mdb_ctx->param->serverdn,
				   mdb_ctx->param->firstou);
	if ( ! write_ldif_string_to_store(mdb_ctx->ldb_ctx, ldif_str)) {
		talloc_free(ldif_str);
		return MAPISTORE_ERR_DATABASE_OPS;
	}
	talloc_free(ldif_str);

	return MAPISTORE_SUCCESS;
}

/**
   \details Top level provisioning for public folders

   \param mdb_ctx pointer to the mapistore database context
   \param context_id context identifier for the (newly created) public folder root
   
   \return MAPISTORE_SUCCESS on success, otherwise a non-zero MAPISTORE_ERROR
 */
enum MAPISTORE_ERROR mapistoredb_provision_publicfolders(struct mapistoredb_context *mdb_ctx, uint32_t *context_id)
{
	char			*ldif_str;
	struct GUID		guid;
	char			*mailboxURI;
	char			*storeGUID;
	uint32_t		contextID = 0;
	enum MAPISTORE_ERROR	ret;
	uint64_t 		fid;

	/* create a GUID for the root public folder */
	guid = GUID_random();
	storeGUID = GUID_string(mdb_ctx, &guid);

	/* Request a new folder identifier for the root pf*/
	ret = mapistore_get_next_fmid(mdb_ctx->mstore_ctx, &fid);
	if (ret) {
		return MAPISTORE_ERROR;
	}

	ldif_str = talloc_asprintf(mdb_ctx, MDB_PF_ROOT_LDIF_TMPL,
				   mdb_ctx->param->firstorgdn,
				   storeGUID,
				   "1",
				   "PublicFolders", mdb_ctx->param->firstorgdn,
				   fid);
	talloc_free(storeGUID);

	if ( ! write_ldif_string_to_store(mdb_ctx->ldb_ctx, ldif_str)) {
		talloc_free(ldif_str);
		return MAPISTORE_ERR_DATABASE_OPS;
	}
	talloc_free(ldif_str);
	
	/* Register the public folder root within the indexing database */
	mailboxURI = talloc_asprintf(mdb_ctx, MDB_MAILBOX_TMPL_URI,
				     "PublicFolders", mdb_ctx->param->firstorgdn);

	ret = mapistore_add_context(mdb_ctx->mstore_ctx, "PublicFolders", mailboxURI, &contextID);
	if (ret != MAPISTORE_SUCCESS) {
		talloc_free(mailboxURI);
		return ret;
	}

	ret = mapistore_indexing_record_add_fid(mdb_ctx->mstore_ctx, "PublicFolders", fid, 
						mailboxURI, 0x0);
	talloc_free(mailboxURI);

	if (ret != MAPISTORE_SUCCESS) {
		return ret;
	}

	*context_id = contextID;

	return MAPISTORE_SUCCESS;
}

/**
   \details Create the mapistore named properties database

   \param mdb_ctx pointer to the mapistore database context

   \return MAPISTORE_SUCCESS on success, otherwise a non-zero MAPISTORE_ERROR
 */
enum MAPISTORE_ERROR mapistoredb_provision_namedprops(struct mapistoredb_context *mdb_ctx)
{
	/* Sanity checks */
	if (!mdb_ctx || !mdb_ctx->mstore_ctx) return MAPISTORE_ERR_NOT_INITIALIZED;

	return mapistore_namedprops_provision(mdb_ctx->mstore_ctx);
}

/**
   \details Create the mapistore database root mailbox container for
   given user

   \param mdb_ctx pointer to the mapistore database context
   \param username pointer to the username of the mailbox to create
   \param _contextID pointer to the context identifier for the created
   mailbox root

   \return MAPISTORE_SUCCESS on success, otherwise a non-zero MAPISTORE_ERROR
 */
enum MAPISTORE_ERROR mapistoredb_new_mailbox(struct mapistoredb_context *mdb_ctx, 
					     const char *username, 
					     uint32_t *_contextID)
{
	const char		*mailbox_ldif_tmpl;
	char			*mailbox_ldif;
	struct ldb_result	*res = NULL;
	const char * const	recipient_attrs[] = { "*", NULL };
	uint64_t		mailboxFID = 0;
	char			*mailboxGUID;
	char			*replicaGUID;
	char			*mailboxURI;
	struct GUID		guid;
	enum MAPISTORE_ERROR	ret;
	uint32_t		contextID = 0;

	/* Sanity checks */
	if (!mdb_ctx || !mdb_ctx->ldb_ctx) return MAPISTORE_ERR_NOT_INITIALIZED;
	if (!username) return MAPISTORE_ERR_INVALID_PARAMETER;

	/* Set ldif template data */
	mailbox_ldif_tmpl = MDB_MAILBOX_TMPL;

	/* Step 1. Does the user mailbox CN entry already exist */
	ret = ldb_search(mdb_ctx->ldb_ctx, mdb_ctx, &res,
			 ldb_get_default_basedn(mdb_ctx->ldb_ctx),
			 LDB_SCOPE_SUBTREE, recipient_attrs, 
			 "(&(objectClass=mailbox)(cn=%s))", username);
	if (ret == LDB_SUCCESS && res->count == 1) {
		return MAPISTORE_ERR_EXIST;
	}

	/* Step 2. Set mailbox parameters */
	guid = GUID_random();
	mailboxGUID = GUID_string(mdb_ctx, &guid);

	guid = GUID_random();
	replicaGUID = GUID_string(mdb_ctx, &guid);

	/* Step 3. Request a new folder identifier */
	ret = mapistore_get_next_fmid(mdb_ctx->mstore_ctx, &mailboxFID);
	if (ret) {
		return MAPISTORE_ERROR;
	}

	/* Step 3. Create the user mailbox root container */
	mailbox_ldif = talloc_asprintf(mdb_ctx, mailbox_ldif_tmpl,
				       username, mdb_ctx->param->firstorgdn,
				       username, mailboxFID, username, mailboxGUID, 
				       "1", replicaGUID, username, 
				       mdb_ctx->param->firstorgdn);
	talloc_free(mailboxGUID);
	talloc_free(replicaGUID);
	if ( ! write_ldif_string_to_store(mdb_ctx->ldb_ctx, mailbox_ldif)) {
		talloc_free(mailbox_ldif);
		return MAPISTORE_ERR_DATABASE_OPS;
	}
	talloc_free(mailbox_ldif);

	/* Our default virtual backend is mstoredb://. We will
	 * probably need to change the API in the future to allow
	 * usage of different backends at the provisioning stage for
	 * mapistore dispatcher database */

	/* Step 4. Register the mailbox DN / FID within the indexing database */
	mailboxURI = talloc_asprintf(mdb_ctx, MDB_MAILBOX_TMPL_URI,
				     username, mdb_ctx->param->firstorgdn);

	ret = mapistore_add_context(mdb_ctx->mstore_ctx, username, mailboxURI, &contextID);
	if (ret != MAPISTORE_SUCCESS) {
		talloc_free(mailboxURI);
		return ret;
	}

	ret = mapistore_indexing_record_add_fid(mdb_ctx->mstore_ctx, username, mailboxFID, 
						mailboxURI, 0x0);
	talloc_free(mailboxURI);

	if (ret != MAPISTORE_SUCCESS) {
		return ret;
	}

	*_contextID = contextID;

	return MAPISTORE_SUCCESS;
}

#if 0
/**
   \details Add a root folder to user mailbox
   
   \param mdb_ctx pointer to the mapistore database context
   \param contextID the context identifier for this mailbox
   \param index the folder index as defined in mapistore.h
   \param name the name of the folder
   \param mapistore_uri the uri namespace with associated parameters

   \return MAPISTORE_SUCCESS on success, otherwise a non-zero MAPISTORE_ERROR
 */
enum MAPISTORE_ERROR mapistoredb_add_mailbox_folder(struct mapistoredb_context *mdb_ctx,
						    uint32_t contextID, uint32_t index,
						    const char *folder_name, 
						    const char *mapistore_uri)
{
	int			ret;
	struct ldb_result	*res = NULL;
	const char * const	recipient_attrs[] = { "*", NULL };
	const char		*mailbox_dn;
	const char		*mailbox_uri;
	int			i;
	uint64_t		fid = 0;
	char			*uri;
	bool			found = false;

	if (!mdb_ctx || !mdb_ctx->ldb_ctx || !mdb_ctx->param) return MAPISTORE_ERR_NOT_INITIALIZED;
	if (!contextID || !mapistore_uri) return MAPISTORE_ERR_INVALID_PARAMETER;

	ret = mapistore_root_mkdir(mdb_ctx->mstore_ctx, contextID, index, folder_name, mapistore_uri);
	DEBUG(0, ("ret = %d\n", ret));
/* 	ret = mapistore_create_mapistoreURI(mdb_ctx->mstore_ctx, mailbox_uri, &fid, &uri); */

/* 	DEBUG(0, ("=================\n")); */
/* 	ret = mapistore_indexing_record_add_fid(mdb_ctx->mstore_ctx, username, fid, uri, 0); */
/* 	DEBUG(0, ("================= (ret = %d)\n", ret)); */

/* 	/\* Step 2. Retrieve the mailbox DN *\/ */
/* 	mailbox_dn = ldb_msg_find_attr_as_string(res->msgs[0], "distinguishedName", NULL); */
/* 	if (!mailbox_dn) return -1; */

/* 	switch (index) { */
/* 	case MDB_DEFERRED_ACTIONS: */
/* 	case MDB_SPOOLER_QUEUE: */
/* 	case MDB_TODO_SEARCH: */
/* 	case MDB_IPM_SUBTREE: */
/* 	case MDB_COMMON_VIEWS: */
/* 	case MDB_SCHEDULE: */
/* 	case MDB_SEARCH: */
/* 	case MDB_VIEWS: */
/* 	case MDB_SHORTCUTS: */
/* 	case MDB_REMINDERS: */
/* 		/\* Step 1. Find the system folder record *\/ */
/* 		for (i = 0; mailbox_system_folders[i].name; i++) { */
/* 			if (mailbox_system_folders[i].systemindex == index) { */
/* 				found = true; */
/* 				break; */
/* 			} */
/* 		} */
/* 		if (found == false) { */
/* 			return -1; */
/* 		} */

/* 		/\* Step 2. Generate a FID *\/ */
/* 		/\* ret = mapistoredb_get_new_fid_from_mailbox(mdb_ctx, username, &fid); *\/ */

/* 		/\* Step 3. Retrieve final mapistore URI *\/ */
/* 		ret = mapistore_create_mapistoreURI(mdb_ctx->mstore_ctx, mapistore_uri, &fid, &uri); */
/* 		if (ret) return ret; */

/* 		DEBUG(0, ("ret = %d\n", ret)); */
/* 		DEBUG(0, ("uri = %s\n", uri)); */

/* 		/\* Step 4. Add the skeleton record to database *\/ */
/* 		/\* folder_ldif = talloc_asprintf(mdb_ctx, MDB_MAILBOX_SYSTEM_FOLDER_TMPL, *\/ */
/* 		/\* 			      fid, fid, 0, mailbox_system_folders[i].systemindex); *\/ */
		
/* 		/\* Step 5. Instantiate the backend and add properties through mapistore *\/ */
/* 		break; */
/* 	case MDB_INBOX: */
/* 	case MDB_OUTBOX: */
/* 	case MDB_SENT_ITEMS: */
/* 	case MDB_DELETED_ITEMS: */
/* 	case MDB_CALENDAR: */
/* 	case MDB_CONTACTS: */
/* 	case MDB_JOURNAL: */
/* 	case MDB_NOTES: */
/* 	case MDB_TASKS: */
/* 	case MDB_DRAFTS: */
/* 		/\* Instantiate IPM Subtree mapistore context *\/ */
/* 		/\* Call mkdir routine for the folder using mstoredb backend *\/ */
/* 		/\* Set the mapistore URI for the new folder (ask the backend for the final one) *\/ */
/* 		/\* Instantiate the new folder mapistore context *\/ */
/* 		/\* Set associated properties *\/ */
/* 		break; */
/* 	default: */
/* 		DEBUG(0, ("Invalid index\n")); */
/* 		return -1; */
/* 	} */

/* 	mapistore_del_context(mdb_ctx->mstore_ctx, context_id); */

	return MAPISTORE_SUCCESS;
}
#endif
