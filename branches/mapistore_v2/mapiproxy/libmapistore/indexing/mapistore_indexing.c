/*
   OpenChange Storage Abstraction Layer library

   OpenChange Project

   Copyright (C) Julien Kerihuel 2010

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>

#include "mapiproxy/libmapistore/mapistore.h"
#include "mapiproxy/libmapistore/mapistore_private.h"
#include "mapiproxy/libmapistore/mapistore_errors.h"
#include "gen_ndr/ndr_mapistore_indexing_db.h"

#include <dlinklist.h>
#include <tdb.h>

/**
   \file mapistore_indexing.c

   \brief Interface API to the mapistore indexing database
   
   This database is used to map FID/MID to mapistore URI, plus additional parameters
 */


/**
   \details Increase the reference counter associated to a given
   mapistore indexing context

   \param mictx pointer to the mapistore indexing context
   
   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE_ERROR
 */
static int mapistore_indexing_context_add_ref_count(struct mapistore_indexing_context_list *mictx)
{
	MAPISTORE_RETVAL_IF(!mictx, MAPISTORE_ERROR, NULL);
	DEBUG(2, ("[%s] ref count was %i, about to increment\n", __FUNCTION__, mictx->ref_count));
	mictx->ref_count += 1;

	return MAPISTORE_SUCCESS;
}

/**
   \details Decrease the reference counter associated to a given
   mapistore indexing context

   \param mictx pointer to the mapistore indexing context

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE_ERROR
 */
static int mapistore_indexing_context_del_ref_count(struct mapistore_indexing_context_list *mictx)
{
	MAPISTORE_RETVAL_IF(!mictx, MAPISTORE_ERROR, NULL);
	MAPISTORE_RETVAL_IF(!mictx->ref_count, MAPISTORE_SUCCESS, NULL);
	DEBUG(2, ("[%s] ref count was %i, about to decrement\n", __FUNCTION__, mictx->ref_count));
	mictx->ref_count -= 1;

	return MAPISTORE_SUCCESS;
}


/**
   \details (Accessor) Retrieve the reference counter associated to a
   given mapistore indexing context

   \param mictx pointer to the mapistore indexing context

   \return the reference counter value associated to the mapistore indexing context
 */
static int mapistore_indexing_context_get_ref_count(struct mapistore_indexing_context_list *mictx)
{
	return mictx->ref_count;
}


/**
   \details Search the mapistore indexing context list for an existing
   record matching the specified username

   \param mstore_ctx pointer to the mapistore context
   \param username the username to lookup

   \return pointer to 
 */
static struct mapistore_indexing_context_list *mapistore_indexing_context_search(struct mapistore_context *mstore_ctx,
										 const char *username)
{
	struct mapistore_indexing_context_list	*el;

	/* Sanity checks */
	if (!mstore_ctx || !username) return NULL;

	for (el = mstore_ctx->indexing_list; el; el = el->next) {
		if (el && el->username && !strcmp(el->username, username)) {
			return el;
		}
	}
	
	return NULL;
}

/**
   \details Open a connection context to the indexing database for
   given user

   \param mstore_ctx pointer to the mapistore context
   \param username username of the indexing database owner
   \param indexing_ctx pointer on pointer to the indexing context to return

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
 */
int mapistore_indexing_context_add(struct mapistore_context *mstore_ctx,
				   const char *username,
				   struct mapistore_indexing_context_list **indexing_ctx)
{
	TALLOC_CTX				*mem_ctx;
	struct mapistore_indexing_context_list	*mictx;
	char					*dbpath;

	/* Sanity checks */
	MAPISTORE_RETVAL_IF(!mstore_ctx, MAPISTORE_ERR_NOT_INITIALIZED, NULL);
	MAPISTORE_RETVAL_IF(!username, MAPISTORE_ERR_INVALID_PARAMETER, NULL);
	MAPISTORE_RETVAL_IF(!indexing_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 1. Return existing context if available */
	mictx = mapistore_indexing_context_search(mstore_ctx, username);
	if (mictx) {
		DEBUG(2, ("[%s] reusing existing context\n", __FUNCTION__));
		mapistore_indexing_context_add_ref_count(mictx);
		*indexing_ctx = mictx;
		return MAPISTORE_SUCCESS;
	}

	/* Step 2. Otherwise create a new context */
	mem_ctx = talloc_named(NULL, 0, "mapistore_indexing_context_add");
	mictx = talloc_zero(mstore_ctx->indexing_list, struct mapistore_indexing_context_list);

	dbpath = talloc_asprintf(mem_ctx, MAPISTORE_INDEXING_DBPATH_TMPL,
				 mapistore_get_mapping_path(), username);
	mictx->tdb_ctx = tdb_wrap_open(mictx, dbpath, 0, 0, O_RDWR|O_CREAT, 0600);
	talloc_free(dbpath);

	if (!mictx->tdb_ctx) {
		DEBUG(3, ("[%s:%d]: %s\n", __FUNCTION__, __LINE__, strerror(errno)));
		talloc_free(mictx);
		talloc_free(mem_ctx);

		return MAPISTORE_ERR_DATABASE_INIT;
	}

	mictx->username = (const char *) talloc_strdup(mictx, username);
	mictx->ref_count = 1;
	DEBUG(2, ("[%s] indexing context value is now %i\n", __FUNCTION__, mictx->ref_count));
	DLIST_ADD_END(mstore_ctx->indexing_list, mictx, struct mapistore_indexing_context_list *);

	*indexing_ctx = mictx;

	talloc_free(mem_ctx);

	return MAPISTORE_SUCCESS;
}

int mapistore_indexing_context_add_ref(struct mapistore_context *mstore_ctx, 
				       const char *username)
{
	struct mapistore_indexing_context_list	*mictx;

	/* Sanity checks */
	MAPISTORE_RETVAL_IF(!mstore_ctx, MAPISTORE_ERR_NOT_INITIALIZED, NULL);
	MAPISTORE_RETVAL_IF(!username, MAPISTORE_ERROR, NULL);

	/* Search for the context */
	mictx = mapistore_indexing_context_search(mstore_ctx, username);
	MAPISTORE_RETVAL_IF(!mictx, MAPISTORE_ERROR, NULL);

	/* Increment the reference counter */
	mapistore_indexing_context_add_ref_count(mictx);

	return MAPISTORE_SUCCESS;
}

/**
   \details Delete indexing database context for given user

   \param mstore_ctx pointer to the mapistore context
   \param username username of the indexing database owner

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
 */
int mapistore_indexing_context_del(struct mapistore_context *mstore_ctx,
				   const char *username)
{
	struct mapistore_indexing_context_list	*mictx;

	/* Sanity checks */
	MAPISTORE_RETVAL_IF(!mstore_ctx, MAPISTORE_ERR_NOT_INITIALIZED, NULL);
	MAPISTORE_RETVAL_IF(!username, MAPISTORE_ERROR, NULL);

	/* Step 1. Search for the context */
	mictx = mapistore_indexing_context_search(mstore_ctx, username);
	MAPISTORE_RETVAL_IF(!mictx, MAPISTORE_ERROR, NULL);

	/* Decrease the reference counter */
	mapistore_indexing_context_del_ref_count(mictx);

	if (mapistore_indexing_context_get_ref_count(mictx)) {
		/* If we still have ref counts, just return */
		return MAPISTORE_SUCCESS;
	} else {
		/* If no more associated references, remove and release memory */
		DLIST_REMOVE(mstore_ctx->indexing_list, mictx);
		talloc_free(mictx);
	}

	return MAPISTORE_SUCCESS;
}


static int mapistore_indexing_record_search_fmid(struct mapistore_indexing_context_list *mictx,
						 uint64_t fmid)
{
	int		ret;
	TDB_DATA	key;

	/* Sanity checks */
	MAPISTORE_RETVAL_IF(!mictx, MAPISTORE_ERROR, NULL);

	key.dptr = (unsigned char *) talloc_asprintf(mictx, "FMID/0x%.16"PRIx64, fmid);
	key.dsize = strlen((const char *)key.dptr);
	
	ret = tdb_exists(mictx->tdb_ctx->tdb, key);
	talloc_free(key.dptr);

	MAPISTORE_RETVAL_IF(ret, MAPISTORE_ERR_EXIST, NULL);

	return MAPISTORE_SUCCESS;
}


/**
   \details Retrieve the mapistore_indexing_entry structure associated
   to a folder or message identifier

   \param mstore_ctx pointer to the mapistore context
   \param username the owner of the indexing database
   \param fmid the folder or message identifier to lookup
   \param entry pointer to the record entry to return on success
   
   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
 */
static int mapistore_indexing_record_get_entry(struct mapistore_context *mstore_ctx,
					       const char *username, uint64_t fmid,
					       struct mapistore_indexing_entry *entry)
{
	struct mapistore_indexing_context_list 	*mictx;
	TDB_DATA				key;
	TDB_DATA				value;
	DATA_BLOB				data;
	enum ndr_err_code			ndr_err;

	/* Sanity checks */
	MAPISTORE_RETVAL_IF(!mstore_ctx, MAPISTORE_ERROR, NULL);
	MAPISTORE_RETVAL_IF(!username, MAPISTORE_ERROR, NULL);
	MAPISTORE_RETVAL_IF(!fmid, MAPISTORE_ERROR, NULL);
	MAPISTORE_RETVAL_IF(!entry, MAPISTORE_ERROR, NULL);

	/* find the mapistore index store for this user */
	mictx = mapistore_indexing_context_search(mstore_ctx, username);
	if ( ! mictx) {
		DEBUG(0, ("[%s] Failed lookup for indexing context for user: %s\n", __FUNCTION__, username));
		return MAPISTORE_ERR_NOT_INITIALIZED;
	}

	/* look up the FID in the indexing store */
	DEBUG(7, ("[%s] Looking for FID: 0x%016"PRIx64"\n", __FUNCTION__, fmid));
	key.dptr = (unsigned char*)talloc_asprintf(mictx, "FMID/0x%016"PRIx64, fmid);
	key.dsize = strlen((const char*)key.dptr);
	
	value = tdb_fetch(mictx->tdb_ctx->tdb, key);
	talloc_free(key.dptr);
	/* TODO: check return from fetch() */

	data.data = value.dptr;
	data.length = value.dsize;

	/* convert to mapistore_indexing_entry */
	ndr_err = ndr_pull_struct_blob(&data, mictx, entry, (ndr_pull_flags_fn_t)ndr_pull_mapistore_indexing_entry);
	if (!NDR_ERR_CODE_IS_SUCCESS(ndr_err)) {
		talloc_free(value.dptr);
		return MAPISTORE_ERROR;
	}
	
	DEBUG(5, ("[%s], entry uri: %s, type: 0x%x, parent: 0x%016"PRIx64"\n", __FUNCTION__,
									       entry->info.mapistore_indexing_v1.mapistoreURI,
									       entry->info.mapistore_indexing_v1.Type,
									       entry->info.mapistore_indexing_v1.ParentFolderID));

	return MAPISTORE_SUCCESS;
}


/**
   \details Add a folder or message record to the indexing database

   \param mictx pointer to the mapistore indexing context
   \param fmid the Folder or Message identifier to register
   \param mapistore_uri the mapistore URI to register
   \param parent_fid the parent folder identifier
   \param type the element type to add

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
 */
_PUBLIC_ int mapistore_indexing_record_add_fmid(struct mapistore_indexing_context_list *mictx,
						uint64_t fmid,
					        const char *mapistore_uri,
					        uint64_t parent_fid,
						uint8_t type)
{
	TALLOC_CTX				*mem_ctx;
	int					ret;
	struct mapistore_indexing_entry		entry;
	DATA_BLOB				data;
	enum ndr_err_code			ndr_err;
	TDB_DATA				key;
	TDB_DATA				dbuf;

	/* Sanity checks */

	MAPISTORE_RETVAL_IF(!fmid, MAPISTORE_ERROR, NULL);

	/* Check if the fid/mid already exists */
	ret = mapistore_indexing_record_search_fmid(mictx, fmid);
	MAPISTORE_RETVAL_IF(ret, ret, NULL);

	/* Create the key/dbuf record */
	mem_ctx = talloc_new(mictx);
	key.dptr = (unsigned char *) talloc_asprintf(mem_ctx, "FMID/0x%.16"PRIx64, fmid);
	key.dsize = strlen((const char *)key.dptr);

	entry.version = 1;
	entry.info.mapistore_indexing_v1.mapistoreURI = mapistore_uri;
	entry.info.mapistore_indexing_v1.Type = type;
	entry.info.mapistore_indexing_v1.ParentFolderID = parent_fid;
	ndr_err = ndr_push_struct_blob(&data, mem_ctx, &entry, (ndr_push_flags_fn_t)ndr_push_mapistore_indexing_entry);
	if (!NDR_ERR_CODE_IS_SUCCESS(ndr_err)) {
		talloc_free(mem_ctx);
		return MAPISTORE_ERROR;
	}

	dbuf.dptr = data.data;
	dbuf.dsize = data.length;

	/* Insert the FMID/ record */
	ret = tdb_store(mictx->tdb_ctx->tdb, key, dbuf, TDB_INSERT);

	if (ret == -1) {
		DEBUG(3, ("[%s:%d]: Unable to create FMID/0x%.16"PRIx64" record: %s\n", 
			  __FUNCTION__, __LINE__, fmid, mapistore_uri));
		talloc_free(mem_ctx);
		return MAPISTORE_ERR_DATABASE_OPS;
	}

	/* Insert the reverse record URI/ reverse record */
	key.dptr = (unsigned char *) talloc_asprintf(mem_ctx, "URI/%s", mapistore_uri);
	key.dsize = strlen((const char *)key.dptr);

	dbuf.dptr = (unsigned char *)talloc_asprintf(mem_ctx, "FMID/0x%.16"PRIx64, fmid);
	dbuf.dsize = strlen((const char *)dbuf.dptr);

	ret = tdb_store(mictx->tdb_ctx->tdb, key, dbuf, TDB_INSERT);
	talloc_free(mem_ctx);

	if (ret == -1) {
		DEBUG(3, ("[%s:%d]: Unable to create URI/%s record: FMID/0x%.16"PRIx64,
			  __FUNCTION__, __LINE__, mapistore_uri, fmid));
		return MAPISTORE_ERR_DATABASE_OPS;
	}

	return MAPISTORE_SUCCESS;
}


/**
   \details Find a URI given the folder identifier (for a for specified user)

   \param mstore_ctx pointer to the mapistore context
   \param username the owner of the indexing database
  
   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
*/
static int mapistore_indexing_lookup_uri(struct mapistore_context *mstore_ctx,
					 const char *username, const uint64_t fmid,
					 char **uri)
{
	struct mapistore_indexing_entry		entry;

	/* sanity checks */
	MAPISTORE_RETVAL_IF(!mstore_ctx, MAPISTORE_ERR_NOT_INITIALIZED, NULL);
	MAPISTORE_RETVAL_IF(!username, MAPISTORE_ERR_INVALID_PARAMETER, NULL);
	MAPISTORE_RETVAL_IF(!uri, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* lookup entry by FID */
	mapistore_indexing_record_get_entry(mstore_ctx, username, fmid, &entry);

	/* parse the uri out of the entry */
	*uri = talloc_strdup(mstore_ctx, entry.info.mapistore_indexing_v1.mapistoreURI);
	
	DEBUG(7, ("0x%016"PRIx64" mapped to %s\n", fmid, *uri));

	return MAPISTORE_SUCCESS;
}


/**
   \details Delete a folder or message record from the indexing database

   \param mstore_ctx pointer to the mapistore context
   \param username the owner of the indexing database
   \param fmid the Folder or Message identifier to delete
   \param flags the type of deletion: soft delete or permanent

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
 */
static int mapistore_indexing_record_del_fmid(struct mapistore_context *mstore_ctx,
					      const char *username, uint64_t fmid,
					      uint8_t flags)
{
	TALLOC_CTX				*mem_ctx;
	int					ret;
	struct mapistore_indexing_context_list	*mictx;
	struct mapistore_indexing_entry		entry;
	DATA_BLOB				data;
	TDB_DATA				key;
	TDB_DATA				dbuf;
	enum ndr_err_code			ndr_err;

	/* Sanity checks */
	MAPISTORE_RETVAL_IF(!mstore_ctx, MAPISTORE_ERROR, NULL);
	MAPISTORE_RETVAL_IF(!username, MAPISTORE_ERROR, NULL);
	MAPISTORE_RETVAL_IF(!fmid, MAPISTORE_ERROR, NULL);
	MAPISTORE_RETVAL_IF((flags != MAPISTORE_SOFT_DELETE) && 
			    (flags != MAPISTORE_PERMANENT_DELETE), 
			    MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 1. Retrieve the indexing context associated to the username */
	mictx = mapistore_indexing_context_search(mstore_ctx, username);
	MAPISTORE_RETVAL_IF(!mictx, MAPISTORE_ERROR, NULL);

	/* Step 2. Ensure the fid/mid still exists within the database */
	ret = mapistore_indexing_record_search_fmid(mictx, fmid);
	MAPISTORE_RETVAL_IF(!ret, MAPISTORE_ERROR, NULL);

	/* Step 3. Behavior depends on flags */
	mem_ctx = talloc_new(mictx);
	key.dptr = (unsigned char *)talloc_asprintf(mem_ctx, "FMID/0x%.16"PRIx64, fmid);
	key.dsize = strlen((const char *)key.dptr);

	switch (flags) {
	case MAPISTORE_SOFT_DELETE:
		/* Retrieve mapistore_indexing_entry structure */
		ret = mapistore_indexing_record_get_entry(mstore_ctx, username, fmid, &entry);
		MAPISTORE_RETVAL_IF(ret, MAPISTORE_ERR_DATABASE_OPS, mem_ctx);

		/* Change the message type to SOFT_DELETED */
		entry.info.mapistore_indexing_v1.Type = MAPISTORE_INDEXING_SOFTDELETE;
		ndr_err = ndr_push_struct_blob(&data, mem_ctx, &entry, (ndr_push_flags_fn_t)ndr_push_mapistore_indexing_entry);
		if (!NDR_ERR_CODE_IS_SUCCESS(ndr_err)) {
			talloc_free(mem_ctx);
			return MAPISTORE_ERROR;
		}

		dbuf.dptr = data.data;
		dbuf.dsize = data.length;

		/* Update the record */
		ret = tdb_store(mictx->tdb_ctx->tdb, key, dbuf, TDB_MODIFY);
		MAPISTORE_RETVAL_IF(ret, MAPISTORE_ERR_DATABASE_OPS, mem_ctx);
		break;
	case MAPISTORE_PERMANENT_DELETE:
	default:
	{
		/* Physically delete the record */
		char 		*uri = NULL;
		TDB_DATA	reversekey;
		ret = mapistore_indexing_lookup_uri(mstore_ctx, username, fmid, &uri);
		MAPISTORE_RETVAL_IF(ret, MAPISTORE_ERR_DATABASE_OPS, mem_ctx);
		reversekey.dptr = (unsigned char *)talloc_asprintf(mem_ctx, "URI/%s", uri);
		reversekey.dsize = strlen((const char*)reversekey.dptr);
		talloc_free(uri);
		ret = tdb_delete(mictx->tdb_ctx->tdb, reversekey);
		talloc_free(reversekey.dptr);
		if (ret != 0) {
			DEBUG(0, ("[%s] tdb_delete %s\n", __FUNCTION__, tdb_errorstr(mictx->tdb_ctx->tdb)));
		}
		MAPISTORE_RETVAL_IF(ret, MAPISTORE_ERR_DATABASE_OPS, mem_ctx);

		ret = tdb_delete(mictx->tdb_ctx->tdb, key);
		talloc_free(key.dptr);
		MAPISTORE_RETVAL_IF(ret, MAPISTORE_ERR_DATABASE_OPS, mem_ctx);
		break;
	}
	}

	talloc_free(mem_ctx);

	return MAPISTORE_SUCCESS;
}


/**
   \details Return the folder or message identifier matching given
   mapistore URI from the indexing database.

   \param mstore_ctx pointer to the mapistore context
   \param username the owner of the indexing database
   \param uri the mapistore uri to lookup
   \param fmid pointer to the folder/message identifier to return

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
 */
static int mapistore_indexing_get_fmid(struct mapistore_context *mstore_ctx,
				       const char *username, const char *uri,
				       uint64_t *fmid)
{
	struct mapistore_indexing_context_list 	*mictx;
	TDB_DATA				key;
	TDB_DATA				value;
	const char 				*fmid_str;
	/* sanity checks */
	MAPISTORE_RETVAL_IF(!mstore_ctx, MAPISTORE_ERR_NOT_INITIALIZED, NULL);
	MAPISTORE_RETVAL_IF(!username, MAPISTORE_ERR_INVALID_PARAMETER, NULL);
	MAPISTORE_RETVAL_IF(!uri, MAPISTORE_ERR_INVALID_PARAMETER, NULL);
	MAPISTORE_RETVAL_IF(!fmid, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* find the mapistore index store for this user */
	mictx = mapistore_indexing_context_search(mstore_ctx, username);
	if ( ! mictx) {
		DEBUG(0, ("Failed lookup for indexing context for user: %s\n", username));
		return MAPISTORE_ERR_NOT_INITIALIZED;
	}

	/* look up the URI in the indexing store */
	/* TODO - figure out why the URI is missing the mstoredb:// prefix */
	DEBUG(7, ("Looking for URI: %s\n", uri));
	key.dptr = (unsigned char*)talloc_asprintf(mictx, "URI/mstoredb://%s", uri);
	key.dsize = strlen((const char*)key.dptr);
	
	value = tdb_fetch(mictx->tdb_ctx->tdb, key);
	talloc_free(key.dptr);

	/* parse the result out */
	fmid_str = talloc_strndup(mstore_ctx, (char*)value.dptr, value.dsize);
	free(value.dptr);
	if ( ! fmid_str) {
		return MAPISTORE_ERR_NOT_FOUND;
	}
	if (sscanf(fmid_str, "FMID/0x%"PRIx64, fmid) != 1) {
		talloc_free((TALLOC_CTX*)fmid_str);
		fmid = 0;
		return MAPISTORE_ERR_NOT_FOUND;
	}
	talloc_free((TALLOC_CTX*)fmid_str);
	DEBUG(7, ("%s mapped to 0x%016"PRIx64"\n", uri, *fmid));

	return MAPISTORE_SUCCESS;
}


/**
   \details Adds a Folder identifier record to the indexing database
   for the specified user

   \param mstore_ctx pointer to the mapistore context
   \param username the owner of the indexing database
   \param fid the folder identifier to register
   \param mapistore_uri the mapistore URI to register
   \param parent_fid the parent folder identifier

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error

   \sa mapistore_indexing_record_add_fmid
 */
int mapistore_indexing_record_add_fid(struct mapistore_context *mstore_ctx,
				      const char *username, uint64_t fid,
				      const char *mapistore_uri, uint64_t parent_fid)
{
	struct mapistore_indexing_context_list *mictx;

	MAPISTORE_RETVAL_IF(!mstore_ctx, MAPISTORE_ERROR, NULL);
	MAPISTORE_RETVAL_IF(!username, MAPISTORE_ERROR, NULL);

	/* Retrieve the indexing context associated to the username */
	mictx = mapistore_indexing_context_search(mstore_ctx, username);
	MAPISTORE_RETVAL_IF(!mictx, MAPISTORE_ERROR, NULL);

	return mapistore_indexing_record_add_fmid(mictx, fid, mapistore_uri,
						  parent_fid, MAPISTORE_INDEXING_FOLDER);
}


/**
   \details Adds a Message identifier record to the indexing database
   for specified user

   \param mstore_ctx pointer to the mapistore context
   \param username the owner of the indexing database
   \param mid the message identifier to register
   \param mapistore_uri the mapistore URI to register
   \param parent_fid the parent folder identifier

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error

   \sa mapistore_indexing_record_add_fmid
 */
int mapistore_indexing_record_add_mid(struct mapistore_context *mstore_ctx,
				      const char *username, uint64_t mid,
				      const char *mapistore_uri, uint64_t parent_fid)
{
	struct mapistore_indexing_context_list *mictx;

	MAPISTORE_RETVAL_IF(!mstore_ctx, MAPISTORE_ERROR, NULL);
	MAPISTORE_RETVAL_IF(!username, MAPISTORE_ERROR, NULL);

	/* Retrieve the indexing context associated to the username */
	mictx = mapistore_indexing_context_search(mstore_ctx, username);
	MAPISTORE_RETVAL_IF(!mictx, MAPISTORE_ERROR, NULL);

	return mapistore_indexing_record_add_fmid(mictx, mid, mapistore_uri,
						  parent_fid, MAPISTORE_INDEXING_MESSAGE);
}


/**
   \details Delete a Folder identifier record from the indexing
   database for specified user

   \param mstore_ctx pointer to the mapistore context
   \param username the owner of the indexing database
   \param fid the folder identifier of the record to delete
   \param flags flags controlling deletion scope

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error

   \sa mapistore_indexing_record_del_fmid
 */
int mapistore_indexing_record_del_fid(struct mapistore_context *mstore_ctx,
				      const char *username, uint64_t fid,
				      uint8_t flags)
{
	return mapistore_indexing_record_del_fmid(mstore_ctx, username, fid, flags);
}

/**
   \details Delete a Message identifier record from the indexing
   database for specified user

   \param mstore_ctx pointer to the mapistore context
   \param username the owner of the indexing database
   \param mid the message identifier of the record to delete
   \param flags flags controlling deletion scope

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
   
   \sa mapistore_indexing_record_del_fmid
 */
int mapistore_indexing_record_del_mid(struct mapistore_context *mstore_ctx,
				      const char *username, uint64_t mid,
				      uint8_t flags)
{
	return mapistore_indexing_record_del_fmid(mstore_ctx, username, mid, flags);
}


/**
   \details Find a Folder identifier given the URI (for a for specified user)

   \param mstore_ctx pointer to the mapistore context
   \param username the owner of the indexing database
   \param uri the URI of the folder to look up
   \param fid the folder identifier that was looked up

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error

   \sa mapistore_indexing_lookup_mid for equivalent code for messages
 */
int mapistore_indexing_lookup_fid(struct mapistore_context *mstore_ctx,
				  const char *username, const char *uri, 
				  uint64_t *fid)
{
	return mapistore_indexing_get_fmid(mstore_ctx, username, uri, fid);
}


/**
   \details Find a Message identifier given the URI (for a for specified user)

   \param mstore_ctx pointer to the mapistore context
   \param username the owner of the indexing database
   \param uri the URI of the message to look up
   \param mid the message identifier that was looked up

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error

   \sa mapistore_indexing_lookup_fid for equivalent code for folders
 */
int mapistore_indexing_lookup_mid(struct mapistore_context *mstore_ctx,
				  const char *username, const char *uri, 
				  uint64_t *mid)
{
	return mapistore_indexing_get_fmid(mstore_ctx, username, uri, mid);
}
