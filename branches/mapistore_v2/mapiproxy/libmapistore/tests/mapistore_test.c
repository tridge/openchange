/*
   OpenChange Storage Abstraction Layer library test tool

   OpenChange Project

   Copyright (C) Julien Kerihuel 2009

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mapiproxy/libmapistore/mapistore.h"
/* TODO: we shouldn't need to do this... */
#include "mapiproxy/libmapistore/mapistore_private.h"
#include <talloc.h>
#include <core/ntstatus.h>
#include <samba/popt.h>
#include <param.h>
#include <util/debug.h>

/**
   \file mapistore_test.c

   \brief Test mapistore implementation
 */


int main(int argc, const char *argv[])
{
	TALLOC_CTX			*mem_ctx;
	enum MAPISTORE_ERROR		retval;
	struct mapistoredb_context	*mstoredb_ctx;
	struct loadparm_context		*lp_ctx;
	poptContext			pc;
	int				opt;
	const char			*opt_debug = NULL;
	uint32_t			mailbox1_context_id = 0;
	uint32_t			mailbox2_context_id = 0;
	uint64_t			testuser2_fid = 0;
	uint64_t			jk_fid = 0;
	uint32_t			testuser2_folder_count = 0;
	uint32_t			jk_folder_count = 0;
	uint64_t			planning_subfolder_fid = 0;
	struct SRow 			planning_subfolder_props;
	uint32_t			pf_context_id = 0;

	enum { OPT_DEBUG=1000 };

	struct poptOption long_options[] = {
		POPT_AUTOHELP
		{ "debuglevel",	'd', POPT_ARG_STRING, NULL, OPT_DEBUG,	"set the debug level", NULL },
		{ NULL, 0, 0, NULL, 0, NULL, NULL }
	};

	/* TODO : chase down the leaks reported by this */
	// talloc_enable_leak_report_full();
	mem_ctx = talloc_named(NULL, 0, "mapistore_test");
	lp_ctx = loadparm_init(mem_ctx);
	lpcfg_load_default(lp_ctx);
	setup_logging(NULL, DEBUG_STDOUT);
	
	pc = poptGetContext("mapistore_test", argc, argv, long_options, 0);
	while ((opt = poptGetNextOpt(pc)) != -1) {
		switch (opt) {
		case OPT_DEBUG:
			opt_debug = poptGetOptArg(pc);
			break;
		}
	}

	poptFreeContext(pc);

	if (opt_debug) {
		lpcfg_set_cmdline(lp_ctx, "log level", opt_debug);
	}

	mstoredb_ctx = mapistoredb_init(mem_ctx, "/tmp/mapistoredb_path");
	if (mstoredb_ctx == NULL) {
		DEBUG(0, ("mapistoredb_init returned NULL\n"));
		exit(1);
	}

	retval = mapistoredb_set_netbiosname(mstoredb_ctx, "nbname");
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistoredb_set_netbiosname() returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}

	retval = mapistoredb_provision(mstoredb_ctx);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistoredb_provision() returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}

	retval = mapistoredb_provision_publicfolders(mstoredb_ctx, &pf_context_id);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistoredb_provision_publicfolders() returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}

	retval = mapistoredb_new_mailbox(mstoredb_ctx, "testuser1", &mailbox1_context_id);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistoredb_new_mailbox() returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}
	DEBUG(2, ("testuser1 context ID: 0x%x\n", mailbox1_context_id));

	retval = mapistoredb_new_mailbox(mstoredb_ctx, "testuser2", &mailbox2_context_id);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistoredb_new_mailbox() returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}
	DEBUG(2, ("testuser2 context ID: 0x%x\n", mailbox2_context_id));

	/* try adding the default Outbox folder */
	retval = mapistore_root_mkdir(mstoredb_ctx->mstore_ctx, mailbox2_context_id, MDB_OUTBOX, 0, NULL, NULL);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_root_mkdir() returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}

	/* try adding the Journal folder with a non-standard name*/
	retval = mapistore_root_mkdir(mstoredb_ctx->mstore_ctx, mailbox2_context_id, MDB_JOURNAL, 0, "MyJournal", NULL);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_root_mkdir() returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}

	/* try an adding a folder with the fsocpf backend */
	retval = mapistore_root_mkdir(mstoredb_ctx->mstore_ctx, mailbox2_context_id, MDB_IPM_SUBTREE, 0, "MyMessages", "fsocpf://tmp/testuser2/mymessages");
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_root_mkdir() returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}

	/* try adding a generic folder */
	retval = mapistore_root_mkdir(mstoredb_ctx->mstore_ctx, mailbox2_context_id, MDB_CUSTOM, 0, "From Julien", NULL);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_root_mkdir() for generic folder returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}

	/* try opening the testuser2 root folder */
	/* TODO consider whether we really want a hack like fid == 0 means open root folder for this context */
	retval = mapistore_opendir(mstoredb_ctx->mstore_ctx, mailbox2_context_id, 0, 0); 
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_opendir() on root folder returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}

	retval = mapistore_get_fid_by_name(mstoredb_ctx->mstore_ctx, mailbox2_context_id, 0, "testuser2", &testuser2_fid); 
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_get_fid_by_name() on testuser2 root folder returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}
	DEBUG(0, ("testuser2 root folder has fid: 0x%016"PRIx64"\n", jk_fid));

	retval = mapistore_get_fid_by_name(mstoredb_ctx->mstore_ctx, mailbox2_context_id, testuser2_fid, "From Julien", &jk_fid); 
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_opendir() on root folder returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}
	DEBUG(0, ("\"From Julien\" folder has fid: 0x%016"PRIx64"\n", jk_fid));

	retval = mapistore_get_folder_count(mstoredb_ctx->mstore_ctx, mailbox2_context_id, testuser2_fid, &testuser2_folder_count);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_get_folder_count() on root folder returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}
	DEBUG(0, ("testuser2 root folder has %i subfolders\n", testuser2_folder_count));

	retval = mapistore_get_folder_count(mstoredb_ctx->mstore_ctx, mailbox2_context_id, jk_fid, &jk_folder_count);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_get_folder_count() on \"From Julien\" folder returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}
	DEBUG(0, ("\"From Julien\" folder has %i subfolder(s)\n", jk_folder_count));

	retval = mapistore_get_next_fmid(mstoredb_ctx->mstore_ctx, &planning_subfolder_fid);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_get_next_fmid() returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}
	DEBUG(0, ("\"Planning January 2011\" subfolder will use FID: 0x%016"PRIx64"\n", planning_subfolder_fid));

	planning_subfolder_props.cValues = 1;
	planning_subfolder_props.lpProps = talloc_zero_array(mem_ctx, struct SPropValue, planning_subfolder_props.cValues);
	planning_subfolder_props.lpProps[0].ulPropTag = PR_DISPLAY_NAME;
	planning_subfolder_props.lpProps[0].value.lpszA = "Planning January 2011";

	retval = mapistore_mkdir(mstoredb_ctx->mstore_ctx, mailbox2_context_id, jk_fid, planning_subfolder_fid, &planning_subfolder_props);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_mkdir() on planning subfolder returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}
	DEBUG(0, ("Created \"Planning January 2011\" subfolder under \"From Julien\" subfolder\n"));

	retval = mapistore_get_folder_count(mstoredb_ctx->mstore_ctx, mailbox2_context_id, jk_fid, &jk_folder_count);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_get_folder_count() on \"From Julien\" folder returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}
	DEBUG(0, ("\"From Julien\" folder now has %i subfolder(s)\n", jk_folder_count));
	
	/* TODO: clean up mailbox contexts */

	mapistoredb_release(mstoredb_ctx);
	talloc_free(lp_ctx);
	talloc_report(mem_ctx, stdout);
	talloc_free(mem_ctx);
	return 0;
}
