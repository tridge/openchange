/*
   OpenChange Storage Abstraction Layer library provisioning test tool

   OpenChange Project

   Copyright (C) Julien Kerihuel 2009
   Copyright (C) Brad Hards <bradh@openchange.org> 2010

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mapiproxy/libmapistore/mapistore.h"
/* TODO: we shouldn't need to do this... */
#include "mapiproxy/libmapistore/mapistore_private.h"
#include <talloc.h>
#include <core/ntstatus.h>
#include <samba/popt.h>
#include <param.h>
#include <util/debug.h>

/**
   \file mapistore_provision.c

   \brief Demo mapistore provisioning
 */

static void do_pf_subtree(struct mapistore_context *mstore_ctx, const uint32_t context_id, const uint64_t root_fid, const char *netbiosname, const char *firstorgname)
{
	int 			i = 0;
	enum MAPISTORE_ERROR	retval;
	uint64_t		index_to_fid_map[PF_LAST_SPECIALFOLDER + 1];
	struct SRow		properties;
	TALLOC_CTX		*mem_ctx;
	
	mem_ctx = talloc_init(NULL);

	for (i = 0; pf_nonipm_folders[i].name != NULL; ++i) {
		char *name;
		name = string_sub_talloc(mem_ctx, pf_nonipm_folders[i].name, "{netbios}", netbiosname);
		name = string_sub_talloc(mem_ctx, name, "{firstorg}", firstorgname);
		DEBUG(0, ("[%s] name post substitution: %s\n", __FUNCTION__, name));
		if (pf_nonipm_folders[i].parent_specialindex == PF_ROOT_FOLDER) {
			retval = mapistore_root_mkdir(mstore_ctx, context_id, 999, root_fid, name, NULL);
			if (retval != MAPISTORE_SUCCESS) {
				talloc_free(mem_ctx);
				DEBUG(0, ("mapistore_root_mkdir() for %s returned %i (%s)\n", name, retval, mapistore_errstr(retval)));
				return;
			}
			mapistore_get_fid_by_name(mstore_ctx, context_id, root_fid, name, &(index_to_fid_map[pf_nonipm_folders[i].index]));
			if (retval != MAPISTORE_SUCCESS) {
				talloc_free(mem_ctx);
				DEBUG(0, ("mapistore_get_fid_by_name for %s returned %i (%s)\n", name, retval, mapistore_errstr(retval)));
				return;
			}
		} else {
			retval = mapistore_root_mkdir(mstore_ctx, context_id, 999,
						      index_to_fid_map[pf_nonipm_folders[i].parent_specialindex],
						      name, NULL);
			if (retval != MAPISTORE_SUCCESS) {
				talloc_free(mem_ctx);
				DEBUG(0, ("mapistore_root_mkdir() for %s returned %i (%s)\n", name, retval, mapistore_errstr(retval)));
				return;
			}
			mapistore_get_fid_by_name(mstore_ctx, context_id, index_to_fid_map[pf_nonipm_folders[i].parent_specialindex],
						  name, &(index_to_fid_map[pf_nonipm_folders[i].index]));
			if (retval != MAPISTORE_SUCCESS) {
				talloc_free(mem_ctx);
				DEBUG(0, ("mapistore_get_fid_by_name for %s returned %i (%s)\n", name, retval, mapistore_errstr(retval)));
				return;
			}
		}
	}
	talloc_free(mem_ctx);

	properties.cValues = 9;
	properties.lpProps = talloc_zero_array(mstore_ctx, struct SPropValue, properties.cValues);
	properties.lpProps[0].ulPropTag = openchange_private_PF_ROOT;
	properties.lpProps[0].value.d = root_fid;
	properties.lpProps[1].ulPropTag = openchange_private_PF_IPM_SUBTREE;
	properties.lpProps[1].value.d = index_to_fid_map[PF_IPM_SUBTREE];
	properties.lpProps[2].ulPropTag = openchange_private_PF_NONIPM_SUBTREE;
	properties.lpProps[2].value.d = index_to_fid_map[PF_NON_IPM_SUBTREE];
	properties.lpProps[3].ulPropTag = openchange_private_PF_EFORMS;
	properties.lpProps[3].value.d = index_to_fid_map[PF_EFORMS_REGISTRY];
	properties.lpProps[4].ulPropTag = openchange_private_PF_FREEBUSY;
	properties.lpProps[4].value.d = index_to_fid_map[PF_FREEBUSY];
	properties.lpProps[5].ulPropTag = openchange_private_PF_OAB;
	properties.lpProps[5].value.d = index_to_fid_map[PF_OAB];
	properties.lpProps[6].ulPropTag = openchange_private_PF_LOCAL_EFORMS;
	properties.lpProps[6].value.d = 0x0;
	properties.lpProps[7].ulPropTag = openchange_private_PF_LOCAL_FREEBUSY;
	properties.lpProps[7].value.d = index_to_fid_map[PF_LOCALFREEBUSY];
	properties.lpProps[8].ulPropTag = openchange_private_PF_LOCAL_OAB;
	properties.lpProps[8].value.d = index_to_fid_map[PF_LOCALOAB];

	mapidump_SRow(&properties, "\t");
	/* add FID list to root folder */
	retval = mapistore_setprops(mstore_ctx, context_id, root_fid, MAPISTORE_FOLDER, &properties);
	talloc_free(properties.lpProps);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_setprops for root folder FID list returned %i (%s)\n", retval, mapistore_errstr(retval)));
		return;
	}
}

static int add_public_folders(TALLOC_CTX *mem_ctx, struct mapistoredb_context *mstoredb_ctx)
{
	enum MAPISTORE_ERROR		retval;
	uint32_t			pf_context_id = 0;
	uint64_t			pf_root_fid = 0;

	retval = mapistoredb_provision_publicfolders(mstoredb_ctx, &pf_context_id);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistoredb_provision_publicfolders() returned %i (%s)\n", retval, mapistore_errstr(retval)));
		return -1;
	}
	DEBUG(3, ("Public folder root context ID: 0x%x\n", pf_context_id));

	retval = mapistore_get_context_fid(mstoredb_ctx->mstore_ctx, pf_context_id, &pf_root_fid); 
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_get_context_fid() on public folder root returned %i (%s)\n", retval, mapistore_errstr(retval)));
		return -3;
	}
	DEBUG(3, ("public folder root folder has fid: 0x%016"PRIx64"\n", pf_root_fid));

	do_pf_subtree(mstoredb_ctx->mstore_ctx, pf_context_id, pf_root_fid, mapistoredb_get_netbiosname(mstoredb_ctx), mapistoredb_get_firstorg(mstoredb_ctx));

	return 0;
}

static int do_initialisation(TALLOC_CTX *mem_ctx, const char *install_path)
{
	struct mapistoredb_context	*mstoredb_ctx;
	enum MAPISTORE_ERROR		retval;

	// TODO: perhaps we should init() in the main() function
	// TODO: probably need to mkdir if it doesn't already exist
	mstoredb_ctx = mapistoredb_init(mem_ctx, install_path);
	if (mstoredb_ctx == NULL) {
		DEBUG(0, ("mapistoredb_init returned NULL\n"));
		return -1;
	}

	retval = mapistoredb_provision(mstoredb_ctx);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistoredb_provision() returned %i (%s)\n", retval, mapistore_errstr(retval)));
		return -2;
	}

	/* We could consider having an option that controls whether we really want public folders */
	if (add_public_folders(mem_ctx, mstoredb_ctx) != 0) {
		mapistoredb_release(mstoredb_ctx);
		return -3;
	}

	mapistoredb_release(mstoredb_ctx);
	
	return 0;
}

static void add_special_folders(struct mapistore_context *mstore_ctx, uint32_t context_id, uint64_t root_fid)
{
	int 			i = 0;
	enum MAPISTORE_ERROR	retval;
	uint64_t		index_to_fid_map[MDB_LAST_SPECIALFOLDER + 1];
	struct SRow		properties;

	for (i = 0; mailbox_special_folders[i].name != NULL; ++i) {
		if (mailbox_special_folders[i].parent_specialindex == MDB_ROOT_FOLDER) {
			retval = mapistore_root_mkdir(mstore_ctx, context_id, mailbox_special_folders[i].index, root_fid, NULL, NULL);
			if (retval != MAPISTORE_SUCCESS) {
				DEBUG(0, ("mapistore_root_mkdir() for %s returned %i (%s)\n", mailbox_special_folders[i].name, retval, mapistore_errstr(retval)));
				return;
			}
			mapistore_get_fid_by_name(mstore_ctx, context_id, root_fid, mailbox_special_folders[i].name,
							&(index_to_fid_map[mailbox_special_folders[i].index]));
			if (retval != MAPISTORE_SUCCESS) {
				DEBUG(0, ("mapistore_get_fid_by_name for %s returned %i (%s)\n", mailbox_special_folders[i].name, retval, mapistore_errstr(retval)));
				return;
			}
		} else {
			retval = mapistore_root_mkdir(mstore_ctx, context_id, mailbox_special_folders[i].index,
						      index_to_fid_map[mailbox_special_folders[i].parent_specialindex],
						      NULL, NULL);
			if (retval != MAPISTORE_SUCCESS) {
				DEBUG(0, ("mapistore_root_mkdir() for %s returned %i (%s)\n", mailbox_special_folders[i].name, retval, mapistore_errstr(retval)));
				return;
			}
			mapistore_get_fid_by_name(mstore_ctx, context_id, index_to_fid_map[mailbox_special_folders[i].parent_specialindex],
						  mailbox_special_folders[i].name, &(index_to_fid_map[mailbox_special_folders[i].index]));
			if (retval != MAPISTORE_SUCCESS) {
				DEBUG(0, ("mapistore_get_fid_by_name for %s returned %i (%s)\n", mailbox_special_folders[i].name, retval, mapistore_errstr(retval)));
				return;
			}
		}
	}
	/* build folder properties for root folder */
	properties.cValues = 19;
	properties.lpProps = talloc_zero_array(mstore_ctx, struct SPropValue, properties.cValues);
	properties.lpProps[0].ulPropTag = openchange_private_ROOT_FOLDER_FID;
	properties.lpProps[0].value.d = root_fid;
	properties.lpProps[1].ulPropTag = openchange_private_DEFERRED_ACTIONS_FID;
	properties.lpProps[1].value.d = index_to_fid_map[MDB_DEFERRED_ACTIONS];
	properties.lpProps[2].ulPropTag = openchange_private_SPOOLER_QUEUE_FID;
	properties.lpProps[2].value.d = index_to_fid_map[MDB_SPOOLER_QUEUE];
	properties.lpProps[3].ulPropTag = openchange_private_IPM_SUBTREE_FID;
	properties.lpProps[3].value.d = index_to_fid_map[MDB_IPM_SUBTREE];
	properties.lpProps[4].ulPropTag = openchange_private_INBOX_FID;
	properties.lpProps[4].value.d = index_to_fid_map[MDB_INBOX];
	properties.lpProps[5].ulPropTag = openchange_private_OUTBOX_FID;
	properties.lpProps[5].value.d = index_to_fid_map[MDB_OUTBOX];
	properties.lpProps[6].ulPropTag = openchange_private_SENT_ITEMS_FID;
	properties.lpProps[6].value.d = index_to_fid_map[MDB_SENT_ITEMS];
	properties.lpProps[7].ulPropTag = openchange_private_DELETED_ITEMS_FID;
	properties.lpProps[7].value.d = index_to_fid_map[MDB_DELETED_ITEMS];
	properties.lpProps[8].ulPropTag = openchange_private_COMMON_VIEWS_FID;
	properties.lpProps[8].value.d = index_to_fid_map[MDB_COMMON_VIEWS];
	properties.lpProps[9].ulPropTag = openchange_private_SCHEDULE_FID;
	properties.lpProps[9].value.d = index_to_fid_map[MDB_SCHEDULE];
	properties.lpProps[10].ulPropTag = openchange_private_SEARCH_FID;
	properties.lpProps[10].value.d = index_to_fid_map[MDB_SEARCH];
	properties.lpProps[11].ulPropTag = openchange_private_VIEWS_FID;
	properties.lpProps[11].value.d = index_to_fid_map[MDB_VIEWS];
	properties.lpProps[12].ulPropTag = openchange_private_SHORTCUTS_FID;
	properties.lpProps[12].value.d = index_to_fid_map[MDB_SHORTCUTS];
	properties.lpProps[13].ulPropTag = openchange_private_CONTACT_FID;
	properties.lpProps[13].value.d = index_to_fid_map[MDB_CONTACTS];
	properties.lpProps[14].ulPropTag = openchange_private_CALENDAR_FID;
	properties.lpProps[14].value.d = index_to_fid_map[MDB_CALENDAR];
	properties.lpProps[15].ulPropTag = openchange_private_JOURNAL_FID;
	properties.lpProps[15].value.d = index_to_fid_map[MDB_JOURNAL];
	properties.lpProps[16].ulPropTag = openchange_private_NOTE_FID;
	properties.lpProps[16].value.d = index_to_fid_map[MDB_NOTES];
	properties.lpProps[17].ulPropTag = openchange_private_TASK_FID;
	properties.lpProps[17].value.d = index_to_fid_map[MDB_TASKS];
	properties.lpProps[18].ulPropTag = openchange_private_DRAFTS_FID;
	properties.lpProps[18].value.d = index_to_fid_map[MDB_DRAFTS];
	mapidump_SRow(&properties, "\t");
	/* add FID list to root folder */
	retval = mapistore_setprops(mstore_ctx, context_id, root_fid, MAPISTORE_FOLDER, &properties);
	talloc_free(properties.lpProps);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_setprops for root folder FID list returned %i (%s)\n", retval, mapistore_errstr(retval)));
		return;
	}

	mapistore_set_ReceiveFolder(mstore_ctx, context_id, "IPC", root_fid);
	mapistore_set_ReceiveFolder(mstore_ctx, context_id, "IPM", index_to_fid_map[MDB_INBOX]);
	mapistore_set_ReceiveFolder(mstore_ctx, context_id, "Report.IPM", index_to_fid_map[MDB_INBOX]);
	mapistore_set_ReceiveFolder(mstore_ctx, context_id, "", index_to_fid_map[MDB_INBOX]);
}

static int do_user_mailbox(TALLOC_CTX *mem_ctx, const char *username, const char *install_path)
{
	struct mapistoredb_context	*mstoredb_ctx;
	enum MAPISTORE_ERROR		retval;
	uint32_t			mailbox_context_id = 0;
	uint64_t			mailbox_root_fid = 0;

	mstoredb_ctx = mapistoredb_init(mem_ctx, install_path);
	if (mstoredb_ctx == NULL) {
		DEBUG(0, ("mapistoredb_init returned NULL\n"));
		return -1;
	}

	retval = mapistoredb_new_mailbox(mstoredb_ctx, username, &mailbox_context_id);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistoredb_new_mailbox() returned %i (%s)\n", retval, mapistore_errstr(retval)));
		return -2;
	}
	DEBUG(3, ("%s context ID: 0x%x\n", username, mailbox_context_id));

	retval = mapistore_get_context_fid(mstoredb_ctx->mstore_ctx, mailbox_context_id, &mailbox_root_fid); 
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistore_get_context_fid() on %s root folder returned %i (%s)\n", username, retval, mapistore_errstr(retval)));
		return -3;
	}
	DEBUG(3, ("%s root folder has fid: 0x%016"PRIx64"\n", username, mailbox_root_fid));

	// TODO: retval = mapistore_indexing_record_get_fid() somehow
	
	add_special_folders(mstoredb_ctx->mstore_ctx, mailbox_context_id, mailbox_root_fid);

	mapistoredb_release(mstoredb_ctx);

	return 0;
}

int main(int argc, const char *argv[])
{
	TALLOC_CTX			*mem_ctx;
	struct loadparm_context		*lp_ctx;
	poptContext			pc;
	int				opt;
	const char			*opt_debug = NULL;
	bool				opt_init = false;
	const char			*opt_mailbox = NULL;
	const char			*opt_installpath = NULL;
#if 0
	uint32_t			mailbox2_context_id = 0;
	uint64_t			testuser2_fid = 0;
	uint64_t			jk_fid = 0;
	uint32_t			testuser2_folder_count = 0;
	uint32_t			jk_folder_count = 0;
	uint64_t			planning_subfolder_fid = 0;
	struct SRow 			planning_subfolder_props;
#endif
	enum { OPT_DEBUG=1000, OPT_INIT, OPT_MAILBOX, OPT_INSTALLPATH };

	struct poptOption long_options[] = {
		POPT_AUTOHELP
		{ "init", 'i', POPT_ARG_NONE, NULL, OPT_INIT, "initialise the database", NULL },
		{ "mailbox", 'm', POPT_ARG_STRING, NULL, OPT_MAILBOX, "create a user mailbox", NULL },
		{ "debuglevel",	'd', POPT_ARG_STRING, NULL, OPT_DEBUG,	"set the debug level", NULL },
		{ "installpath", 'p', POPT_ARG_STRING, NULL, OPT_INSTALLPATH, "installation location", NULL },
		{ NULL, 0, 0, NULL, 0, NULL, NULL }
	};

	/* TODO : chase down the leaks reported by this */
	// talloc_enable_leak_report_full();
	mem_ctx = talloc_named(NULL, 0, "mapistore_test");
	lp_ctx = loadparm_init(mem_ctx);
	lpcfg_load_default(lp_ctx);
	setup_logging(NULL, DEBUG_STDOUT);
	
	pc = poptGetContext("mapistore_test", argc, argv, long_options, 0);
	while ((opt = poptGetNextOpt(pc)) != -1) {
		switch (opt) {
		case OPT_DEBUG:
			opt_debug = poptGetOptArg(pc);
			break;
		case OPT_INIT:
			opt_init = true;
			break;
		case OPT_MAILBOX:
			opt_mailbox = poptGetOptArg(pc);
			break;
		case OPT_INSTALLPATH:
			opt_installpath = poptGetOptArg(pc);
			break;
		default:
			poptPrintUsage(pc, stderr, 0);
			exit(1);
		}
	}

	poptFreeContext(pc);

	if (opt_debug) {
		lpcfg_set_cmdline(lp_ctx, "log level", opt_debug);
	}
	
	if ( ! opt_installpath) {
		opt_installpath = "/tmp/provision_test";
	}

	if (opt_init) {
		do_initialisation(mem_ctx, opt_installpath);
	}
	
	if (opt_mailbox) {
		do_user_mailbox(mem_ctx, opt_mailbox, opt_installpath);
	}

#if 0
	// TODO: more features we could add....

	retval = mapistoredb_set_netbiosname(mstoredb_ctx, "nbname");
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("mapistoredb_set_netbiosname() returned %i (%s)\n", retval, mapistore_errstr(retval)));
		exit(1);
	}
#endif

	talloc_free(lp_ctx);
	talloc_report(mem_ctx, stdout);
	talloc_free(mem_ctx);
	return 0;
}
