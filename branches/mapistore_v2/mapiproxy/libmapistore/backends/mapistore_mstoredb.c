/*
   OpenChange Storage Abstraction Layer library
   MAPIStore database backend

   OpenChange Project

   Copyright (C) Julien Kerihuel 2010

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mapistore_mstoredb.h"

#include "mapiproxy/libmapiproxy/libmapiproxy.h" /* for entryid_set_folder_EntryID */

#include <string.h>
#include <assert.h>

/**
   \details Initialize mstoredb mapistore backend

   \return MAPISTORE_SUCCESS on success
 */
static enum MAPISTORE_ERROR mstoredb_init(void)
{
	DEBUG(0, ("mstoredb backend initialized\n"));

	return MAPISTORE_SUCCESS;
}

static enum MAPISTORE_ERROR mstoredb_create_uri(TALLOC_CTX *mem_ctx, const char *uri, uint64_t *fid, char **mapistore_uri)
{
	DEBUG(0, ("mstore_create_uri: %s\n", uri));
	*mapistore_uri = talloc_asprintf(mem_ctx, "mstoredb://%s/modified", uri);

	return MAPISTORE_SUCCESS;
}

/**
   \details Create a connection context to the mstoredb backend

   \param ctx pointer to the opaque mapistore backend context
   \param uri pointer to the mstoredb DN to open
   \param private_data pointer to the private backend context
 */
static enum MAPISTORE_ERROR mstoredb_create_context(struct mapistore_backend_context *ctx,
				   const char *uri,
				   void **private_data)
{
	TALLOC_CTX		*mem_ctx;
	struct mstoredb_context	*mstoredb_ctx;
	struct ldb_result	*res = NULL;
	const char * const	recipient_attrs[] = { "*", NULL };
	int			ret;

	DEBUG(0, ("mstoredb_create_context: uri = %s\n", uri));

	mem_ctx = (TALLOC_CTX *) ctx;

	/* Initialize mstoredb context */
	mstoredb_ctx = talloc_zero(mem_ctx, struct mstoredb_context);
	mstoredb_ctx->context_dn = talloc_strdup(mstoredb_ctx, uri);
	mstoredb_ctx->mdb_ctx = ctx;

	/* Retrieve path to mapistore database */
	mstoredb_ctx->dbpath = mapistore_get_database_path();
	DEBUG(0, ("[mstoredb] database path = '%s'\n", mstoredb_ctx->dbpath));

	/* Open a wrapped connection on mapistore.ldb */
	mstoredb_ctx->ldb_ctx = mapistore_public_ldb_connect(mstoredb_ctx->mdb_ctx, mstoredb_ctx->dbpath);
	if (!mstoredb_ctx->ldb_ctx) {
		talloc_free(mstoredb_ctx);
		return MAPISTORE_ERROR;
	}

	/* Check if uri (DN) is correct */
	ret = ldb_search(mstoredb_ctx->ldb_ctx, mstoredb_ctx, &res,
			 ldb_get_default_basedn(mstoredb_ctx->ldb_ctx),
			 LDB_SCOPE_SUBTREE, recipient_attrs, 
			 "(dn=%s)", uri);

	if (ret != LDB_SUCCESS || !res || res->count != 1) {
		talloc_free(mstoredb_ctx);
		return MAPISTORE_ERROR;
	}

	mstoredb_ctx->context_fid = ldb_msg_find_attr_as_uint64(res->msgs[0], "PidTagFolderId", 0);
	DEBUG(0, ("[mstoredb] Root folder identifier for this context is 0x%016"PRIx64"\n", mstoredb_ctx->context_fid));

	mstoredb_ctx->basedn = ldb_dn_new(mstoredb_ctx, mstoredb_ctx->ldb_ctx, uri);
	if (!mstoredb_ctx->basedn) {
		DEBUG(0, ("[mstoredb] Unable to create DN from uri\n"));
	}

	*private_data = (void *)mstoredb_ctx;

	return MAPISTORE_SUCCESS;
}


/**
   \details Delete a connection context from the mstoredb backend

   \param private_data pointer to the current mstoredb context

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE_ERROR
 */
static enum MAPISTORE_ERROR mstoredb_delete_context(void *private_data)
{
	DEBUG(5, ("[%s:%d]\n", __FUNCTION__, __LINE__));

	return MAPISTORE_SUCCESS;
}

/* TODO: this is a copy of code in mapistoredb.c */
static bool write_ldif_string_to_store(struct ldb_context *ldb_ctx, const char *ldif_string)
{
	struct ldb_ldif	*ldif;
	while ((ldif = ldb_ldif_read_string(ldb_ctx, (const char **)&ldif_string))) {
		int ret;
		ret = ldb_msg_normalize(ldb_ctx, ldif, ldif->msg, &ldif->msg);
		if (ret != LDB_SUCCESS) {
			ldb_ldif_read_free(ldb_ctx, ldif);
			return false;
		}
		ret = ldb_add(ldb_ctx, ldif->msg);
		if (ret != LDB_SUCCESS) {
			ldb_ldif_read_free(ldb_ctx, ldif);
			return false;
		}
		ldb_ldif_read_free(ldb_ctx, ldif);
	}
	return true;
}

/**
   \details Create a root system/special folder within a user mailbox

   \param private_data pointer to the current mstoredb context
   \param parent_fid the parent folder identifier
   \param fid the folder identifier for this folder
   \param index the system/special internal index
   \param name the name of the folder to be created
   \param uri the optional mapistore URI to set for this folder
   
   \return MAPISTORE_SUCCESS if the folder is successfully created, otherwise
   a non-zero MAPISTORE_ERROR value

   \p uri will be set to the generated URI for this folder, if not specified.
 */
static enum MAPISTORE_ERROR mstoredb_op_root_mkdir(void *private_data,
						   uint64_t parent_fid,
						   uint64_t fid,
						   uint32_t index,
						   const char *name,
						   char **uri)
{
	TALLOC_CTX				*mem_ctx;
	struct mstoredb_context			*mstoredb_ctx = (struct mstoredb_context *)private_data;
	struct ldb_result			*res = NULL;
	const char * const			folder_attrs[] = { "*", NULL };
	int					ret;
	const char				*folder_dn;
	char					*ldif_str;
	const struct mailbox_special_folder	*sfld_info = NULL;
	int 					i = 0;

	mem_ctx = talloc_named(NULL, 0, __FUNCTION__);

	DEBUG(5, ("[%s:%d]\n", __FUNCTION__, __LINE__));

	DEBUG(4, ("[%s] name = %s\n", __FUNCTION__, name));
	DEBUG(4, ("[%s] uri = %s\n", __FUNCTION__, *uri));
	DEBUG(4, ("[%s] parent_fid: 0x%016"PRIx64"\n", __FUNCTION__, parent_fid));
	DEBUG(4, ("[%s] fid: 0x%016"PRIx64"\n", __FUNCTION__, fid));
	DEBUG(4, ("[%s] index: %d\n", __FUNCTION__, index));

	/* sanity checks */
	if ( ! mstoredb_ctx) {
		DEBUG(0, ("No mstoredb context found. Failing %s\n", __FUNCTION__));
		talloc_free(mem_ctx);
		return MAPISTORE_ERROR;
	}

	/* check the parent exists */
	ret = ldb_search(mstoredb_ctx->ldb_ctx, mstoredb_ctx, &res,
			 ldb_get_default_basedn(mstoredb_ctx->ldb_ctx),
			 LDB_SCOPE_SUBTREE, folder_attrs, 
			 "(PidTagFolderId=0x%016"PRIx64")", parent_fid);

	if (ret != LDB_SUCCESS || !res || res->count != 1) {
		DEBUG(0, ("%s failed parent check\n", __FUNCTION__));
		talloc_free(mem_ctx);
		return MAPISTORE_ERROR;
	}

	DEBUG(4, ("[%s] Parent folder name is %s\n", __FUNCTION__, ldb_msg_find_attr_as_string(res->msgs[0], "PidTagDisplayName", NULL)));
	DEBUG(4, ("[%s] Parent DN is %s\n", __FUNCTION__, ldb_msg_find_attr_as_string(res->msgs[0], "distinguishedName", NULL)));

	/* build the dn for the folder we will create */
	folder_dn = talloc_asprintf(mem_ctx, "CN=%s,%s", name, ldb_msg_find_attr_as_string(res->msgs[0], "distinguishedName", NULL));
	talloc_free(res);
	
	/* check that the name doesn't already exist */
	DEBUG(4, ("[%s] checking if %s already exists\n", __FUNCTION__, name));
	ret = ldb_search(mstoredb_ctx->ldb_ctx, mstoredb_ctx, &res,
			 ldb_get_default_basedn(mstoredb_ctx->ldb_ctx),
			 LDB_SCOPE_SUBTREE, folder_attrs, 
			 "(distinguishedName=%s)", folder_dn);

	if (ret != LDB_SUCCESS || !res || res->count != 0) {
		DEBUG(0, ("%s failed exist check\n", __FUNCTION__));
		talloc_free(mem_ctx);
		return MAPISTORE_ERR_EXIST;
	}
	talloc_free(res);

	/* build the LDIF */
	if ( strlen(*uri) == 0) {
		/* user didn't specify a URI, so assume its just a subfolder in this store */
		*uri = talloc_asprintf_append(*uri, "mstoredb://%s", folder_dn);
	}
	for (i = 0; mailbox_special_folders[i].name != NULL; ++i) {
		if (index == mailbox_special_folders[i].index) {
			sfld_info = &(mailbox_special_folders[i]);
			break;
		}
	}
	if (sfld_info) {
		ldif_str = talloc_asprintf(mem_ctx, MDB_MAILBOX_SPEC_FOLDER_TMPL,
					  folder_dn,
					  "specialfolder",
					  name,
					  parent_fid,
					  fid,
					  name,
					  sfld_info->PidType,
					  sfld_info->index,
					  *uri);
	} else {
		ldif_str = talloc_asprintf(mem_ctx, MDB_MAILBOX_FOLDER_TMPL,
					  folder_dn,
					  "folder",
					  name,
					  parent_fid,
					  fid,
					  name,
					  *uri);
	}
	DEBUG(7, ("[%s] Generated LDIF: %s\n", __FUNCTION__, ldif_str));
	/* submit the LDIF */
	if ( ! write_ldif_string_to_store(mstoredb_ctx->ldb_ctx, ldif_str)) {
		DEBUG(0, ("[%s] Failed to write LDIF\n", __FUNCTION__));
		talloc_free(ldif_str);
		return MAPISTORE_ERR_DATABASE_OPS;
	}
	talloc_free(ldif_str);
	
	talloc_free(mem_ctx);
	DEBUG(4, ("[%s] returning MAPISTORE_SUCCESS\n", __FUNCTION__));

	return MAPISTORE_SUCCESS;
}

/**
   \details Delete a folder 

   \param private_data pointer to the current mstoredb context
   \param parent_fid the parent folder identifier
   \param fid the folder identifier for this folder
   
   \return MAPISTORE_SUCCESS if the folder is successfully deleted, otherwise
   a non-zero MAPISTORE_ERROR value
 */
static enum MAPISTORE_ERROR mstoredb_op_rmdir(void *private_data,
						   uint64_t parent_fid,
						   uint64_t fid)
{
	TALLOC_CTX				*mem_ctx;
	struct mstoredb_context			*mstoredb_ctx = (struct mstoredb_context *)private_data;
	struct ldb_result			*res = NULL;
	const char * const			folder_attrs[] = { "*", NULL };
	const char				*dn_str;
	struct ldb_dn				*dn;
	int					ret;


	DEBUG(5, ("[%s:%d]\n", __FUNCTION__, __LINE__));

	DEBUG(4, ("[%s] parent_fid: 0x%016"PRIx64"\n", __FUNCTION__, parent_fid));
	DEBUG(4, ("[%s] fid: 0x%016"PRIx64"\n", __FUNCTION__, fid));

	/* sanity checks */
	if ( ! mstoredb_ctx) {
		DEBUG(0, ("No mstoredb context found. Failing %s\n", __FUNCTION__));
		return MAPISTORE_ERROR;
	}

	/* check the folder exists */
	/* TODO: the search may need to be modified to either just delete folders, or to delete children (like tables, FAI messages) as well */
	ret = ldb_search(mstoredb_ctx->ldb_ctx, mstoredb_ctx, &res,
			 ldb_get_default_basedn(mstoredb_ctx->ldb_ctx),
			 LDB_SCOPE_SUBTREE, folder_attrs, 
			 "(PidTagFolderId=0x%016"PRIx64")", fid);

	if (ret != LDB_SUCCESS || !res || res->count != 1) {
		DEBUG(0, ("%s failed exist check\n", __FUNCTION__));
		return MAPISTORE_ERR_NOT_FOUND;
	}

	dn_str = ldb_msg_find_attr_as_string(res->msgs[0], "distinguishedName", NULL);
	DEBUG(4, ("[%s] folder DN is %s\n", __FUNCTION__, dn_str));
	mem_ctx = talloc_init(NULL);
	dn = ldb_dn_new(mem_ctx, mstoredb_ctx->ldb_ctx, dn_str);

	ret = ldb_delete(mstoredb_ctx->ldb_ctx, dn);
	talloc_free(mem_ctx);
	talloc_free(res);

	return MAPISTORE_SUCCESS;
}

/**
   \details Create a folder in the mstoredb backend

   \param private_data pointer to the current mstoredb context
   \param parent_fid the parent folder identifier
   \param fid the folder identifier of the folder to create
   \param aRow pointer to the list of properties to add to the folder record
   \param uri the resulting mapistore URI for this folder

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE_ERROR
 */
static enum MAPISTORE_ERROR mstoredb_op_mkdir(void *private_data, 
					      uint64_t parent_fid, 
					      uint64_t fid,
					      struct SRow *aRow,
					      char **uri)
{
	int 			i = 0;
	const char 		*new_folder_name = NULL;
	enum MAPISTORE_ERROR 	result;

	/* sanity checks - most performed in mstoredb_op_root_mkdir() */
	if ( ! aRow) {
		return MAPISTORE_ERR_INVALID_PARAMETER;
	}

	for (i = 0; i < aRow->cValues; ++i) {
		if (aRow->lpProps[i].ulPropTag == PR_DISPLAY_NAME) {
			new_folder_name = aRow->lpProps[i].value.lpszA;
		}
	}
	DEBUG(5, ("[%s] new_folder_name: %s\n", __FUNCTION__, new_folder_name));

	if ( ! new_folder_name) {
		DEBUG(0, ("[%s] null folder name\n", __FUNCTION__));
		return MAPISTORE_ERR_INVALID_PARAMETER;
	}

	result = mstoredb_op_root_mkdir(private_data, parent_fid, fid, MDB_CUSTOM, new_folder_name, uri);

	return result;
}

/**
   \details Open a folder from the mstoredb backend

   \param private_data pointer to the current mstoredb context
   \param parent_fid the parent folder identifier
   \param fid the identifier of the colder to open

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE_ERROR
 */
static enum MAPISTORE_ERROR mstoredb_op_opendir(void *private_data, uint64_t parent_fid, uint64_t fid)
{
	// TALLOC_CTX			*mem_ctx;
	struct mstoredb_context		*mstoredb_ctx = (struct mstoredb_context *)private_data;

	DEBUG(5, ("[%s:%d]\n", __FUNCTION__, __LINE__));

	if (!mstoredb_ctx) {
		return MAPISTORE_ERROR;
	}
	/* TODO: this is just a stub implementation, but mstoredb is fairly tolerant :-) */
	DEBUG(5, ("[%s] parent_fid: 0x%016"PRIx64", fid: 0x%016"PRIx64"\n", __FUNCTION__, parent_fid, fid));

	return MAPISTORE_SUCCESS;
}

/**
   \details Read directory content from the mstoredb backend

   \param private_data pointer to the current mstoredb context
   \param fid the folder ID of the folder to read from
   \param table_type the type of table (MAPISTORE_FOLDER_TABLE or MAPISTORE_MESSAGE_TABLE)
   \param RowCount the return value (number of folders or messages)

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE_ERROR
 */
static enum MAPISTORE_ERROR mstoredb_op_readdir_count(void *private_data, uint64_t fid, uint8_t table_type, uint32_t *RowCount)
{
	struct mstoredb_context		*mstoredb_ctx = (struct mstoredb_context *)private_data;
	struct ldb_result		*res = NULL;
	const char * const		folder_attrs[] = { "*", NULL };
	int				ret;

	DEBUG(5, ("[%s:%d]\n", __FUNCTION__, __LINE__));

	if (!mstoredb_ctx) {
		return MAPISTORE_ERROR;
	}
	DEBUG(5, ("[%s] fid: 0x%016"PRIx64"\n", __FUNCTION__, fid));

	/* construct query */
	switch (table_type) {
	case MAPISTORE_FOLDER_TABLE:
	{
		ret = ldb_search(mstoredb_ctx->ldb_ctx, mstoredb_ctx, &res,
				ldb_get_default_basedn(mstoredb_ctx->ldb_ctx),
				LDB_SCOPE_SUBTREE, folder_attrs, 
				"(&(PidTagParentFolderId=0x%016"PRIx64")(PidTagFolderId=*))", fid);

		if (ret != LDB_SUCCESS || !res) {
			DEBUG(0, ("%s failed lookup\n", __FUNCTION__));
			return MAPISTORE_ERR_NOT_FOUND;
		}
		/* extract results */
		*RowCount = res->count;
		break;
	}
	case MAPISTORE_MESSAGE_TABLE:
	{
		DEBUG(0, ("[%s] not yet implemented!\n", __FUNCTION__));
		break;
	}
	default:
		DEBUG(0, ("[%s] unexpected table type: %i\n", __FUNCTION__, table_type));
		break;
	}
	
	return MAPISTORE_SUCCESS;
}


static enum MAPISTORE_ERROR mstoredb_op_get_fid_by_name(void *private_data, uint64_t parent_fid, const char* foldername, uint64_t *fid)
{
	// TALLOC_CTX			*mem_ctx;
	struct mstoredb_context		*mstoredb_ctx = (struct mstoredb_context *)private_data;
	struct ldb_result		*res = NULL;
	const char * const		folder_attrs[] = { "*", NULL };
	int				ret;

	DEBUG(5, ("[%s:%d]\n", __FUNCTION__, __LINE__));

	if (!mstoredb_ctx) {
		return MAPISTORE_ERROR;
	}
	DEBUG(5, ("[%s] parent_fid: 0x%016"PRIx64", foldername: %s\n", __FUNCTION__, parent_fid, foldername));

	/* construct query */
	ret = ldb_search(mstoredb_ctx->ldb_ctx, mstoredb_ctx, &res,
			 ldb_get_default_basedn(mstoredb_ctx->ldb_ctx),
			 LDB_SCOPE_SUBTREE, folder_attrs, 
			 "(&(PidTagParentFolderId=0x%016"PRIx64")(cn=%s))", parent_fid, foldername);

	if (ret != LDB_SUCCESS || !res || res->count != 1) {
		DEBUG(5, ("%s folder not found\n", __FUNCTION__));
		return MAPISTORE_ERR_NOT_FOUND;
	}

	/* extract results */
	*fid = ldb_msg_find_attr_as_uint64(res->msgs[0], "PidTagFolderId", 0);
	DEBUG(4, ("[%s] FID for %s is 0x%016"PRIx64"\n", __FUNCTION__, foldername, *fid));

	talloc_free(res);

	return MAPISTORE_SUCCESS;
}

static bool mstoredb_convert_string_to_value(TALLOC_CTX *mem_ctx, const char *str, struct SPropValue *val)
{
	bool ret = false;

	if ( ! str) {
		val->ulPropTag = (val->ulPropTag & 0xFFFF0000) | PT_ERROR;
		val->value.err = MAPI_E_NOT_FOUND;
	} else {
		switch (val->ulPropTag & 0xFFFF) {
		case PT_SHORT:
			errno = 0;
			val->value.i = strtol(str, NULL, 0);
			ret = true;
			if (errno != 0) {
				val->ulPropTag = (val->ulPropTag & 0xFFFF0000) | PT_ERROR;
				val->value.err = MAPI_E_CORRUPT_STORE;
				ret = false;
			}
			break;
		case PT_STRING8:
			val->value.lpszA = talloc_strdup(mem_ctx, str);
			ret = true;
			break;
		case PT_UNICODE:
			val->value.lpszW = talloc_strdup(mem_ctx, str);
			ret = true;
			break;
		case PT_I8:
		{
			errno = 0;
			val->value.d = strtoll(str, NULL, 0);
			ret = true;
			if (errno != 0) {
				val->ulPropTag = (val->ulPropTag & 0xFFFF0000) | PT_ERROR;
				val->value.err = MAPI_E_CORRUPT_STORE;
				ret = false;
			}
			break;
		}
		case PT_CLSID:
		{
			DEBUG(7, ("[%s] guid_str: %s\n", __FUNCTION__, str));
			val->value.lpguid = talloc(mem_ctx, struct FlatUID_r);
			GUID_from_string(str, (struct GUID*)val->value.lpguid);
			break;
		}
		case PT_SYSTIME:
		{
			uint64_t i8;
			errno = 0;
			i8 = strtoll(str, NULL, 0);
			if (errno == 0) {
				val->value.ft.dwHighDateTime = i8 >> 32;
				val->value.ft.dwLowDateTime = i8 & 0xFFFFFFFF;
				ret = true;
			} else {
				val->ulPropTag = (val->ulPropTag & 0xFFFF0000) | PT_ERROR;
				val->value.err = MAPI_E_CORRUPT_STORE;
			}
			break;
		}
		default:
			/* if you hit this, you need to implement the missing property type */
			DEBUG(0, ("[%s] unhandled property type: 0x%04x\n", __FUNCTION__, (val->ulPropTag & 0xFFFF)));
			assert(0);
		}
	}
	return ret;
}

static void mstoredb_handle_entryid(TALLOC_CTX *parent_talloc_ctx, struct ldb_message *msg, struct SPropValue *prop)
{
	TALLOC_CTX		*mem_ctx;
	struct GUID		*mailbox_guid;
	const char		*ldb_str;
	struct GUID		*repl_guid;
	uint16_t		folder_type = 0x0001; /* TODO look this up instead of hard-coding it */
	uint64_t		fid;
	struct Binary_r		*entry_id;
	struct SPropValue	*tmp_prop;
	
	mem_ctx = talloc_new(parent_talloc_ctx);

	ldb_str = ldb_msg_find_attr_as_string(msg, "openchange_private_MailboxGUID", NULL);
	if (ldb_str == NULL) {
		DEBUG(0, ("[%s] failed to look up mailbox GUID\n", __FUNCTION__));
		talloc_free(mem_ctx);
		return;
	}
	tmp_prop = talloc_zero(mem_ctx, struct SPropValue);
	tmp_prop->ulPropTag = openchange_private_MailboxGUID;
	mstoredb_convert_string_to_value(mem_ctx, ldb_str, tmp_prop);
	mailbox_guid = talloc_memdup(mem_ctx, tmp_prop->value.lpguid, sizeof(struct GUID));
	talloc_free(tmp_prop);

	ldb_str = ldb_msg_find_attr_as_string(msg, "openchange_private_ReplicaGUID", NULL);
	if (ldb_str == NULL) {
		DEBUG(0, ("[%s] failed to look up replica GUID\n", __FUNCTION__));
		talloc_free(mem_ctx);
		return;
	}
	tmp_prop = talloc_zero(mem_ctx, struct SPropValue);
	tmp_prop->ulPropTag = openchange_private_ReplicaGUID;
	mstoredb_convert_string_to_value(mem_ctx, ldb_str, tmp_prop);
	repl_guid = talloc_memdup(mem_ctx, tmp_prop->value.lpguid, sizeof(struct GUID));
	talloc_free(tmp_prop);

	/* read fid for applicable property */
	tmp_prop = talloc_zero(mem_ctx, struct SPropValue);
	switch (prop->ulPropTag) {
	case PidTagIpmAppointmentEntryId:
		tmp_prop->ulPropTag = openchange_private_CALENDAR_FID;
		ldb_str = ldb_msg_find_attr_as_string(msg, "openchange_private_CALENDAR_FID", NULL);
		break;
	case PidTagIpmContactEntryId:
		tmp_prop->ulPropTag = openchange_private_CONTACT_FID;
		ldb_str = ldb_msg_find_attr_as_string(msg, "openchange_private_CONTACT_FID", NULL);
		break;
	case PidTagIpmJournalEntryId:
		tmp_prop->ulPropTag = openchange_private_JOURNAL_FID;
		ldb_str = ldb_msg_find_attr_as_string(msg, "openchange_private_JOURNAL_FID", NULL);
		break;
	case PidTagIpmNoteEntryId:
		tmp_prop->ulPropTag = openchange_private_NOTE_FID;
		ldb_str = ldb_msg_find_attr_as_string(msg, "openchange_private_NOTE_FID", NULL);
		break;
	case PidTagIpmTaskEntryId:
		tmp_prop->ulPropTag = openchange_private_TASK_FID;
		ldb_str = ldb_msg_find_attr_as_string(msg, "openchange_private_TASK_FID", NULL);
		break;
	case PidTagIpmDraftsEntryId:
		tmp_prop->ulPropTag = openchange_private_DRAFTS_FID;
		ldb_str = ldb_msg_find_attr_as_string(msg, "openchange_private_DRAFTS_FID", NULL);
		break;
	default:
		DEBUG(0, ("[%s] unhandled entry_id type: 0x%08x (%s)\n", __FUNCTION__, tmp_prop->ulPropTag, get_proptag_name(tmp_prop->ulPropTag)));
		break;
	}
	if (ldb_str == NULL) {
		DEBUG(0, ("[%s] failed to look up FID\n", __FUNCTION__));
		talloc_free(mem_ctx);
		return;
	}
	mstoredb_convert_string_to_value(mem_ctx, ldb_str, tmp_prop);
	fid = tmp_prop->value.d;
	talloc_free(tmp_prop);
	DEBUG(8, ("[%s] fid: 0x%016"PRIx64"\n", __FUNCTION__, fid));

	/* produce the value */
	entryid_set_folder_EntryID(mem_ctx, mailbox_guid, repl_guid, folder_type, fid, &entry_id);

	/* return the entry id */
	prop->value.bin.cb = entry_id->cb;
	prop->value.bin.lpb = talloc_memdup(parent_talloc_ctx, entry_id->lpb, entry_id->cb);
	talloc_free(mem_ctx);
}

static enum MAPISTORE_ERROR mstoredb_op_getprops(void *private_data, uint64_t fmid, uint8_t type, struct SPropTagArray *proptags, struct SRow *props)
{
	struct mstoredb_context		*mstoredb_ctx = (struct mstoredb_context *)private_data;
	struct ldb_result		*res = NULL;
	const char * const		folder_attrs[] = { "*", NULL };
	int				ret;
	int				i = 0;

	DEBUG(5, ("[%s:%d]\n", __FUNCTION__, __LINE__));

	if (!mstoredb_ctx) {
		DEBUG(0, ("[%s] null context\n", __FUNCTION__));
		return MAPISTORE_ERROR;
	}

	DEBUG(5, ("[%s] fmid: 0x%016"PRIx64"\n", __FUNCTION__, fmid));

	switch (type) {
	case MAPISTORE_FOLDER:
		/* construct query */
		ret = ldb_search(mstoredb_ctx->ldb_ctx, mstoredb_ctx, &res,
 				ldb_get_default_basedn(mstoredb_ctx->ldb_ctx),
				LDB_SCOPE_SUBTREE, folder_attrs, 
				"(&(PidTagFolderId=0x%016"PRIx64")(objectClass=folder))", fmid);
		break;
	case MAPISTORE_MESSAGE:
		DEBUG(0, ("[%s] getprops not implemented for messages yet\n", __FUNCTION__));
		return MAPISTORE_ERR_INVALID_PARAMETER;
		break;
	default:
		DEBUG(0, ("[%s] unexpected type: %i\n", __FUNCTION__, type));
		return MAPISTORE_ERR_INVALID_PARAMETER;
	}

	if (ret != LDB_SUCCESS || !res || res->count != 1) {
		DEBUG(0, ("[%s] failed lookup\n", __FUNCTION__));
		if (ret != LDB_SUCCESS) {
			DEBUG(0, ("[%s] ret = %i\n", __FUNCTION__, ret));
		} else if ( ! res) {
			DEBUG(0, ("[%s] null result\n", __FUNCTION__));
		} else {
			DEBUG(0, ("[%s] result count: %i\n", __FUNCTION__, res->count));
		}
		return MAPISTORE_ERR_NOT_FOUND;
	}

	/* extract results and return them */
	for (i = 0; i < proptags->cValues; ++i) {
	  	struct SPropValue		prop;
		prop.ulPropTag = proptags->aulPropTag[i];
		/* handle special cases */
		switch (prop.ulPropTag) {
		case PidTagIpmAppointmentEntryId:
		case PidTagIpmContactEntryId:
		case PidTagIpmJournalEntryId:
		case PidTagIpmNoteEntryId:
		case PidTagIpmTaskEntryId:
		case PidTagIpmDraftsEntryId:
			mstoredb_handle_entryid(props, res->msgs[0], &prop);
			break;
		default:
			mstoredb_convert_string_to_value(props, ldb_msg_find_attr_as_string(res->msgs[0], get_proptag_name(proptags->aulPropTag[i]), NULL), &prop);
			break;
		}
		SRow_addprop(props, prop);
	}
	talloc_free(res);

	return MAPISTORE_SUCCESS;
}


static enum MAPISTORE_ERROR mstoredb_op_setprops(void *private_data, uint64_t fmid, uint8_t type, struct SRow *props)
{
	struct mstoredb_context		*mstoredb_ctx = (struct mstoredb_context *)private_data;
	TALLOC_CTX			*mem_ctx;
	struct ldb_result		*res = NULL;
	const char * const		folder_attrs[] = { "*", NULL };
	int				ret;
	int				i = 0;
	struct ldb_message		*msg;

	DEBUG(5, ("[%s:%d]\n", __FUNCTION__, __LINE__));

	if (!mstoredb_ctx) {
		return MAPISTORE_ERROR;
	}

	switch (type) {
	case MAPISTORE_FOLDER:
		/* construct query */
		ret = ldb_search(mstoredb_ctx->ldb_ctx, mstoredb_ctx, &res,
				ldb_get_default_basedn(mstoredb_ctx->ldb_ctx),
				LDB_SCOPE_SUBTREE, folder_attrs, 
				"(&(PidTagFolderId=0x%016"PRIx64")(objectClass=folder))", fmid);
		break;
	case MAPISTORE_MESSAGE:
		DEBUG(0, ("[%s] getprops not implemented for messages yet\n", __FUNCTION__));
		return MAPISTORE_ERR_INVALID_PARAMETER;
		break;
	default:
		DEBUG(0, ("[%s] unexpected type: %i\n", __FUNCTION__, type));
		return MAPISTORE_ERR_INVALID_PARAMETER;
	}

	if (ret != LDB_SUCCESS || !res || res->count != 1) {
		DEBUG(0, ("%s failed lookup\n", __FUNCTION__));
		return MAPISTORE_ERR_NOT_FOUND;
	}

	mem_ctx = talloc_init(NULL);

	msg = ldb_msg_new(mem_ctx);
	msg->dn = ldb_dn_copy(msg, ldb_msg_find_attr_as_dn(mstoredb_ctx->ldb_ctx, mem_ctx, res->msgs[0], "distinguishedName"));
	talloc_free(res);
	for (i = 0; i < props->cValues; ++i) {
		/* TODO: handle other property types */
		ldb_msg_add_fmt(msg, get_proptag_name(props->lpProps[i].ulPropTag), "0x%"PRIx64, props->lpProps[i].value.d);
		msg->elements[i].flags = LDB_FLAG_MOD_REPLACE;
	}
	ret = ldb_modify(mstoredb_ctx->ldb_ctx, msg);
	if (ret != LDB_SUCCESS) {
		DEBUG(0, ("[%s] failed to set properties\n", __FUNCTION__));
		talloc_free(mem_ctx);
		return MAPISTORE_ERR_DATABASE_OPS;
	}

	talloc_free(mem_ctx);

	return MAPISTORE_SUCCESS;
}

/* TODO: this is a bit strange, because we're looking up a "table" that 
   could potentially change the row order between calls */
static enum MAPISTORE_ERROR mstoredb_op_get_table_property(void *private_data,
							   uint64_t fid,
							   uint8_t table_type,
							   uint32_t pos,
							   uint32_t proptag,
							   void **data)
{
	struct mstoredb_context		*mstoredb_ctx = (struct mstoredb_context *)private_data;
	struct ldb_result		*res = NULL;
	const char * const		folder_attrs[] = { "*", NULL };
	int				ret;
	const char			*proptag_name;
	const char			*lookup_result;
	enum MAPISTORE_ERROR		retval;

	if (!mstoredb_ctx) {
		DEBUG(0, ("[%s] null context\n", __FUNCTION__));
		return MAPISTORE_ERROR;
	}

	proptag_name = get_proptag_name(proptag);
	if ( ! proptag_name) {
		DEBUG(0, ("[%s] can't look up property 0x%08x\n", __FUNCTION__, proptag));
		return MAPISTORE_ERR_NOT_FOUND;
	}

	DEBUG(9, ("[%s] fid: 0x%016"PRIx64"\n", __FUNCTION__, fid));
	DEBUG(9, ("[%s] table type: %i\n", __FUNCTION__, table_type));
	DEBUG(9, ("[%s] pos: %i\n", __FUNCTION__, pos));
	DEBUG(9, ("[%s] proptag: 0x%08x (%s)\n", __FUNCTION__, proptag, proptag_name));

	/* special case - calculated property */
	if (proptag == PR_FOLDER_CHILD_COUNT) {
		uint64_t *subfolder_fid = 0;
		uint32_t *subfolder_count = talloc_zero(mstoredb_ctx, uint32_t);
		retval = mstoredb_op_get_table_property(private_data, fid, table_type, pos, PidTagFolderId, (void**)&subfolder_fid);
		if (retval == MAPISTORE_SUCCESS) {
			DEBUG(0, ("[%s] getting child count for folder: 0x%016"PRIx64"\n", __FUNCTION__, *subfolder_fid));
			retval = mstoredb_op_readdir_count(private_data, *subfolder_fid, table_type, subfolder_count);
			*data = subfolder_count;
		}
		talloc_free(subfolder_fid);
		return retval;
	}

	switch (table_type) {
	case MAPISTORE_FOLDER:
		/* construct query */
		ret = ldb_search(mstoredb_ctx->ldb_ctx, mstoredb_ctx, &res,
				ldb_get_default_basedn(mstoredb_ctx->ldb_ctx),
				LDB_SCOPE_SUBTREE, folder_attrs, 
				"(&(PidTagParentFolderId=0x%016"PRIx64")(objectClass=folder))", fid);
		break;
	case MAPISTORE_MESSAGE:
		DEBUG(0, ("[%s] not implemented for messages yet\n", __FUNCTION__));
		return MAPISTORE_ERR_INVALID_PARAMETER;
		break;
	default:
		DEBUG(0, ("[%s] unexpected table type: %i\n", __FUNCTION__, table_type));
		return MAPISTORE_ERR_INVALID_PARAMETER;
	}

	if (ret != LDB_SUCCESS || !res || res->count < 1) {
		DEBUG(0, ("[%s] failed lookup\n", __FUNCTION__));
		if (ret != LDB_SUCCESS) {
			DEBUG(0, ("[%s] ret = %i\n", __FUNCTION__, ret));
		} else if ( ! res) {
			DEBUG(0, ("[%s] null result\n", __FUNCTION__));
		} else {
			DEBUG(0, ("[%s] result count: %i\n", __FUNCTION__, res->count));
			talloc_free(res);
		}
		return MAPISTORE_ERR_NOT_FOUND;
	}
	if (pos >= res->count) {
		DEBUG(4, ("[%s] pos: %i, count: %i\n", __FUNCTION__, pos, res->count));
		talloc_free(res);
		return MAPISTORE_ERR_NOT_FOUND;
	}

	/* extract result and return it */
	lookup_result = ldb_msg_find_attr_as_string(res->msgs[pos], proptag_name, NULL);
	DEBUG(4, ("[%s] pos: %i, value = %s\n", __FUNCTION__, pos, lookup_result));
	if ( ! lookup_result) {
		*data = NULL;
		retval =  MAPISTORE_ERR_NOT_FOUND;
	} else {
		/* TODO: share this code with op_getprops() */
		/* TODO: this needs to be completed, and to handle the case where we don't find the property */
		switch(proptag & 0xFFFF) {
			case PT_STRING8:
			case PT_UNICODE:
				*data = (void*) talloc_strdup(mstoredb_ctx, lookup_result);
				retval = MAPISTORE_SUCCESS;
				break;
			case PT_I8:
			{
				uint64_t *i8 = talloc_zero(mstoredb_ctx, uint64_t);
				errno = 0;
				*i8 = strtoll(lookup_result, NULL, 0);
				if (errno == 0) {
					*data = i8;
					retval = MAPISTORE_SUCCESS;
				} else {
					*data = NULL;
					retval = MAPISTORE_ERR_NOT_FOUND;
				}
				break;
			}
			default:
				/* if you hit this, you need to implement the missing property type */
				*data = NULL;
				DEBUG(0, ("[%s] unhandled property type: 0x%04x\n", __FUNCTION__, (proptag & 0xFFFF)));
				retval = MAPISTORE_ERR_NOT_FOUND;
				// assert(0);
		}
	}
	talloc_free(res);

	return retval;
}

static enum MAPISTORE_ERROR mstoredb_op_get_table(void *private_data,
						  const uint64_t fid,
						  const uint8_t table_type,
						  const struct SPropTagArray *props,
						  struct SRowSet *rows)
{
	struct mstoredb_context		*mstoredb_ctx = (struct mstoredb_context *)private_data;
	struct ldb_result		*res = NULL;
	const char * const		all_attrs[] = { "*", NULL };
	int				ret;
	const char			*lookup_result;
	enum MAPISTORE_ERROR		retval = MAPISTORE_SUCCESS;
	int				i = 0;
	int				j = 0;

	if (!mstoredb_ctx) {
		DEBUG(0, ("[%s] null context\n", __FUNCTION__));
		return MAPISTORE_ERROR;
	}

	DEBUG(5, ("[%s] fid: 0x%016"PRIx64"\n", __FUNCTION__, fid));
	DEBUG(5, ("[%s] table type: %i\n", __FUNCTION__, table_type));
	mapidump_SPropTagArray(props);

	switch (table_type) {
	case MAPISTORE_RECEIVE_TABLE:
		/* construct query */
		ret = ldb_search(mstoredb_ctx->ldb_ctx, mstoredb_ctx, &res,
				ldb_get_default_basedn(mstoredb_ctx->ldb_ctx),
				LDB_SCOPE_SUBTREE, all_attrs, 
				"(&(PidTagParentFolderId=0x%016"PRIx64")(objectClass=tablerow))", fid);
		break;
	default:
		DEBUG(0, ("[%s] unhandled table type: %i\n", __FUNCTION__, table_type));
		assert(0); /* just for debugging */
		return MAPISTORE_ERR_INVALID_PARAMETER;
	}
	
	/* check query result */
	if (ret != LDB_SUCCESS || !res) {
		DEBUG(0, ("[%s] failed lookup\n", __FUNCTION__));
		if (ret != LDB_SUCCESS) {
			DEBUG(0, ("[%s] ret = %i\n", __FUNCTION__, ret));
		} else if ( ! res) {
			DEBUG(0, ("[%s] null result\n", __FUNCTION__));
		}
		return MAPISTORE_ERR_NOT_FOUND;
	}

	rows->cRows = res->count;
	rows->aRow = talloc_zero_array(rows, struct SRow, rows->cRows);

	/* extract results and return them */
	for (i = 0; i < res->count; ++i) {
		rows->aRow[i].cValues = props->cValues;
		rows->aRow[i].lpProps = talloc_zero_array(rows->aRow, struct SPropValue, rows->aRow[i].cValues);
		for (j = 0; j < props->cValues; ++j) {
			const char *proptag_name = get_proptag_name(props->aulPropTag[j]);
			if ( ! proptag_name) {
				DEBUG(0, ("[%s] can't look up property 0x%08x\n", __FUNCTION__, props->aulPropTag[j]));
				// TODO: handle this case
			}
			lookup_result = ldb_msg_find_attr_as_string(res->msgs[i], proptag_name, NULL);
			DEBUG(4, ("[%s] row: %i, tag: %s, value = %s\n", __FUNCTION__, i, proptag_name, lookup_result));
			rows->aRow[i].lpProps[j].ulPropTag = props->aulPropTag[j];
			mstoredb_convert_string_to_value(rows->aRow[i].lpProps, lookup_result, &(rows->aRow[i].lpProps[j]));
		}
	}

	talloc_free(res);

#if 0
	/* special case - calculated property */
	if (proptag == PR_FOLDER_CHILD_COUNT) {
		uint64_t *subfolder_fid = 0;
		uint32_t *subfolder_count = talloc_zero(mstoredb_ctx, uint32_t);
		retval = mstoredb_op_get_table_property(private_data, fid, table_type, pos, PidTagFolderId, (void**)&subfolder_fid);
		if (retval == MAPISTORE_SUCCESS) {
			DEBUG(0, ("[%s] getting child count for folder: 0x%016"PRIx64"\n", __FUNCTION__, *subfolder_fid));
			retval = mstoredb_op_readdir_count(private_data, *subfolder_fid, table_type, subfolder_count);
			*data = subfolder_count;
		}
		talloc_free(subfolder_fid);
		return retval;
	}

	switch (table_type) {
	case MAPISTORE_FOLDER:
		/* construct query */
		ret = ldb_search(mstoredb_ctx->ldb_ctx, mstoredb_ctx, &res,
				ldb_get_default_basedn(mstoredb_ctx->ldb_ctx),
				LDB_SCOPE_SUBTREE, folder_attrs, 
				"(&(PidTagParentFolderId=0x%016"PRIx64")(objectClass=folder))", fid);
		break;
	}
#endif

	return retval;
}

static enum MAPISTORE_ERROR mstoredb_op_add_table_row(void *private_data, const uint64_t fid, const uint8_t table_type, const struct SRow *rowdata)
{
	/* TODO: sanity checks */
	
	/* TODO: construct LDIF */
	
	/* TODO: write to store */
	
	return MAPISTORE_SUCCESS;
}


static enum MAPISTORE_ERROR mstoredb_op_create_message(void *private_data,
						       uint64_t fid,
						       uint64_t mid)
{
	DEBUG(0, ("[%s:%d][%s] not yet implemented\n", __FILE__, __LINE__, __FUNCTION__));

	/* TODO: sanity checks */
	
	/* TODO: temporary object */
	
	return MAPISTORE_SUCCESS;
}


static enum MAPISTORE_ERROR mstoredb_op_release_record(void *private_data, uint64_t fmid, uint8_t type)
{
	DEBUG(0, ("[%s:%d][%s] not yet implemented\n", __FILE__, __LINE__, __FUNCTION__));

	/* TODO: sanity checks */

	/* TODO: temporary object */

	return MAPISTORE_SUCCESS;
}


static enum MAPISTORE_ERROR mstoredb_op_savechangesmessage(void *private_data,
							   uint64_t mid,
							   uint8_t flags)
{
	DEBUG(0, ("[%s:%d][%s] not yet implemented\n", __FILE__, __LINE__, __FUNCTION__));

	/* TODO: sanity checks */

	/* TODO: construct LDIF */
	
	/* TODO: write to store */

	return MAPISTORE_SUCCESS;
}


static enum MAPISTORE_ERROR mstoredb_op_deletemessage(void *private_data,
						      uint64_t mid,
						      uint8_t flags)
{
	DEBUG(0, ("[%s:%d][%s] not yet implemented\n", __FILE__, __LINE__, __FUNCTION__));

	/* TODO: sanity checks */

	/* TODO: construct LDIF */
	
	/* TODO: commit to store */

	return MAPISTORE_SUCCESS;
}


static enum MAPISTORE_ERROR mstoredb_op_open_message(void *private_data,
						     uint64_t fid,
						     uint64_t mid,
						     struct mapistore_message *msg)
{
	/* TODO: sanity checks */

	/* TODO: construct query */

	/* TODO: retrieve data */

	/* TODO: construct real result */
	msg->recipients = NULL;
	msg->properties = NULL;

	return MAPISTORE_ERR_NOT_FOUND;
}


/**
   \details Entry point for mapistore MSTOREDB backend

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
 */
enum MAPISTORE_ERROR mapistore_init_backend(void)
{
	struct mapistore_backend	backend;
	int				ret;

	/* Fill in our name */
	backend.name = "mstoredb";
	backend.description = "mapistore database backend";
	backend.uri_namespace = "mstoredb://";

	/* Fill in all the operations */
	backend.init = mstoredb_init;
	backend.create_context = mstoredb_create_context;
	backend.delete_context = mstoredb_delete_context;
	backend.op_root_mkdir = mstoredb_op_root_mkdir;
	backend.create_uri = mstoredb_create_uri;
	backend.release_record = mstoredb_op_release_record;
	backend.get_path = NULL;
	backend.op_mkdir = mstoredb_op_mkdir;
	backend.op_rmdir = mstoredb_op_rmdir;
	backend.op_opendir = mstoredb_op_opendir;
	backend.op_closedir = NULL;
	backend.op_readdir_count = mstoredb_op_readdir_count;
	backend.op_get_table_property = mstoredb_op_get_table_property;
	backend.op_openmessage = mstoredb_op_open_message;
	backend.op_createmessage = mstoredb_op_create_message;
	backend.op_savechangesmessage = mstoredb_op_savechangesmessage;
	backend.op_submitmessage = NULL;
	backend.op_getprops = mstoredb_op_getprops;
	backend.op_get_fid_by_name = mstoredb_op_get_fid_by_name;
	backend.op_setprops = mstoredb_op_setprops;
	backend.op_deletemessage = mstoredb_op_deletemessage;
	backend.op_get_table = mstoredb_op_get_table;
	backend.op_add_table_row = mstoredb_op_add_table_row;

	/* Register ourselves with the MAPISTORE subsystem */
	ret = mapistore_backend_register(&backend);
	if (ret != MAPISTORE_SUCCESS) {
		DEBUG(0, ("Failed to register the '%s' mapistore backend!\n", backend.name));
		return ret;
	}

	return MAPISTORE_SUCCESS;
}
