/*
   OpenChange Storage Abstraction Layer library

   OpenChange Project

   Copyright (C) Julien Kerihuel 2009-2010

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef	__MAPISTORE_PRIVATE_H__
#define	__MAPISTORE_PRIVATE_H__

#include <talloc.h>

void mapistore_set_errno(int);

#define	MAPISTORE_RETVAL_IF(x,e,c)	\
do {					\
	if (x) {			\
		mapistore_set_errno(e);	\
		if (c) {		\
			talloc_free(c);	\
		}			\
		return (e);		\
	}				\
} while (0);

#define	MAPISTORE_SANITY_CHECKS(x,c)						\
MAPISTORE_RETVAL_IF(!x, MAPISTORE_ERR_NOT_INITIALIZED, c);			\
MAPISTORE_RETVAL_IF(!x->processing_ctx, MAPISTORE_ERR_NOT_INITIALIZED, c);	\
MAPISTORE_RETVAL_IF(!x->context_list, MAPISTORE_ERR_NOT_INITIALIZED, c);

#ifndef	ISDOT
#define ISDOT(path) ( \
			*((const char *)(path)) == '.' && \
			*(((const char *)(path)) + 1) == '\0' \
		    )
#endif

#ifndef	ISDOTDOT
#define ISDOTDOT(path)	( \
			    *((const char *)(path)) == '.' && \
			    *(((const char *)(path)) + 1) == '.' && \
			    *(((const char *)(path)) + 2) == '\0' \
			)
#endif

/* These are essentially local versions of part of the 
   C99 __STDC_FORMAT_MACROS */
#ifndef PRIx64
#if __WORDSIZE == 64
  #define PRIx64        "lx"
#else
  #define PRIx64        "llx"
#endif
#endif

#ifndef PRIX64
#if __WORDSIZE == 64
  #define PRIX64        "lX"
#else
  #define PRIX64        "llX"
#endif
#endif

struct tdb_wrap {
	struct tdb_context	*tdb;
	const char		*name;
	struct tdb_wrap		*prev;
	struct tdb_wrap		*next;
};


struct ldb_wrap {
  struct ldb_wrap			*next;
	struct ldb_wrap			*prev;
	struct ldb_wrap_context {
		const char		*url;
		struct tevent_context	*ev;
		unsigned int		flags;
	} context;
	struct ldb_context		*ldb;
};

/**
   Mapistore database context.
   
   This structure stores parameters for mapistore database and backend
   initialization.
 */

/* #define DFLT_MDB_STORE_PATH  defined at compilation time, see config.mk.in */
#define	DFLT_MDB_FIRSTORG	"First Organization"
#define	DFLT_MDB_FIRSTOU	"First Organization Unit"
#define	TMPL_MDB_SERVERDN	"CN=%s,%s"
#define	TMPL_MDB_FIRSTORGDN	"CN=%s,CN=%s,%s"

struct mapistoredb_conf {
	char		*db_path;
	char		*mstore_path;
	char		*netbiosname;
	char		*dnsdomain;
	char		*domain;
	char		*domaindn;
	char		*serverdn;
	char		*firstorg;
	char		*firstou;
	char		*firstorgdn;
};

struct mapistoredb_context {
	struct ldb_context		*ldb_ctx;
	struct loadparm_context		*lp_ctx;
	struct tevent_context		*ev;
	struct mapistoredb_conf		*param;
	struct mapistore_context	*mstore_ctx;
};

#define	MDB_INIT_LDIF_TMPL		\
	"dn: @OPTIONS\n"		\
	"checkBaseOnSearch: TRUE\n\n"	\
	"dn: @INDEXLIST\n"		\
	"@IDXATTR: cn\n\n"		\
	"dn: @ATTRIBUTES@\n"		\
	"cn: CASE_INSENSITIVE\n"	\
	"dn: CASE_INSENSITIVE\n\n"

#define	MDB_ROOTDSE_LDIF_TMPL						\
	"dn: @ROOTDSE\n"						\
	"defaultNamingContext: CN=%s,%s,%s\n"				\
	"rootDomainNamingContext: %s\n"					\
	"vendorName: OpenChange Project (http://www.openchange.org)\n\n"

#define	MDB_SERVER_LDIF_TMPL		\
	"dn: %s\n"			\
	"objectClass: top\n"		\
	"objectClass: server\n"		\
	"cn: %s\n"			\
	"ReplicaID: 0x1\n\n"		\
					\
	"dn: CN=%s,%s\n"		\
	"objectClass: top\n"		\
	"objectClass: org\n"		\
	"cn: %s\n\n"			\
					\
	"dn: CN=%s,CN=%s,%s\n"		\
	"objectClass: top\n"		\
	"objectClass: ou\n"		\
	"cn: %s\n"

#define	MDB_PF_ROOT_LDIF_TMPL			\
	"dn: CN=publicfolders,%s\n"		\
	"objectClass: folder\n"			\
	"objectClass: container\n"		\
	"PidTagDisplayName: Public Folders\n"	\
	"openchange_private_MailboxGUID: %s\n"	\
	"openchange_private_ReplicaID: %s\n"	\
	"mapistore_uri: mstoredb://CN=%s,%s\n"	\
	"PidTagFolderId: 0x%.16"PRIx64"\n"	\
	"cn: publicfolders\n\n"

#define	MDB_MAILBOX_TMPL				\
	"dn: CN=%s,%s\n"				\
	"objectClass: mailbox\n"			\
	"objectClass: folder\n"				\
	"objectClass: container\n"			\
	"FolderType: 0x0001\n"				\
	"PidTagDisplayName: OpenChange Mailbox: %s\n"	\
	"PidTagParentFolderId: 0x0000000000000000\n"	\
	"PidTagFolderId: 0x%.16"PRIx64"\n"		\
	"cn: %s\n"					\
	"openchange_private_MailboxGUID: %s\n"		\
	"openchange_private_ReplicaID: %s\n"		\
	"openchange_private_ReplicaGUID: %s\n"		\
	"mapistore_uri: mstoredb://CN=%s,%s\n\n"

#define	MDB_MAILBOX_TMPL_URI	"mstoredb://CN=%s,%s"

#if 0
#define	MDB_MAILBOX_SYSTEM_FOLDER_TMPL			\
	"dn: CN=0x%.16llx,%s\n"				\
	"objectClass: systemfolder\n"			\
	"objectClass: container\n"			\
	"cn: 0x%.16llx\n"				\
	"PidTagParentFolderId: 0x%.16"PRIx64"\n"	\
	"SystemIdx: %d\n\n"

#define	MDB_MAILBOX_SPECIAL_FOLDER_TMPL			\
	"dn: CN=0x%llx,%s\n"				\
	"objectClass: specialfolder\n"			\
	"cn: 0x%llx\n"					\
	"PidTagParentFolderId: 0x%llx\n"		\
	"PidTagFolderId: 0x%llx\n"			\
	"mapistore_uri: %s\n\n"				
#endif

#define MDB_MAILBOX_SPEC_FOLDER_TMPL			\
	"dn: %s\n"					\
	"objectClass: %s\n"				\
	"objectClass: folder\n"				\
	"objectClass: container\n"			\
	"FolderType: 0x0001\n"				\
	"PidTagDisplayName: %s\n"			\
	"PidTagParentFolderId: 0x%.16"PRIx64"\n"	\
	"PidTagFolderId: 0x%.16"PRIx64"\n"		\
	"cn: %s\n"					\
	"PidTagContainerClass: %s\n"			\
	"special_folder_type: %i\n"			\
	"mapistore_uri: %s\n\n"

#define MDB_MAILBOX_FOLDER_TMPL				\
	"dn: %s\n"					\
	"objectClass: %s\n"				\
	"objectClass: folder\n"				\
	"objectClass: container\n"			\
	"FolderType: 0x0001\n"				\
	"PidTagDisplayName: %s\n"			\
	"PidTagParentFolderId: 0x%.16"PRIx64"\n"	\
	"PidTagFolderId: 0x%.16"PRIx64"\n"		\
	"cn: %s\n"					\
	"mapistore_uri: %s\n\n"

#define	DFLT_GLOBCOUNT_INCREMENT	0x1000

/**
   Identifier mapping context.

   This structure stores PR_MID and PR_FID identifiers to backend
   identifiers mapping. It points to a database containing the used
   identifiers.

   The last_id structure member references the last identifier value
   which got created. There is no identifier available with a value
   higher than last_id.
 */
struct id_mapping_context {
	struct tdb_wrap		*used_ctx;
	uint64_t		last_id;
};


/**
   Free context identifier list

   This structure is a double chained list storing unused context
   identifiers.
 */
struct context_id_list {
	uint32_t		context_id;
	struct context_id_list	*prev;
	struct context_id_list	*next;
};


struct processing_context {
	struct id_mapping_context	*mapping_ctx;
	struct context_id_list		*free_ctx;
	uint32_t			last_context_id;
	uint64_t			dflt_start_id;
};

/**
   MAPIStore indexing context list
 */
struct mapistore_indexing_context_list {
	struct tdb_wrap				*tdb_ctx;
	const char				*username;
	uint32_t				ref_count;
	struct mapistore_indexing_context_list	*prev;
	struct mapistore_indexing_context_list	*next;
};

#define	MAPISTORE_INDEXING_DBPATH_TMPL	"%s/mapistore_indexing_%s.tdb"

#define	MAPISTORE_DB_NAMED		"named_properties.ldb"
#define	MAPISTORE_DB_INDEXING		"indexing.tdb"

/**
   The database name where in use ID mappings are stored
 */
#define	MAPISTORE_DB_LAST_ID_KEY	"mapistore_last_id"
#define	MAPISTORE_DB_LAST_ID_VAL	(0x1000 << 16)

#define	MAPISTORE_DB_NAME_USED_ID	"mapistore_id_mapping_used.tdb"


/**
   Mapistore opaque context to pass down to backends

   Semantically, encapsulating the mstore_ctx within a container's
   structure help abstracting mapistore logic from backend's
   perspective and write a specific API dealing exclusively with this
   particular context.
 */
struct mapistore_backend_context {
	struct mapistore_context	*mstore_ctx;
};


__BEGIN_DECLS

/* definitions from mapistore_processing.c */
const char *mapistore_get_mapping_path(void);
const char *mapistore_get_database_path(void);
int mapistore_init_mapping_context(struct processing_context *);
int mapistore_get_context_id(struct processing_context *, uint32_t *);
int mapistore_free_context_id(struct processing_context *, uint32_t);


/* definitions from mapistore_backend.c */
int mapistore_backend_init(TALLOC_CTX *, const char *);
struct backend_context *mapistore_backend_create_context(struct mapistore_context *, const char *, const char *, const char *);
char *mapistore_backend_create_mapistoreURI(TALLOC_CTX *, const char *, const char *, uint64_t *);
const char *mapistore_backend_get_uri(struct backend_context *);
const char *mapistore_backend_get_username(struct backend_context *);
int mapistore_backend_root_mkdir(struct backend_context *, uint64_t, uint64_t, uint32_t, const char *, const char *);
int mapistore_backend_add_ref_count(struct backend_context *);
int mapistore_backend_delete_context(struct backend_context *);
int mapistore_backend_release_record(struct backend_context *, uint64_t, uint8_t);
int mapistore_get_path(struct backend_context *, uint64_t, uint8_t, char **);
int mapistore_backend_opendir(struct backend_context *, uint64_t, uint64_t);
int mapistore_backend_mkdir(struct backend_context *, uint64_t, uint64_t, struct SRow *);
int mapistore_backend_readdir_count(struct backend_context *, uint64_t, uint8_t, uint32_t *);
int mapistore_backend_rmdir(struct backend_context *, uint64_t, uint64_t);
int mapistore_backend_get_table_property(struct backend_context *, uint64_t, uint8_t, uint32_t, 
					 uint32_t, void **);
int mapistore_backend_openmessage(struct backend_context *, uint64_t, uint64_t, struct mapistore_message *);
int mapistore_backend_createmessage(struct backend_context *, uint64_t, uint64_t);
int mapistore_backend_savechangesmessage(struct backend_context *, uint64_t, uint8_t);
int mapistore_backend_submitmessage(struct backend_context *, uint64_t, uint8_t);
int mapistore_backend_getprops(struct backend_context *, uint64_t, uint8_t, 
			       struct SPropTagArray *, struct SRow *);
int mapistore_backend_setprops(struct backend_context *, uint64_t, uint8_t, struct SRow *);
int mapistore_backend_get_fid_by_name(struct backend_context *, uint64_t, const char *, uint64_t *);
int mapistore_backend_deletemessage(struct backend_context *, uint64_t, uint8_t);
enum MAPISTORE_ERROR mapistore_backend_get_table(struct backend_context *bctx, const uint64_t fid, const uint8_t table_type, const struct SPropTagArray *proptags, struct SRowSet *rows);
enum MAPISTORE_ERROR mapistore_backend_add_table_row(struct backend_context *bctx, const uint64_t fid, const uint8_t table_type, const struct SRow *rowdata);

/* definitions from mapistore_tdb_wrap.c */
struct tdb_wrap *tdb_wrap_open(TALLOC_CTX *, const char *, int, int, int, mode_t);

/* definitions from mapistore_ldb_wrap.c */
struct ldb_context *mapistore_ldb_wrap_connect(TALLOC_CTX *, struct tevent_context *, const char *, unsigned int);

/* definitions from mapistore_indexing.c */
int mapistore_indexing_context_add(struct mapistore_context *, const char *, struct mapistore_indexing_context_list **);
int mapistore_indexing_context_del(struct mapistore_context *, const char *);
int mapistore_indexing_record_add_fid(struct mapistore_context *, const char *, uint64_t, const char *, uint64_t);
int mapistore_indexing_record_add_mid(struct mapistore_context *, const char *, uint64_t, const char *, uint64_t);
int mapistore_indexing_record_del_fid(struct mapistore_context *, const char *, uint64_t, uint8_t);
int mapistore_indexing_record_del_mid(struct mapistore_context *, const char *, uint64_t, uint8_t);
int mapistore_indexing_lookup_mid(struct mapistore_context *, const char *, const char *, uint64_t *);
int mapistore_indexing_lookup_fid(struct mapistore_context *, const char *, const char *, uint64_t *);
int mapistore_indexing_record_add_fmid(struct mapistore_indexing_context_list *, uint64_t, const char *, uint64_t, uint8_t);
int mapistore_indexing_context_add_ref(struct mapistore_context *mstore_ctx, const char *username);

/* definitions from mapistore_namedprops.c */
int mapistore_namedprops_init(TALLOC_CTX *, void **);
int mapistore_namedprops_provision(TALLOC_CTX *);

__END_DECLS

#endif	/* ! __MAPISTORE_PRIVATE_H__ */
