/*
   OpenChange Storage Abstraction Layer library

   OpenChange Project

   Copyright (C) Julien Kerihuel 2009

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mapistore.h"
#include "mapistore_errors.h"
#include "mapistore_private.h"
#include <dlinklist.h>
#include "libmapi/libmapi_private.h"

#include <string.h>

/**
   \details Initialize the mapistore context

   \param mem_ctx pointer to the memory context
   \param path path to the backend DSO folder

   \return allocate mapistore context on success, otherwise NULL
 */
_PUBLIC_ struct mapistore_context *mapistore_init(TALLOC_CTX *mem_ctx, const char *path)
{
	int				retval;
	struct mapistore_context	*mstore_ctx;

	mstore_ctx = talloc_zero(mem_ctx, struct mapistore_context);
	if (!mstore_ctx) {
		return NULL;
	}

	mstore_ctx->processing_ctx = talloc_zero(mstore_ctx, struct processing_context);

	retval = mapistore_init_mapping_context(mstore_ctx->processing_ctx);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(5, ("[%s:%d]: %s\n", __FUNCTION__, __LINE__, mapistore_errstr(retval)));
		talloc_free(mstore_ctx);
		return NULL;
	}

	retval = mapistore_backend_init(mstore_ctx, path);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(5, ("[%s:%d]: %s\n", __FUNCTION__, __LINE__, mapistore_errstr(retval)));
		talloc_free(mstore_ctx);
		return NULL;
	}

	mstore_ctx->context_list = NULL;
	mstore_ctx->indexing_list = talloc_zero(mstore_ctx, struct mapistore_indexing_context_list);

	mstore_ctx->nprops_ctx = NULL;
	retval = mapistore_namedprops_init(mstore_ctx, &(mstore_ctx->nprops_ctx));

	/* Initialize the mapistore backend context */
	mstore_ctx->backend_ctx = talloc_zero(mstore_ctx, struct mapistore_backend_context);
	mstore_ctx->backend_ctx->mstore_ctx = mstore_ctx;

	return mstore_ctx;
}


/**
   \details Release the mapistore context and destroy any data
   associated

   \param mstore_ctx pointer to the mapistore context

   \note The function needs to rely on talloc destructors which is not
   implemented in code yet.

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_release(struct mapistore_context *mstore_ctx)
{
	if (!mstore_ctx) return MAPISTORE_ERR_NOT_INITIALIZED;

	talloc_free(mstore_ctx->nprops_ctx);
	talloc_free(mstore_ctx->processing_ctx);
	talloc_free(mstore_ctx->context_list);
	talloc_free(mstore_ctx);

	return MAPISTORE_SUCCESS;
}


/**
   \details Add a new connection context to mapistore

   \param mstore_ctx pointer to the mapistore context
   \param username the user to associate to this mapistore context
   \param uri the connection context URI
   \param context_id pointer to the context identifier the function returns

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_add_context(struct mapistore_context *mstore_ctx, 
						    const char *username, const char *uri,
						    uint32_t *context_id)
{
	TALLOC_CTX				*mem_ctx;
	int					retval;
	struct backend_context			*backend_ctx;
	struct backend_context_list    		*backend_list;
	char					*uri_namespace;
	char					*namespace_start;
	char					*backend_uri;

	/* Sanity checks */
	if (!mstore_ctx) return MAPISTORE_ERR_NOT_INITIALIZED;
	if (!context_id) return MAPISTORE_ERR_INVALID_PARAMETER;
	if (!uri || strlen(uri) < 4) {
		return MAPISTORE_ERR_INVALID_NAMESPACE;
	}

	/* Step 1. Initialize the mapistore backend context */
	mem_ctx = talloc_named(NULL, 0, "mapistore_add_context");
	uri_namespace = talloc_strdup(mem_ctx, uri);
	namespace_start = uri_namespace;
	uri_namespace = strchr(uri_namespace, ':');
	if (!uri_namespace) {
		DEBUG(0, ("[%s:%d]: Error - Invalid namespace '%s'\n", __FUNCTION__, __LINE__, namespace_start));
		talloc_free(mem_ctx);
		return MAPISTORE_ERR_INVALID_NAMESPACE;
	}

	if (uri_namespace[1] && uri_namespace[1] == '/' &&
	    uri_namespace[2] && uri_namespace[2] == '/' &&
	    uri_namespace[3]) {
		backend_uri = talloc_strdup(mem_ctx, &uri_namespace[3]);
		uri_namespace[3] = '\0';
		backend_ctx = mapistore_backend_create_context(mstore_ctx, username, namespace_start, backend_uri);
		if (!backend_ctx) {
			talloc_free(mem_ctx);
			return MAPISTORE_ERR_CONTEXT_FAILED;
		}

		backend_list = talloc_zero((TALLOC_CTX *) mstore_ctx, struct backend_context_list);
		talloc_steal(backend_list, backend_ctx);
		backend_list->ctx = backend_ctx;
		retval = mapistore_get_context_id(mstore_ctx->processing_ctx, &backend_list->ctx->context_id);
		if (retval != MAPISTORE_SUCCESS) {
			talloc_free(mem_ctx);
			return MAPISTORE_ERR_CONTEXT_FAILED;
		}
		*context_id = backend_list->ctx->context_id;
		DLIST_ADD_END(mstore_ctx->context_list, backend_list, struct backend_context_list *);
	} else {
		DEBUG(0, ("[%s:%d]: Error - Invalid URI '%s'\n", __FUNCTION__, __LINE__, uri));
		talloc_free(mem_ctx);
		return MAPISTORE_ERR_INVALID_NAMESPACE;
	}

	/* Step 2. Initialize the mapistore indexing context for this backend */
	retval = mapistore_indexing_context_add(mstore_ctx, username, &backend_ctx->indexing);
	if (retval != MAPISTORE_SUCCESS) {
		mapistore_backend_delete_context(backend_ctx);
		talloc_free(mem_ctx);
		return retval;
	} else {
		DEBUG(0, ("[%s:%d]: indexing context added for %s\n", 
			  __FUNCTION__, __LINE__, username));
	}

	talloc_free(mem_ctx);
	return MAPISTORE_SUCCESS;
}


/**
   \details Increase the reference counter of an existing context

   \param mstore_ctx pointer to the mapistore context
   \param contex_id the context identifier referencing the context to
   update

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_add_context_ref_count(struct mapistore_context *mstore_ctx,
							      uint32_t context_id)
{
	struct backend_context		*backend_ctx;
	int				retval;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	if (context_id == -1) return MAPISTORE_ERROR;

	/* Step 0. Ensure the context exists */
	DEBUG(0, ("mapistore_add_context_ref_count: context_id to increment is %d\n", context_id));
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	retval = mapistore_indexing_context_add_ref(mstore_ctx, backend_ctx->username);
	DEBUG(0, ("[%s] indexing_add_ref_count: 0x%x\n", __FUNCTION__, retval));

	/* Increment the backend ref count */
	retval = mapistore_backend_add_ref_count(backend_ctx);

	return retval;
}


/**
   \details Search for an existing context given its uri

   \param mstore_ctx pointer to the mapistore context
   \param uri the URI to lookup
   \param context_id pointer to the context identifier to return

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_search_context_by_uri(struct mapistore_context *mstore_ctx,
							      const char *uri,
							      uint32_t *context_id)
{
	struct backend_context		*backend_ctx;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	if (!uri) return MAPISTORE_ERROR;

	backend_ctx = mapistore_backend_lookup_by_uri(mstore_ctx->context_list, uri);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_NOT_FOUND, NULL);

	*context_id = backend_ctx->context_id;
	return MAPISTORE_SUCCESS;
}


/**
   \details Delete an existing connection context from mapistore

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the context to
   delete

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_del_context(struct mapistore_context *mstore_ctx, 
						    uint32_t context_id)
{
	struct backend_context_list	*backend_list;
	struct backend_context		*backend_ctx;
	int				retval;
	bool				found = false;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	if (context_id == -1) return MAPISTORE_ERROR;

	/* Step 0. Ensure the context exists */
	DEBUG(0, ("mapistore_del_context: context_id to del is %d\n", context_id));
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* search the backend_list item */
	for (backend_list = mstore_ctx->context_list; backend_list; backend_list = backend_list->next) {
		if (backend_list->ctx->context_id == context_id) {
			found = true;
			break;
		}		
	}
	if (found == false) {
		return MAPISTORE_ERROR;
	}

	/* Step 1. Delete the indexing context associated to the backend */
	retval = mapistore_indexing_context_del(mstore_ctx, backend_ctx->username);
	DEBUG(0, ("mapistore_del_context: indexing_del: %d\n", retval));

	/* Step 1. Delete the context within backend */
	retval = mapistore_backend_delete_context(backend_ctx);
	switch (retval) {
	case MAPISTORE_ERR_REF_COUNT:
		return MAPISTORE_SUCCESS;
	case MAPISTORE_SUCCESS:
		DLIST_REMOVE(mstore_ctx->context_list, backend_list);
		/* Step 2. Add the free'd context id to the free list */
		retval = mapistore_free_context_id(mstore_ctx->processing_ctx, context_id);
		break;
	default:
		return retval;
	}

	return retval;
}


/**
   \details Release private backend data associated a folder / message
   opened within the mapistore backend

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   \param fmid a folder or message identifier
   \param type the type of fmid, either MAPISTORE_FOLDER or MAPISTORE_MESSAGE

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_release_record(struct mapistore_context *mstore_ctx,
						       uint32_t context_id,
						       uint64_t fmid,
						       uint8_t type)
{
	struct backend_context		*backend_ctx;
	int				ret;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Step 1. Search the context */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 2. Call backend release_record */
	ret = mapistore_backend_release_record(backend_ctx, fmid, type);

	return !ret ? MAPISTORE_SUCCESS : MAPISTORE_ERROR;
}


void mapistore_set_errno(int status)
{
	errno = status;
}


/**
   \details return a string explaining what a mapistore error constant
   means.

   \param mapistore_err the mapistore error constant

   \return constant string
 */
_PUBLIC_ const char *mapistore_errstr(enum MAPISTORE_ERROR mapistore_err)
{
	switch (mapistore_err) {
	case MAPISTORE_SUCCESS:
		return "Success";
	case MAPISTORE_ERROR:
		return "Non-specific error";
	case MAPISTORE_ERR_NO_MEMORY:
		return "No memory available";
	case MAPISTORE_ERR_ALREADY_INITIALIZED:
		return "Already initialized";
	case MAPISTORE_ERR_NOT_INITIALIZED:
		return "Not initialized";
	case MAPISTORE_ERR_CORRUPTED:
		return "Store corrupted";
	case MAPISTORE_ERR_INVALID_PARAMETER:
		return "Invalid parameter";
	case MAPISTORE_ERR_NO_DIRECTORY:
		return "No such file or directory";
	case MAPISTORE_ERR_DATABASE_INIT:
		return "Database initialization failed";
	case MAPISTORE_ERR_DATABASE_OPS:
		return "Database operation failed";
	case MAPISTORE_ERR_BACKEND_REGISTER:
		return "Storage backend registration failed";
	case MAPISTORE_ERR_BACKEND_INIT:
		return "Storage backend initialization failed";
	case MAPISTORE_ERR_CONTEXT_FAILED:
		return "Context creation failed";
	case MAPISTORE_ERR_INVALID_NAMESPACE:
		return "Invalid namespace";
	case MAPISTORE_ERR_NOT_FOUND:
		return "Not found";
	case MAPISTORE_ERR_REF_COUNT:
		return "Non-zero reference count";
	case MAPISTORE_ERR_EXIST:
		return "Entry already exists";
	}

	return "Unknown error";
}


/**
   \details Create a mapistore uri in specific backend

   \param mstore_ctx pointer to the mapistore context
   \param uri partial mapistore uri used to identify the backend and
   pass potential configuration parameters
   \param fid the generated fid if available
   \param mapistore_uri the mapistore uri to return

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_create_mapistoreURI(struct mapistore_context *mstore_ctx,
							    const char *uri,
							    uint64_t *fid,
							    char **mapistore_uri)
{
	TALLOC_CTX	*mem_ctx;
	char		*uri_namespace;
	char		*namespace_start;
	char		*backend_uri = NULL;
	char		*tmp_uri;

	/* Sanity checks */
	if (!mstore_ctx) return MAPISTORE_ERR_NOT_INITIALIZED;
	if (!mapistore_uri) return MAPISTORE_ERROR;
	if (!uri || strlen(uri) < 4) {
		return MAPISTORE_ERR_INVALID_NAMESPACE;
	}

	mem_ctx = talloc_named(NULL, 0, "mapistore_create_mapistoreURI");
	uri_namespace = talloc_strdup(mem_ctx, uri);
	namespace_start = uri_namespace;
	uri_namespace = strchr(uri_namespace, ':');
	if (!uri_namespace) {
		DEBUG(0, ("[%s:%d]: Error - Invalid namespace '%s'\n", __FUNCTION__, __LINE__, namespace_start));
		talloc_free(mem_ctx);
		return MAPISTORE_ERR_INVALID_NAMESPACE;
	}

	if (uri_namespace[1] && uri_namespace[1] == '/' &&
	    uri_namespace[2] && uri_namespace[2] == '/') {
		if (uri_namespace[3]) {
			backend_uri = talloc_strdup(mem_ctx, &uri_namespace[3]);
			uri_namespace[3] = '\0';
		}
		DEBUG(0, ("namespace_start = %s\n", namespace_start));
		tmp_uri = mapistore_backend_create_mapistoreURI((TALLOC_CTX *)mstore_ctx, namespace_start, 
								backend_uri, fid);
		if (!tmp_uri) {
			talloc_free(mem_ctx);
			return MAPISTORE_ERROR;
		}
	} else {
		talloc_free(mem_ctx);
		return MAPISTORE_ERR_INVALID_NAMESPACE;
	}

	*mapistore_uri = talloc_strndup(mstore_ctx, tmp_uri, strlen(tmp_uri));

	talloc_free(tmp_uri);
	talloc_free(mem_ctx);

	return MAPISTORE_SUCCESS;
}


/**
   \details Retrieve the folder identifier associated to the mapistore
   context identifier

   \param mstore_ctx pointer to the mapistore context
   \param context_id the mapistore context identifier
   \param _fid pointer to the folder identifier to return

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE error
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_get_context_fid(struct mapistore_context *mstore_ctx,
							uint32_t context_id, uint64_t *_fid)
{
	struct backend_context	*backend_ctx;
	const char		*uri;
	const char		*username;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);
	MAPISTORE_RETVAL_IF(!_fid, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 1. Search the context */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 2. Retrieve the backend uri */
	uri = mapistore_backend_get_uri(backend_ctx);
	username = mapistore_backend_get_username(backend_ctx);
	DEBUG(5, ("uri = %s\n", uri));
	DEBUG(5, ("username = %s\n", username));

	/* Step 3. Search for the FID matching the URI in the indexing database */
	mapistore_indexing_lookup_fid(mstore_ctx, username, uri, _fid);
	DEBUG(5, ("context fid: 0x%016"PRIx64"\n", *_fid));
	return MAPISTORE_SUCCESS;
}


/**
   \details Open a directory in mapistore

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   where the directory will be opened
   \param parent_fid the parent folder identifier
   \param fid folder identifier to open

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_opendir(struct mapistore_context *mstore_ctx,
						uint32_t context_id,
						uint64_t parent_fid,
						uint64_t fid)
{
	struct backend_context		*backend_ctx;
	int				ret;

	DEBUG(0, ("[%s, %d]\n", __FUNCTION__, __LINE__));

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Step 1. Search the context */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 2. Call backend opendir */
	ret = mapistore_backend_opendir(backend_ctx, parent_fid, fid);

	return !ret ? MAPISTORE_SUCCESS : MAPISTORE_ERROR;
}


/**
   \details Close a directory in mapistore

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   where the directory has to be closed/released
   \param fid the folder identifier referencing the folder to close

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_closedir(struct mapistore_context *mstore_ctx,
						 uint32_t context_id,
						 uint64_t fid)
{
	struct backend_context		*backend_ctx;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Step 0. Search the context */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* mapistore_backend_closedir() */

	return MAPISTORE_SUCCESS;
}


static const char * mapistore_folder_name_from_index(const uint32_t index)
{
	int i = 0;
	for (i = 0; mailbox_special_folders[i].name != NULL; ++i) {
		if (index == mailbox_special_folders[i].index) {
			return mailbox_special_folders[i].name;
		}
	}
	return NULL; /* not found */
}

/**
   \details Create a root directory in mapistore

   This hook is primarily intended for "virtual backends" such as
   mstoredb which references mapistore URI and global mailbox data
   structure.

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   where the root directory will be created
   \param index the indexing referencing this system/special folder
   \param parent_fid parent Folder ID for the new folder. Pass 0 to add below
   the root of the context.
   \param name optional name to be used instead of the default one
   \param uri the optional mapistore uri to use for this folder

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_root_mkdir(struct mapistore_context *mstore_ctx,
						   uint32_t context_id,
						   uint32_t index,
						   uint64_t parent_fid,
						   const char *name,
						   const char *mapistore_uri)
{
	struct backend_context	*backend_ctx;
	enum MAPISTORE_ERROR	ret;
	uint64_t		fid;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Search the context */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);
	DEBUG(8, ("[%s] backend_ctx is %s (%s)\n", __FUNCTION__, backend_ctx->uri, backend_ctx->username));

	if (parent_fid == 0) {
		/* Retrieve the parent fid matching the context */
		mapistore_get_context_fid(mstore_ctx, context_id, &parent_fid);
		DEBUG(4, ("[%s] context FID: 0x%016"PRIx64"\n", __FUNCTION__, parent_fid));
	}

	/* Generate a new fid for this folder */
	mapistore_get_next_fmid(mstore_ctx, &fid);

	/* Fill in the name if not provided */
	if ( ! name) {
		name = mapistore_folder_name_from_index(index);
		if ( ! name) {
			DEBUG(0, ("[%s] Null name, folder index was %i\n", __FUNCTION__, index));
			return MAPISTORE_ERR_INVALID_PARAMETER;
		}
	}
	/* Call backend root_mkdir */
	ret = mapistore_backend_root_mkdir(backend_ctx, parent_fid, fid, index, name, mapistore_uri);
	if (ret != MAPISTORE_SUCCESS) {
		DEBUG(0, ("[%s] failed to create backend root folder : %i (%s)\n", __FUNCTION__, ret, mapistore_errstr(ret)));
		return ret;
	}
	return ret;
}


/**
   \details Create a directory in mapistore

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   where the directory will be created
   \param parent_fid the parent folder identifier
   \param new_fid the folder identifier for the new folder
   \param aRow pointer to MAPI data structures with properties to be
   added to the new folder

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_mkdir(struct mapistore_context *mstore_ctx,
					      uint32_t context_id,
					      uint64_t parent_fid,
					      uint64_t fid,
					      struct SRow *aRow)
{
	struct backend_context		*backend_ctx;
	int				ret;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Step 1. Search the context */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);	
	
	/* Step 2. Call backend mkdir */
	ret = mapistore_backend_mkdir(backend_ctx, parent_fid, fid, aRow);

	return ret;
}


/**
   \details Remove a directory in mapistore

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   \param parent_fid the parent folder identifier
   \param fid the folder identifier representing the folder to delete
   \param flags flags that control the behaviour of the operation

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_rmdir(struct mapistore_context *mstore_ctx,
					      uint32_t context_id,
					      uint64_t parent_fid,
					      uint64_t fid,
					      uint8_t flags)
{
	struct backend_context		*backend_ctx;
	int				ret;
	enum MAPISTORE_ERROR		retval;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);
	DEBUG(4, ("mapistore_rmdir interface, fid 0x%"PRIx64" from parent 0x%"PRIx64"\n", fid, parent_fid));

	/* Step 1. Find the backend context */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);	

	/* Step 2. Handle deletion of child folders / messages */
	if (flags | DEL_FOLDERS) {
		uint64_t	*childFolders;
		uint32_t	childFolderCount;
		int		retval;
		uint32_t	i;

		/* Get subfolders list */
		retval = mapistore_get_child_fids(mstore_ctx, context_id, fid,
						  &childFolders, &childFolderCount);
		DEBUG(4, ("mapistore_rmdir fid: 0x%"PRIx64", child count: %d\n", fid, childFolderCount));
		if (retval) {
			DEBUG(4, ("mapistore_rmdir bad retval: 0x%x", retval));
			return MAPI_E_NOT_FOUND;
		}

		/* Delete each subfolder in mapistore */
		for (i = 0; i < childFolderCount; ++i) {
			DEBUG(4, ("mapistore_rmdir child: %d, FID: 0x%"PRIx64", parent: 0x%"PRIx64"\n", i, childFolders[i], fid));
			retval = mapistore_rmdir(mstore_ctx, context_id, fid, childFolders[i], flags);
			if (retval) {
				  DEBUG(4, ("mapistore_rmdir failed to delete fid 0x%"PRIx64" (0x%x)", childFolders[i], retval));
				  talloc_free(childFolders);
				  return MAPI_E_NOT_FOUND;
			}
		}

	}
	
	/* Step 3. Call backend rmdir */
	DEBUG(4, ("mapistore_rmdir backend delete of fid 0x%"PRIx64" from parent 0x%"PRIx64"\n", fid, parent_fid));
	ret = mapistore_backend_rmdir(backend_ctx, parent_fid, fid);

	/* TODO: permanent delete probably isn't the right thing to do here */
	retval = mapistore_indexing_record_del_fid(mstore_ctx, backend_ctx->username, fid, MAPISTORE_PERMANENT_DELETE); 
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("[%s] failed to delete indexing record for %s, fid: 0x%016"PRIx64"\n", __FUNCTION__, backend_ctx->username, fid));
	}

	return !ret ? MAPISTORE_SUCCESS : MAPISTORE_ERROR;
}


/**
   \details Retrieve the number of child folders within a mapistore
   folder

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   \param fid the folder identifier
   \param RowCount pointer to the count result to return

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_get_folder_count(struct mapistore_context *mstore_ctx,
							 uint32_t context_id,
							 uint64_t fid,
							 uint32_t *RowCount)
{
	struct backend_context		*backend_ctx;
	int				ret;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Step 0. Ensure the context exists */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 1. Call backend readdir */
	ret = mapistore_backend_readdir_count(backend_ctx, fid, MAPISTORE_FOLDER_TABLE, RowCount);

	return ret;
}


/**
   \details Retrieve the number of child messages within a mapistore folder

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   \param fid the folder identifier
   \param RowCount pointer to the count result to return

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_get_message_count(struct mapistore_context *mstore_ctx,
							  uint32_t context_id,
							  uint64_t fid,
							  uint32_t *RowCount)
{
	struct backend_context		*backend_ctx;
	int				ret;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Step 0. Ensure the context exists */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 2. Call backend readdir_count */
	ret = mapistore_backend_readdir_count(backend_ctx, fid, MAPISTORE_MESSAGE_TABLE, RowCount);

	return ret;
}


/**
   \details Retrieve a MAPI property from a table

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   \param table_type the type of table (folders or messages)
   \param fid the folder identifier where the search takes place
   \param proptag the MAPI property tag to retrieve value for
   \param pos the record position in search results
   \param data pointer on pointer to the data the function returns

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_get_table_property(struct mapistore_context *mstore_ctx,
							   uint32_t context_id,
							   uint8_t table_type,
							   uint64_t fid,
							   uint32_t proptag,
							   uint32_t pos,
							   void **data)
{
	struct backend_context		*backend_ctx;
	int				ret;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	DEBUG(0, ("[%s, %d]\n", __FUNCTION__, __LINE__));

	/* Step 1. Ensure the context exists */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 2. Call backend readdir */
	ret = mapistore_backend_get_table_property(backend_ctx, fid, table_type, pos, proptag, data);

	return ret;
}


/**
   \details Open a message in mapistore

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   where the directory will be opened
   \param parent_fid the parent folder identifier
   \param mid the message identifier to open
   \param pointer to the mapistore_message structure (result)

   \return MAPISTORE SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_openmessage(struct mapistore_context *mstore_ctx,
						    uint32_t context_id,
						    uint64_t parent_fid,
						    uint64_t mid,
						    struct mapistore_message *msg)
{
	struct backend_context		*backend_ctx;
	int				ret;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Step 1. Search the context */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 2. Call backend openmessage */
	ret = mapistore_backend_openmessage(backend_ctx, parent_fid, mid, msg);

	return !ret ? MAPISTORE_SUCCESS : MAPISTORE_ERROR;
}


/**
   \details Create a message in mapistore

   \param mstore_ctx pointer to the mapistore context

   \param context_id the context identifier referencing the backend
   where the messagewill be created
   \param parent_fid the parent folder identifier
   \param mid the message identifier to create

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_createmessage(struct mapistore_context *mstore_ctx,
						      uint32_t context_id,
						      uint64_t parent_fid,
						      uint64_t mid)
{
	struct backend_context		*backend_ctx;
	int				ret;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Step 1. Search the context */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);
	
	/* Step 2. Call backend createmessage */
	ret = mapistore_backend_createmessage(backend_ctx, parent_fid, mid);

	return !ret ? MAPISTORE_SUCCESS : MAPISTORE_ERROR;
}


/**
   \details Commit the changes made to a message in mapistore

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   where the message's changes will be saved
   \param mid the message identifier to save
   \param flags flags associated to the commit operation

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_savechangesmessage(struct mapistore_context *mstore_ctx,
							   uint32_t context_id,
							   uint64_t mid,
							   uint8_t flags)
{
	struct backend_context	*backend_ctx;
	int			ret;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Step 1. Search the context */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 2. Call backend savechangesmessage */
	ret = mapistore_backend_savechangesmessage(backend_ctx, mid, flags);

	return !ret ? MAPISTORE_SUCCESS : MAPISTORE_ERROR;
}


/**
   \details Submits a message for sending.

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   where the message will be submitted
   \param mid the message identifier representing the message to submit
   \param flags flags associated to the submit operation

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_submitmessage(struct mapistore_context *mstore_ctx,
						      uint32_t context_id,
						      uint64_t mid,
						      uint8_t flags)
{
	struct backend_context	*backend_ctx;
	int			ret;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Step 1. Search the context */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 2. Call backend submitmessage */
	ret = mapistore_backend_submitmessage(backend_ctx, mid, flags);

	return !ret ? MAPISTORE_SUCCESS : MAPISTORE_ERROR;
}


/**
   \details Get properties of a message/folder in mapistore

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   where properties will be fetched
   \param fmid the identifier referencing the message/folder
   \param type the object type (folder or message)
   \param properties pointer to the list of properties to fetch
   \param aRow pointer to the SRow structure

   \note aRow needs to be a valid talloc context

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_getprops(struct mapistore_context *mstore_ctx,
						 uint32_t context_id,
						 uint64_t fmid,
						 uint8_t type,
						 struct SPropTagArray *properties,
						 struct SRow *aRow)
{
	struct backend_context	*backend_ctx;
	int			ret;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Step 1. Search the context */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 2. Call backend getprops */
	ret = mapistore_backend_getprops(backend_ctx, fmid, type, properties, aRow);

	return !ret ? MAPISTORE_SUCCESS : MAPISTORE_ERROR;
}

/**
   \details Search for a folder ID by name

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   where the folder will be searched for
   \param parent_fid the parent folder identifier
   \param foldername the name of the folder to search for
   \param fid the fid (result)

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_get_fid_by_name(struct mapistore_context *mstore_ctx,
							uint32_t context_id,
							uint64_t parent_fid,
							const char *name,
							uint64_t *fid)
{
	struct backend_context	*backend_ctx;
	int			ret;

	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);
	MAPISTORE_RETVAL_IF(!name, MAPISTORE_ERR_INVALID_PARAMETER, NULL);
	MAPISTORE_RETVAL_IF(!fid, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 1. Search the context */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 2. Call backend get_fid_by_name */
	ret = mapistore_backend_get_fid_by_name(backend_ctx, parent_fid, name, fid);

	return ret;
}

/**
   \details Set properties of a message/folder in mapistore

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   where properties will be stored
   \param fmid the identifier referencing the message/folder
   \param type the object type (folder or message)
   \param aRow pointer to the SRow structure

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_setprops(struct mapistore_context *mstore_ctx,
						 uint32_t context_id,
						 uint64_t fmid,
						 uint8_t type,
						 struct SRow *aRow)
{
	struct backend_context	*backend_ctx;
	int			ret;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Step 1. Search the context */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 2. Call backend setprops */
	ret = mapistore_backend_setprops(backend_ctx, fmid, type, aRow);

	return !ret ? MAPISTORE_SUCCESS : MAPISTORE_ERROR;
}


/**
   \details Retrieve the folder IDs of child folders within a mapistore
   folder

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   \param fid the folder identifier (for the parent folder)
   \param child_fids pointer to where to return the array of child fids
   \param child_fid_count pointer to the count result to return

   \note The caller is responsible for freeing the \p child_fids array
   when it is no longer required.
   
   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_get_child_fids(struct mapistore_context *mstore_ctx,
						       uint32_t context_id,
						       uint64_t fid,
						       uint64_t *child_fids[],
						       uint32_t *child_fid_count)
{
	struct backend_context		*backend_ctx;
	uint32_t			i;
	void				*data;
	int				ret;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Step 0. Ensure the context exists */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 1. Call backend readdir to get the folder count */
	ret = mapistore_backend_readdir_count(backend_ctx, fid, MAPISTORE_FOLDER_TABLE, child_fid_count);
	MAPISTORE_RETVAL_IF(ret, MAPISTORE_ERR_NO_DIRECTORY, NULL);
	
	/* Step 2. Create a suitable sized array for the fids */
	*child_fids = talloc_zero_array((TALLOC_CTX *)mstore_ctx, uint64_t, *child_fid_count);

	/* Step 3. Fill the array */
	for (i = 0; i < *child_fid_count; ++i) {
		// TODO: add error checking for this call
		ret = mapistore_get_table_property(mstore_ctx, context_id, MAPISTORE_FOLDER_TABLE, fid,
						   PR_FID, i, &data);
		(*child_fids)[i] = *((uint64_t*)(data));
	}

	return MAPISTORE_SUCCESS;
}

/**
   \details Delete a message from mapistore

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context identifier referencing the backend
   where the message's to be located is stored
   \param mid the message identifier of the folder to delete
   \param flags flags that control the behaviour of the operation (MAPISTORE_SOFT_DELETE
   or MAPISTORE_PERMANENT_DELETE)

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE errors
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_deletemessage(struct mapistore_context *mstore_ctx,
						      uint32_t context_id,
						      uint64_t mid,
						      uint8_t flags)
{
	struct backend_context	*backend_ctx;
	int			ret;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Step 1. Search the context */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Step 2. Call backend operation */
	ret = mapistore_backend_deletemessage(backend_ctx, mid, flags);

	return !ret ? MAPISTORE_SUCCESS : MAPISTORE_ERROR;
}

// TODO: should this be in mapistore_processing.c instead?
/**
 \details Obtain the next Folder or message identifier (FID/MID)
 
 \param mstore_ctx pointer to the mapistore context
 \param fmid the returned folder / message identifier value
 
 \return MAPISTORE_SUCCESS on success, otherwise a non-zero MAPISTORE error value
*/
_PUBLIC_ enum MAPISTORE_ERROR mapistore_get_next_fmid(struct mapistore_context *mstore_ctx,
						      uint64_t *fmid)
{
	struct tdb_wrap 	*fmid_ctx;
	TDB_DATA		key;
	TDB_DATA		value;
	const char		*fmid_str;
	enum MAPISTORE_ERROR	ret = MAPISTORE_SUCCESS;
	TALLOC_CTX		*mem_ctx;

	/* TODO: sanity checks */
	if ( ! mstore_ctx) {
		return MAPISTORE_ERR_NOT_INITIALIZED;
	}
	if ( ! mstore_ctx->processing_ctx) {
		return MAPISTORE_ERR_NOT_INITIALIZED;
	}
	if ( ! mstore_ctx->processing_ctx->mapping_ctx) {
		return MAPISTORE_ERR_NOT_INITIALIZED;
	}
	if ( ! mstore_ctx->processing_ctx->mapping_ctx->used_ctx) {
		return MAPISTORE_ERR_NOT_INITIALIZED;
	}

	mem_ctx = talloc_init(NULL);

	fmid_ctx = mstore_ctx->processing_ctx->mapping_ctx->used_ctx;

	key.dptr = (unsigned char *)MAPISTORE_DB_LAST_ID_KEY;
	key.dsize = strlen(MAPISTORE_DB_LAST_ID_KEY);

	if (tdb_lockall(fmid_ctx->tdb) == -1) {
		DEBUG(0, ("[%s] failed to lock\n", __FUNCTION__));
		return MAPISTORE_ERR_DATABASE_OPS;
	}

	/* get the current "last used" value */
	value = tdb_fetch(fmid_ctx->tdb, key);
	if ( ! value.dptr) {
		DEBUG(0, ("[%s] failed to fetch old value\n", __FUNCTION__));
		ret = MAPISTORE_ERR_DATABASE_OPS;
		goto finish;
	}
	fmid_str = talloc_strndup(mstore_ctx, (char *)value.dptr, value.dsize);
	free(value.dptr);
	*fmid = strtoull(fmid_str, NULL, 16); 
	DEBUG(8, ("[%s] old fmid value: 0x%016"PRIx64"\n", __FUNCTION__, *fmid));
	talloc_free((TALLOC_CTX*)fmid_str);

	/* increment by default increment - TODO: make this a parameter */
	(*fmid) += (DFLT_GLOBCOUNT_INCREMENT << 16); /* low 16 bits are replica ID */
	DEBUG(8, ("[%s] new fmid value: 0x%016"PRIx64"\n", __FUNCTION__, *fmid));

	/* save it back */
	value.dptr = (unsigned char*)talloc_asprintf(mem_ctx, "0x%016"PRIx64, *fmid);
	value.dsize = strlen((const char *)value.dptr);
	if (tdb_store(fmid_ctx->tdb, key, value, TDB_REPLACE) == -1) {
		DEBUG(0, ("[%s] failed to store new value\n", __FUNCTION__));
		talloc_free(value.dptr);
		ret = MAPISTORE_ERR_DATABASE_OPS;
		goto finish;
	}
	talloc_free(value.dptr);

	/* add one to indicate this is the first replica */
	(*fmid) += 1;
	mstore_ctx->processing_ctx->mapping_ctx->last_id = *fmid;

finish:
	tdb_unlockall(fmid_ctx->tdb);
	talloc_free(mem_ctx);
	return ret;
}

/**
   \details Retrieve the mailbox FolderID for given recipient from
   mapistore database

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context_id for the recipient
   \param FolderId pointer to the folder identifier that the function returns

   \return MAPI_E_SUCCESS on success, otherwise MAPI error
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_get_SystemFolderIDs(struct mapistore_context *mstore_ctx,
							    const uint32_t context_id,
							    uint64_t (*FolderId)[13])
{
	struct SPropTagArray	props;
	struct SRow		*propvalues;
	TALLOC_CTX		*mem_ctx = 0;
	int 			i = 0;
	uint64_t		mailbox_fid = 0;
	enum MAPISTORE_ERROR	retval;

	mem_ctx = talloc_init(NULL);

	props.cValues = 13;
	props.aulPropTag = talloc_zero_array(mem_ctx, enum MAPITAGS, props.cValues);
	props.aulPropTag[0]  = openchange_private_ROOT_FOLDER_FID;
	props.aulPropTag[1]  = openchange_private_DEFERRED_ACTIONS_FID;
	props.aulPropTag[2]  = openchange_private_SPOOLER_QUEUE_FID;
	props.aulPropTag[3]  = openchange_private_IPM_SUBTREE_FID;
	props.aulPropTag[4]  = openchange_private_INBOX_FID;
	props.aulPropTag[5]  = openchange_private_OUTBOX_FID;
	props.aulPropTag[6]  = openchange_private_SENT_ITEMS_FID;
	props.aulPropTag[7]  = openchange_private_DELETED_ITEMS_FID;
	props.aulPropTag[8]  = openchange_private_COMMON_VIEWS_FID;
	props.aulPropTag[9]  = openchange_private_SCHEDULE_FID;
	props.aulPropTag[10] = openchange_private_SEARCH_FID;
	props.aulPropTag[11] = openchange_private_VIEWS_FID;
	props.aulPropTag[12] = openchange_private_SHORTCUTS_FID;

	propvalues = talloc_zero(mem_ctx, struct SRow);
        propvalues->cValues = 0;

	retval = mapistore_get_context_fid(mstore_ctx, context_id, &mailbox_fid);
	if (retval != MAPISTORE_SUCCESS) {
		return MAPISTORE_ERR_CORRUPTED;
	}

	retval = mapistore_getprops(mstore_ctx, context_id, mailbox_fid, MAPISTORE_FOLDER, &props, propvalues);
	if ((retval != MAPISTORE_SUCCESS) || (propvalues->cValues != 13)) {
		DEBUG(0, ("[%s] retval: %i (%s)\n", __FUNCTION__, retval, mapistore_errstr(retval)));
		if (retval == MAPISTORE_SUCCESS) {
			DEBUG(0, ("[%s] number of values: %i\n", __FUNCTION__, propvalues->cValues));
		}
		talloc_free(mem_ctx);
		return MAPISTORE_ERR_CORRUPTED;
	}

	for (i = 0; i < 13; ++i) {
		(*FolderId)[i] = propvalues->lpProps[i].value.d;
	}

	talloc_free(mem_ctx);
	return MAPISTORE_SUCCESS;
}

/**
   \details Retrieve the public folder FolderIDs for special folders from the mapistore database

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context_id for the public folder root
   \param FolderId pointer to the folder identifier array that the function returns

   \return MAPI_E_SUCCESS on success, otherwise MAPI error
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_get_PublicFolderIDs(struct mapistore_context *mstore_ctx,
							    const uint32_t context_id,
							    uint64_t (*FolderId)[13])
{
	struct SPropTagArray	props;
	struct SRow		*propvalues;
	TALLOC_CTX		*mem_ctx = 0;
	int 			i = 0;
	uint64_t		root_fid = 0;
	enum MAPISTORE_ERROR	retval;

	mem_ctx = talloc_init(NULL);

	props.cValues = 9;
	props.aulPropTag = talloc_zero_array(mem_ctx, enum MAPITAGS, props.cValues);
	props.aulPropTag[0]  = openchange_private_PF_ROOT;
	props.aulPropTag[1]  = openchange_private_PF_IPM_SUBTREE;
	props.aulPropTag[2]  = openchange_private_PF_NONIPM_SUBTREE;
	props.aulPropTag[3]  = openchange_private_PF_EFORMS;
	props.aulPropTag[4]  = openchange_private_PF_FREEBUSY;
	props.aulPropTag[5]  = openchange_private_PF_OAB;
	props.aulPropTag[6]  = openchange_private_PF_LOCAL_EFORMS;
	props.aulPropTag[7]  = openchange_private_PF_LOCAL_FREEBUSY;
	props.aulPropTag[8]  = openchange_private_PF_LOCAL_OAB;
	propvalues = talloc_zero(mem_ctx, struct SRow);
        propvalues->cValues = 0;

	retval = mapistore_get_context_fid(mstore_ctx, context_id, &root_fid);
	if (retval != MAPISTORE_SUCCESS) {
		return MAPISTORE_ERR_CORRUPTED;
	}

	retval = mapistore_getprops(mstore_ctx, context_id, root_fid, MAPISTORE_FOLDER, &props, propvalues);
	if ((retval != MAPISTORE_SUCCESS) || (propvalues->cValues != 9)) {
		DEBUG(0, ("[%s] retval: %i (%s)\n", __FUNCTION__, retval, mapistore_errstr(retval)));
		if (retval == MAPISTORE_SUCCESS) {
			DEBUG(0, ("[%s] number of values: %i\n", __FUNCTION__, propvalues->cValues));
		}
		talloc_free(mem_ctx);
		return MAPISTORE_ERR_CORRUPTED;
	}

	for (i = 0; i < 9; ++i) {
		(*FolderId)[i] = propvalues->lpProps[i].value.d;
	}
	for (i = 9; i < 13; ++i) {
		(*FolderId)[i] = propvalues->lpProps[i].value.d;
	}

	talloc_free(mem_ctx);
	return MAPISTORE_SUCCESS;
}

_PUBLIC_ enum MAPISTORE_ERROR mapistore_get_MailboxGuid(struct mapistore_context *mstore_ctx,
							const uint32_t context_id,
							struct GUID *MailboxGUID)
{
	struct SPropTagArray	props;
	struct SRow		*propvalues;
	TALLOC_CTX		*mem_ctx = 0;
	uint64_t		mailbox_fid = 0;
	enum MAPISTORE_ERROR	retval;

	mem_ctx = talloc_init(NULL);

	props.cValues = 1;
	props.aulPropTag = talloc_zero_array(mem_ctx, enum MAPITAGS, props.cValues);
	props.aulPropTag[0]  = openchange_private_MailboxGUID;

	propvalues = talloc_zero(mem_ctx, struct SRow);
        propvalues->cValues = 0;

	retval = mapistore_get_context_fid(mstore_ctx, context_id, &mailbox_fid);
	if (retval != MAPISTORE_SUCCESS) {
		return MAPISTORE_ERR_CORRUPTED;
	}

	retval = mapistore_getprops(mstore_ctx, context_id, mailbox_fid, MAPISTORE_FOLDER, &props, propvalues);
	if ((retval != MAPISTORE_SUCCESS) || (propvalues->cValues != 1)) {
		DEBUG(0, ("[%s] retval: %i (%s)\n", __FUNCTION__, retval, mapistore_errstr(retval)));
		if (retval == MAPISTORE_SUCCESS) {
			DEBUG(0, ("[%s] number of values: %i\n", __FUNCTION__, propvalues->cValues));
		}
		talloc_free(mem_ctx);
		return MAPISTORE_ERR_CORRUPTED;
	}

	memcpy(MailboxGUID, propvalues->lpProps[0].value.lpguid, sizeof(struct GUID));

	return MAPISTORE_SUCCESS;
}


_PUBLIC_ enum MAPISTORE_ERROR mapistore_get_MailboxReplica(struct mapistore_context *mstore_ctx,
							   const uint32_t context_id,
							   uint16_t *ReplId, struct GUID *ReplGUID)
{
	struct SPropTagArray	props;
	struct SRow		*propvalues;
	TALLOC_CTX		*mem_ctx = 0;
	uint64_t		mailbox_fid = 0;
	enum MAPISTORE_ERROR	retval;

	mem_ctx = talloc_init(NULL);

	props.cValues = 2;
	props.aulPropTag = talloc_zero_array(mem_ctx, enum MAPITAGS, props.cValues);
	props.aulPropTag[0] = openchange_private_ReplicaID;
	props.aulPropTag[1] = openchange_private_MailboxGUID;

	propvalues = talloc_zero(mem_ctx, struct SRow);
        propvalues->cValues = 0;

	retval = mapistore_get_context_fid(mstore_ctx, context_id, &mailbox_fid);
	if (retval != MAPISTORE_SUCCESS) {
		return MAPISTORE_ERR_CORRUPTED;
	}

	retval = mapistore_getprops(mstore_ctx, context_id, mailbox_fid, MAPISTORE_FOLDER, &props, propvalues);
	if ((retval != MAPISTORE_SUCCESS) || (propvalues->cValues != 2)) {
		DEBUG(0, ("[%s] retval: %i (%s)\n", __FUNCTION__, retval, mapistore_errstr(retval)));
		if (retval == MAPISTORE_SUCCESS) {
			DEBUG(0, ("[%s] number of values: %i\n", __FUNCTION__, propvalues->cValues));
		}
		talloc_free(mem_ctx);
		return MAPISTORE_ERR_CORRUPTED;
	}

	*ReplId = propvalues->lpProps[0].value.i;
	memcpy(ReplGUID, propvalues->lpProps[1].value.lpguid, sizeof(struct GUID));

	return MAPISTORE_SUCCESS;
}

/**
   \details fetch table contents

   \param mstore_ctx pointer to the mapistore context
   \param context_id the context_id for the parent folder
   \param table_type the type of table
   \param property_tags the property tags of the properties to return (column headers)
   \param properties the returned properties
 
   \note properties must be a valid talloc context
 
   \todo this API may need some kind of start pos + range to handle very large tables

   \return MAPISTORE_SUCCESS on success, otherwise a non-zero MAPISTORE_ERROR value
*/
_PUBLIC_ enum MAPISTORE_ERROR mapistore_get_table(struct mapistore_context *mstore_ctx,
						  const uint32_t context_id,
						  const uint8_t table_type,
						  const struct SPropTagArray *property_tags,
						  struct SRowSet *properties)
{
	uint64_t		parent_fid = 0;
	struct backend_context	*backend_ctx;
	enum MAPISTORE_ERROR	ret;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);

	/* Ensure the context exists */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Get the fid (TODO: get URI instead) */
	ret = mapistore_get_context_fid(mstore_ctx, context_id, &parent_fid);
	if (ret != MAPISTORE_SUCCESS) {
		return MAPISTORE_ERR_CORRUPTED;
	}

	/* Call backend operation */
	ret = mapistore_backend_get_table(backend_ctx, parent_fid, table_type, property_tags, properties);

	return !ret ? MAPISTORE_SUCCESS : MAPISTORE_ERROR;
}

/**
   \details Retrieve the Explicit message class and Folder identifier
   associated with the MessageClass search pattern.

   \param MessageClass substring to search for
   \param fid pointer to the folder identifier the function returns
   \param ExplicitMessageClass pointer on pointer to the complete
   message class the function returns

   The caller is responsible for calling talloc_free() on the returned ExplicitMessageClass
  
   \return MAPI_E_SUCCESS on success, otherwise MAPI_E_NOT_FOUND
 */
_PUBLIC_ enum MAPISTORE_ERROR mapistore_get_ReceiveFolder(struct mapistore_context *mstore_ctx,
							  const uint32_t context_id,
							  const char *MessageClass,
							  uint64_t *fid,
							  char **ExplicitMessageClass)
{
	struct SPropTagArray	props;
	struct SRowSet		*proprows;
	TALLOC_CTX		*mem_ctx = 0;
	enum MAPISTORE_ERROR	retval;
	int			rownum = 0;
	int			rownum_longest_match = -1;
	size_t			longest_match_len = 0;

	mem_ctx = talloc_init(NULL);

	props.cValues = 3;
	props.aulPropTag = talloc_zero_array(mem_ctx, enum MAPITAGS, props.cValues);
	props.aulPropTag[0] = PidTagFolderId;
	props.aulPropTag[1] = PidTagLastModificationTime;
	props.aulPropTag[2] = PidTagMessageClass;

	proprows = talloc(mem_ctx, struct SRowSet);
	retval = mapistore_get_table(mstore_ctx, context_id, MAPISTORE_RECEIVE_TABLE, &props, proprows);
	if (retval != MAPISTORE_SUCCESS) {
		talloc_free(mem_ctx);
		return retval;
	}

	mapidump_SRowSet(proprows, "\t");

	/* first find the "All" target */
	for (rownum = 0; rownum < proprows->cRows; ++rownum) {
		if (strcasecmp(proprows->aRow[rownum].lpProps[2].value.lpszW, "All") == 0) {
			rownum_longest_match = rownum;
		}
	}
	/* return the "All" target if applicable (we asked for "") */
	if ((rownum_longest_match != -1) &&(strcasecmp(MessageClass, "") == 0)) {
		*fid = proprows->aRow[rownum_longest_match].lpProps[0].value.d;
		*ExplicitMessageClass = talloc_strdup(mstore_ctx, "");
		talloc_free(mem_ctx);
		return MAPISTORE_SUCCESS;
	}

	/* find the longest match */
	for (rownum = 0; rownum < proprows->cRows; ++rownum) {
		size_t len = strlen(proprows->aRow[rownum].lpProps[2].value.lpszW);
		if (strncasecmp(proprows->aRow[rownum].lpProps[2].value.lpszW, MessageClass, len) == 0) {
			/* this matched up to the length of the message class in the row */
			if (strlen(MessageClass) == len) {
			      /* exact match - its not going to get better than this */
			      rownum_longest_match = rownum;
			      break;
			}
			if (len > longest_match_len) {
			      /* this is a better match that the match we've previous found */
			      longest_match_len = len;
			      rownum_longest_match = rownum;
			}
		}
	}

	/* return results */
	if (rownum_longest_match == -1) {
		/* we didn't find anything, so just return the root folder */
		mapistore_get_context_fid(mstore_ctx, context_id, fid);
		*ExplicitMessageClass = talloc_strdup(mstore_ctx, "");
	} else {
		*fid = proprows->aRow[rownum_longest_match].lpProps[0].value.d;
		*ExplicitMessageClass = talloc_strdup(mstore_ctx, proprows->aRow[rownum_longest_match].lpProps[2].value.lpszW);
	}
	talloc_free(mem_ctx);

	return MAPISTORE_SUCCESS;
}

// TODO: add documentation
_PUBLIC_ enum MAPISTORE_ERROR mapistore_add_table_row(struct mapistore_context *mstore_ctx,
						      const uint32_t context_id,
						      const uint64_t fid,
						      const uint8_t table_type,
						      const struct SRow *rowdata)
{
	struct backend_context	*backend_ctx;

	/* Sanity checks */
	MAPISTORE_SANITY_CHECKS(mstore_ctx, NULL);
	if ( ! rowdata) {
		return MAPISTORE_ERR_INVALID_PARAMETER;
	}

	/* Ensure the context exists */
	backend_ctx = mapistore_backend_lookup(mstore_ctx->context_list, context_id);
	MAPISTORE_RETVAL_IF(!backend_ctx, MAPISTORE_ERR_INVALID_PARAMETER, NULL);

	/* Call backend operation */
	return mapistore_backend_add_table_row(backend_ctx, fid, table_type, rowdata);
}

// TODO: add documentation
_PUBLIC_ enum MAPISTORE_ERROR mapistore_set_ReceiveFolder(struct mapistore_context *mstore_ctx,
							  const uint32_t context_id,
							  const char *MessageClass,
							  uint64_t fid)

{
	TALLOC_CTX		*mem_ctx;
	enum MAPISTORE_ERROR	retval;
	uint64_t		table_parent_fid;
	uint64_t		message_class_fid;
	char			*explicit_message_class = NULL;
	struct SRow		rowdata;

	/* sanity checks */
	DEBUG(0, ("[%s,%d]\n", __FUNCTION__, __LINE__));

	/* find previous entry for MessageClass, if any */
	/* note that message_class_fid return value isn't used here */
	retval = mapistore_get_ReceiveFolder(mstore_ctx, context_id, MessageClass, &message_class_fid, &explicit_message_class);

	/* TODO: get the fid for the receive folder */
	retval = mapistore_get_context_fid(mstore_ctx, context_id, &table_parent_fid);
	if (retval != MAPISTORE_SUCCESS) {
		DEBUG(0, ("[%s] failed to get receive folder parent\n", __FUNCTION__));
		return MAPISTORE_ERR_CORRUPTED;
	}

	/* delete previous receive folder table entry, if we found it */
	if (strcasecmp(MessageClass, explicit_message_class) == 0) {
		/* TODO: delete row with matching fid */
		/* TODO: may need to add special handling for the "All" target if its just the root folder */
	}
	
	mem_ctx = talloc_init(NULL);
	
	/* build row data */
	rowdata.cValues = 3;
	rowdata.lpProps = talloc_zero_array(mem_ctx, struct SPropValue, rowdata.cValues);
	rowdata.lpProps[0].ulPropTag = PidTagFolderId;
	rowdata.lpProps[0].value.d = fid;
	rowdata.lpProps[1].ulPropTag = PidTagMessageClass;
	rowdata.lpProps[1].value.lpszW = MessageClass;
	rowdata.lpProps[2].ulPropTag = PidTagLastModificationTime;
	rowdata.lpProps[2].value.ft.dwLowDateTime = 0; /* TODO: set this to the current time */
	rowdata.lpProps[2].value.ft.dwHighDateTime = 0;

	/* add new table row */
	retval = mapistore_add_table_row(mstore_ctx, context_id, table_parent_fid, MAPISTORE_RECEIVE_TABLE, &rowdata);

	talloc_free(mem_ctx);

	return retval;
}
