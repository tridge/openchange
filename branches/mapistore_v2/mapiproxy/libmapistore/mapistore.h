/*
   OpenChange Storage Abstraction Layer library

   OpenChange Project

   Copyright (C) Julien Kerihuel 2009-2010

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef	__MAPISTORE_H
#define	__MAPISTORE_H

#ifndef	_GNU_SOURCE
#define	_GNU_SOURCE
#endif

#ifndef	_PUBLIC_
#define	_PUBLIC_
#endif

#include <sys/types.h>
#include <sys/stat.h>

#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>

#include <tdb.h>
#include <ldb.h>
#include <talloc.h>
#include <util/debug.h>

#include "libmapi/libmapi.h"

#include "mapiproxy/libmapistore/mapistore_errors.h"

typedef	int (*init_backend_fn) (void);

#define	MAPISTORE_INIT_MODULE	"mapistore_init_backend"

#define	MAPISTORE_FOLDER_TABLE	1
#define	MAPISTORE_MESSAGE_TABLE	2
#define MAPISTORE_RECEIVE_TABLE 5

#define MAPISTORE_FOLDER	1
#define	MAPISTORE_MESSAGE	2

#define	MAPISTORE_SOFT_DELETE		1
#define	MAPISTORE_PERMANENT_DELETE	2

/* Forward declaration */
struct mapistoredb_context;
struct mapistore_backend_context;

struct mapistore_message {
	struct SRowSet			*recipients;
	struct SRow			*properties;
};

struct mapistore_backend {
	const char	*name;
	const char	*description;
	const char	*uri_namespace;

	enum MAPISTORE_ERROR (*init)(void);
	enum MAPISTORE_ERROR (*create_context)(struct mapistore_backend_context *, const char *, void **);
	enum MAPISTORE_ERROR (*delete_context)(void *);
	enum MAPISTORE_ERROR (*release_record)(void *, uint64_t, uint8_t);
	enum MAPISTORE_ERROR (*get_path)(void *, uint64_t, uint8_t, char **);
	/* provisioning semantics */
	enum MAPISTORE_ERROR (*op_root_mkdir)(void *, uint64_t, uint64_t, uint32_t, const char *, char **);
	enum MAPISTORE_ERROR (*create_uri)(void *, const char *, uint64_t *, char **);
	/* folders semantic */
	enum MAPISTORE_ERROR (*op_mkdir)(void *, uint64_t, uint64_t, struct SRow *, char **);
	enum MAPISTORE_ERROR (*op_rmdir)(void *, uint64_t parent_fid, uint64_t fid);
	enum MAPISTORE_ERROR (*op_opendir)(void *, uint64_t, uint64_t);
	enum MAPISTORE_ERROR (*op_closedir)(void *);
	enum MAPISTORE_ERROR (*op_readdir_count)(void *, uint64_t, uint8_t, uint32_t *);
	enum MAPISTORE_ERROR (*op_get_table_property)(void *, uint64_t fid, uint8_t table_type, uint32_t pos, uint32_t proptag, void **data);
	enum MAPISTORE_ERROR (*op_get_table)(void *ctx, const uint64_t fid, const uint8_t table_type, const struct SPropTagArray *proptags, struct SRowSet *rows);
	/* message semantics */
	enum MAPISTORE_ERROR (*op_openmessage)(void *, uint64_t, uint64_t, struct mapistore_message *);
	enum MAPISTORE_ERROR (*op_createmessage)(void *, uint64_t, uint64_t);
	enum MAPISTORE_ERROR (*op_savechangesmessage)(void *, uint64_t, uint8_t);
	enum MAPISTORE_ERROR (*op_submitmessage)(void *, uint64_t, uint8_t);
	enum MAPISTORE_ERROR (*op_getprops)(void *, uint64_t, uint8_t, struct SPropTagArray *, struct SRow *);
	enum MAPISTORE_ERROR (*op_get_fid_by_name)(void *, uint64_t, const char *, uint64_t *);
	enum MAPISTORE_ERROR (*op_setprops)(void *priv, uint64_t fmid, uint8_t type, struct SRow *props);
	enum MAPISTORE_ERROR (*op_deletemessage)(void *, uint64_t mid, uint8_t flags);
	/* table semantics */
	enum MAPISTORE_ERROR (*op_add_table_row)(void *, const uint64_t fid, const uint8_t table_type, const struct SRow *rowdata);
};

struct indexing_context_list;

struct backend_context {
	const struct mapistore_backend		*backend;
	void					*private_data;
	struct mapistore_indexing_context_list	*indexing;
	uint32_t				context_id;
	uint32_t				ref_count;
	char					*uri;
	const char				*username;
};

struct backend_context_list {
	struct backend_context		*ctx;
	struct backend_context_list	*prev;
	struct backend_context_list	*next;
};

struct processing_context;

struct mapistore_context {
	struct processing_context		*processing_ctx;
	struct backend_context_list		*context_list;
	struct mapistore_indexing_context_list	*indexing_list;
	void					*nprops_ctx;
	struct mapistore_backend_context	*backend_ctx;
};

struct indexing_folders_list {
	uint64_t			*folderID;
	uint32_t			count;
};

/* Special Folders index defines */
/* TODO: align this with the list in libmapi/mapidefs.h */
#define	MDB_ROOT_FOLDER		1
#define	MDB_DEFERRED_ACTIONS	2
#define	MDB_SPOOLER_QUEUE	3
#define	MDB_TODO_SEARCH		4
#define	MDB_IPM_SUBTREE		5
#define	MDB_INBOX		6
#define	MDB_OUTBOX		7
#define	MDB_SENT_ITEMS		8
#define	MDB_DELETED_ITEMS	9
#define	MDB_COMMON_VIEWS	10
#define	MDB_SCHEDULE		11
#define	MDB_SEARCH		12
#define	MDB_VIEWS		13
#define	MDB_SHORTCUTS		14
#define	MDB_REMINDERS		15
#define	MDB_CALENDAR		16
#define	MDB_CONTACTS		17
#define	MDB_JOURNAL		18
#define	MDB_NOTES		19
#define	MDB_TASKS		20
#define	MDB_DRAFTS		21
#define	MDB_TRACKED_MAIL	22
#define	MDB_SYNC_ISSUES		23
#define	MDB_CONFLICTS		24
#define	MDB_LOCAL_FAILURES	25
#define	MDB_SERVER_FAILURES	26
#define	MDB_JUNK_EMAIL		27
#define	MDB_RSS_FEEDS		28
#define	MDB_CONVERSATION_ACT	29
#define MDB_LAST_SPECIALFOLDER	MDB_CONVERSATION_ACT
#define	MDB_CUSTOM		999

struct mailbox_special_folder {
	uint32_t	parent_specialindex;
	uint32_t	index;
	char		*name;
	char		*PidType;
	char		*PidTagIpm;
};
static const struct mailbox_special_folder mailbox_special_folders[] = {
	{ MDB_ROOT_FOLDER,	MDB_DEFERRED_ACTIONS,	"Deferred Actions",		"IPF.Note",			NULL },
	{ MDB_ROOT_FOLDER,	MDB_SEARCH,		"Finder",			"IPF.Note",			NULL },
	{ MDB_ROOT_FOLDER,	MDB_REMINDERS,		"Reminders",			"Outlook.Reminder",		"PidTagRemindersOnlineEntryId" },
	{ MDB_ROOT_FOLDER,	MDB_TRACKED_MAIL,	"Tracked Mail",			"IPF.Note",			NULL },
	{ MDB_ROOT_FOLDER,	MDB_TODO_SEARCH,	"To-Do Search",			"IPF.Task",			NULL },
	{ MDB_ROOT_FOLDER,	MDB_COMMON_VIEWS,	"Common Views",			"IPF.Note",			NULL },
	{ MDB_ROOT_FOLDER,	MDB_VIEWS,		"Views",			"IPF.Note",			NULL },
	{ MDB_ROOT_FOLDER,	MDB_SCHEDULE,		"Schedule",			"IPF.Note",			NULL },
	{ MDB_ROOT_FOLDER,	MDB_SPOOLER_QUEUE,	"Spooler Queue",		"IPF.Note",			NULL },
	{ MDB_ROOT_FOLDER,	MDB_SHORTCUTS,		"Shortcuts",			"IPF.Note",			NULL },
	{ MDB_ROOT_FOLDER,	MDB_IPM_SUBTREE,	"Top of Information Store",	"IPF.Note",			NULL },
	{ MDB_IPM_SUBTREE,	MDB_DELETED_ITEMS,	"Deleted Items",		"IPF.Note",			NULL },
	{ MDB_IPM_SUBTREE,	MDB_OUTBOX,		"Outbox",			"IPF.Note",			NULL },
	{ MDB_IPM_SUBTREE,	MDB_SENT_ITEMS,		"Sent Items",			"IPF.Note",			NULL },
	{ MDB_IPM_SUBTREE,	MDB_INBOX,		"Inbox",			"IPF.Note",			NULL },
	{ MDB_IPM_SUBTREE,	MDB_CALENDAR,		"Calendar",			"IPF.Appointment",		"PidTagIpmAppointmentEntryId"	},
	{ MDB_IPM_SUBTREE,	MDB_CONTACTS,		"Contacts",			"IPF.Contact",			"PidTagIpmContactEntryId"	},
	{ MDB_IPM_SUBTREE,	MDB_JOURNAL,		"Journal",			"IPF.Journal",			"PidTagIpmJournalEntryId"	},
	{ MDB_IPM_SUBTREE,	MDB_NOTES,		"Notes",			"IPF.StickyNote",		"PidTagIpmNoteEntryId"		},
	{ MDB_IPM_SUBTREE,	MDB_TASKS,		"Tasks",			"IPF.Task",			"PidTagIpmTaskEntryId"		},
	{ MDB_IPM_SUBTREE,	MDB_DRAFTS,		"Drafts",			"IPF.Note",			"PidTagIpmDraftsEntryId"	},
	{ MDB_IPM_SUBTREE,	MDB_SYNC_ISSUES,	"Sync Issues",			"IPF.Note",			NULL },
	{ MDB_SYNC_ISSUES,	MDB_CONFLICTS,		"Conflicts",			"IPF.Note",			NULL },
	{ MDB_SYNC_ISSUES,	MDB_LOCAL_FAILURES,	"Local Failures",		"IPF.Note",			NULL },
	{ MDB_SYNC_ISSUES,	MDB_SERVER_FAILURES,	"Server Failures",		"IPF.Note",			NULL },
	{ MDB_IPM_SUBTREE,	MDB_JUNK_EMAIL,		"Junk Email",			"IPF.Note",			NULL },
	{ MDB_IPM_SUBTREE,	MDB_RSS_FEEDS,		"RSS Feeds",			"IPF.Note.OutlookHomepage",	NULL },
	{ MDB_IPM_SUBTREE,	MDB_CONVERSATION_ACT,	"Conversation Actions",		"IPF.Configuration",		NULL },
	{ 0,			0,			NULL,				NULL,				NULL }
};

#define PF_ROOT_FOLDER			0
#define PF_IPM_SUBTREE			1
#define PF_NON_IPM_SUBTREE		2
#define PF_EFORMS_REGISTRY		3
#define PF_FREEBUSY			4
#define PF_OAB				5
#define PF_LOCALEFORMS			6
#define PF_LOCALFREEBUSY		7
#define PF_LOCALOAB			8
#define PF_LAST_SPECIALFOLDER		PF_LOCALOAB

struct pf_nonipm_folder {
	uint32_t	parent_specialindex;
	uint32_t	index;
	char		*name;
};
static const struct pf_nonipm_folder pf_nonipm_folders[] = {
	{ PF_ROOT_FOLDER,	PF_IPM_SUBTREE,		"IPM_SUBTREE"								},
	{ PF_ROOT_FOLDER,	PF_NON_IPM_SUBTREE,	"NON_IPM_SUBTREE"							},
	{ PF_NON_IPM_SUBTREE,	PF_EFORMS_REGISTRY,	"EFORMS REGISTRY"							},
	{ PF_NON_IPM_SUBTREE,	PF_OAB,			"OFFLINE ADDRESS BOOK"							},
	{ PF_NON_IPM_SUBTREE,	PF_FREEBUSY,		"SCHEDULE+ FREE BUSY"							},
//	{ PF_FREEBUSY,		PF_LOCALFREEBUSY,	"EX:/o={firstorg}/ou=Exchange Administrative Group ({netbios})"		},
//	{ PF_OAB,		PF_LOCALOAB,		"/o={firstorg}/cn=addrlists/cn=oabs/cn=Default Offline Address Book"	},
	{ 0,			0,			NULL,									}
};

#ifndef __BEGIN_DECLS
#ifdef __cplusplus
#define __BEGIN_DECLS		extern "C" {
#define __END_DECLS		}
#else
#define __BEGIN_DECLS
#define __END_DECLS
#endif
#endif

__BEGIN_DECLS

/* definitions from mapistore_interface.c */
struct mapistore_context*	mapistore_init(TALLOC_CTX *, const char *);
enum MAPISTORE_ERROR		mapistore_release(struct mapistore_context *);
enum MAPISTORE_ERROR		mapistore_add_context(struct mapistore_context *, const char *, const char *, uint32_t *);
enum MAPISTORE_ERROR		mapistore_add_context_ref_count(struct mapistore_context *, uint32_t);
enum MAPISTORE_ERROR		mapistore_del_context(struct mapistore_context *, uint32_t);
enum MAPISTORE_ERROR		mapistore_release_record(struct mapistore_context *, uint32_t, uint64_t, uint8_t);
enum MAPISTORE_ERROR		mapistore_search_context_by_uri(struct mapistore_context *, const char *, uint32_t *);
const char*			mapistore_errstr(enum MAPISTORE_ERROR);
enum MAPISTORE_ERROR		mapistore_root_mkdir(struct mapistore_context *, uint32_t, uint32_t, uint64_t, const char *, const char *);
enum MAPISTORE_ERROR		mapistore_create_mapistoreURI(struct mapistore_context *, const char *, uint64_t *, char **);
enum MAPISTORE_ERROR		mapistore_get_context_fid(struct mapistore_context *, uint32_t, uint64_t *);
/* enum MAPISTORE_ERROR 	mapistore_add_context_indexing(struct mapistore_context *, const char *, uint32_t); */
enum MAPISTORE_ERROR		mapistore_opendir(struct mapistore_context *, uint32_t, uint64_t, uint64_t);
enum MAPISTORE_ERROR		mapistore_closedir(struct mapistore_context *mstore_ctx, uint32_t, uint64_t);
enum MAPISTORE_ERROR		mapistore_mkdir(struct mapistore_context *, uint32_t, uint64_t, uint64_t, struct SRow *);
enum MAPISTORE_ERROR		mapistore_rmdir(struct mapistore_context *, uint32_t, uint64_t, uint64_t, uint8_t);
enum MAPISTORE_ERROR		mapistore_get_folder_count(struct mapistore_context *, uint32_t, uint64_t, uint32_t *);
enum MAPISTORE_ERROR		mapistore_get_message_count(struct mapistore_context *, uint32_t, uint64_t, uint32_t *);
enum MAPISTORE_ERROR		mapistore_get_table_property(struct mapistore_context *, uint32_t, uint8_t, uint64_t,
							     uint32_t, uint32_t, void **);
enum MAPISTORE_ERROR		mapistore_openmessage(struct mapistore_context *, uint32_t, uint64_t, uint64_t, struct mapistore_message *);
enum MAPISTORE_ERROR		mapistore_createmessage(struct mapistore_context *, uint32_t, uint64_t, uint64_t);
enum MAPISTORE_ERROR		mapistore_savechangesmessage(struct mapistore_context *, uint32_t, uint64_t, uint8_t);
enum MAPISTORE_ERROR		mapistore_submitmessage(struct mapistore_context *, uint32_t, uint64_t, uint8_t);
enum MAPISTORE_ERROR		mapistore_getprops(struct mapistore_context *, uint32_t, uint64_t, uint8_t, struct SPropTagArray *, struct SRow *);
enum MAPISTORE_ERROR		mapistore_get_fid_by_name(struct mapistore_context *, uint32_t, uint64_t, const char *, uint64_t*);
enum MAPISTORE_ERROR		mapistore_setprops(struct mapistore_context *, uint32_t, uint64_t, uint8_t, struct SRow *);
enum MAPISTORE_ERROR		mapistore_get_child_fids(struct mapistore_context *, uint32_t, uint64_t, uint64_t **, uint32_t *);
enum MAPISTORE_ERROR		mapistore_deletemessage(struct mapistore_context *, uint32_t, uint64_t, uint8_t);
enum MAPISTORE_ERROR		mapistore_get_next_fmid(struct mapistore_context *mstore_ctx, uint64_t *fmid);
enum MAPISTORE_ERROR		mapistore_get_SystemFolderIDs(struct mapistore_context *mstore_ctx, const uint32_t context_id, uint64_t (*FolderId)[13]);
enum MAPISTORE_ERROR		mapistore_get_PublicFolderIDs(struct mapistore_context *mstore_ctx, const uint32_t context_id, uint64_t (*FolderId)[13]);
enum MAPISTORE_ERROR		mapistore_get_MailboxGuid(struct mapistore_context *mstore_ctx, const uint32_t context_id, struct GUID *MailboxGUID);
enum MAPISTORE_ERROR		mapistore_get_MailboxReplica(struct mapistore_context *mstore_ctx, const uint32_t context_id, uint16_t *ReplId, struct GUID *ReplGUID);
enum MAPISTORE_ERROR		mapistore_get_ReceiveFolder(struct mapistore_context *mstore_ctx, const uint32_t context_id, const char *MessageClass, uint64_t *fid, char **ExplicitMessageClass);
enum MAPISTORE_ERROR		mapistore_get_table(struct mapistore_context *mstore_ctx, const uint32_t context_id, const uint8_t table_type, const struct SPropTagArray *property_tags, struct SRowSet *properties);
enum MAPISTORE_ERROR		mapistore_set_ReceiveFolder(struct mapistore_context *mstore_ctx, const uint32_t context_id, const char *MessageClass, uint64_t fid);
enum MAPISTORE_ERROR		mapistore_add_table_row(struct mapistore_context *mstore_ctx, const uint32_t context_id, const uint64_t fid, const uint8_t table_type, const struct SRow *rowdata);

/* definitions from mapistore_processing.c */
enum MAPISTORE_ERROR		mapistore_set_mapping_path(const char *);
enum MAPISTORE_ERROR		mapistore_set_database_path(const char *);

/* definitions from mapistore_backend.c */
extern int			mapistore_backend_register(const void *);
const char*			mapistore_backend_get_installdir(void);
init_backend_fn*		mapistore_backend_load(TALLOC_CTX *, const char *);
struct backend_context*		mapistore_backend_lookup(struct backend_context_list *, uint32_t);
struct backend_context*		mapistore_backend_lookup_by_uri(struct backend_context_list *, const char *);
bool				mapistore_backend_run_init(init_backend_fn *);

/* definitions from mapistore_namedprops.c */
enum MAPISTORE_ERROR		mapistore_namedprops_get_mapped_id(void *ldb_ctx, struct MAPINAMEID, uint16_t *);

/* definitions from mapistore_backend_public.c */
struct ldb_context*		mapistore_public_ldb_connect(struct mapistore_backend_context *, const char *);

/* definitions from mapistoredb.c */
struct mapistoredb_context*	mapistoredb_init(TALLOC_CTX *, const char *);
void				mapistoredb_release(struct mapistoredb_context *);
enum MAPISTORE_ERROR		mapistoredb_provision(struct mapistoredb_context *);
enum MAPISTORE_ERROR		mapistoredb_provision_namedprops(struct mapistoredb_context *);
enum MAPISTORE_ERROR		mapistoredb_provision_publicfolders(struct mapistoredb_context *, uint32_t *context_id);
enum MAPISTORE_ERROR		mapistoredb_new_mailbox(struct mapistoredb_context *, const char *, uint32_t *);
enum MAPISTORE_ERROR		mapistoredb_add_mailbox_default_folder(struct mapistoredb_context *, uint32_t,
								       uint32_t, const char *, const char *);
#if 0
enum MAPISTORE_ERROR		mapistoredb_add_mailbox_folder(struct mapistoredb_context *, uint32_t, uint32_t,
							       const char *, const char *);
#endif

/* definitions from mapistoredb_conf.c */
void				mapistoredb_dump_conf(struct mapistoredb_context *);
enum MAPISTORE_ERROR		mapistoredb_set_netbiosname(struct mapistoredb_context *, const char *);
enum MAPISTORE_ERROR		mapistoredb_set_firstorg(struct mapistoredb_context *, const char *);
enum MAPISTORE_ERROR		mapistoredb_set_firstou(struct mapistoredb_context *, const char *);
const char*			mapistoredb_get_netbiosname(struct mapistoredb_context *);
const char*			mapistoredb_get_firstorg(struct mapistoredb_context *);
const char*			mapistoredb_get_firstou(struct mapistoredb_context *);

__END_DECLS

#endif	/* ! __MAPISTORE_H */
