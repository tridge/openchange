/*
   OpenChange MAPI implementation.

   Python interface to mapistore database

   Copyright (C) Julien Kerihuel 2010.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Python.h>
#include "pyopenchange/pymapistore.h"

void initmapistoredb(void);

static PyObject *py_MAPIStoreDB_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
	TALLOC_CTX			*mem_ctx;
	struct mapistoredb_context	*mdb_ctx;
	PyMAPIStoreDBObject		*mdbobj;
	char				*kwnames[] = { "path", NULL };
	const char			*path = NULL;

	/* Path is optional */
	PyArg_ParseTupleAndKeywords(args, kwargs, "|s", kwnames, &path);

	mem_ctx = talloc_new(NULL);
	if (mem_ctx == NULL) {
		PyErr_NoMemory();
		return NULL;
	}

	mdb_ctx = mapistoredb_init(mem_ctx, path);
	if (mdb_ctx == NULL) {
		DEBUG(0, ("mapistoredb_init returned NULL\n"));
		return NULL;
	}

	mdbobj = PyObject_New(PyMAPIStoreDBObject, &PyMAPIStoreDB);
	mdbobj->mem_ctx = mem_ctx;
	mdbobj->mdb_ctx = mdb_ctx;

	return (PyObject *) mdbobj;
}

static void py_MAPIStoreDB_dealloc(PyObject *_self)
{
	PyMAPIStoreDBObject	*self = (PyMAPIStoreDBObject *)_self;

	talloc_free(self->mem_ctx);
	PyObject_Del(_self);
}

static PyObject *py_MAPIStoreDB_dump_configuration(PyMAPIStoreDBObject *self, PyObject *args)
{
	mapistoredb_dump_conf(self->mdb_ctx);
	return PyInt_FromLong(0);
}

static PyObject *py_MAPIStoreDB_provision(PyMAPIStoreDBObject *self, PyObject *args)
{
	return PyInt_FromLong(mapistoredb_provision(self->mdb_ctx));
}

static PyObject *py_MAPIStoreDB_provision_namedprops(PyMAPIStoreDBObject *self, PyObject *args)
{
	return PyInt_FromLong(mapistoredb_provision_namedprops(self->mdb_ctx));
}

static PyObject *py_MAPIStoreDB_newmailbox(PyMAPIStoreDBObject *self, PyObject *args)
{
	PyObject	*retval;
	const char	*username;
	int		ret;
	uint32_t	contextID = 0;

	if (!PyArg_ParseTuple(args, "s", &username)) {
		return NULL;
	}

	ret = mapistoredb_new_mailbox(self->mdb_ctx, username, &contextID);

	retval = PyTuple_New(2);
	PyTuple_SetItem(retval, 0, PyInt_FromLong(ret));
	PyTuple_SetItem(retval, 1, PyInt_FromLong(contextID));

	return retval;
}

static PyObject *py_MAPIStoreDB_newdefaultfolder(PyMAPIStoreDBObject *self, PyObject *args)
{
	uint32_t	contextID;
	uint32_t	index;
	const char	*name;
	const char	*mapistore_uri;

	if (!PyArg_ParseTuple(args, "kk|ss", &contextID, &index, &name, &mapistore_uri)) {
		return NULL;
	}

	DEBUG(0, ("[py_MAPIStoreDB_newdefaultdolder] name = %s\n", name));
	DEBUG(0, ("[py_MAPIStoreDB_newdefaultdolder] mapistore_uri = %s\n", mapistore_uri));

	return PyInt_FromLong(mapistore_root_mkdir(self->mdb_ctx->mstore_ctx, contextID, index, 0, name, mapistore_uri));
}

static int PyMAPIStoreDB_setParameter(PyObject *_self, PyObject *value, void *data)
{
	PyMAPIStoreDBObject	*self = (PyMAPIStoreDBObject *) _self;
	const char		*attr = (const char *) data;
	const char		*str;

	if (!self->mdb_ctx) return -1;

	if (!PyArg_Parse(value, "s", &str)) {
		return -1;
	}

	if (!strcmp(attr, "netbiosname")) {
		return mapistoredb_set_netbiosname(self->mdb_ctx, str);
	} else if (!strcmp(attr, "firstorg")) {
		return mapistoredb_set_firstorg(self->mdb_ctx, str);
	} else if (!strcmp(attr, "firstou")) {
		return mapistoredb_set_firstou(self->mdb_ctx, str);
	}

	return 0;
}

static PyObject *PyMAPIStoreDB_getParameter(PyObject *_self, void *data)
{
	PyMAPIStoreDBObject	*self = (PyMAPIStoreDBObject *) _self;
	const char		*attr = (const char *) data;

	if (!strcmp(attr, "netbiosname")) {
		return PyString_FromString(mapistoredb_get_netbiosname(self->mdb_ctx));
	} else if (!strcmp(attr, "firstorg")) {
		return PyString_FromString(mapistoredb_get_firstorg(self->mdb_ctx));
	} else if (!strcmp(attr, "firstou")) {
		return PyString_FromString(mapistoredb_get_firstou(self->mdb_ctx));
	}
	
	return NULL;
}

static PyMethodDef mapistoredb_methods[] = {
	{ "dump_configuration", (PyCFunction)py_MAPIStoreDB_dump_configuration, METH_VARARGS },
	{ "provision", (PyCFunction)py_MAPIStoreDB_provision, METH_VARARGS },
	{ "provision_namedprops", (PyCFunction)py_MAPIStoreDB_provision_namedprops, METH_VARARGS },
	{ "newmailbox", (PyCFunction)py_MAPIStoreDB_newmailbox, METH_VARARGS },
	{ "newdefaultfolder", (PyCFunction)py_MAPIStoreDB_newdefaultfolder, METH_VARARGS },
	{ NULL },
};

static PyGetSetDef mapistoredb_getsetters[] = {
	{ "netbiosname", (getter)PyMAPIStoreDB_getParameter,
	  (setter)PyMAPIStoreDB_setParameter, "netbiosname", "netbiosname"},
	{ "firstorg", (getter)PyMAPIStoreDB_getParameter,
	  (setter)PyMAPIStoreDB_setParameter, "firstorg", "firstorg"},
	{ "firstou", (getter)PyMAPIStoreDB_getParameter,
	  (setter)PyMAPIStoreDB_setParameter, "firstou", "firstou"},
	{ NULL },
};

PyTypeObject PyMAPIStoreDB = {
	PyObject_HEAD_INIT(NULL) 0,
	.tp_name = "mapistoredb",
	.tp_basicsize = sizeof (PyMAPIStoreDBObject),
	.tp_methods = mapistoredb_methods,
	.tp_getset = mapistoredb_getsetters,
	.tp_doc = "mapistore database object",
	.tp_new = py_MAPIStoreDB_new,
	.tp_dealloc = (destructor) py_MAPIStoreDB_dealloc,
	.tp_flags = Py_TPFLAGS_DEFAULT,
};

static PyMethodDef py_mapistoredb_global_methods[] = {
	{ NULL },
};

void initmapistoredb(void)
{
	PyObject	*m;

	if (PyType_Ready(&PyMAPIStoreDB) < 0) {
		return;
	}

	m = Py_InitModule3("mapistoredb", py_mapistoredb_global_methods,
			   "An interface to MAPIStore database");
	if (m == NULL) {
		return;
	}
	
	/* Default system/special folders define */
	PyModule_AddObject(m, "DEFERRED_ACTIONS",	PyInt_FromLong(MDB_DEFERRED_ACTIONS));
	PyModule_AddObject(m, "SPOOLER_QUEUE",		PyInt_FromLong(MDB_SPOOLER_QUEUE));
	PyModule_AddObject(m, "TODO_SEARCH",		PyInt_FromLong(MDB_TODO_SEARCH));
	PyModule_AddObject(m, "IPM_SUBTREE",		PyInt_FromLong(MDB_IPM_SUBTREE));
	PyModule_AddObject(m, "INBOX",			PyInt_FromLong(MDB_INBOX));
	PyModule_AddObject(m, "OUTBOX",			PyInt_FromLong(MDB_OUTBOX));
	PyModule_AddObject(m, "SENT_ITEMS",		PyInt_FromLong(MDB_SENT_ITEMS));
	PyModule_AddObject(m, "DELETED_ITEMS",		PyInt_FromLong(MDB_DELETED_ITEMS));
	PyModule_AddObject(m, "COMMON_VIEW",		PyInt_FromLong(MDB_COMMON_VIEWS));
	PyModule_AddObject(m, "SCHEDULE",		PyInt_FromLong(MDB_SCHEDULE));
	PyModule_AddObject(m, "SEARCH",			PyInt_FromLong(MDB_SEARCH));
	PyModule_AddObject(m, "VIEWS",			PyInt_FromLong(MDB_VIEWS));
	PyModule_AddObject(m, "SHORTCUTS",		PyInt_FromLong(MDB_SHORTCUTS));
	PyModule_AddObject(m, "REMINDERS",		PyInt_FromLong(MDB_REMINDERS));
	PyModule_AddObject(m, "CALENDAR",		PyInt_FromLong(MDB_CALENDAR));
	PyModule_AddObject(m, "CONTACTS",		PyInt_FromLong(MDB_CONTACTS));
	PyModule_AddObject(m, "JOURNAL",		PyInt_FromLong(MDB_JOURNAL));
	PyModule_AddObject(m, "NOTES",			PyInt_FromLong(MDB_NOTES));
	PyModule_AddObject(m, "TASKS",			PyInt_FromLong(MDB_TASKS));
	PyModule_AddObject(m, "DRAFTS",			PyInt_FromLong(MDB_DRAFTS));

	PyModule_AddObject(m, "mapistoredb", (PyObject *)&PyMAPIStoreDB);
}
