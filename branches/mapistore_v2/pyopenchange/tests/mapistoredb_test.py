#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys

sys.path.append("python")

import os
import openchange
import openchange.mapistoredb as mapistoredb
import openchange.mapistore as mapistore
from openchange import mapi

os.mkdir("/tmp/mapistoredb");

print "[Step 1]. Initializing mapistore database"
print "========================================="

MAPIStoreDB = mapistoredb.mapistoredb("/tmp/mapistoredb")
print ""

print "[Step 2]. Modify and dump configuration"
print "======================================="

MAPIStoreDB.netbiosname = "server"
MAPIStoreDB.firstorg = "OpenChange Project"
MAPIStoreDB.firstou = "OpenChange Development Unit"

MAPIStoreDB.dump_configuration()
print ""

print "[Step 3]. Creating the mapistore named properties database"
print "=========================================================="
#MAPIStoreDB.provision_namedprops()
print ""

print "[Step 4]. Provisioning mapistore database"
print "========================================="
ret = MAPIStoreDB.provision()
if (ret == 0):
    print "Provisioning: SUCCESS"
else:
    print "Provisioning: FAILURE"
print ""

print "[Step 5]. Creating mailbox containers"
print "====================================="
(retval,contextID) = MAPIStoreDB.newmailbox("openchange")
print "* Creating openchange mailbox (0 expected, == MAPISTORE_SUCCESS): %d with contextID (1 expected): %d" % (retval, contextID)
(retval,contextID2) = MAPIStoreDB.newmailbox("openchange")
print "* Creating openchange mailbox again (16 expected, == MAPISTORE_ERR_EXIST): %d" % retval
print ""
(retval,contextID) = MAPIStoreDB.newmailbox("openchange2")
print "* Creating openchange2 mailbox (0 expected, == MAPISTORE_SUCCESS): %d with contextID (2 expected): %d" % (retval, contextID)
(retval,contextID2) = MAPIStoreDB.newmailbox("openchange")
print "* Creating openchange mailbox again (16 expected, == MAPISTORE_ERR_EXIST): %d" % retval
print ""

print "[Step 6]. Testing API parts"
print "==========================="

print "A. Testing NetBIOS name"
print "-----------------------"
print "* NetBIOS name: %s" %MAPIStoreDB.netbiosname
print ""

print "B. Testing First OU"
print "-------------------"
print "* FirstOU: %s" %MAPIStoreDB.firstou
print ""

print "C. Testing First Organisation"
print "-----------------------------"
print "* First Organisation: %s" %MAPIStoreDB.firstorg
print ""

print "D. Create a new system/special default folder"
print "---------------------------------------------"
MAPIStoreDB.newdefaultfolder(contextID, mapistoredb.IPM_SUBTREE, "test", "mstoredb://")

print "[Step 7]. Testing mapistore tie-in"
print "=================================="

print "A. Testing mkdir"
MAPIStore = mapistore.mapistore()
mapistore.set_mapping_path("/tmp/mapistore")
ctx_id = MAPIStore.add_context("openchange", "mstoredb://CN=openchange2,CN=OpenChange Development Unit,CN=OpenChange Project,CN=server,CN=kubuntu1010")
SPropValue = mapi.SPropValue()
SPropValue.add(mapi.PR_PARENT_FID, 0x0000000000035001)
SPropValue.add(mapi.PR_DISPLAY_NAME, "test")
SPropValue.add(mapi.PR_COMMENT, "test folder")
SPropValue.add(mapi.PR_FOLDER_TYPE, 1)
MAPIStore.mkdir(contextID2, 0x0000000000035001, 0x0000000000036001, SPropValue)
