/* 
   OpenChange implementation.

   Copyright (C) Julien Kerihuel 2005-2007
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <libmapi/libmapi.h>
#include <ndr.h>
#include <tdr.h>
#include <gen_ndr/ndr_exchange.h>
#include <gen_ndr/tdr_mapi.h>
#include <libmapi/proto_private.h>
#include <core/nterr.h>

/**
 * obfuscate/deobfuscate function
 */
static void obfuscate_data(uint8_t *data, uint32_t size, uint8_t salt)
{
	uint32_t	i;

	for (i = 0; i < size; i++) {
		data[i] ^= salt;
	}
}


/**
 * print a mapi_request
 */

void ndr_print_mapi_request(struct ndr_print *ndr, const char *name, 
			    const struct mapi_request *r)
{
	TALLOC_CTX		*mem_ctx;
	struct tdr_print	*tdr;
	struct EcDoRpc_MAPI_REQ	*mapi_req;
	uint32_t		rlength;

	mem_ctx = (TALLOC_CTX *) ndr;

	tdr = talloc_zero(mem_ctx, struct tdr_print);
	tdr->print = tdr_print_debug_helper;

	rlength = r->mapi_len - r->length;

	ndr_print_uint32(ndr, "mapi_len", r->mapi_len);
	if (r->length && r->length > sizeof (uint16_t)) {
		uint32_t	cntr;

		ndr_print_uint16(ndr, "length", r->length);

		ndr->depth++;
		mapi_req = (struct EcDoRpc_MAPI_REQ *) r->mapi_req;
		for (cntr = 0; mapi_req[cntr].opnum; cntr++) {
			char	*idx = NULL;

			asprintf(&idx, "[%d]", cntr);
			if (idx) {
				tdr->level = ndr->depth;
				tdr_print_EcDoRpc_MAPI_REQ(tdr, "mapi_req", &mapi_req[cntr]);
				free(idx);
				ndr->depth = tdr->level;
			}
			ndr->depth--;
		}
		ndr->depth--;
	}

	if (rlength) {
		uint32_t	cntr;

		ndr->depth++;
		ndr->print(ndr, "%-25s: (handles) number=%u", name, rlength / 4);
		ndr->depth++;
		for (cntr = 0; cntr < (rlength / 4); cntr++) {
			ndr_print_uint32(ndr, "handle", r->handles[cntr]);
		}
		ndr->depth--;
	}
}


/**
 * print a mapi response
 */
void ndr_print_mapi_response(struct ndr_print *ndr, const char *name, 
			     const struct mapi_response *r)
{
	TALLOC_CTX			*mem_ctx;
	struct tdr_print		*tdr;
	struct EcDoRpc_MAPI_REPL	*mapi_repl;
	uint32_t			rlength;

	mem_ctx = (TALLOC_CTX *)ndr;

	tdr = talloc_zero(mem_ctx, struct tdr_print);
	tdr->print = tdr_print_debug_helper;

	rlength = r->mapi_len - r->length;

	ndr->print(ndr, "%-25s: length=%u", name, r->length);
	if (r->length && r->length > sizeof (uint16_t)) {
		uint32_t	cntr;

		ndr->print(ndr, "%s: ARRAY(%d)", name, r->length - 2);
		ndr->depth++;
		
		mapi_repl = (struct EcDoRpc_MAPI_REPL *) r->mapi_repl;
		{
			DATA_BLOB d;

			d.data = r->mapi_repl;
			d.length = r->length;
		}
		for (cntr = 0; mapi_repl[cntr].opnum; cntr++) {
			char	*idx = NULL;
			
			asprintf(&idx, "[%d]", cntr);
			if (idx) {
				tdr->level = ndr->depth;
				tdr_print_EcDoRpc_MAPI_REPL(tdr, "mapi_repl", &mapi_repl[cntr]);
				free(idx);
			}
		}
		ndr->depth--;
	}

	ndr->print(ndr, "%-25s: (handles) number=%u", name, rlength / 4);

	if (rlength) {
		uint32_t	cntr;

		ndr->depth++;
		for (cntr = 0; cntr < (rlength / 4); cntr++) {
			ndr_print_uint32(ndr, "handle", r->handles[cntr]);
		}
		ndr->depth--;
	}
}


/**
 * push mapi_request on the wire
 *
 * MAPI length field includes length bytes, but these bytes do not
 * belong to the mapi content length context. We had to add them when
 * pushing mapi content length (uint16_t) and next substract when
 * pushing the content blob
 */
NTSTATUS ndr_push_mapi_request(struct ndr_push *ndr, int ndr_flags, const struct mapi_request *r)
{
	TALLOC_CTX		*mem_ctx;
	struct tdr_push		*tdr;
	uint32_t		cntr;
	uint32_t		count;
	struct EcDoRpc_MAPI_REQ	*mapi_req;

	mem_ctx = (TALLOC_CTX *) ndr;
	tdr = talloc_zero(mem_ctx, struct tdr_push);

	ndr_set_flags(&ndr->flags, LIBNDR_FLAG_NOALIGN);
	NDR_CHECK(ndr_push_uint16(ndr, NDR_SCALARS, r->length));

	mapi_req = (struct EcDoRpc_MAPI_REQ *) r->mapi_req;
	for (cntr = 0; tdr->offset < r->length - 2; cntr++) {
		TDR_CHECK(tdr_push_EcDoRpc_MAPI_REQ(tdr, &mapi_req[cntr]));
		tdr->offset = tdr->data.length;
	}
	NDR_CHECK(ndr_push_array_uint8(ndr, NDR_SCALARS, tdr->data.data, tdr->data.length));

	count = (r->mapi_len - r->length) / sizeof (uint32_t);
	for (cntr = 0; cntr < count; cntr++) {
		NDR_CHECK(ndr_push_uint32(ndr, NDR_SCALARS, r->handles[cntr]));
	}

	obfuscate_data(ndr->data, ndr->offset, 0xA5);

	return NT_STATUS_OK;
}


/**
 * push mapi_response on the wire
 */
NTSTATUS ndr_push_mapi_response(struct ndr_push *ndr, int ndr_flags, const struct mapi_response *r)
{
	struct mapi_response	blob = *r;
	uint32_t		cntr;
	uint32_t		count;

	if (!(ndr->flags & LIBNDR_FLAG_REMAINING)) {
		NDR_CHECK(ndr_push_uint32(ndr, NDR_SCALARS, blob.mapi_len));
	}
	blob.length += 2;
	NDR_CHECK(ndr_push_uint16(ndr, NDR_SCALARS, blob.length));

	blob.mapi_len -= 2;
	blob.length -= 2;

	count = (blob.mapi_len - blob.length) / sizeof (uint32_t);
	for (cntr = 0; cntr < count; cntr++) {
		NDR_CHECK(ndr_push_uint32(ndr, NDR_SCALARS, blob.handles[cntr]));
	}
	obfuscate_data(ndr->data, ndr->alloc_size, 0xA5);

	return NT_STATUS_OK;
}


/**
 * pull a mapi_request from the wire
 */
NTSTATUS ndr_pull_mapi_request(struct ndr_pull *ndr, int ndr_flags, struct mapi_request *r)
{
	uint32_t		length;
	uint32_t		count;
	uint32_t		cntr;
	TALLOC_CTX		*_mem_save_handles_0;

	obfuscate_data(ndr->data, ndr->data_size, 0xA5);

	/* pull mapi_len */
	if (ndr->flags & LIBNDR_FLAG_REMAINING) {
		length = ndr->data_size - ndr->offset;
	} else {
		NDR_CHECK(ndr_pull_uint32(ndr, NDR_SCALARS, &length));
	}
	r->mapi_len = length;

	/* pull mapi data length */
	NDR_CHECK(ndr_pull_uint16(ndr, NDR_SCALARS, &r->length));

	/* If length == length field size, then skip subcontext */
	if (r->length > sizeof (r->length)) {
		NDR_CHECK(ndr_pull_array_uint8(ndr, NDR_SCALARS, r->mapi_req, r->length));
	}

	/* pull request handles */
	_mem_save_handles_0 = NDR_PULL_GET_MEM_CTX(ndr);
	count = (r->mapi_len - r->length) / sizeof (uint32_t);
	r->handles = talloc_array(_mem_save_handles_0, uint32_t, count + 1);
	for (cntr = 0; cntr < count; cntr++) {
		NDR_CHECK(ndr_pull_uint32(ndr, NDR_SCALARS, &r->handles[cntr]));
	}

	return NT_STATUS_OK;
}


/**
 * pull a mapi_response from the wire
 */
NTSTATUS ndr_pull_mapi_response(struct ndr_pull *ndr, int ndr_flags, struct mapi_response *r)
{
	uint32_t			length;
	uint32_t			count;
	uint32_t			cntr;
	TALLOC_CTX			*_mem_save_handles_0;
	TALLOC_CTX			*mem_ctx;
	struct EcDoRpc_MAPI_REPL	*mapi_repl;
	struct tdr_pull			*tdr;

	obfuscate_data(ndr->data, ndr->data_size, 0xA5);

	ndr_set_flags(&ndr->flags, LIBNDR_FLAG_NOALIGN);
	
	/* pull mapi_len */
	if (ndr->flags & LIBNDR_FLAG_REMAINING) {
		length = ndr->data_size - ndr->offset;
	} else {
		NDR_CHECK(ndr_pull_uint32(ndr, NDR_SCALARS, &length));
	}
	r->mapi_len = length;

	/* pull mapi data length */
	NDR_CHECK(ndr_pull_uint16(ndr, NDR_SCALARS, &r->length));

	mem_ctx = (TALLOC_CTX *) ndr;

	/* If length == length field size, then skip subcontext */
	if (r->length > sizeof (r->length)) {
		tdr = talloc_zero(ndr, struct tdr_pull);
		tdr->data.length = r->length - sizeof (r->length);
		tdr->data.data = talloc_array(mem_ctx, uint8_t, tdr->data.length);
		NDR_CHECK(ndr_pull_bytes(ndr, tdr->data.data, tdr->data.length));

		/* work on the blob */
		tdr->offset = 0;
		mapi_repl = talloc_zero(mem_ctx, struct EcDoRpc_MAPI_REPL);
		for (cntr = 0; tdr->offset < tdr->data.length; cntr++) {
			TDR_CHECK(tdr_pull_EcDoRpc_MAPI_REPL(tdr, (TALLOC_CTX *)ndr, &mapi_repl[cntr]));
 			mapi_repl = talloc_realloc(mem_ctx, mapi_repl, struct EcDoRpc_MAPI_REPL, cntr + 2);
		}
		mapi_repl = talloc_realloc(mem_ctx, mapi_repl, struct EcDoRpc_MAPI_REPL, cntr + 2);
		mapi_repl[cntr].opnum = 0;

		r->mapi_repl = (uint8_t *)mapi_repl;
	}

	/* pull request handles */
	_mem_save_handles_0 = NDR_PULL_GET_MEM_CTX(ndr);
	count = (r->mapi_len - r->length) / sizeof (uint32_t);
	r->handles = talloc_array(_mem_save_handles_0, uint32_t, count + 1);
	for (cntr = 0; cntr < count; cntr++) {
		NDR_CHECK(ndr_pull_uint32(ndr, NDR_SCALARS, &r->handles[cntr]));
	}

	return NT_STATUS_OK;	
}


NTSTATUS tdr_pull_EcDoRpc_MAPI_REPL(struct tdr_pull *tdr, TALLOC_CTX *mem_ctx, struct EcDoRpc_MAPI_REPL *r)
{
	TDR_CHECK(tdr_pull_uint8(tdr, mem_ctx, &r->opnum));
	if (r->opnum == op_MAPI_Notify) {
		TDR_CHECK(tdr_pull_EcDoRpc_MAPI_REPL_UNION(tdr, mem_ctx, r->opnum, &r->u));
	} else {
		TDR_CHECK(tdr_pull_uint8(tdr, mem_ctx, &r->handle_idx));
		TDR_CHECK(tdr_pull_uint32(tdr, mem_ctx, &r->error_code));
		if (r->error_code == MAPI_E_SUCCESS) {
			TDR_CHECK(tdr_pull_EcDoRpc_MAPI_REPL_UNION(tdr, mem_ctx, r->opnum, &r->u));
		}
	}

	return NT_STATUS_OK;
}


NTSTATUS tdr_print_EcDoRpc_MAPI_REPL(struct tdr_print *tdr, const char *name, struct EcDoRpc_MAPI_REPL *r)
{
	tdr->print(tdr, "%-25s: struct EcDoRpc_MAPI_REPL", name);
	tdr->level++;
	tdr_print_uint8(tdr, "opnum", &r->opnum);
	if (r->opnum != op_MAPI_Notify) {
		tdr_print_uint8(tdr, "handle idx", &r->handle_idx);
		tdr_print_uint32(tdr, "error code", &r->error_code);
		if (r->error_code == MAPI_E_SUCCESS) {
			tdr_print_EcDoRpc_MAPI_REPL_UNION(tdr, "u", r->opnum, &r->u);
		}
	} else {
		tdr_print_EcDoRpc_MAPI_REPL_UNION(tdr, "u", r->opnum, &r->u);
	}
	tdr->level--;

	return NT_STATUS_OK;
}


NTSTATUS tdr_pull_QueryRows_repl(struct tdr_pull *tdr, TALLOC_CTX *mem_ctx, struct QueryRows_repl *r)
{
	TDR_CHECK(tdr_pull_uint8(tdr, mem_ctx, &r->unknown));
	TDR_CHECK(tdr_pull_uint16(tdr, mem_ctx, &r->results_count));

	if (r->results_count) {
		uint32_t flags = tdr->flags;

		TDR_CHECK(tdr_pull_uint8(tdr, mem_ctx, &r->layout));
		tdr->flags |= TDR_REMAINING;
		TDR_CHECK(tdr_pull_DATA_BLOB(tdr, mem_ctx, &r->inbox));
		tdr->flags = flags;
	}

	return NT_STATUS_OK;
}


NTSTATUS tdr_print_QueryRows_repl(struct tdr_print *tdr, const char *name, struct QueryRows_repl *r)
{
	tdr->level++;
	tdr_print_uint8(tdr, "unknown", &r->unknown);
	tdr_print_uint16(tdr, "results_count", &r->results_count);
	tdr_print_uint8(tdr, "layout", &r->layout);
	if (r->results_count) {
		tdr_print_DATA_BLOB(tdr, "inbox", &r->inbox);
	}
	tdr->level--;

	return NT_STATUS_OK;
}

