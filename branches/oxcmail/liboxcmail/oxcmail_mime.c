/*
   OpenChange MIME mail <-> Email object conversion library.

   Copyright (C) Brad Hards <bradh@frogmouth.net> 2010.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "oxcmail_mime.h"

struct oxcmail_mime_info *oxcmail_mime_info_init(TALLOC_CTX *mem_ctx)
{
	struct oxcmail_mime_info *mime_info = talloc_zero(mem_ctx, struct oxcmail_mime_info);
	mime_info->is_attachment = false;
	return mime_info;
}

void oxcmail_mime_info_dump(struct oxcmail_mime_info *mime_info, const char *seperator)
{
	const char *sep;
	if (seperator) {
		sep = seperator;
	} else {
		sep = "";
	}

	if (! mime_info) {
		printf("%s(NULL)", sep);
		return;
	}

	if (mime_info->mime_version) {
		printf("%sMIME version: %s\n", sep, mime_info->mime_version);
	} else {
		printf("%sMIME version: Not specified\n", sep);
	}

	if (mime_info->full_content_type) {
		printf("%sFull content type: %s\n", sep, mime_info->full_content_type);
	}

	if (mime_info->content_type) {
		printf("%sContent Type: %s\n", sep, mime_info->content_type);
	}

	if (mime_info->boundary_marker) {
		printf("%sBoundary marker: %s\n", sep, mime_info->boundary_marker);
	}

	if (mime_info->charset) {
		printf("%sCharacter set: %s\n", sep, mime_info->charset);
	}
	
	if (mime_info->transfer_encoding) {
		printf("%sTransfer encoding: %s\n", sep, mime_info->transfer_encoding);
	}
	if (mime_info->is_attachment) {
		if (mime_info->attachment_filename) {
			printf("%sAttachment name: %s\n", sep, mime_info->attachment_filename);
		} else {
			printf("%sUnknown attachment name\n", sep);
		}
	}
}
