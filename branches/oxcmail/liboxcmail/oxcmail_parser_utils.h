/*
   OpenChange MIME mail <-> Email object conversion library.

   Copyright (C) Brad Hards <bradh@frogmouth.net> 2010.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OXCMAIL_PARSER_UTILS
#define OXCMAIL_PARSER_UTILS 1

#include "libmapi/libmapi.h"

#ifndef _BSD_SOURCE
#define _BSD_SOURCE 1
#endif
#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 1/* glibc2 needs this */
#endif
#include <time.h>

bool oxcmail_util_tm_to_FILETIME(struct tm *unix_time, struct FILETIME *filetime);

bool oxcmail_util_parse_address(TALLOC_CTX *mem_ctx, const char *addr_string, char **displayname, char **emailaddress);

#endif
