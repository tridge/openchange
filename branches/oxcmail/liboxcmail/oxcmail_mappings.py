# -*- coding: utf-8 -*-

# oxcmail property mappings for cog
# Copyright (C) Brad Hards <bradh@openchange.org> 2010
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#   
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#   
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

directstringconverts = [
	( "Thread-Topic", "PR_CONVERSATION_TOPIC" ),
	( "List-Help", "PR_LIST_HELP" ),
	( "List-Subscribe", "PR_LIST_SUBSCRIBE" ),
	( "List-Unsubscribe", "PR_LIST_UNSUBSCRIBE" ),
	( "Message-ID", "PR_INTERNET_MESSAGE_ID" ),
	( "In-Reply-To", "PR_IN_REPLY_TO_ID" ),
	( "References", "PR_INTERNET_REFERENCES" )
	]

importance_enum = [
	( "0x0000000", "Low" ),
	( "0x0000001", "Normal" ),
	( "0x0000002", "High" )
	]

sensitivity_enum = [
	( "0x00000000", "Normal" ),
	( "0x00000001", "Personal" ),
	( "0x00000002", "Private" ),
	( "0x00000003", "Company-Confidential" )
	]

enumconverts = [
	( "Importance", "PR_IMPORTANCE", importance_enum ),
	( "Sensitivity", "PR_SENSITIVITY", sensitivity_enum ),
	]

dateconverts = [
	( "Date", "PR_CLIENT_SUBMIT_TIME" ),
	( "Reply-By", "PR_REPLY_TIME" ),
	( "Expires", "PR_EXPIRY_TIME" ),
	]

binaryconverts = [
	( "Thread-Index", "PR_CONVERSATION_INDEX" ),
	]

addressconverts = [
	( "From", "PR_SENT_REPRESENTING_NAME", "PR_SENT_REPRESENTING_EMAIL_ADDRESS", "PR_SENT_REPRESENTING_ADDRTYPE", "PR_SENT_REPRESENTING_ENTRYID"),
	( "Sender", "PR_SENDER_NAME", "PR_SENDER_EMAIL_ADDRESS", "PR_SENDER_ADDRTYPE", "PR_SENDER_ENTRYID" ),
	]

addresslistconverts = [
	( "To", "MAPI_TO" ),
	( "Cc", "MAPI_CC" ),
	( "Bcc", "MAPI_BCC" ),
	]

customconverts = [
	( "Subject", "oxcmail_parse_Subject" ),
	( "Content-Language", "oxcmail_parse_Content_Language" ),
	]
