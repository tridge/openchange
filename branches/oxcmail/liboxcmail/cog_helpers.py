#!/usr/bin/python
# -*- coding: utf-8 -*-

#
# Helper functions for cog
# Copyright (C) Brad Hards <bradh@openchange.org> 2010
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#   
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#   
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import string

# Make a function a legitimate C function name (or part of)
# 
# This will probably need many more changes
def legitimise_function_name(original_fn_name):
	cleaner_fn_name = original_fn_name
	cleaner_fn_name = cleaner_fn_name.replace('-', '_')
	cleaner_fn_name = cleaner_fn_name.replace('.', '_')
	return cleaner_fn_name
	
def escape_quotes(original_str):
	cleaner_str = original_str
	cleaner_str = cleaner_str.replace('\\', '\\\\')
	cleaner_str = cleaner_str.replace('"', '\\"')
	return cleaner_str

