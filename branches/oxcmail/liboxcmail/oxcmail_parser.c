/*
   OpenChange MIME mail <-> Email object conversion library.

   Copyright (C) Brad Hards <bradh@frogmouth.net> 2010.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Note: this file uses cog (http://nedbatchelder.com/code/cog/index.html),
 so you can not just update the code. You need to update the cog script,
 then run cog -r again */

#include "oxcmail_parser.h"
#include "oxcmail_public_proto.h"
#include "oxcmail_proto.h"
#include "oxcmail_parser_utils.h"

#include "libexchange2ical/libexchange2ical.h"

#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE /* glibc2 needs this */
#endif
#include <time.h>

#include "ldb.h" /* for ldb_base64_decode */
#include <ctype.h> /* for isspace */

/*[[[cog
import cog
from cog_helpers import legitimise_function_name

from oxcmail_mappings import directstringconverts

for mimeheader,proptagname in directstringconverts:
    cog.outl('bool oxcmail_parse_%s(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)' % legitimise_function_name(mimeheader))
    cog.outl('{')
    cog.outl('\tif (strcasestr(header, "%s:") == header) {' % mimeheader)
    cog.outl('\t\tchar *header_value = talloc_strndup(properties->lpProps, header + strlen("%s:"), strlen(header) - strlen("%s:"));' % (mimeheader, mimeheader));
    cog.outl('\t\tstruct SPropValue prop;')
    cog.outl('\t\tset_SPropValue_proptag(&prop, %s, header_value);' % proptagname)
    cog.outl('\t\tSRow_addprop(properties, prop);')
    cog.outl('\t\treturn true;')
    cog.outl('\t} else {')
    cog.outl('\t\treturn false;')
    cog.outl('\t}')
    cog.outl('}')
    cog.outl('')

]]]*/
bool oxcmail_parse_Thread_Topic(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	if (strcasestr(header, "Thread-Topic:") == header) {
		char *header_value = talloc_strndup(properties->lpProps, header + strlen("Thread-Topic:"), strlen(header) - strlen("Thread-Topic:"));
		struct SPropValue prop;
		set_SPropValue_proptag(&prop, PR_CONVERSATION_TOPIC, header_value);
		SRow_addprop(properties, prop);
		return true;
	} else {
		return false;
	}
}

bool oxcmail_parse_List_Help(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	if (strcasestr(header, "List-Help:") == header) {
		char *header_value = talloc_strndup(properties->lpProps, header + strlen("List-Help:"), strlen(header) - strlen("List-Help:"));
		struct SPropValue prop;
		set_SPropValue_proptag(&prop, PR_LIST_HELP, header_value);
		SRow_addprop(properties, prop);
		return true;
	} else {
		return false;
	}
}

bool oxcmail_parse_List_Subscribe(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	if (strcasestr(header, "List-Subscribe:") == header) {
		char *header_value = talloc_strndup(properties->lpProps, header + strlen("List-Subscribe:"), strlen(header) - strlen("List-Subscribe:"));
		struct SPropValue prop;
		set_SPropValue_proptag(&prop, PR_LIST_SUBSCRIBE, header_value);
		SRow_addprop(properties, prop);
		return true;
	} else {
		return false;
	}
}

bool oxcmail_parse_List_Unsubscribe(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	if (strcasestr(header, "List-Unsubscribe:") == header) {
		char *header_value = talloc_strndup(properties->lpProps, header + strlen("List-Unsubscribe:"), strlen(header) - strlen("List-Unsubscribe:"));
		struct SPropValue prop;
		set_SPropValue_proptag(&prop, PR_LIST_UNSUBSCRIBE, header_value);
		SRow_addprop(properties, prop);
		return true;
	} else {
		return false;
	}
}

bool oxcmail_parse_Message_ID(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	if (strcasestr(header, "Message-ID:") == header) {
		char *header_value = talloc_strndup(properties->lpProps, header + strlen("Message-ID:"), strlen(header) - strlen("Message-ID:"));
		struct SPropValue prop;
		set_SPropValue_proptag(&prop, PR_INTERNET_MESSAGE_ID, header_value);
		SRow_addprop(properties, prop);
		return true;
	} else {
		return false;
	}
}

bool oxcmail_parse_In_Reply_To(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	if (strcasestr(header, "In-Reply-To:") == header) {
		char *header_value = talloc_strndup(properties->lpProps, header + strlen("In-Reply-To:"), strlen(header) - strlen("In-Reply-To:"));
		struct SPropValue prop;
		set_SPropValue_proptag(&prop, PR_IN_REPLY_TO_ID, header_value);
		SRow_addprop(properties, prop);
		return true;
	} else {
		return false;
	}
}

bool oxcmail_parse_References(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	if (strcasestr(header, "References:") == header) {
		char *header_value = talloc_strndup(properties->lpProps, header + strlen("References:"), strlen(header) - strlen("References:"));
		struct SPropValue prop;
		set_SPropValue_proptag(&prop, PR_INTERNET_REFERENCES, header_value);
		SRow_addprop(properties, prop);
		return true;
	} else {
		return false;
	}
}

/*[[[end]]] (checksum: 091f25c83c1bafc7a31111562e432b45)*/


/*[[[cog

from oxcmail_mappings import enumconverts

for mimeheader,proptagname,enum_mapping in enumconverts:
    cog.outl('static bool oxcmail_parse_lookup_%s(const char *string_value, uint32_t *result)' % legitimise_function_name(mimeheader))
    cog.outl('{')
    for enum_num, enum_str in enum_mapping:
	cog.outl('\tif ((strcasecmp(string_value, "%s") == 0)) {' % enum_str )
	cog.outl('\t\t*result = %s;' %enum_num)
	cog.outl('\t\treturn true;')
	cog.outl('\t}')
    cog.outl('\tprintf("unhandled case in oxcmail_parse_lookup_%s: \\"%%s\\"\\n", string_value);' % legitimise_function_name(mimeheader) )
    cog.outl('\treturn false;')
    cog.outl('}')
    cog.outl('')
    
    cog.outl('static bool oxcmail_parse_%s(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)' % legitimise_function_name(mimeheader))
    cog.outl('{')
    cog.outl('\tif (strcasestr(header, "%s:") == header) {' % mimeheader)
    cog.outl('\t\tuint32_t header_value;')
    cog.outl('\t\tchar *string_value = talloc_strdup(mem_ctx, header + strlen("%s:"));' % legitimise_function_name(mimeheader))
    cog.outl('\t\ttrim_string(string_value, " ", " ");')
    cog.outl('\t\ttrim_string(string_value, "\\t", "\\n");')
    cog.outl('\t\tif (oxcmail_parse_lookup_%s(string_value, &header_value)) {' % mimeheader)
    cog.outl('\t\t\tstruct SPropValue prop;')
    cog.outl('\t\t\tset_SPropValue_proptag(&prop, %s, &header_value);' % proptagname)
    cog.outl('\t\t\tSRow_addprop(properties, prop);')
    cog.outl('\t\t\ttalloc_free((void*)string_value);')
    cog.outl('\t\t\treturn true;')
    cog.outl('\t\t} else {')
    cog.outl('\t\t\ttalloc_free((void*)string_value);')
    cog.outl('\t\t\treturn false;')
    cog.outl('\t\t}')
    cog.outl('\t} else {')
    cog.outl('\t\treturn false;')
    cog.outl('\t}')
    cog.outl('}')
    cog.outl('')

]]]*/
static bool oxcmail_parse_lookup_Importance(const char *string_value, uint32_t *result)
{
	if ((strcasecmp(string_value, "Low") == 0)) {
		*result = 0x0000000;
		return true;
	}
	if ((strcasecmp(string_value, "Normal") == 0)) {
		*result = 0x0000001;
		return true;
	}
	if ((strcasecmp(string_value, "High") == 0)) {
		*result = 0x0000002;
		return true;
	}
	printf("unhandled case in oxcmail_parse_lookup_Importance: \"%s\"\n", string_value);
	return false;
}

static bool oxcmail_parse_Importance(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	if (strcasestr(header, "Importance:") == header) {
		uint32_t header_value;
		char *string_value = talloc_strdup(mem_ctx, header + strlen("Importance:"));
		trim_string(string_value, " ", " ");
		trim_string(string_value, "\t", "\n");
		if (oxcmail_parse_lookup_Importance(string_value, &header_value)) {
			struct SPropValue prop;
			set_SPropValue_proptag(&prop, PR_IMPORTANCE, &header_value);
			SRow_addprop(properties, prop);
			talloc_free((void*)string_value);
			return true;
		} else {
			talloc_free((void*)string_value);
			return false;
		}
	} else {
		return false;
	}
}

static bool oxcmail_parse_lookup_Sensitivity(const char *string_value, uint32_t *result)
{
	if ((strcasecmp(string_value, "Normal") == 0)) {
		*result = 0x00000000;
		return true;
	}
	if ((strcasecmp(string_value, "Personal") == 0)) {
		*result = 0x00000001;
		return true;
	}
	if ((strcasecmp(string_value, "Private") == 0)) {
		*result = 0x00000002;
		return true;
	}
	if ((strcasecmp(string_value, "Company-Confidential") == 0)) {
		*result = 0x00000003;
		return true;
	}
	printf("unhandled case in oxcmail_parse_lookup_Sensitivity: \"%s\"\n", string_value);
	return false;
}

static bool oxcmail_parse_Sensitivity(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	if (strcasestr(header, "Sensitivity:") == header) {
		uint32_t header_value;
		char *string_value = talloc_strdup(mem_ctx, header + strlen("Sensitivity:"));
		trim_string(string_value, " ", " ");
		trim_string(string_value, "\t", "\n");
		if (oxcmail_parse_lookup_Sensitivity(string_value, &header_value)) {
			struct SPropValue prop;
			set_SPropValue_proptag(&prop, PR_SENSITIVITY, &header_value);
			SRow_addprop(properties, prop);
			talloc_free((void*)string_value);
			return true;
		} else {
			talloc_free((void*)string_value);
			return false;
		}
	} else {
		return false;
	}
}

/*[[[end]]] (checksum: 458246ba9939de5fc2b446a6b419c6bb)*/

/*[[[cog
from oxcmail_mappings import dateconverts

for mimeheader,proptagname in dateconverts:
    cog.outl('bool oxcmail_parse_%s(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)' % legitimise_function_name(mimeheader))
    cog.outl('{')
    cog.outl('\tif (strcasestr(header, "%s:") == header) {' % mimeheader)
    cog.outl('\t\tstruct tm *time;')
    cog.outl('\t\tchar *residual;')
    cog.outl('\t\tstruct SPropValue prop;')
    cog.outl('\t\ttime = talloc_zero(mem_ctx, struct tm);')
    cog.outl('\t\tstruct FILETIME filetime;')
    cog.outl('\t\tchar *header_value = talloc_strndup(mem_ctx, header + strlen("%s:"), strlen(header) - strlen("%s:"));' % (mimeheader, mimeheader));
    cog.outl('\t\ttrim_string(header_value, " ", " ");')
    cog.outl('\t\ttrim_string(header_value, "\\t", "\\n");')
    cog.outl('\t\tresidual = strptime(header_value, "%a, %d %b %Y %H:%M:%S %z", time);')
    cog.outl('\t\ttalloc_free((void*)header_value);')
    cog.outl('\t\toxcmail_util_tm_to_FILETIME(time, &filetime);')
    cog.outl('\t\ttalloc_free(time);')
    cog.outl('\t\tset_SPropValue_proptag(&prop, %s, &filetime);' % proptagname)
    cog.outl('\t\tSRow_addprop(properties, prop);')
    cog.outl('\t\treturn true;')
    cog.outl('\t} else {')
    cog.outl('\t\treturn false;')
    cog.outl('\t}')
    cog.outl('}')
    cog.outl('')

]]]*/
bool oxcmail_parse_Date(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	if (strcasestr(header, "Date:") == header) {
		struct tm *time;
		char *residual;
		struct SPropValue prop;
		time = talloc_zero(mem_ctx, struct tm);
		struct FILETIME filetime;
		char *header_value = talloc_strndup(mem_ctx, header + strlen("Date:"), strlen(header) - strlen("Date:"));
		trim_string(header_value, " ", " ");
		trim_string(header_value, "\t", "\n");
		residual = strptime(header_value, "%a, %d %b %Y %H:%M:%S %z", time);
		talloc_free((void*)header_value);
		oxcmail_util_tm_to_FILETIME(time, &filetime);
		talloc_free(time);
		set_SPropValue_proptag(&prop, PR_CLIENT_SUBMIT_TIME, &filetime);
		SRow_addprop(properties, prop);
		return true;
	} else {
		return false;
	}
}

bool oxcmail_parse_Reply_By(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	if (strcasestr(header, "Reply-By:") == header) {
		struct tm *time;
		char *residual;
		struct SPropValue prop;
		time = talloc_zero(mem_ctx, struct tm);
		struct FILETIME filetime;
		char *header_value = talloc_strndup(mem_ctx, header + strlen("Reply-By:"), strlen(header) - strlen("Reply-By:"));
		trim_string(header_value, " ", " ");
		trim_string(header_value, "\t", "\n");
		residual = strptime(header_value, "%a, %d %b %Y %H:%M:%S %z", time);
		talloc_free((void*)header_value);
		oxcmail_util_tm_to_FILETIME(time, &filetime);
		talloc_free(time);
		set_SPropValue_proptag(&prop, PR_REPLY_TIME, &filetime);
		SRow_addprop(properties, prop);
		return true;
	} else {
		return false;
	}
}

bool oxcmail_parse_Expires(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	if (strcasestr(header, "Expires:") == header) {
		struct tm *time;
		char *residual;
		struct SPropValue prop;
		time = talloc_zero(mem_ctx, struct tm);
		struct FILETIME filetime;
		char *header_value = talloc_strndup(mem_ctx, header + strlen("Expires:"), strlen(header) - strlen("Expires:"));
		trim_string(header_value, " ", " ");
		trim_string(header_value, "\t", "\n");
		residual = strptime(header_value, "%a, %d %b %Y %H:%M:%S %z", time);
		talloc_free((void*)header_value);
		oxcmail_util_tm_to_FILETIME(time, &filetime);
		talloc_free(time);
		set_SPropValue_proptag(&prop, PR_EXPIRY_TIME, &filetime);
		SRow_addprop(properties, prop);
		return true;
	} else {
		return false;
	}
}

/*[[[end]]] (checksum: f1d5a15523ff244f46dc7918baaa5a81) */

/*[[[cog
from oxcmail_mappings import binaryconverts

for mimeheader,proptagname in binaryconverts:
    cog.outl('bool oxcmail_parse_%s(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)' % legitimise_function_name(mimeheader))
    cog.outl('{')
    cog.outl('\tif (strcasestr(header, "%s:") == header) {' % mimeheader)
    cog.outl('\t\tstruct Binary_r bindata;')
    cog.outl('\t\tstruct SPropValue prop;')
    cog.outl('\t\tchar *header_value = talloc_strndup(mem_ctx, header + strlen("%s:"), strlen(header) - strlen("%s:") );' % (mimeheader, mimeheader));
    cog.outl('\t\ttrim_string(header_value, " ", " ");')
    cog.outl('\t\ttrim_string(header_value, "\\t", "\\n");')
    cog.outl('\t\tbindata.cb = ldb_base64_decode(header_value);')
    cog.outl('\t\tbindata.lpb = (uint8_t*)talloc_strndup(properties->lpProps, header_value, bindata.cb);')
    cog.outl('\t\tset_SPropValue_proptag(&prop, %s, &bindata);' % proptagname)
    cog.outl('\t\tSRow_addprop(properties, prop);')
    cog.outl('\t\ttalloc_free(header_value);')
    cog.outl('\t\treturn true;')
    cog.outl('\t} else {')
    cog.outl('\t\treturn false;')
    cog.outl('\t}')
    cog.outl('}')
    cog.outl('')

]]]*/
bool oxcmail_parse_Thread_Index(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	if (strcasestr(header, "Thread-Index:") == header) {
		struct Binary_r bindata;
		struct SPropValue prop;
		char *header_value = talloc_strndup(mem_ctx, header + strlen("Thread-Index:"), strlen(header) - strlen("Thread-Index:") );
		trim_string(header_value, " ", " ");
		trim_string(header_value, "\t", "\n");
		bindata.cb = ldb_base64_decode(header_value);
		bindata.lpb = (uint8_t*)talloc_strndup(properties->lpProps, header_value, bindata.cb);
		set_SPropValue_proptag(&prop, PR_CONVERSATION_INDEX, &bindata);
		SRow_addprop(properties, prop);
		talloc_free(header_value);
		return true;
	} else {
		return false;
	}
}

/*[[[end]]] (checksum: 7c39bd1ce5c4ea91d7a67e0cdbca918d) */

/*[[[cog
from oxcmail_mappings import addressconverts

for mimeheader,displayname,email_address,address_type,entry_id in addressconverts:
    cog.outl('bool oxcmail_parse_%s(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)' % legitimise_function_name(mimeheader))
    cog.outl('{')
    cog.outl('\tchar *displayname;')
    cog.outl('\tchar *emailaddress;')
    cog.outl('\tif (strcasestr(header, "%s:") == header) {' % mimeheader)
    cog.outl('\t\tchar *header_value = talloc_strndup(mem_ctx, header + strlen("%s:"), strlen(header) - strlen("%s:"));' % (mimeheader, mimeheader));
    cog.outl('\t\ttrim_string(header_value, "\\t", "\\n");')
    cog.outl('\t\ttrim_string(header_value, " ", " ");')
    cog.outl('\t\tproperties->cValues += 3;')
    cog.outl('\t\tproperties->lpProps = talloc_realloc(NULL, properties->lpProps, struct SPropValue, properties->cValues);')
    cog.outl('\t\toxcmail_util_parse_address(properties->lpProps, header_value, &displayname, &emailaddress);')
    cog.outl('\t\tset_SPropValue_proptag(&(properties->lpProps[properties->cValues -3]), %s, displayname);' % displayname)
    cog.outl('\t\tset_SPropValue_proptag(&(properties->lpProps[properties->cValues -2]), %s, "SMTP");' % address_type)
    cog.outl('\t\tset_SPropValue_proptag(&(properties->lpProps[properties->cValues -1]), %s, emailaddress);' % email_address)
    cog.outl('\t\ttalloc_free((char*)header_value);')
    cog.outl('\t\treturn true;')
    cog.outl('\t} else {')
    cog.outl('\t\treturn false;')
    cog.outl('\t}')
    cog.outl('}')
    cog.outl('')

]]]*/
bool oxcmail_parse_From(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	char *displayname;
	char *emailaddress;
	if (strcasestr(header, "From:") == header) {
		char *header_value = talloc_strndup(mem_ctx, header + strlen("From:"), strlen(header) - strlen("From:"));
		trim_string(header_value, "\t", "\n");
		trim_string(header_value, " ", " ");
		properties->cValues += 3;
		properties->lpProps = talloc_realloc(NULL, properties->lpProps, struct SPropValue, properties->cValues);
		oxcmail_util_parse_address(properties->lpProps, header_value, &displayname, &emailaddress);
		set_SPropValue_proptag(&(properties->lpProps[properties->cValues -3]), PR_SENT_REPRESENTING_NAME, displayname);
		set_SPropValue_proptag(&(properties->lpProps[properties->cValues -2]), PR_SENT_REPRESENTING_ADDRTYPE, "SMTP");
		set_SPropValue_proptag(&(properties->lpProps[properties->cValues -1]), PR_SENT_REPRESENTING_EMAIL_ADDRESS, emailaddress);
		talloc_free((char*)header_value);
		return true;
	} else {
		return false;
	}
}

bool oxcmail_parse_Sender(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	char *displayname;
	char *emailaddress;
	if (strcasestr(header, "Sender:") == header) {
		char *header_value = talloc_strndup(mem_ctx, header + strlen("Sender:"), strlen(header) - strlen("Sender:"));
		trim_string(header_value, "\t", "\n");
		trim_string(header_value, " ", " ");
		properties->cValues += 3;
		properties->lpProps = talloc_realloc(NULL, properties->lpProps, struct SPropValue, properties->cValues);
		oxcmail_util_parse_address(properties->lpProps, header_value, &displayname, &emailaddress);
		set_SPropValue_proptag(&(properties->lpProps[properties->cValues -3]), PR_SENDER_NAME, displayname);
		set_SPropValue_proptag(&(properties->lpProps[properties->cValues -2]), PR_SENDER_ADDRTYPE, "SMTP");
		set_SPropValue_proptag(&(properties->lpProps[properties->cValues -1]), PR_SENDER_EMAIL_ADDRESS, emailaddress);
		talloc_free((char*)header_value);
		return true;
	} else {
		return false;
	}
}

/*[[[end]]] (checksum: a9db9a6f3f053fd47ef2b66e6354e96f)*/

/*[[[cog
from oxcmail_mappings import addresslistconverts

for mimeheader,reciptype in addresslistconverts:
    cog.outl('bool oxcmail_parse_%s(TALLOC_CTX *mem_ctx, const char *header, struct SRowSet *propset)' % legitimise_function_name(mimeheader))
    cog.outl('{')
    cog.outl('\tchar *addr_string = NULL;')
    cog.outl('\tif (strcasestr(header, "%s:") == header) {' % mimeheader)
    cog.outl('\t\tconst char *header_idx;')
    cog.outl('\t\tuint32_t recipient_type = %s;' % reciptype)
    cog.outl('\t\tuint32_t obj_type = MAPI_MAILUSER;')
    cog.outl('\t\tconst char *header_value = talloc_strndup(mem_ctx, header + strlen("%s:"), strlen(header) - strlen("%s:"));' % (mimeheader, mimeheader));
    cog.outl('\t\theader_idx = header_value;')
    cog.outl('\t\twhile (next_token_talloc(mem_ctx, &header_idx, &addr_string, ",")) {')
    cog.outl('\t\t\ttrim_string(addr_string, " ", " ");')
    cog.outl('\t\t\tchar *displayname;')
    cog.outl('\t\t\tchar *emailaddress;')
    cog.outl('\t\t\tpropset->cRows = propset->cRows + 1;')
    cog.outl('\t\t\tpropset->aRow = talloc_realloc(NULL, propset->aRow, struct SRow, propset->cRows);')
    cog.outl('\t\t\tpropset->aRow[propset->cRows -1].cValues = 5;')
    cog.outl('\t\t\tpropset->aRow[propset->cRows -1].lpProps = talloc_array(propset->aRow, struct SPropValue, propset->aRow[propset->cRows -1].cValues);')
    cog.outl('\t\t\toxcmail_util_parse_address(propset->aRow[propset->cRows -1].lpProps, addr_string, &displayname, &emailaddress);')
    cog.outl('\t\t\tset_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[0]), PR_DISPLAY_NAME, displayname);')
    cog.outl('\t\t\tset_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[1]), PR_ADDRTYPE, "SMTP");')
    cog.outl('\t\t\tset_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[2]), PR_EMAIL_ADDRESS, emailaddress);')
    cog.outl('\t\t\tset_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[3]), PR_RECIPIENT_TYPE, &(recipient_type));')
    cog.outl('\t\t\tset_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[4]), PR_OBJECT_TYPE, &(obj_type));')
    cog.outl('\t\t\ttalloc_free(addr_string);')
    cog.outl('\t\t}')
    cog.outl('\t\ttalloc_free((char*)header_value);')
    cog.outl('\t\treturn true;')
    cog.outl('\t} else {')
    cog.outl('\t\treturn false;')
    cog.outl('\t}')
    cog.outl('}')
    cog.outl('')

]]]*/
bool oxcmail_parse_To(TALLOC_CTX *mem_ctx, const char *header, struct SRowSet *propset)
{
	char *addr_string = NULL;
	if (strcasestr(header, "To:") == header) {
		const char *header_idx;
		uint32_t recipient_type = MAPI_TO;
		uint32_t obj_type = MAPI_MAILUSER;
		const char *header_value = talloc_strndup(mem_ctx, header + strlen("To:"), strlen(header) - strlen("To:"));
		header_idx = header_value;
		while (next_token_talloc(mem_ctx, &header_idx, &addr_string, ",")) {
			trim_string(addr_string, " ", " ");
			char *displayname;
			char *emailaddress;
			propset->cRows = propset->cRows + 1;
			propset->aRow = talloc_realloc(NULL, propset->aRow, struct SRow, propset->cRows);
			propset->aRow[propset->cRows -1].cValues = 5;
			propset->aRow[propset->cRows -1].lpProps = talloc_array(propset->aRow, struct SPropValue, propset->aRow[propset->cRows -1].cValues);
			oxcmail_util_parse_address(propset->aRow[propset->cRows -1].lpProps, addr_string, &displayname, &emailaddress);
			set_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[0]), PR_DISPLAY_NAME, displayname);
			set_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[1]), PR_ADDRTYPE, "SMTP");
			set_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[2]), PR_EMAIL_ADDRESS, emailaddress);
			set_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[3]), PR_RECIPIENT_TYPE, &(recipient_type));
			set_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[4]), PR_OBJECT_TYPE, &(obj_type));
			talloc_free(addr_string);
		}
		talloc_free((char*)header_value);
		return true;
	} else {
		return false;
	}
}

bool oxcmail_parse_Cc(TALLOC_CTX *mem_ctx, const char *header, struct SRowSet *propset)
{
	char *addr_string = NULL;
	if (strcasestr(header, "Cc:") == header) {
		const char *header_idx;
		uint32_t recipient_type = MAPI_CC;
		uint32_t obj_type = MAPI_MAILUSER;
		const char *header_value = talloc_strndup(mem_ctx, header + strlen("Cc:"), strlen(header) - strlen("Cc:"));
		header_idx = header_value;
		while (next_token_talloc(mem_ctx, &header_idx, &addr_string, ",")) {
			trim_string(addr_string, " ", " ");
			char *displayname;
			char *emailaddress;
			propset->cRows = propset->cRows + 1;
			propset->aRow = talloc_realloc(NULL, propset->aRow, struct SRow, propset->cRows);
			propset->aRow[propset->cRows -1].cValues = 5;
			propset->aRow[propset->cRows -1].lpProps = talloc_array(propset->aRow, struct SPropValue, propset->aRow[propset->cRows -1].cValues);
			oxcmail_util_parse_address(propset->aRow[propset->cRows -1].lpProps, addr_string, &displayname, &emailaddress);
			set_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[0]), PR_DISPLAY_NAME, displayname);
			set_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[1]), PR_ADDRTYPE, "SMTP");
			set_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[2]), PR_EMAIL_ADDRESS, emailaddress);
			set_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[3]), PR_RECIPIENT_TYPE, &(recipient_type));
			set_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[4]), PR_OBJECT_TYPE, &(obj_type));
			talloc_free(addr_string);
		}
		talloc_free((char*)header_value);
		return true;
	} else {
		return false;
	}
}

bool oxcmail_parse_Bcc(TALLOC_CTX *mem_ctx, const char *header, struct SRowSet *propset)
{
	char *addr_string = NULL;
	if (strcasestr(header, "Bcc:") == header) {
		const char *header_idx;
		uint32_t recipient_type = MAPI_BCC;
		uint32_t obj_type = MAPI_MAILUSER;
		const char *header_value = talloc_strndup(mem_ctx, header + strlen("Bcc:"), strlen(header) - strlen("Bcc:"));
		header_idx = header_value;
		while (next_token_talloc(mem_ctx, &header_idx, &addr_string, ",")) {
			trim_string(addr_string, " ", " ");
			char *displayname;
			char *emailaddress;
			propset->cRows = propset->cRows + 1;
			propset->aRow = talloc_realloc(NULL, propset->aRow, struct SRow, propset->cRows);
			propset->aRow[propset->cRows -1].cValues = 5;
			propset->aRow[propset->cRows -1].lpProps = talloc_array(propset->aRow, struct SPropValue, propset->aRow[propset->cRows -1].cValues);
			oxcmail_util_parse_address(propset->aRow[propset->cRows -1].lpProps, addr_string, &displayname, &emailaddress);
			set_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[0]), PR_DISPLAY_NAME, displayname);
			set_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[1]), PR_ADDRTYPE, "SMTP");
			set_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[2]), PR_EMAIL_ADDRESS, emailaddress);
			set_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[3]), PR_RECIPIENT_TYPE, &(recipient_type));
			set_SPropValue_proptag(&(propset->aRow[propset->cRows -1].lpProps[4]), PR_OBJECT_TYPE, &(obj_type));
			talloc_free(addr_string);
		}
		talloc_free((char*)header_value);
		return true;
	} else {
		return false;
	}
}

/*[[[end]]] (checksum: 263f29a271fec096dba64086048bcb79)*/

/* these are hand written */
bool oxcmail_parse_Subject_custom(TALLOC_CTX *mem_ctx, const char *header_value, struct SRow *properties)
{
	char *strindex;
	struct SPropValue prop;
	set_SPropValue_proptag(&(prop), PR_SUBJECT, talloc_strdup(properties->lpProps, header_value));
	SRow_addprop(properties, prop);
	strindex = strstr(header_value, ": ");
	if (strindex && (strindex != header_value) && (strindex - header_value <= 3)) {
		char *normalized_subject = talloc_strdup(properties->lpProps, strindex + strlen(": "));
		char *subject_prefix = talloc_strndup(properties->lpProps, header_value, (strindex - header_value));
		set_SPropValue_proptag(&prop, PR_NORMALIZED_SUBJECT, normalized_subject);
		SRow_addprop(properties, prop);
		set_SPropValue_proptag(&prop, PR_SUBJECT_PREFIX, subject_prefix);
		SRow_addprop(properties, prop);
	} else {
		set_SPropValue_proptag(&prop, PR_NORMALIZED_SUBJECT, talloc_strdup(properties->lpProps, header_value));
		SRow_addprop(properties, prop);
		set_SPropValue_proptag(&prop, PR_SUBJECT_PREFIX, "");
		SRow_addprop(properties, prop);
	}

	return true;
}

bool oxcmail_parse_Content_Language_custom(TALLOC_CTX *mem_ctx, const char *header_value, struct SRow *properties)
{
	struct SPropValue prop;
	int lcid = mapi_get_lcid_from_locale(header_value);
	if (lcid == 0) {
		return false;
	}
	set_SPropValue_proptag(&(prop), PR_MESSAGE_LOCALE_ID, &lcid);
	SRow_addprop(properties, prop);
	return true;
}

bool oxcmail_parse_mime_info(TALLOC_CTX *mem_ctx, const char *header, struct oxcmail_mime_info *mime_info)
{
	if (strcasestr(header, "MIME-Version:") == header) {
		char * tmp_str;
		tmp_str = talloc_strndup(mem_ctx, header + strlen("MIME-Version:"), strlen(header) - strlen("MIME-Version:"));
		trim_string(tmp_str, " ", " ");
		if (mime_info->mime_version) {
			talloc_free((void*)mime_info->mime_version);
		}
		mime_info->mime_version = talloc_strdup(mime_info, tmp_str);
		talloc_free(tmp_str);
		return true;
	} else if (strcasestr(header, "Content-Type:") == header) {
		char *tmp_str;
		const char *sep_index;
		tmp_str = talloc_strndup(mem_ctx, header + strlen("Content-Type:"), strlen(header) - strlen("Content-Type:"));
		trim_string(tmp_str, " ", " ");
		mime_info->full_content_type = talloc_strdup(mem_ctx, tmp_str);
		sep_index = strchr(tmp_str, ';');
		if (sep_index) {
			char *tmp_content_type_part;

			tmp_content_type_part = talloc_strndup(mem_ctx, tmp_str, sep_index - tmp_str);
			trim_string(tmp_content_type_part, " ", " ");
			mime_info->content_type = talloc_strdup(mime_info, tmp_content_type_part);
			talloc_free(tmp_content_type_part);

			while (next_token_talloc(mem_ctx, &(sep_index), &tmp_content_type_part, ";")) {
				trim_string(tmp_content_type_part, " ", " ");
				if (strcasestr(tmp_content_type_part, "boundary=") == tmp_content_type_part) {
					trim_string(tmp_content_type_part, "boundary=", "\"");
					mime_info->boundary_marker = talloc_strdup(mime_info, tmp_content_type_part);
					talloc_free(tmp_content_type_part);
				} else if (strcasestr(tmp_content_type_part, "charset=") == tmp_content_type_part) {
					trim_string(tmp_content_type_part, "charset=", "\"");
					mime_info->charset = talloc_strdup(talloc_new(mime_info), tmp_content_type_part);
					talloc_free(tmp_content_type_part);
				} else if (strcasestr(tmp_content_type_part, "name=") == tmp_content_type_part) {
					if (!mime_info->attachment_filename) {
						trim_string(tmp_content_type_part, "name=", "\"");
						mime_info->attachment_filename = talloc_strdup(mime_info, tmp_content_type_part);
						talloc_free(tmp_content_type_part);
					}
				} else {
					printf("unhandled parameter in Content-Type: %s\n", tmp_content_type_part);
					talloc_free(tmp_content_type_part);
				}
			}
		} else {
			mime_info->content_type = talloc_strdup(talloc_new(mime_info), tmp_str);
		}
		talloc_free(tmp_str);
		if (!mime_info->mime_version) {
			/* no mime_version yet, assume 1.0 */
			mime_info->mime_version = talloc_strdup(talloc_new(mime_info), "1.0");
		}
		return true;
	} else if (strcasestr(header, "Content-Transfer-Encoding") == header) {
		char *tmp_str;
		tmp_str = talloc_strndup(mem_ctx, header + strlen("Content-Transfer-Encoding:"), strlen(header) - strlen("Content-Transfer-Encoding:"));
		trim_string(tmp_str, " ", " ");
		trim_string(tmp_str, "\t", "\n");
		mime_info->transfer_encoding = talloc_strdup(talloc_new(mime_info), tmp_str);
		talloc_free(tmp_str);
		return true;
	} else if (strcasestr(header, "Content-Disposition") == header) {
		char *tmp_str;
		const char *sep_index;
		tmp_str = talloc_strndup(mem_ctx, header + strlen("Content-Disposition:"), strlen(header) - strlen("Content-Disposition:"));
		trim_string(tmp_str, " ", " ");
		sep_index = strchr(tmp_str, ';');
		if (sep_index) {
			char *tmp_content_disposition_part;

			tmp_content_disposition_part = talloc_strndup(mem_ctx, tmp_str, sep_index - tmp_str);
			trim_string(tmp_content_disposition_part, " ", " ");
			if (strcasecmp(tmp_content_disposition_part, "attachment") == 0) {
				mime_info->is_attachment = true;
			} else {
				printf("main part of content disposition: %s\n", tmp_content_disposition_part);
			}
			talloc_free(tmp_content_disposition_part);

			while (next_token_talloc(mem_ctx, &(sep_index), &tmp_content_disposition_part, ";")) {
				trim_string(tmp_content_disposition_part, " ", " ");
				if (strcasestr(tmp_content_disposition_part, "filename=") == tmp_content_disposition_part) {
					trim_string(tmp_content_disposition_part, "filename=", "\"");
					if (mime_info->attachment_filename) {
						talloc_free((void*)mime_info->attachment_filename);
					}
					mime_info->attachment_filename = talloc_strdup(mime_info, tmp_content_disposition_part);
					mime_info->is_attachment = true;
					talloc_free(tmp_content_disposition_part);
				} else {
					printf("unhandled parameter in Content-Disposition: %s\n", tmp_content_disposition_part);
					talloc_free(tmp_content_disposition_part);
				}
			}
		} else {
			printf("did not properly handle Content-Disposition header: %s\n", tmp_str);
		}
		talloc_free(tmp_str);
		return true;  
	}
	printf("unhandled mime header entry in oxcmail_parse_mime_info(): %s!\n", header);
	return false;
}

/*[[[cog
from oxcmail_mappings import customconverts

for mimeheader,customfunction in customconverts:
    cog.outl('bool oxcmail_parse_%s(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)' % legitimise_function_name(mimeheader))
    cog.outl('{')
    cog.outl('\tif (strcasestr(header, "%s:") == header) {' % mimeheader)
    cog.outl('\t\tchar *header_value = talloc_strndup(mem_ctx, header + strlen("%s:"), strlen(header) - strlen("%s:"));' % (mimeheader, mimeheader));
    cog.outl('\t\ttrim_string(header_value, " ", " ");')
    cog.outl('\t\ttrim_string(header_value, "\\t", "\\n");')
    cog.outl('\t\tif (%s_custom(mem_ctx, header_value, properties)) {' % customfunction)
    cog.outl('\t\t\ttalloc_free(header_value);')
    cog.outl('\t\t\treturn true;')
    cog.outl('\t\t} else {')
    cog.outl('\t\t\ttalloc_free(header_value);')
    cog.outl('\t\t\treturn false;')
    cog.outl('\t\t}')
    cog.outl('\t} else {')
    cog.outl('\t\treturn false;')
    cog.outl('\t}')
    cog.outl('}')
    cog.outl('')

]]]*/
bool oxcmail_parse_Subject(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	if (strcasestr(header, "Subject:") == header) {
		char *header_value = talloc_strndup(mem_ctx, header + strlen("Subject:"), strlen(header) - strlen("Subject:"));
		trim_string(header_value, " ", " ");
		trim_string(header_value, "\t", "\n");
		if (oxcmail_parse_Subject_custom(mem_ctx, header_value, properties)) {
			talloc_free(header_value);
			return true;
		} else {
			talloc_free(header_value);
			return false;
		}
	} else {
		return false;
	}
}

bool oxcmail_parse_Content_Language(TALLOC_CTX *mem_ctx, const char *header, struct SRow *properties)
{
	if (strcasestr(header, "Content-Language:") == header) {
		char *header_value = talloc_strndup(mem_ctx, header + strlen("Content-Language:"), strlen(header) - strlen("Content-Language:"));
		trim_string(header_value, " ", " ");
		trim_string(header_value, "\t", "\n");
		if (oxcmail_parse_Content_Language_custom(mem_ctx, header_value, properties)) {
			talloc_free(header_value);
			return true;
		} else {
			talloc_free(header_value);
			return false;
		}
	} else {
		return false;
	}
}

/*[[[end]]] (checksum: 4bf4a4c47db9f6afdde35b30e6e47055) */

/*[[[cog
def add_parse_case(functionname):
    cog.outl('\t} else if (oxcmail_parse_%s(mem_ctx, header_line, properties)) {' % legitimise_function_name(functionname))
    cog.outl('\t\treturn true;')

def add_parse_case_recipients(functionname):
    cog.outl('\t} else if (oxcmail_parse_%s(mem_ctx, header_line, recipients)) {' % legitimise_function_name(functionname))
    cog.outl('\t\treturn true;')

cog.outl('bool oxcmail_parse_line(TALLOC_CTX *mem_ctx, const char *header_line, struct SRow *properties, struct SRowSet *recipients, struct oxcmail_mime_info *mime_info)')
cog.outl('{')
cog.outl('\tif (strlen(header_line) == 0) {')
cog.outl('\t\treturn false;')
for mimeheader,proptagname in directstringconverts:
    add_parse_case(mimeheader)
for mimeheader,proptagname,enum_mapping in enumconverts:
    add_parse_case(mimeheader)
for mimeheader,proptagname in dateconverts:
    add_parse_case(mimeheader)
for mimeheader,proptagname in binaryconverts:
    add_parse_case(mimeheader)
for mimeheader,displayname,email_address,address_type,entry_id in addressconverts:
    add_parse_case(mimeheader)
for mimeheader,recipienttype in addresslistconverts:
    add_parse_case_recipients(mimeheader)
for mimeheader,customfunction in customconverts:
    add_parse_case(mimeheader)
cog.outl('\t} else if (oxcmail_parse_mime_info(mem_ctx, header_line, mime_info)) {')
cog.outl('\t\treturn true;')
cog.outl('\t} else {')
cog.outl('\t\tprintf("%s did not match anything we handle yet\\n", header_line);')
cog.outl('\t}')
cog.outl('\treturn false;')
cog.outl('}')
cog.outl('')

]]]*/
bool oxcmail_parse_line(TALLOC_CTX *mem_ctx, const char *header_line, struct SRow *properties, struct SRowSet *recipients, struct oxcmail_mime_info *mime_info)
{
	if (strlen(header_line) == 0) {
		return false;
	} else if (oxcmail_parse_Thread_Topic(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_List_Help(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_List_Subscribe(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_List_Unsubscribe(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_Message_ID(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_In_Reply_To(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_References(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_Importance(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_Sensitivity(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_Date(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_Reply_By(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_Expires(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_Thread_Index(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_From(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_Sender(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_To(mem_ctx, header_line, recipients)) {
		return true;
	} else if (oxcmail_parse_Cc(mem_ctx, header_line, recipients)) {
		return true;
	} else if (oxcmail_parse_Bcc(mem_ctx, header_line, recipients)) {
		return true;
	} else if (oxcmail_parse_Subject(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_Content_Language(mem_ctx, header_line, properties)) {
		return true;
	} else if (oxcmail_parse_mime_info(mem_ctx, header_line, mime_info)) {
		return true;
	} else {
		printf("%s did not match anything we handle yet\n", header_line);
	}
	return false;
}

/*[[[end]]] (checksum: 37a8c838e884d66b7ab2e543696a044f)*/

static void oxcmail_parse_as_plain_text_body(TALLOC_CTX *mem_ctx, DATA_BLOB bodytext, struct SRow *properties)
{
	struct SPropValue	body;
	uint32_t		editor_format = EDITOR_FORMAT_PLAINTEXT;
	struct SPropValue	format;

	set_SPropValue_proptag(&(body), PR_BODY, talloc_strdup(properties->lpProps, (const char*)bodytext.data));
	SRow_addprop(properties, body);

	set_SPropValue_proptag(&(format), PR_MSG_EDITOR_FORMAT, &editor_format);
	SRow_addprop(properties, format);
}

static void oxcmail_parse_as_html_text_body(TALLOC_CTX *mem_ctx, DATA_BLOB bodytext, struct SRow *properties)
{
	struct SPropValue	body;
	uint32_t		editor_format = EDITOR_FORMAT_HTML;
	struct SPropValue	format;

	set_SPropValue_proptag(&(body), PR_BODY_HTML, talloc_strdup(properties->lpProps, (const char *)bodytext.data));
	SRow_addprop(properties, body);

	set_SPropValue_proptag(&(format), PR_MSG_EDITOR_FORMAT, &editor_format);
	SRow_addprop(properties, format);
}

struct mime_element {
	struct mime_element		*prev;
	struct mime_element		*next;
	struct oxcmail_mime_info	*mime_info;
	DATA_BLOB			payload;
};

static void oxcmail_parse_mime_headers(TALLOC_CTX *mem_ctx, FILE *fp, struct oxcmail_mime_info *mime_info)
{
	ssize_t		read = 0;
	char		*line = NULL;
	char		*tmpline = NULL;
	size_t		len = 0;

	while ((read = getline(&tmpline, &len, fp)) != -1) {
		if (read > 1) {
			if (! isspace(tmpline[0])) {
				/* then this is not a continuation line */
				if (line) {
					/* parse what we've previously accumulated */
					oxcmail_parse_mime_info(mem_ctx, line, mime_info);
					talloc_free(line);
					line = NULL;
				}
				/* create a new line */
				trim_string(tmpline, "", "\n");
				line = talloc_strndup(mem_ctx, tmpline, strlen(tmpline));
			} else {
				/* this is a continuation, so add to previous line */
				if (line) {
					line = talloc_strdup_append(line, " ");
					trim_string(tmpline, "\t", "\n");
					trim_string(tmpline, " ", " " );
					line = talloc_strndup_append(line, tmpline, strlen(tmpline));
				}
			}
		} else {
			/* empty, or just a end-of-line marker, parse what we have left ... */
			oxcmail_parse_mime_info(mem_ctx, line, mime_info);
			talloc_free(line);
			line = NULL;
			/* ... and finish up */
			break;
		}
	}

	if (tmpline) {
	    free(tmpline);
	}
}

static void read_remaining_lines(TALLOC_CTX *mem_ctx, FILE *fp, DATA_BLOB *bodytext, bool merge_lines)
{
	char				*tmpline = NULL;
	size_t				len = 0;
	ssize_t				read;
	uint32_t			bodytext_idx = 0;

	bodytext->length = 4096; /* just a reasonable starting value */
	bodytext->data = talloc_array(mem_ctx, uint8_t, bodytext->length);
	while ((read = getline(&tmpline, &len, fp)) != -1) {
		if (bodytext_idx + read >= bodytext->length) {
			/* there won't be enough space */
			bodytext->length = bodytext->length * 2;
			bodytext->data = talloc_realloc(NULL, bodytext->data, uint8_t, bodytext->length);
		}
		if (merge_lines) {
			memcpy(bodytext->data + bodytext_idx, tmpline, read - 1);
			bodytext_idx = bodytext_idx + read - 1;
		} else {
			memcpy(bodytext->data + bodytext_idx, tmpline, read);
			bodytext_idx += read;
		}
	}
	if (tmpline) {
	    free(tmpline);
	}

	bodytext->length = bodytext_idx + 1;
	bodytext->data = talloc_realloc(NULL, bodytext->data, uint8_t, bodytext->length);
	bodytext->data[bodytext_idx] = '\0';
}

static void oxcmail_parse_multipart_element(TALLOC_CTX *mem_ctx, char *element, struct mime_element *result)
{
	FILE 				*fp;
	
	result->mime_info = oxcmail_mime_info_init(talloc_new(mem_ctx));

	fp = fmemopen(element, strlen(element), "r");
	if (!fp) {
		printf("failed to fmemopen\n");
		return;
	}
	
	oxcmail_parse_mime_headers(mem_ctx, fp, result->mime_info);

	/* the headers are done, time to get the payload */
	if (strcasecmp(result->mime_info->transfer_encoding, "base64") == 0) {
		read_remaining_lines(mem_ctx, fp, &(result->payload), true);
		result->payload.length = ldb_base64_decode((char*)result->payload.data);
		if (result->payload.length == -1) {
			result->payload.length = 0;
			talloc_free(result->payload.data);
		}
	} else {
		/* assume printable */
		read_remaining_lines(mem_ctx, fp, &(result->payload), false);
	}

	fclose(fp);
}

static void oxcmail_parse_as_ical(TALLOC_CTX *mem_ctx, DATA_BLOB ical_string, struct SRow *properties)
{
	icalcomponent		*ical;
	icalcomponent		*vevent;

	ical = icalparser_parse_string((const char*)ical_string.data);
	icalcomponent_strip_errors(ical);
	/* assume that there is only one vevent */
	if ((vevent = icalcomponent_get_first_component(ical, ICAL_VEVENT_COMPONENT)) != NULL) {
		int i = 0;
		struct ical2exchange ical2exchange;
		ical2exchange_init(&ical2exchange, talloc_new(properties->lpProps));
		ical2exchange_get_properties(&ical2exchange, vevent);
		ical2exchange_convert_event(&ical2exchange);
		for (i = 0; i < ical2exchange.cValues; ++i) {
			SRow_addprop(properties, ical2exchange.lpProps[i]);
		}
		ical2exchange_reset(&ical2exchange);
	}
	icalcomponent_free(ical);

}

static char * sanitise_filename(TALLOC_CTX *mem_ctx, const char *filename)
{
	char *sanitised_filename = talloc_strdup(mem_ctx, filename);
	size_t filename_idx;
	for (filename_idx = 0; sanitised_filename[filename_idx] != '\0'; /* increment done in loop */) {
		if (iscntrl(sanitised_filename[filename_idx]) ||
	            (sanitised_filename[filename_idx] == '"') ||
	            (sanitised_filename[filename_idx] == '/') ||
	            (sanitised_filename[filename_idx] == ':') ||
	            (sanitised_filename[filename_idx] == '<') ||
	            (sanitised_filename[filename_idx] == '>') ||
	            (sanitised_filename[filename_idx] == '|') ||
	            (sanitised_filename[filename_idx] == '\\')) {
			/* move the following characters down one position */
			int i;
			for (i = filename_idx; i < strlen(sanitised_filename); ++i) {
				sanitised_filename[i] = sanitised_filename[i+1]; 
			}
		} else {
			++filename_idx;
		}
	}
	for (filename_idx = 0; sanitised_filename[filename_idx] != '\0'; ++filename_idx) {
		if (isspace(sanitised_filename[filename_idx])) {
			sanitised_filename[filename_idx] = ' '; 
		}
	}
	return sanitised_filename;
}

static void oxcmail_parse_as_multipart(TALLOC_CTX *mem_ctx, DATA_BLOB bodytext, struct oxcmail_mime_info *mime_info, struct SRow *properties, struct SRowSet *attachments)
{
	char			*sep;
	char 			*element_string;
	char 			*sep_index;
	const char 		*search_start = (const char*)bodytext.data;
	struct mime_element	*mime_elements; /* a double linked list of the various alternative parts */
	struct mime_element	*element_iterator;

	if (mime_info->boundary_marker == NULL) {
		/* we have no chance of parsing this */
		return;
	}
	sep = talloc_strdup(mem_ctx, "--");
	sep = talloc_strdup_append(sep, mime_info->boundary_marker);
	mime_elements = talloc_zero(mem_ctx, struct mime_element);

	while ((sep_index = strstr(search_start, sep)) != NULL) {
		struct mime_element *mime_element; /* the data for one alternative part */
		if (sep_index == search_start) {
			/* found separator at the start of the search */
			search_start += strlen(sep);
			continue;
		}
		mime_element = talloc_zero(mime_elements, struct mime_element);
		element_string = talloc_strndup(mem_ctx, search_start, sep_index - search_start);
		trim_string(element_string, "\n", "\n");
		oxcmail_parse_multipart_element(mime_elements, element_string, mime_element);
		DLIST_ADD_END(mime_elements, mime_element, struct mime_element);
		search_start = sep_index;
		talloc_free(element_string);
	}
	talloc_free(sep);

	for (element_iterator = mime_elements; element_iterator->next; element_iterator = element_iterator->next) {
		if (strcasecmp(element_iterator->mime_info->content_type, "text/plain") == 0) {
			oxcmail_parse_as_plain_text_body(mem_ctx, element_iterator->payload, properties);
		} else if (strcasecmp(element_iterator->mime_info->content_type, "text/HTML") == 0) {
			oxcmail_parse_as_html_text_body(mem_ctx, element_iterator->payload, properties);
		} else if (strcasecmp(element_iterator->mime_info->content_type, "text/calendar") == 0) {
			oxcmail_parse_as_ical(mem_ctx, element_iterator->payload, properties);
		} else {
			/* treat this element as an attachment */
			struct Binary_r *binary_data;
			char *filename = NULL;
			char *longfilename = NULL;
			char *displayname = NULL;
			char *attachment_extension = NULL;
			attachments->cRows = attachments->cRows + 1;
			attachments->aRow = talloc_realloc(NULL, attachments->aRow, struct SRow, attachments->cRows);
			attachments->aRow[attachments->cRows -1].cValues = 9;
			attachments->aRow[attachments->cRows -1].lpProps = talloc_array(attachments->aRow, struct SPropValue, attachments->aRow[attachments->cRows -1].cValues);
			attachments->aRow[attachments->cRows -1].lpProps[0].ulPropTag = PR_ATTACH_METHOD;
			attachments->aRow[attachments->cRows -1].lpProps[0].dwAlignPad = 0;
			attachments->aRow[attachments->cRows -1].lpProps[0].value.l = ATTACH_BY_VALUE;
			attachments->aRow[attachments->cRows -1].lpProps[1].ulPropTag = PR_RENDERING_POSITION;
			attachments->aRow[attachments->cRows -1].lpProps[1].dwAlignPad = 0;
			attachments->aRow[attachments->cRows -1].lpProps[1].value.l = 0;
			if (element_iterator->mime_info->attachment_filename) {
				filename = sanitise_filename(attachments->aRow[attachments->cRows -1].lpProps, element_iterator->mime_info->attachment_filename);
			}
			if (!filename || (strlen(filename) == 0)) {
				filename = talloc_strdup(attachments->aRow[attachments->cRows -1].lpProps, "NONAME"); /* good enough for Exchange 2003... */
				longfilename = talloc_strdup(attachments->aRow[attachments->cRows -1].lpProps, "NONAME");
				attachment_extension = talloc_strdup(attachments->aRow[attachments->cRows -1].lpProps, "");
			} else {
				char *last_dot_offset = rindex(filename, '.');
				if (last_dot_offset) {
					/* we have a name.ext type string */
					attachment_extension = talloc_strdup(attachments->aRow[attachments->cRows -1].lpProps, last_dot_offset + 1);
				} else {
					attachment_extension = talloc_strdup(attachments->aRow[attachments->cRows -1].lpProps, "");
				}
				longfilename = talloc_strdup(attachments->aRow[attachments->cRows -1].lpProps, filename);
			}
			displayname = talloc_strdup(attachments->aRow[attachments->cRows -1].lpProps, longfilename);

			set_SPropValue_proptag(&(attachments->aRow[attachments->cRows -1].lpProps[2]), PR_ATTACH_FILENAME,
					       talloc_strdup(attachments->aRow[attachments->cRows -1].lpProps, filename));
			set_SPropValue_proptag(&(attachments->aRow[attachments->cRows -1].lpProps[3]), PR_ATTACH_CONTENT_ID, 
					       talloc_strdup(attachments->aRow[attachments->cRows -1].lpProps, filename));
			binary_data = talloc_zero(attachments->aRow[attachments->cRows -1].lpProps, struct Binary_r);
			binary_data->cb = element_iterator->payload.length;
			binary_data->lpb = talloc_memdup(binary_data, element_iterator->payload.data, element_iterator->payload.length);
			set_SPropValue_proptag(&(attachments->aRow[attachments->cRows -1].lpProps[4]), PR_ATTACH_DATA_BIN, binary_data);
			set_SPropValue_proptag(&(attachments->aRow[attachments->cRows -1].lpProps[5]), PR_ATTACH_MIME_TAG,
					       talloc_strdup(attachments->aRow[attachments->cRows -1].lpProps, element_iterator->mime_info->full_content_type));
			set_SPropValue_proptag(&(attachments->aRow[attachments->cRows -1].lpProps[6]), PR_ATTACH_LONG_FILENAME, longfilename);
			set_SPropValue_proptag(&(attachments->aRow[attachments->cRows -1].lpProps[7]), PR_ATTACH_EXTENSION, attachment_extension);
			set_SPropValue_proptag(&(attachments->aRow[attachments->cRows -1].lpProps[8]), PR_DISPLAY_NAME, displayname);
			if (element_iterator->mime_info->charset && (strncasecmp(element_iterator->mime_info->content_type, "text/", strlen("text/")) == 0)) {
				/* TODO: add this when we support PidTagTextAttachmentCharset */
				/*
				attachments->aRow[attachments->cRows -1].cValues += 1;
				attachments->aRow[attachments->cRows -1].lpProps = talloc_array(attachments->aRow, struct SPropValue, attachments->aRow[attachments->cRows -1].cValues);
				set_SPropValue_proptag(&(attachments->aRow[attachments->cRows -1].lpProps[attachments->aRow[attachments->cRows -1].cValues - 1]), PidTagTextAttachmentCharset, 
						       talloc_strdup(attachments->aRow[attachments->cRows -1].lpProps, element_iterator->mime_info->charset));
				*/
			}
		}
		talloc_free(element_iterator->payload.data);
		talloc_free(element_iterator->mime_info);
		talloc_free(element_iterator->prev);
	}
	talloc_free(element_iterator);
}

_PUBLIC_ bool oxcmail_parse_file(TALLOC_CTX *mem_ctx, FILE *fp, struct SRow *properties, struct SRowSet *recipients, struct SRowSet *attachments)
{
	char				*tmpline = NULL;
	char				*line = NULL;
	size_t				len = 0;
	ssize_t				read;
	struct oxcmail_mime_info	*mime_info;
	DATA_BLOB			bodytext;


	properties->cValues = 0;
	properties->lpProps = talloc_array(mem_ctx, struct SPropValue, properties->cValues);
	
	recipients->cRows = 0;
	recipients->aRow = talloc_array(mem_ctx, struct SRow, recipients->cRows);

	attachments->cRows = 0;
	attachments->aRow = talloc_array(mem_ctx, struct SRow, attachments->cRows);

	mime_info = oxcmail_mime_info_init(mem_ctx);
	
	while ((read = getline(&tmpline, &len, fp)) != -1) {
		if (read > 1) {
			if (! isspace(tmpline[0])) {
				/* then this is not a continuation line */
				if (line) {
					/* parse what we've previously accumulated */
					oxcmail_parse_line(mem_ctx, line, properties, recipients, mime_info);
					talloc_free(line);
					line = NULL;
				}
				/* create a new line */
				trim_string(tmpline, "", "\n");
				line = talloc_strndup(mem_ctx, tmpline, strlen(tmpline));
			} else {
				/* this is a continuation, so add to previous line */
				if (line) {
					line = talloc_strdup_append(line, " ");
					trim_string(tmpline, "\t", "\n");
					trim_string(tmpline, " ", " " );
					line = talloc_strndup_append(line, tmpline, strlen(tmpline));
				}
			}
		} else {
			/* empty, or just a end-of-line marker, parse what we have left ... */
			oxcmail_parse_line(mem_ctx, line, properties, recipients, mime_info);
			talloc_free(line);
			line = NULL;
			/* ... and finish up */
			break;
		}
	}
	if (tmpline) {
		free(tmpline);
	}

	read_remaining_lines(mem_ctx, fp, &bodytext, false);

	if ((mime_info->mime_version == NULL) || (mime_info->content_type == NULL)) {
		/* not really MIME, assume its all plain text body for now */
		oxcmail_parse_as_plain_text_body(mem_ctx, bodytext, properties);
	} else if (strcasecmp(mime_info->content_type, "text/plain") == 0) {
		oxcmail_parse_as_plain_text_body(mem_ctx, bodytext, properties);
	} else if (strcasecmp(mime_info->content_type, "text/HTML") == 0) {
		oxcmail_parse_as_html_text_body(mem_ctx, bodytext, properties);
	} else if (strcasecmp(mime_info->content_type, "multipart/alternative") == 0) {
		oxcmail_parse_as_multipart(mem_ctx, bodytext, mime_info, properties, attachments);
	} else if (strcasecmp(mime_info->content_type, "multipart/mixed" ) == 0) {
		oxcmail_parse_as_multipart(mem_ctx, bodytext, mime_info, properties, attachments);
	} else {
		printf("TODO: unhandled mime type:\n");
		oxcmail_mime_info_dump(mime_info, "\t");
	}

	talloc_free(bodytext.data);
	talloc_free(mime_info);

	return true;
}
