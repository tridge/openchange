/*
   OpenChange MIME mail <-> Email object conversion library.

   Copyright (C) Brad Hards <bradh@frogmouth.net> 2010.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "liboxcmail/oxcmail_parser.h"
#include "liboxcmail/oxcmail_public_proto.h"
#include <talloc.h>
#include <samba/popt.h>
#include <popt.h>

#define DEFAULT_PROFDB        "%s/.openchange/profiles.ldb"

static void popt_openchange_version_callback(poptContext con,
					     enum poptCallbackReason reason,
					     const struct poptOption *opt,
					     const char *arg,
					     const void *data)
{
	switch (opt->val) {
	case 'V':
		printf("Version %s\n", OPENCHANGE_VERSION_STRING);
		exit (0);
	}
}

struct poptOption popt_openchange_version[] = {
	{ NULL, '\0', POPT_ARG_CALLBACK, (void *)popt_openchange_version_callback, '\0', NULL, NULL },
	{ "version", 'V', POPT_ARG_NONE, NULL, 'V', "Print version ", NULL },
	POPT_TABLEEND
};

#define	POPT_OPENCHANGE_VERSION { NULL, 0, POPT_ARG_INCLUDE_TABLE, popt_openchange_version, 0, "Common openchange options:", NULL },

static void dump_file(TALLOC_CTX *mem_ctx, const char* filename)
{
	FILE				*fp;
	struct SRow			properties;
	struct SRowSet			recipients;
	struct SRowSet			attachments;

	fp = fopen(filename, "r");
	if (fp == NULL) {
		printf("could not open %s\n", filename);
		return;
	}

	oxcmail_parse_file(mem_ctx, fp, &properties, &recipients, &attachments);

	printf("returned props for %s:\n", filename);
	printf("    Recipients:\n");
	mapidump_SRowSet(&recipients, "\t");
	talloc_free(recipients.aRow);
	printf("    Other properties:\n");
	mapidump_SRow(&properties, "\t");
	talloc_free(properties.lpProps);
	if (attachments.cRows > 0) {
		printf("    Attachments:\n");
		mapidump_SRowSet(&attachments, "\t");
		talloc_free(attachments.aRow);
	}

	fclose(fp);
	return;
}

static void remove_prohibited_props(TALLOC_CTX *mem_ctx, struct SRow *properties, struct SRow *cleaned_up_props)
{
	struct SPropTagArray	*prohibited_prop_tags;
	int			i = 0;

	prohibited_prop_tags = set_SPropTagArray(mem_ctx, 0x03,
						 PR_SENT_REPRESENTING_NAME,
						 PR_SENT_REPRESENTING_ADDRTYPE,
						 PR_SENT_REPRESENTING_EMAIL_ADDRESS);

	cleaned_up_props->cValues = 0;
	cleaned_up_props->lpProps = talloc_array(mem_ctx, struct SPropValue, cleaned_up_props->cValues);
	
	for (i = 0; i < properties->cValues; ++i) {
		int j;
		bool prop_is_ok = true;
		for (j = 0; j < prohibited_prop_tags->cValues; ++j) {
			if (properties->lpProps[i].ulPropTag == prohibited_prop_tags->aulPropTag[j]) {
				prop_is_ok = false;
			}
		}
		if (prop_is_ok) {
			SRow_addprop(cleaned_up_props, properties->lpProps[i]);
		}
	}
	talloc_free(prohibited_prop_tags->aulPropTag);
	MAPIFreeBuffer(prohibited_prop_tags);
}

static bool set_external_recipients(TALLOC_CTX *mem_ctx, struct SRowSet *SRowSet, const char *username, enum ulRecipClass RecipClass)
{
	uint32_t		last;
	struct SPropValue	SPropValue;

	SRowSet->aRow = talloc_realloc(mem_ctx, SRowSet->aRow, struct SRow, SRowSet->cRows + 1);
	last = SRowSet->cRows;
	SRowSet->aRow[last].cValues = 0;
	SRowSet->aRow[last].lpProps = talloc_zero(mem_ctx, struct SPropValue);
	
	/* PR_OBJECT_TYPE */
	SPropValue.ulPropTag = PR_OBJECT_TYPE;
	SPropValue.value.l = MAPI_MAILUSER;
	SRow_addprop(&(SRowSet->aRow[last]), SPropValue);

	/* PR_DISPLAY_TYPE */
	SPropValue.ulPropTag = PR_DISPLAY_TYPE;
	SPropValue.value.l = 0;
	SRow_addprop(&(SRowSet->aRow[last]), SPropValue);

	/* PR_GIVEN_NAME */
	SPropValue.ulPropTag = PR_GIVEN_NAME_UNICODE;
	SPropValue.value.lpszW = username;
	SRow_addprop(&(SRowSet->aRow[last]), SPropValue);

	/* PR_DISPLAY_NAME */
	SPropValue.ulPropTag = PR_DISPLAY_NAME_UNICODE;
	SPropValue.value.lpszW = username;
	SRow_addprop(&(SRowSet->aRow[last]), SPropValue);

	/* PR_7BIT_DISPLAY_NAME */
	SPropValue.ulPropTag = PR_7BIT_DISPLAY_NAME_UNICODE;
	SPropValue.value.lpszW = username;
	SRow_addprop(&(SRowSet->aRow[last]), SPropValue);

	/* PR_SMTP_ADDRESS */
	SPropValue.ulPropTag = PR_SMTP_ADDRESS_UNICODE;
	SPropValue.value.lpszW = username;
	SRow_addprop(&(SRowSet->aRow[last]), SPropValue);

	/* PR_ADDRTYPE */
	SPropValue.ulPropTag = PR_ADDRTYPE_UNICODE;
	SPropValue.value.lpszW = "SMTP";
	SRow_addprop(&(SRowSet->aRow[last]), SPropValue);

	SetRecipientType(&(SRowSet->aRow[last]), RecipClass);

	SRowSet->cRows += 1;
	return true;
}

#define MAX_READ_SIZE 4096

static enum MAPISTATUS upload_file(TALLOC_CTX *mem_ctx, const char* filename, mapi_object_t *obj_outbox)
{
	FILE				*fp;
	struct SRow			properties;
	struct SRow			cleaned_up_props;
	struct SRowSet			recipients;
	enum MAPISTATUS			retval = MAPI_E_SUCCESS;
	mapi_object_t			obj_message;
	uint32_t			msg_flags = MSGFLAG_UNSENT;
	struct SPropValue		msg_flags_prop;
	struct SPropTagArray		*proptags;
	struct SRowSet			attachments;
	int				attachment_idx;

	fp = fopen(filename, "r");
	if (fp == NULL) {
		printf("could not open %s\n", filename);
		return MAPI_E_NO_ACCESS;
	}

	mapi_object_init(&obj_message);

	if (oxcmail_parse_file(mem_ctx, fp, &properties, &recipients, &attachments)) {
		struct SRowSet			*resolved_recipients;
		struct SPropValue		encoding;
		int 				i;
		int				counter = 0;
		const char			**usernames;
		struct PropertyTagArray_r	*flaglist = NULL;

		retval = CreateMessage(obj_outbox, &obj_message);
		if (retval != MAPI_E_SUCCESS) {
			goto cleanup;
		}

		proptags = set_SPropTagArray(mem_ctx, 0xA,
					     PR_ENTRYID,
					     PR_DISPLAY_NAME_UNICODE,
					     PR_OBJECT_TYPE,
					     PR_DISPLAY_TYPE,
					     PR_TRANSMITTABLE_DISPLAY_NAME_UNICODE,
					     PR_EMAIL_ADDRESS_UNICODE,
					     PR_ADDRTYPE_UNICODE,
					     PR_SEND_RICH_INFO,
					     PR_7BIT_DISPLAY_NAME_UNICODE,
					     PR_SMTP_ADDRESS_UNICODE);

		usernames = talloc_array(mem_ctx, const char *, recipients.cRows + 1);
		for (i = 0; i < recipients.cRows; ++i) {
			usernames[i] = (const char*)find_SPropValue_data(&(recipients.aRow[i]), PR_DISPLAY_NAME);
		}
		usernames[recipients.cRows] = 0;

		ResolveNames(mapi_object_get_session(obj_outbox), usernames, proptags, &resolved_recipients, &flaglist, MAPI_UNICODE);
		if (retval != MAPI_E_SUCCESS) {
			mapi_errstr("ResolveNames", retval);
			goto cleanup;
		}
		talloc_free(proptags->aulPropTag);
		MAPIFreeBuffer(proptags);

		if (!resolved_recipients) {
			resolved_recipients = talloc(mem_ctx, struct SRowSet);
			resolved_recipients->cRows = 0;
			resolved_recipients->aRow = talloc_array(resolved_recipients, struct SRow, resolved_recipients->cRows);
		}
		for (i = 0; usernames[i]; ++i) {
			if (flaglist->aulPropTag[i] == MAPI_UNRESOLVED) {
				set_external_recipients(resolved_recipients, resolved_recipients, usernames[i], *((uint32_t*)find_SPropValue_data(&(recipients.aRow[i]), PR_RECIPIENT_TYPE)));
			}
			if (flaglist->aulPropTag[i] == MAPI_RESOLVED) {
				SetRecipientType(&(resolved_recipients->aRow[counter]), *((uint32_t*)find_SPropValue_data(&(recipients.aRow[i]), PR_RECIPIENT_TYPE)));
				counter++;
			}
		}

		encoding.ulPropTag = PR_SEND_INTERNET_ENCODING;
		encoding.value.l = 1;
		SRowSet_propcpy(mem_ctx, resolved_recipients, encoding);

		retval = ModifyRecipients(&obj_message, resolved_recipients);
		if (retval != MAPI_E_SUCCESS) {
			mapi_errstr("ModifyRecipients", retval);
			goto cleanup;
		}

		talloc_free(resolved_recipients);
		talloc_free(recipients.aRow);
		talloc_free(usernames);

		set_SPropValue_proptag(&(msg_flags_prop), PR_MESSAGE_FLAGS, &msg_flags);
		SRow_addprop(&properties, msg_flags_prop);

		remove_prohibited_props(mem_ctx, &properties, &cleaned_up_props);
		retval = SetProps(&obj_message, cleaned_up_props.lpProps, cleaned_up_props.cValues);
		talloc_free(properties.lpProps);
		talloc_free(cleaned_up_props.lpProps);
		if (retval != MAPI_E_SUCCESS) {
			goto cleanup;
		}

		for (attachment_idx = 0; attachment_idx < attachments.cRows; ++attachment_idx) {
			mapi_object_t attach_obj;
			uint32_t	prop_idx;

			mapi_object_init(&attach_obj);
			retval = CreateAttach(&obj_message, &attach_obj);
			if (retval != MAPI_E_SUCCESS) {
				mapi_errstr("CreateAttach", retval);
				goto cleanup;
			}
			for (prop_idx = 0; prop_idx < attachments.aRow[attachment_idx].cValues; ++prop_idx) {
				if (attachments.aRow[attachment_idx].lpProps[prop_idx].ulPropTag == PR_ATTACH_DATA_BIN) {
					mapi_object_t	obj_stream;
					uint32_t	offset = 0;
					DATA_BLOB	data_for_this_write;
					uint16_t	read_size = 0;

					mapi_object_init(&obj_stream);
					/* use a stream */
					retval = OpenStream(&attach_obj, PR_ATTACH_DATA_BIN, OpenStream_Create, &obj_stream);
					if (retval != MAPI_E_SUCCESS) {
						mapi_object_release(&attach_obj);
						mapi_errstr("OpenStream", retval);
						goto cleanup;
					}
					do {
						if ((offset + MAX_READ_SIZE) < attachments.aRow[attachment_idx].lpProps[prop_idx].value.bin.cb) {
							data_for_this_write.length = MAX_READ_SIZE;
						} else {
							data_for_this_write.length = attachments.aRow[attachment_idx].lpProps[prop_idx].value.bin.cb - offset;
						}
						data_for_this_write.data = attachments.aRow[attachment_idx].lpProps[prop_idx].value.bin.lpb + offset;
						retval = WriteStream(&obj_stream, &data_for_this_write, &read_size);
						if (retval != MAPI_E_SUCCESS) {
							mapi_object_release(&obj_stream);
							mapi_object_release(&attach_obj);
							mapi_errstr("WriteStream", retval);
							goto cleanup;
						}
						if (read_size == 0) {
							/* all done */
							break;
						}
						offset += read_size;
					} while (offset < attachments.aRow[attachment_idx].lpProps[prop_idx].value.bin.cb);
				} else {
					retval = SetProps(&attach_obj, &(attachments.aRow[attachment_idx].lpProps[prop_idx]), 1);
					if (retval != MAPI_E_SUCCESS) {
						mapi_object_release(&attach_obj);
						mapi_errstr("SetProps", retval);
						goto cleanup;
					}
				}
			}
			retval = SaveChangesAttachment(&obj_message, &attach_obj, KeepOpenReadWrite);
			mapi_object_release(&attach_obj);
			if (retval != MAPI_E_SUCCESS) {
				mapi_errstr("SetProps", retval);
				goto cleanup;
			}
			mapi_object_release(&attach_obj);
		}
		talloc_free(attachments.aRow);
		retval = SubmitMessage(&obj_message);
		if (retval != MAPI_E_SUCCESS) {
			goto cleanup;
		}
	}
	
cleanup:
	mapi_object_release(&obj_message);
	fclose(fp);
	return retval;
}

int main(int argc, const char *argv[])
{
	TALLOC_CTX		*mem_ctx;
	poptContext		pc;
	int			opt;
	const char		*filename;

	const char		*opt_profdb = NULL;
	char			*opt_profname = NULL;
	const char		*opt_password = NULL;
	bool			opt_dump = false;
	const char		*opt_debug = NULL;
	bool			opt_dumpdata = false;

	enum {OPT_PROFILE_DB=1000, OPT_PROFILE, OPT_PASSWORD, OPT_DEBUG, OPT_DUMPDATA, OPT_DUMP};

	struct poptOption long_options[] = {
		POPT_AUTOHELP
		POPT_OPENCHANGE_VERSION
		{"database", 'f', POPT_ARG_STRING, NULL, OPT_PROFILE_DB, "set the profile database path", NULL },
		{"profile", 'p', POPT_ARG_STRING, NULL, OPT_PROFILE, "set the profile name", NULL },
		{"password", 'P', POPT_ARG_STRING, NULL, OPT_PASSWORD, "set the profile password", NULL },
		{"debuglevel", 'd', POPT_ARG_STRING, NULL, OPT_DEBUG, "set Debug Level", NULL },
		{"dump-data", 0, POPT_ARG_NONE, NULL, OPT_DUMPDATA, "dump the hex data", NULL },
		{"dump",0, POPT_ARG_NONE, NULL, OPT_DUMP, "dump each message as it is parsed, instead of trying to send", NULL},
		{ NULL, 0, POPT_ARG_NONE, NULL, 0, NULL, NULL }
	};

	mem_ctx = talloc_named(NULL, 0, "oxcmail_test");

	pc = poptGetContext("oxcmail_test", argc, argv, long_options, 0);

	if (!opt_profdb) {
		opt_profdb = talloc_asprintf(mem_ctx, DEFAULT_PROFDB, getenv("HOME"));
	}

	while ((opt = poptGetNextOpt(pc)) != -1) {
		switch (opt) {
		case OPT_DUMP:
			opt_dump = true;
			break;
		case OPT_PROFILE_DB:
			opt_profdb = poptGetOptArg(pc);
			break;
		case OPT_PROFILE:
			opt_profname = talloc_strdup(mem_ctx, poptGetOptArg(pc));
			break;
		case OPT_DEBUG:
			opt_debug = poptGetOptArg(pc);
			break;
		case OPT_DUMPDATA:
			opt_dumpdata = true;
			break;
		default:
			break;
		};
	}

	if (opt_dump) {
		while ((filename = poptGetArg(pc)) != NULL) {  
			dump_file(mem_ctx, filename);
		}
	} else {
		enum MAPISTATUS		retval;
		struct mapi_context	*mapi_ctx;
		struct mapi_session	*session = NULL;
		mapi_object_t		obj_store;
		mapi_id_t		id_outbox;
		mapi_object_t		obj_outbox;
		
		mapi_object_init(&obj_store);
		mapi_object_init(&obj_outbox);

		retval = MAPIInitialize(&mapi_ctx, opt_profdb);
		if (retval != MAPI_E_SUCCESS) {
			mapi_errstr("MAPIInitialize", retval);
			exit (1);
		}

		/* debug options */
		SetMAPIDumpData(mapi_ctx, opt_dumpdata);

		if (opt_debug) {
			SetMAPIDebugLevel(mapi_ctx, atoi(opt_debug));
		}

		if (!opt_profname) {
			retval = GetDefaultProfile(mapi_ctx, &opt_profname);
			if (retval != MAPI_E_SUCCESS) {
				mapi_errstr("GetDefaultProfile", retval);
				exit (1);
			}
		}

		retval = MapiLogonEx(mapi_ctx, &session, opt_profname, opt_password);

		if (retval != MAPI_E_SUCCESS) {
			mapi_errstr("MapiLogonEx", retval);
			exit (1);
		}
	
		retval = OpenMsgStore(session, &obj_store);
		if (retval != MAPI_E_SUCCESS) {
			mapi_errstr("OpenMsgStore", retval);
			exit (1);
		}

		retval = GetDefaultFolder(&obj_store, &id_outbox, olFolderSentMail);
		if (retval != MAPI_E_SUCCESS){
			mapi_errstr("GetDefaultFolder", retval);
			exit (1);
		}

		/* Open outbox folder */
		retval = OpenFolder(&obj_store, id_outbox, &obj_outbox);
		if (retval != MAPI_E_SUCCESS){
			mapi_errstr("OpenFolder", retval);
			exit (1);
		}

		while ((filename = poptGetArg(pc)) != NULL) {  
			upload_file(mem_ctx, filename, &obj_outbox);
		}

		mapi_object_release(&obj_outbox);
		mapi_object_release(&obj_store);
	}
	talloc_free(opt_profname);
	talloc_free((char*)opt_profdb);

	poptFreeContext(pc);
	/* talloc_report_full(mem_ctx, stderr); */
	talloc_free(mem_ctx);
	return 0;
}
