/*
   OpenChange MIME mail <-> Email object conversion library.

   Copyright (C) Brad Hards <bradh@frogmouth.net> 2010.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "oxcmail_parser_utils.h"

/*
    This function converts a unix time format to a FILETIME in UTC.
    TODO:
    - check return values
    - see if we can handle times outside of the time_t range
*/
bool oxcmail_util_tm_to_FILETIME(struct tm *unix_time, struct FILETIME *filetime)
{
	time_t caltime;
	NTTIME nttime;
	long utc_offset = unix_time->tm_gmtoff;
	caltime = mktime(unix_time);
	caltime = caltime - utc_offset;
	unix_to_nt_time(&nttime, caltime);
	filetime->dwHighDateTime = nttime >> 32;
	filetime->dwLowDateTime = nttime & (0xFFFFFFFF);
	return true;
}

bool oxcmail_util_parse_address(TALLOC_CTX *mem_ctx, const char *addr_string, char **displayname, char **emailaddress)
{
	if ((strchr(addr_string, '<')) && (strchr(addr_string, '>'))) {
		char *start_email_address = strchr(addr_string, '<');
		*displayname = talloc_strndup(mem_ctx, addr_string, strlen(addr_string) - strlen(start_email_address));
		trim_string(*displayname, " ", " ");
		trim_string(*displayname, "\"", "\"");
		*emailaddress = talloc_strdup(mem_ctx, start_email_address);
		trim_string(*emailaddress, "<", ">");
	} else {
		*displayname = talloc_strdup(mem_ctx, addr_string);
		*emailaddress = talloc_strdup(mem_ctx, addr_string);
	}
	return true;
}
