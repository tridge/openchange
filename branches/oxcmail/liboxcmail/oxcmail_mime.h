/*
   OpenChange MIME mail <-> Email object conversion library.

   Copyright (C) Brad Hards <bradh@frogmouth.net> 2010.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OXCMAIL_MIME
#define OXCMAIL_MIME 1

#include <talloc.h>
#include <stdbool.h>

/**
  A simple structure for holding MIME information
  
  \note if you add members here, you probably need to extend oxcmail_mime_info_dump() implementation
*/
struct oxcmail_mime_info {
	const char *mime_version;		/*!< mime version, as a string */
	const char *content_type;		/*!< the Content-Type, without any parameters */
	const char *full_content_type;		/*!< the full Content-Type header including any parameters */
	const char *boundary_marker;		/*!< the boundary marker text */
	const char *charset;			/*!< the character set (for text/plain) */
	const char *transfer_encoding;		/*!< the transfer encoding (e.g quoted printable, base64) */
	bool is_attachment;			/*!< true if this mime description corresponds to an attachment */
	const char *attachment_filename;	/*!< the filename for the attachment (not for top level) */
};

struct oxcmail_mime_info *oxcmail_mime_info_init(TALLOC_CTX *);

void oxcmail_mime_info_dump(struct oxcmail_mime_info *, const char *);

#endif