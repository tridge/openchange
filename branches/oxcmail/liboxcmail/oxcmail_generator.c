/*
   OpenChange MIME mail <-> Email object conversion library.

   Copyright (C) Brad Hards <bradh@frogmouth.net> 2010.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Note: this file uses cog (http://nedbatchelder.com/code/cog/index.html),
 so you can not just update the code. You need to update the cog script,
 then run cog -r again */

#include "oxcmail_generator.h"
#include "oxcmail_public_proto.h"
#include "oxcmail_proto.h"

/**
 \brief write a string to the output stream
 
 \param str the string to output
 \param fp FILE pointer to write to
 
 \note This somewhat strange construction works around a nuisance warning
 in glibc that requires checking the return value from fwrite().
 
 \return true if the string was written correctly, otherwise false.
*/
static bool oxcmail_write_string_to_stream(const char *str, FILE *fp)
{
	size_t bytes_written;
	bytes_written = fwrite(str, strlen(str), sizeof(char), fp);
	if (bytes_written == strlen(str) * sizeof(char)) {
		return true;
	} else {
		return false;
	}
}

/**
 \brief write the angle-address part of an address out, adding the angles
 
 \param angle_address the address (without <> markers)
 \param fp FILE pointer to write to
*/
static void oxcmail_generate_write_angle_address(const char *angle_address, FILE *fp)
{
	oxcmail_write_string_to_stream("<", fp);
	oxcmail_write_string_to_stream(angle_address, fp);
	oxcmail_write_string_to_stream(">", fp);
}

/**
 \brief write a newline (end-of-line) to the stream
 
 \param fp FILE pointer to write to
*/
static void oxcmail_generate_write_newline(FILE *fp)
{
	oxcmail_write_string_to_stream("\n", fp);
}

static bool proptag_is_string(enum MAPITAGS proptag)
{
	uint32_t proptype = proptag & 0xFFFF;
	if ((proptype == PT_UNICODE) || (proptype == PT_STRING8)) {
		return true;
	} else {
		return false;
	}
}

static const char* oxcmail_find_SPropValue_string(struct SRow *properties, enum MAPITAGS proptag)
{
	uint32_t i;

	if ( ! proptag_is_string(proptag)) {
		/* this code only handles string type values */
		return NULL;
	}
	for (i = 0; i < properties->cValues; i++) {
		if (((properties->lpProps[i].ulPropTag & 0xFFFF0000) == (proptag & 0xFFFF0000)) && proptag_is_string(properties->lpProps[i].ulPropTag)) {
			if ((properties->lpProps[i].ulPropTag & 0xFFFF) == PT_UNICODE) {
				return properties->lpProps[i].value.lpszW;
			} else {
				return properties->lpProps[i].value.lpszA;
			}
		}
	}
	return NULL;
}

/*[[[cog
import cog
from cog_helpers import legitimise_function_name

from oxcmail_mappings import directstringconverts

for mimeheader,proptagname in directstringconverts:
    cog.c_comment('This function does direct string mapping conversions and was generated from a Cog script. DO NOT EDIT')
    cog.outl('void oxcmail_generate_%s(struct SPropValue prop, FILE *fp)' % legitimise_function_name(proptagname))
    cog.outl('{')
    cog.outl('\toxcmail_write_string_to_stream("%s: ", fp);' % mimeheader)
    cog.outl('\toxcmail_write_string_to_stream(prop.value.lpszA, fp);')
    cog.outl('\toxcmail_generate_write_newline(fp);')
    cog.outl('}')
    cog.outl('')

]]]*/
/* This function does direct string mapping conversions and was generated from a Cog script. DO NOT EDIT */
void oxcmail_generate_PR_CONVERSATION_TOPIC(struct SPropValue prop, FILE *fp)
{
	oxcmail_write_string_to_stream("Thread-Topic: ", fp);
	oxcmail_write_string_to_stream(prop.value.lpszA, fp);
	oxcmail_generate_write_newline(fp);
}

/* This function does direct string mapping conversions and was generated from a Cog script. DO NOT EDIT */
void oxcmail_generate_PR_LIST_HELP(struct SPropValue prop, FILE *fp)
{
	oxcmail_write_string_to_stream("List-Help: ", fp);
	oxcmail_write_string_to_stream(prop.value.lpszA, fp);
	oxcmail_generate_write_newline(fp);
}

/* This function does direct string mapping conversions and was generated from a Cog script. DO NOT EDIT */
void oxcmail_generate_PR_LIST_SUBSCRIBE(struct SPropValue prop, FILE *fp)
{
	oxcmail_write_string_to_stream("List-Subscribe: ", fp);
	oxcmail_write_string_to_stream(prop.value.lpszA, fp);
	oxcmail_generate_write_newline(fp);
}

/* This function does direct string mapping conversions and was generated from a Cog script. DO NOT EDIT */
void oxcmail_generate_PR_LIST_UNSUBSCRIBE(struct SPropValue prop, FILE *fp)
{
	oxcmail_write_string_to_stream("List-Unsubscribe: ", fp);
	oxcmail_write_string_to_stream(prop.value.lpszA, fp);
	oxcmail_generate_write_newline(fp);
}

/* This function does direct string mapping conversions and was generated from a Cog script. DO NOT EDIT */
void oxcmail_generate_PR_INTERNET_MESSAGE_ID(struct SPropValue prop, FILE *fp)
{
	oxcmail_write_string_to_stream("Message-ID: ", fp);
	oxcmail_write_string_to_stream(prop.value.lpszA, fp);
	oxcmail_generate_write_newline(fp);
}

/* This function does direct string mapping conversions and was generated from a Cog script. DO NOT EDIT */
void oxcmail_generate_PR_IN_REPLY_TO_ID(struct SPropValue prop, FILE *fp)
{
	oxcmail_write_string_to_stream("In-Reply-To: ", fp);
	oxcmail_write_string_to_stream(prop.value.lpszA, fp);
	oxcmail_generate_write_newline(fp);
}

/* This function does direct string mapping conversions and was generated from a Cog script. DO NOT EDIT */
void oxcmail_generate_PR_INTERNET_REFERENCES(struct SPropValue prop, FILE *fp)
{
	oxcmail_write_string_to_stream("References: ", fp);
	oxcmail_write_string_to_stream(prop.value.lpszA, fp);
	oxcmail_generate_write_newline(fp);
}

/*[[[end]]] (checksum: fddaa1d3d4d84a2c2c045af81666b769) */

/*[[[cog

from oxcmail_mappings import enumconverts

for mimeheader,proptagname,enum_mapping in enumconverts:
    cog.outl('static bool oxcmail_generate_lookup_%s(TALLOC_CTX *mem_ctx, uint32_t integer_value, const char **result)' % legitimise_function_name(proptagname))
    cog.outl('{')
    cog.outl('\tbool ret;')
    cog.outl('\tswitch (integer_value) {')
    for enum_num, enum_str in enum_mapping:
	cog.outl('\tcase (%s):' % enum_num)
	cog.outl('\t\t*result = talloc_strdup(mem_ctx, "%s");' % enum_str)
	cog.outl('\t\tret = true;')
	cog.outl('\t\tbreak;')
    cog.outl('\tdefault:')
    cog.outl('\t\tprintf("unhandled case in oxcmail_generate_lookup_%s: %%i\\n", integer_value);' % legitimise_function_name(proptagname) )
    cog.outl('\t\tret = false;')
    cog.outl('\t}')
    cog.outl('\treturn ret;')
    cog.outl('}')
    cog.outl('')
    
    cog.outl('static bool oxcmail_generate_%s(TALLOC_CTX *mem_ctx, struct SPropValue prop, FILE *fp)' % legitimise_function_name(proptagname))
    cog.outl('{')
    cog.outl('\tconst char *result = NULL;')
    cog.outl('\tif (oxcmail_generate_lookup_%s(mem_ctx, prop.value.l, &result)) {' % legitimise_function_name(proptagname))
    cog.outl('\t\toxcmail_write_string_to_stream("%s: ", fp);' % mimeheader)
    cog.outl('\t\toxcmail_write_string_to_stream(result, fp);')
    cog.outl('\t\toxcmail_generate_write_newline(fp);')
    cog.outl('\t\ttalloc_free((TALLOC_CTX*)result);')
    cog.outl('\t\treturn true;')
    cog.outl('\t}')
    cog.outl('\treturn false;')
    cog.outl('}')
    cog.outl('')

]]]*/
static bool oxcmail_generate_lookup_PR_IMPORTANCE(TALLOC_CTX *mem_ctx, uint32_t integer_value, const char **result)
{
	bool ret;
	switch (integer_value) {
	case (0x0000000):
		*result = talloc_strdup(mem_ctx, "Low");
		ret = true;
		break;
	case (0x0000001):
		*result = talloc_strdup(mem_ctx, "Normal");
		ret = true;
		break;
	case (0x0000002):
		*result = talloc_strdup(mem_ctx, "High");
		ret = true;
		break;
	default:
		printf("unhandled case in oxcmail_generate_lookup_PR_IMPORTANCE: %i\n", integer_value);
		ret = false;
	}
	return ret;
}

static bool oxcmail_generate_PR_IMPORTANCE(TALLOC_CTX *mem_ctx, struct SPropValue prop, FILE *fp)
{
	const char *result = NULL;
	if (oxcmail_generate_lookup_PR_IMPORTANCE(mem_ctx, prop.value.l, &result)) {
		oxcmail_write_string_to_stream("Importance: ", fp);
		oxcmail_write_string_to_stream(result, fp);
		oxcmail_generate_write_newline(fp);
		talloc_free((TALLOC_CTX*)result);
		return true;
	}
	return false;
}

static bool oxcmail_generate_lookup_PR_SENSITIVITY(TALLOC_CTX *mem_ctx, uint32_t integer_value, const char **result)
{
	bool ret;
	switch (integer_value) {
	case (0x00000000):
		*result = talloc_strdup(mem_ctx, "Normal");
		ret = true;
		break;
	case (0x00000001):
		*result = talloc_strdup(mem_ctx, "Personal");
		ret = true;
		break;
	case (0x00000002):
		*result = talloc_strdup(mem_ctx, "Private");
		ret = true;
		break;
	case (0x00000003):
		*result = talloc_strdup(mem_ctx, "Company-Confidential");
		ret = true;
		break;
	default:
		printf("unhandled case in oxcmail_generate_lookup_PR_SENSITIVITY: %i\n", integer_value);
		ret = false;
	}
	return ret;
}

static bool oxcmail_generate_PR_SENSITIVITY(TALLOC_CTX *mem_ctx, struct SPropValue prop, FILE *fp)
{
	const char *result = NULL;
	if (oxcmail_generate_lookup_PR_SENSITIVITY(mem_ctx, prop.value.l, &result)) {
		oxcmail_write_string_to_stream("Sensitivity: ", fp);
		oxcmail_write_string_to_stream(result, fp);
		oxcmail_generate_write_newline(fp);
		talloc_free((TALLOC_CTX*)result);
		return true;
	}
	return false;
}

/*[[[end]]] (checksum: 1aca9ce13faf2ac98cfd55af887bf4a1) */

#include <time.h>
/*[[[cog
from oxcmail_mappings import dateconverts

for mimeheader,proptagname in dateconverts:
    cog.c_comment('This function does date conversions and was generated from a Cog script. DO NOT EDIT')
    cog.outl('static bool oxcmail_generate_%s(struct SPropValue prop, FILE *fp)' % legitimise_function_name(mimeheader))
    cog.outl('{')
    cog.outl('\tNTTIME nttime;')
    cog.outl('\ttime_t timet_time;')
    cog.outl('\tchar outstr[200];')
    cog.outl('\tstruct FILETIME *ft = &prop.value.ft;')
    cog.outl('\tif (! ft) {')
    cog.outl('\t\treturn false;')
    cog.outl('\t}')
    cog.outl('\tnttime = ft->dwHighDateTime;')
    cog.outl('\tnttime = nttime << 32;')
    cog.outl('\tnttime += ft->dwLowDateTime;')
    cog.outl('\ttimet_time = nt_time_to_unix(nttime);')
    cog.outl('\toxcmail_write_string_to_stream("%s: ", fp);' % mimeheader)
    cog.outl('\tstrftime(&outstr[0], 200, "%a, %d %b %Y %T %z", localtime(&timet_time));')
    cog.outl('\toxcmail_write_string_to_stream(outstr, fp);')
    cog.outl('\toxcmail_generate_write_newline(fp);');
    cog.outl('\treturn true;')
    cog.outl('}')
    cog.outl()
    
]]] */
/* This function does date conversions and was generated from a Cog script. DO NOT EDIT */
static bool oxcmail_generate_Date(struct SPropValue prop, FILE *fp)
{
	NTTIME nttime;
	time_t timet_time;
	char outstr[200];
	struct FILETIME *ft = &prop.value.ft;
	if (! ft) {
		return false;
	}
	nttime = ft->dwHighDateTime;
	nttime = nttime << 32;
	nttime += ft->dwLowDateTime;
	timet_time = nt_time_to_unix(nttime);
	oxcmail_write_string_to_stream("Date: ", fp);
	strftime(&outstr[0], 200, "%a, %d %b %Y %T %z", localtime(&timet_time));
	oxcmail_write_string_to_stream(outstr, fp);
	oxcmail_generate_write_newline(fp);
	return true;
}

/* This function does date conversions and was generated from a Cog script. DO NOT EDIT */
static bool oxcmail_generate_Reply_By(struct SPropValue prop, FILE *fp)
{
	NTTIME nttime;
	time_t timet_time;
	char outstr[200];
	struct FILETIME *ft = &prop.value.ft;
	if (! ft) {
		return false;
	}
	nttime = ft->dwHighDateTime;
	nttime = nttime << 32;
	nttime += ft->dwLowDateTime;
	timet_time = nt_time_to_unix(nttime);
	oxcmail_write_string_to_stream("Reply-By: ", fp);
	strftime(&outstr[0], 200, "%a, %d %b %Y %T %z", localtime(&timet_time));
	oxcmail_write_string_to_stream(outstr, fp);
	oxcmail_generate_write_newline(fp);
	return true;
}

/* This function does date conversions and was generated from a Cog script. DO NOT EDIT */
static bool oxcmail_generate_Expires(struct SPropValue prop, FILE *fp)
{
	NTTIME nttime;
	time_t timet_time;
	char outstr[200];
	struct FILETIME *ft = &prop.value.ft;
	if (! ft) {
		return false;
	}
	nttime = ft->dwHighDateTime;
	nttime = nttime << 32;
	nttime += ft->dwLowDateTime;
	timet_time = nt_time_to_unix(nttime);
	oxcmail_write_string_to_stream("Expires: ", fp);
	strftime(&outstr[0], 200, "%a, %d %b %Y %T %z", localtime(&timet_time));
	oxcmail_write_string_to_stream(outstr, fp);
	oxcmail_generate_write_newline(fp);
	return true;
}

/*[[[end]]] (checksum: 2a0eccb4a287336b88bac09319e15d4f) */
  
/*[[[cog
from oxcmail_mappings import addressconverts

for mimeheader,displayname,email_address,address_type,entry_id in addressconverts:
    cog.c_comment('This function does address conversions and was generated from a Cog script. DO NOT EDIT')
    cog.outl('static bool oxcmail_generate_angle_address_%s(struct SRow *properties, const char **angle_address)' % legitimise_function_name(mimeheader))
    cog.outl('{')
    # TODO: entry id
    cog.outl('\tconst char *address_type = NULL;')
    cog.outl('\tconst char *email_address = NULL;')
    cog.outl('\tconst char *smtp_email_address = NULL;')
    cog.outl('\tconst char *display_name = NULL;')
    cog.outl('\tbool ret = true;')
    # TODO: entry id
    cog.outl('\taddress_type = oxcmail_find_SPropValue_string(properties, %s);' % address_type)
    cog.outl('\temail_address = oxcmail_find_SPropValue_string(properties, %s);' % email_address)
    cog.outl('\tsmtp_email_address = oxcmail_find_SPropValue_string(properties, PR_SMTP_ADDRESS );')
    cog.outl('\tdisplay_name = oxcmail_find_SPropValue_string(properties, %s);' % displayname )
    cog.outl('')
    cog.outl('\tif (address_type && email_address && (strcmp(address_type, "SMTP") == 0)) {')
    cog.outl('\t\t*angle_address = email_address;')
    cog.outl('\t} else if (smtp_email_address) {')
    cog.outl('\t\t*angle_address = smtp_email_address;')
    cog.outl('\t} else if (display_name) {')
    cog.outl('\t\t*angle_address = display_name;')
    cog.outl('\t} else {')
    cog.outl('\t\t*angle_address = NULL;')
    cog.outl('\t\tret = false;')
    cog.outl('\t}')
    cog.outl('\treturn ret;')
    cog.outl('}')
    cog.outl()

    cog.c_comment('This function does address conversions and was generated from a Cog script. DO NOT EDIT')
    cog.outl('bool oxcmail_generate_%s(struct SRow *properties, FILE *fp)' % legitimise_function_name(mimeheader))
    cog.outl('{')
    cog.outl('\tconst char *display_name = NULL;')
    cog.outl('\tconst char *angle_address = NULL;')
    cog.outl('')
    cog.outl('\tdisplay_name = oxcmail_find_SPropValue_string(properties, %s);' % displayname)
    cog.outl('\tif (oxcmail_generate_angle_address_%s(properties, &angle_address)) {' % legitimise_function_name(mimeheader))
    cog.outl('\t\toxcmail_write_string_to_stream("%s: ", fp);' % mimeheader)
    cog.outl('\t\tif (display_name) {')
    cog.outl('\t\t\toxcmail_write_string_to_stream(display_name, fp);')
    cog.outl('\t\t\toxcmail_write_string_to_stream(" ", fp);')
    cog.outl('\t\t}')
    cog.outl('\t\toxcmail_generate_write_angle_address(angle_address, fp);')
    cog.outl('\t\toxcmail_generate_write_newline(fp);')
    cog.outl('\t\treturn true;')
    cog.outl('\t} else {')
    cog.outl('\t\treturn false;')
    cog.outl('\t}')
    cog.outl('}')
    cog.outl()
]]]*/
/* This function does address conversions and was generated from a Cog script. DO NOT EDIT */
static bool oxcmail_generate_angle_address_From(struct SRow *properties, const char **angle_address)
{
	const char *address_type = NULL;
	const char *email_address = NULL;
	const char *smtp_email_address = NULL;
	const char *display_name = NULL;
	bool ret = true;
	address_type = oxcmail_find_SPropValue_string(properties, PR_SENT_REPRESENTING_ADDRTYPE);
	email_address = oxcmail_find_SPropValue_string(properties, PR_SENT_REPRESENTING_EMAIL_ADDRESS);
	smtp_email_address = oxcmail_find_SPropValue_string(properties, PR_SMTP_ADDRESS );
	display_name = oxcmail_find_SPropValue_string(properties, PR_SENT_REPRESENTING_NAME);

	if (address_type && email_address && (strcmp(address_type, "SMTP") == 0)) {
		*angle_address = email_address;
	} else if (smtp_email_address) {
		*angle_address = smtp_email_address;
	} else if (display_name) {
		*angle_address = display_name;
	} else {
		*angle_address = NULL;
		ret = false;
	}
	return ret;
}

/* This function does address conversions and was generated from a Cog script. DO NOT EDIT */
bool oxcmail_generate_From(struct SRow *properties, FILE *fp)
{
	const char *display_name = NULL;
	const char *angle_address = NULL;

	display_name = oxcmail_find_SPropValue_string(properties, PR_SENT_REPRESENTING_NAME);
	if (oxcmail_generate_angle_address_From(properties, &angle_address)) {
		oxcmail_write_string_to_stream("From: ", fp);
		if (display_name) {
			oxcmail_write_string_to_stream(display_name, fp);
			oxcmail_write_string_to_stream(" ", fp);
		}
		oxcmail_generate_write_angle_address(angle_address, fp);
		oxcmail_generate_write_newline(fp);
		return true;
	} else {
		return false;
	}
}

/* This function does address conversions and was generated from a Cog script. DO NOT EDIT */
static bool oxcmail_generate_angle_address_Sender(struct SRow *properties, const char **angle_address)
{
	const char *address_type = NULL;
	const char *email_address = NULL;
	const char *smtp_email_address = NULL;
	const char *display_name = NULL;
	bool ret = true;
	address_type = oxcmail_find_SPropValue_string(properties, PR_SENDER_ADDRTYPE);
	email_address = oxcmail_find_SPropValue_string(properties, PR_SENDER_EMAIL_ADDRESS);
	smtp_email_address = oxcmail_find_SPropValue_string(properties, PR_SMTP_ADDRESS );
	display_name = oxcmail_find_SPropValue_string(properties, PR_SENDER_NAME);

	if (address_type && email_address && (strcmp(address_type, "SMTP") == 0)) {
		*angle_address = email_address;
	} else if (smtp_email_address) {
		*angle_address = smtp_email_address;
	} else if (display_name) {
		*angle_address = display_name;
	} else {
		*angle_address = NULL;
		ret = false;
	}
	return ret;
}

/* This function does address conversions and was generated from a Cog script. DO NOT EDIT */
bool oxcmail_generate_Sender(struct SRow *properties, FILE *fp)
{
	const char *display_name = NULL;
	const char *angle_address = NULL;

	display_name = oxcmail_find_SPropValue_string(properties, PR_SENDER_NAME);
	if (oxcmail_generate_angle_address_Sender(properties, &angle_address)) {
		oxcmail_write_string_to_stream("Sender: ", fp);
		if (display_name) {
			oxcmail_write_string_to_stream(display_name, fp);
			oxcmail_write_string_to_stream(" ", fp);
		}
		oxcmail_generate_write_angle_address(angle_address, fp);
		oxcmail_generate_write_newline(fp);
		return true;
	} else {
		return false;
	}
}

/*[[[end]]] (checksum: f01bd9ee5e09d402f7e6dd90ca223b04) */

static bool oxcmail_generate_output_one_recipient(struct SRow *recipient, FILE *fp, char **envelope_rcpts)
{
	/* TODO: handle entry ID */
	const char *address_type;
	const char *email_address;
	const char *smtp_email_address;
	const char *display_name;
	const char *angle_address = NULL; /* derived */
	address_type = oxcmail_find_SPropValue_string(recipient, PR_ADDRTYPE);
	email_address = oxcmail_find_SPropValue_string(recipient, PR_EMAIL_ADDRESS);
	smtp_email_address = oxcmail_find_SPropValue_string(recipient, PR_SMTP_ADDRESS);
	display_name = oxcmail_find_SPropValue_string(recipient, PR_DISPLAY_NAME);
	
	if (address_type && email_address && (strcmp(address_type, "SMTP") == 0)) {
		angle_address = email_address;
	} else if (smtp_email_address) {
		angle_address = smtp_email_address;
	} else {
		angle_address = NULL;
	}
	if (angle_address) {
		if (display_name) {
			oxcmail_write_string_to_stream(display_name, fp);
			oxcmail_write_string_to_stream(" ", fp);
		}
		oxcmail_generate_write_angle_address(angle_address, fp);
		if (strcmp(*envelope_rcpts, "") == 0) {
			*envelope_rcpts = talloc_asprintf_append(*envelope_rcpts, "<%s>", angle_address);
		} else {
			*envelope_rcpts = talloc_asprintf_append_buffer(*envelope_rcpts, ", <%s>", angle_address);
		}
		return true;
	} else {
		return false;
	}
}

static void oxcmail_generate_Subject(struct SRow *properties, FILE *fp)
{
	const char *normalized_subject;
	const char *subject_prefix;
	const char *subject;
	
	normalized_subject = oxcmail_find_SPropValue_string(properties, PR_NORMALIZED_SUBJECT_UNICODE);
	subject_prefix = oxcmail_find_SPropValue_string(properties, PR_SUBJECT_PREFIX_UNICODE);
	subject = oxcmail_find_SPropValue_string(properties, PR_SUBJECT_UNICODE);
	
	if (normalized_subject && subject_prefix) {
		oxcmail_write_string_to_stream("Subject: ", fp);
		if (strlen(subject_prefix) > 0) {
			oxcmail_write_string_to_stream(subject_prefix, fp);
			oxcmail_write_string_to_stream(": ", fp);
		}
		oxcmail_write_string_to_stream(normalized_subject, fp);
		oxcmail_generate_write_newline(fp);
	} else if (subject) {
		oxcmail_write_string_to_stream("Subject: ", fp);
		oxcmail_write_string_to_stream(subject, fp);
		oxcmail_generate_write_newline(fp);
	}
}

/* TODO: consider whether we should check PR_OBJECT_TYPE = MAPI_RECIPIENT */
/*[[[cog
from oxcmail_mappings import addresslistconverts

cog.outl('static void oxcmail_generate_recipients(struct SRowSet *recipients, FILE *fp, char **envelope_rcpts)')
cog.outl('{')
cog.outl('\tint recipient_idx;')
cog.outl('\tbool is_first_entry;')
for mimeheader,reciptype in addresslistconverts:
    cog.outl('\tis_first_entry = true;')
    cog.outl('\tfor (recipient_idx = 0; recipient_idx < recipients->cRows; ++recipient_idx) {')
    cog.outl('\t\tconst int *recipient_type = (int*)find_SPropValue_data(&(recipients->aRow[recipient_idx]), PR_RECIPIENT_TYPE);')
    cog.outl('\t\tif (*recipient_type != %s) {' % reciptype)
    cog.outl('\t\t\tcontinue;')
    cog.outl('\t\t}')
    cog.outl('\t\tif (is_first_entry) {')
    cog.outl('\t\t\toxcmail_write_string_to_stream("%s: ", fp);' % mimeheader)
    cog.outl('\t\t\tis_first_entry = false;')
    cog.outl('\t\t} else {')
    cog.outl('\t\t\toxcmail_write_string_to_stream(", ", fp);')
    cog.outl('\t\t}')
    cog.outl('\t\toxcmail_generate_output_one_recipient(&(recipients->aRow[recipient_idx]), fp, envelope_rcpts);')
    cog.outl('\t}')
    cog.outl('\tif( ! is_first_entry) {')
    cog.outl('\t\toxcmail_generate_write_newline(fp);')
    cog.outl('\t}')
cog.outl('}')
cog.outl('')
]]]*/
static void oxcmail_generate_recipients(struct SRowSet *recipients, FILE *fp, char **envelope_rcpts)
{
	int recipient_idx;
	bool is_first_entry;
	is_first_entry = true;
	for (recipient_idx = 0; recipient_idx < recipients->cRows; ++recipient_idx) {
		const int *recipient_type = (int*)find_SPropValue_data(&(recipients->aRow[recipient_idx]), PR_RECIPIENT_TYPE);
		if (*recipient_type != MAPI_TO) {
			continue;
		}
		if (is_first_entry) {
			oxcmail_write_string_to_stream("To: ", fp);
			is_first_entry = false;
		} else {
			oxcmail_write_string_to_stream(", ", fp);
		}
		oxcmail_generate_output_one_recipient(&(recipients->aRow[recipient_idx]), fp, envelope_rcpts);
	}
	if( ! is_first_entry) {
		oxcmail_generate_write_newline(fp);
	}
	is_first_entry = true;
	for (recipient_idx = 0; recipient_idx < recipients->cRows; ++recipient_idx) {
		const int *recipient_type = (int*)find_SPropValue_data(&(recipients->aRow[recipient_idx]), PR_RECIPIENT_TYPE);
		if (*recipient_type != MAPI_CC) {
			continue;
		}
		if (is_first_entry) {
			oxcmail_write_string_to_stream("Cc: ", fp);
			is_first_entry = false;
		} else {
			oxcmail_write_string_to_stream(", ", fp);
		}
		oxcmail_generate_output_one_recipient(&(recipients->aRow[recipient_idx]), fp, envelope_rcpts);
	}
	if( ! is_first_entry) {
		oxcmail_generate_write_newline(fp);
	}
	is_first_entry = true;
	for (recipient_idx = 0; recipient_idx < recipients->cRows; ++recipient_idx) {
		const int *recipient_type = (int*)find_SPropValue_data(&(recipients->aRow[recipient_idx]), PR_RECIPIENT_TYPE);
		if (*recipient_type != MAPI_BCC) {
			continue;
		}
		if (is_first_entry) {
			oxcmail_write_string_to_stream("Bcc: ", fp);
			is_first_entry = false;
		} else {
			oxcmail_write_string_to_stream(", ", fp);
		}
		oxcmail_generate_output_one_recipient(&(recipients->aRow[recipient_idx]), fp, envelope_rcpts);
	}
	if( ! is_first_entry) {
		oxcmail_generate_write_newline(fp);
	}
}

/*[[[end]]] (checksum: b3630b9a61c18d5083c4f0d8889f2c09)*/

/*[[[cog

cog.outl('bool oxcmail_generate_payload(TALLOC_CTX *mem_ctx, struct SRow *properties, struct SRowSet *recipients, FILE *fp, char **envelope_rcpts)')
cog.outl('{')
cog.outl('\tint prop_idx = 0;')
for mimeheader,displayname,email_address,address_type,entry_id in addressconverts:
    cog.outl('\toxcmail_generate_%s(properties, fp);' % legitimise_function_name(mimeheader))
cog.outl('\t*envelope_rcpts = talloc_strdup(mem_ctx, "");')
cog.outl('\toxcmail_generate_recipients(recipients, fp, envelope_rcpts);')
cog.outl('\toxcmail_generate_Subject(properties, fp);')
cog.outl('\tfor (prop_idx = 0; prop_idx < properties->cValues; ++prop_idx) {')
cog.outl('\t\tswitch (properties->lpProps[prop_idx].ulPropTag) {')
for mimeheader,proptagname in directstringconverts:
    cog.outl('\t\tcase %s:' % proptagname)
    cog.outl('\t\t\toxcmail_generate_%s(properties->lpProps[prop_idx], fp);' % proptagname)
    cog.outl('\t\t\tbreak;')
for mimeheader,proptagname in dateconverts:
    cog.outl('\t\tcase %s:' % proptagname)
    cog.outl('\t\t\toxcmail_generate_%s(properties->lpProps[prop_idx], fp);' % legitimise_function_name(mimeheader))
    cog.outl('\t\t\tbreak;')  
for mimeheader,proptagname,enum_mapping in enumconverts:
    cog.outl('\t\tcase %s:' % proptagname)
    cog.outl('\t\t\toxcmail_generate_%s(mem_ctx, properties->lpProps[prop_idx], fp);' % proptagname)
    cog.outl('\t\t\tbreak;')  
for mimeheader,displayname,email_address,address_type,entry_id in addressconverts:
    cog.outl('\t\tcase %s:' % displayname)
    cog.outl('\t\tcase %s:' % email_address)
    cog.outl('\t\tcase %s:' % address_type)
    cog.outl('\t\tcase %s:' % entry_id)
    cog.out('\t\t\t')
    cog.c_comment('handled previously')
    cog.outl('\t\t\tbreak;')
# stuff we've already done
for mimeheader in ['PR_SMTP_ADDRESS', 'PR_BODY', 'PR_BODY_UNICODE', 'PR_SUBJECT', 'PR_SUBJECT_UNICODE', 'PR_NORMALIZED_SUBJECT', 'PR_NORMALIZED_SUBJECT_UNICODE', 'PR_SUBJECT_PREFIX', 'PR_SUBJECT_PREFIX_UNICODE']:
    cog.outl('\t\tcase %s:' % mimeheader)
    cog.out('\t\t\t')
    cog.c_comment('handled previously')
    cog.outl('\t\t\tbreak;')
cog.outl('\t\tdefault:')
cog.outl('\t\t\tmapidump_SPropValue(properties->lpProps[prop_idx], "[unhandled properties]");')
cog.outl('\t\t}')
cog.outl('\t}')
cog.outl('\treturn true;')
cog.outl('}')
cog.outl('')

]]]*/
bool oxcmail_generate_payload(TALLOC_CTX *mem_ctx, struct SRow *properties, struct SRowSet *recipients, FILE *fp, char **envelope_rcpts)
{
	int prop_idx = 0;
	oxcmail_generate_From(properties, fp);
	oxcmail_generate_Sender(properties, fp);
	*envelope_rcpts = talloc_strdup(mem_ctx, "");
	oxcmail_generate_recipients(recipients, fp, envelope_rcpts);
	oxcmail_generate_Subject(properties, fp);
	for (prop_idx = 0; prop_idx < properties->cValues; ++prop_idx) {
		switch (properties->lpProps[prop_idx].ulPropTag) {
		case PR_CONVERSATION_TOPIC:
			oxcmail_generate_PR_CONVERSATION_TOPIC(properties->lpProps[prop_idx], fp);
			break;
		case PR_LIST_HELP:
			oxcmail_generate_PR_LIST_HELP(properties->lpProps[prop_idx], fp);
			break;
		case PR_LIST_SUBSCRIBE:
			oxcmail_generate_PR_LIST_SUBSCRIBE(properties->lpProps[prop_idx], fp);
			break;
		case PR_LIST_UNSUBSCRIBE:
			oxcmail_generate_PR_LIST_UNSUBSCRIBE(properties->lpProps[prop_idx], fp);
			break;
		case PR_INTERNET_MESSAGE_ID:
			oxcmail_generate_PR_INTERNET_MESSAGE_ID(properties->lpProps[prop_idx], fp);
			break;
		case PR_IN_REPLY_TO_ID:
			oxcmail_generate_PR_IN_REPLY_TO_ID(properties->lpProps[prop_idx], fp);
			break;
		case PR_INTERNET_REFERENCES:
			oxcmail_generate_PR_INTERNET_REFERENCES(properties->lpProps[prop_idx], fp);
			break;
		case PR_CLIENT_SUBMIT_TIME:
			oxcmail_generate_Date(properties->lpProps[prop_idx], fp);
			break;
		case PR_REPLY_TIME:
			oxcmail_generate_Reply_By(properties->lpProps[prop_idx], fp);
			break;
		case PR_EXPIRY_TIME:
			oxcmail_generate_Expires(properties->lpProps[prop_idx], fp);
			break;
		case PR_IMPORTANCE:
			oxcmail_generate_PR_IMPORTANCE(mem_ctx, properties->lpProps[prop_idx], fp);
			break;
		case PR_SENSITIVITY:
			oxcmail_generate_PR_SENSITIVITY(mem_ctx, properties->lpProps[prop_idx], fp);
			break;
		case PR_SENT_REPRESENTING_NAME:
		case PR_SENT_REPRESENTING_EMAIL_ADDRESS:
		case PR_SENT_REPRESENTING_ADDRTYPE:
		case PR_SENT_REPRESENTING_ENTRYID:
			/* handled previously */
			break;
		case PR_SENDER_NAME:
		case PR_SENDER_EMAIL_ADDRESS:
		case PR_SENDER_ADDRTYPE:
		case PR_SENDER_ENTRYID:
			/* handled previously */
			break;
		case PR_SMTP_ADDRESS:
			/* handled previously */
			break;
		case PR_BODY:
			/* handled previously */
			break;
		case PR_BODY_UNICODE:
			/* handled previously */
			break;
		case PR_SUBJECT:
			/* handled previously */
			break;
		case PR_SUBJECT_UNICODE:
			/* handled previously */
			break;
		case PR_NORMALIZED_SUBJECT:
			/* handled previously */
			break;
		case PR_NORMALIZED_SUBJECT_UNICODE:
			/* handled previously */
			break;
		case PR_SUBJECT_PREFIX:
			/* handled previously */
			break;
		case PR_SUBJECT_PREFIX_UNICODE:
			/* handled previously */
			break;
		default:
			mapidump_SPropValue(properties->lpProps[prop_idx], "[unhandled properties]");
		}
	}
	return true;
}

/*[[[end]]] (checksum: 1419d19938b4248903887af82ee2dfd6)*/

bool oxcmail_generate_body(TALLOC_CTX *mem_ctx, struct SRow *properties, struct SRowSet *attachments, FILE *fp)
{
	/* TODO : handle HTML / alternative MIME */
	/* TODO : handle attachments */
	/* TODO : try to use old mime skeleton */
	/* TODO : handle PidTagInternetCodepage */
	const char *plain_text_body = oxcmail_find_SPropValue_string(properties, PR_BODY_UNICODE);
	if (plain_text_body) {
		oxcmail_write_string_to_stream("MIME-Version: 1.0\n", fp);
		oxcmail_write_string_to_stream("Content-Type: text/plain; charset=\"utf8\"\n", fp);
		oxcmail_generate_write_newline(fp);
		oxcmail_write_string_to_stream(plain_text_body, fp);
		oxcmail_generate_write_newline(fp); /* just to be sure */
		return true;
	} else {
		/* TODO: other format? */
		return false;
	}
}

/**
 \brief Generate an email format file
 
 This function generates an RFC5322 format file from MAPI properties.
 
 It also generates the RFC5321 envelope forward and reverse path information.
 
 \param mem_ctx the memory context to use for any allocations
 \param properties the main properties to convert to MIME headers and the message body
 \param recipients a list of recipients (TO, CC and BCC)
 \param attachments a list of attachments
 \param fp the file pointer to write the resulting file to
 \param envelope_from the RFC5321 reverse path
 \param envelope_rcpt the RFC5321 forward path
 
 The caller must talloc_free the envelope_from (and probably some other parts - add in here)
 
 \return true on success, false on failure
*/
_PUBLIC_ bool oxcmail_generate_file(TALLOC_CTX *mem_ctx, struct SRow *properties, struct SRowSet *recipients, struct SRowSet *attachments, FILE *fp, char **envelope_from, char **envelope_rcpt)
{
	const char *reverse_path = NULL;
	if ( ! oxcmail_generate_angle_address_From(properties, &reverse_path)) {
		return false;
	}
	*envelope_from = talloc_strdup(mem_ctx, reverse_path);
	if ( ! oxcmail_generate_payload(mem_ctx, properties, recipients, fp, envelope_rcpt)) {
		return false;
	}
	return oxcmail_generate_body(mem_ctx, properties, attachments, fp);
}
