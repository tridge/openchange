/*
   sendmail style front end to oxcmail

   OpenChange Project

   Copyright (C) Brad Hards <bradh@frogmouth.net> 2010

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "liboxcmail/oxcmail_parser.h"
#include "liboxcmail/oxcmail_public_proto.h"
#include "libmapi/libmapi.h"
#include <samba/popt.h>
#include <param.h>

#include "openchange-tools.h"

static void dump_file(TALLOC_CTX *mem_ctx, const char* filename)
{
	FILE				*fp;
	struct SRow			properties;
	struct SRowSet			recipients;
	struct SRowSet			attachments;

	fp = fopen(filename, "r");
	if (fp == NULL) {
		printf("could not open %s\n", filename);
		return;
	}

	oxcmail_parse_file(mem_ctx, fp, &properties, &recipients, &attachments);

	printf("returned props for %s:\n", filename);
	printf("    Recipients:\n");
	mapidump_SRowSet(&recipients, "\t");
	printf("    Other properties:\n");
	mapidump_SRow(&properties, "\t");

	//TODO: attachments
	
	// TODO: clean up returned properties.
	fclose(fp);
	return;
}

int main(int argc, const char *argv[])
{
	TALLOC_CTX		*mem_ctx;
	enum MAPISTATUS		retval;
	struct mapi_context	*mapi_ctx;
	struct mapi_session	*session = NULL;
	int			exitcode = 0;
	poptContext		pc;
	int			opt;
	const char		*opt_profdb = NULL;
	char			*opt_profname = NULL;
	const char		*opt_password = NULL;
	const char		*opt_debug = NULL;
	bool			opt_dumpdata = false;
	
	bool			opt_ignore_dot_line = false; /* lines that just contain a "." character normally terminate input, this turns that off */
	const char		*opt_from_address = NULL; /* a notional sender address */
	const char		*addr = NULL;

	enum {OPT_PROFILE_DB=1000, OPT_PROFILE, OPT_PASSWORD, OPT_DEBUG, OPT_DUMPDATA, OPT_IGNORE_DOT_LINE, OPT_FROM};

	struct poptOption long_options[] = {
		POPT_AUTOHELP
		{"database", 'f', POPT_ARG_STRING, NULL, OPT_PROFILE_DB, "set the profile database path", "PATH"},
		{"profile", 'p', POPT_ARG_STRING, NULL, OPT_PROFILE, "set the profile name", "PROFILE"},
		{"password", 'P', POPT_ARG_STRING, NULL, OPT_PASSWORD, "set the profile password", "PASSWORD"},
		{"debuglevel", 0, POPT_ARG_STRING, NULL, OPT_DEBUG, "set debug level", "LEVEL"},
		{"dump-data", 0, POPT_ARG_NONE, NULL, OPT_DUMPDATA, "Dump the hex data", NULL},
		{NULL, 'i', POPT_ARG_NONE, NULL, OPT_IGNORE_DOT_LINE, "Don't treat lines that only contain a . character as terminating input", NULL},
		{NULL, 'f', POPT_ARG_STRING, NULL, OPT_FROM, "set the sender (ignored for now, since Exchange will often bounce this)", "FROM"},
		POPT_OPENCHANGE_VERSION
		POPT_TABLEEND
	};

	mem_ctx = talloc_init("openchange_sendmail");

	pc = poptGetContext(NULL, argc, argv, long_options, 0);
	printf("starting arg parsing\n");

	while ((opt = poptGetNextOpt(pc)) >= 0) {
		switch (opt) {
		case OPT_DEBUG:
			opt_debug = poptGetOptArg(pc);
			break;
		case OPT_DUMPDATA:
			opt_dumpdata = true;
			break;
		case OPT_PROFILE_DB:
			opt_profdb = poptGetOptArg(pc);
			break;
		case OPT_PROFILE:
			opt_profname = talloc_strdup(mem_ctx, poptGetOptArg(pc));
			break;
		case OPT_PASSWORD:
			opt_password = poptGetOptArg(pc);
			break;
		case OPT_IGNORE_DOT_LINE:
			opt_ignore_dot_line = true;
			break;
		case OPT_FROM:
			opt_from_address = poptGetOptArg(pc);
			break;
		default:
			break;
		};
	}

	while ((addr = poptGetArg(pc)) != NULL) {  
		printf("extra command line arguments: %s\n", addr);
	}

	if (!opt_profdb) {
		opt_profdb = talloc_asprintf(mem_ctx, DEFAULT_PROFDB, getenv("HOME"));
	}

	/**
	 * Initialize MAPI subsystem
	 */
	retval = MAPIInitialize(&mapi_ctx, opt_profdb);
	if (retval != MAPI_E_SUCCESS) {
		mapi_errstr("MAPIInitialize", retval);
		exitcode = 1;
		goto end;
	}

	/* debug options */
	SetMAPIDumpData(mapi_ctx, opt_dumpdata);

	if (opt_debug) {
		SetMAPIDebugLevel(mapi_ctx, atoi(opt_debug));
	}

	dump_file(mem_ctx, "/dev/stdin");

	/**
	 * If no profile is specified, try to load the default one
	 * from the database
	 */
	if (!opt_profname) {
		retval = GetDefaultProfile(mapi_ctx, &opt_profname);
		if (retval != MAPI_E_SUCCESS) {
			mapi_errstr("GetDefaultProfile", retval);
			exitcode = 1;
			goto end;
		}
	}

	retval = MapiLogonEx(mapi_ctx, &session, opt_profname, opt_password);
	talloc_free(opt_profname);
	if (retval != MAPI_E_SUCCESS) {
		mapi_errstr("MapiLogonEx", retval);
		exitcode = 1;
		goto end;
	}

end:
	poptFreeContext(pc);
	/**
	 * Uninitialize MAPI subsystem
	 */
	MAPIUninitialize(mapi_ctx);
	talloc_free(mem_ctx);
	return exitcode;
}
