#!/usr/bin/python
# OpenChange provision script
#
# Copyright (C) Jelmer Vernooij <jelmer@openchange.org> 2008
#   
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#   
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#   
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import optparse
import os,sys

# To allow running from the source directory
sys.path.append("python")

import openchange

import samba
import samba.getopt as options
import openchange.provision as openchange

parser = optparse.OptionParser("openchange_provision [options]")

sambaopts = options.SambaOptions(parser)
parser.add_option_group(sambaopts)

credopts = options.CredentialsOptions(parser)
parser.add_option_group(credopts)
parser.add_option("--firstorg", type="string", metavar="FIRSTORG", 
                  help="set OpenChange First Organization (otherwise First Organization)")
parser.add_option("--firstou", type="string", metavar="FIRSTOU", 
                  help="set OpenChange First Organization Unit (otherwise First Organization Unit)")
parser.add_option("--openchangedb", action="store_true", help="Initialize OpenChange dispatcher database")
parser.add_option("--mapistore", type="string", metavar="STORE",
		  help="The backend storage type to use (only for --openchangedb)")
opts,args = parser.parse_args()
if len(args) != 0:
    parser.print_usage()
    sys.exit(1)
    
lp = sambaopts.get_loadparm()
creds = credopts.get_credentials(lp)

def setup_path(*args):
    return os.path.join(os.path.dirname(__file__), *args)

backend = "fsocpf" #default
if opts.mapistore is not None:
	backend = opts.mapistore

if not opts.openchangedb:
	openchange.provision(setup_path, lp, creds, firstorg=opts.firstorg, firstou=opts.firstou)
else:
	openchange.openchangedb_provision(lp, firstorg=opts.firstorg, firstou=opts.firstou, mapistore=backend)
