/*
   OpenChange MAPI implementation.

   Python interface to openchange profiles

   Copyright (C) Julien Kerihuel 2010.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "pyopenchange/mapi/pymapi.h"

typedef struct {
	PyObject_HEAD
	TALLOC_CTX		*mem_ctx;
	const char		*profdb;
	struct mapi_context	*mapi_ctx;
} PyMAPIProfileObject;

static PyObject *py_profile_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
	TALLOC_CTX		*mem_ctx;
	const char		*profdb = NULL;
	PyMAPIProfileObject	*prof_obj;
	PyMAPIObject		*mapi_obj = NULL;
	char			*kwnames[] = { "mapi", "profdb", NULL };
	bool			dflt = false;

	mem_ctx = talloc_new(NULL);
	if (mem_ctx == NULL) {
		PyErr_NoMemory();
		return NULL;
	}

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "|Os", kwnames, &mapi_obj, &profdb)) {
		return NULL;
	}

	if (!profdb) {
		profdb = talloc_asprintf(mem_ctx, DEFAULT_PROFDB, getenv("HOME"));
		dflt = true;
	}

	prof_obj = PyObject_New(PyMAPIProfileObject, &PyMAPIProfileType);
	prof_obj->mem_ctx = mem_ctx;
	if (dflt == true) {
		prof_obj->profdb = (dflt == true) ? profdb : talloc_strdup(mem_ctx, profdb);
	}
	prof_obj->mapi_ctx = (mapi_obj) ? mapi_obj->mapi_ctx : NULL;

	return (PyObject *) prof_obj;
}

static PyObject *py_profile_newdb(PyObject *_self, PyObject *args)
{
	PyMAPIProfileObject	*self = (PyMAPIProfileObject *)_self;
	const char		*profdb;
	const char		*ldif;
	enum MAPISTATUS		retval;

	/* Step 1. Parse argument (look for profile database path) */
	if (!PyArg_ParseTuple(args, "|s", &profdb)) {
		profdb = self->profdb;
	}

	/* Step 2. Retrieve the default LDIF path for provisioning */
	ldif = talloc_strdup(self->mem_ctx, mapi_profile_get_ldif_path());
	if (!ldif) {
		return NULL;
	}

	/* Step 3. Ensure the database doesn't already exist */
	if (access(profdb, F_OK) == 0) {
		return NULL;
	}

	/* Step 4. Create the profile store */
	retval = CreateProfileStore(profdb, ldif);
	if (retval != MAPI_E_SUCCESS) {
		return PyInt_FromLong(retval);
	}

	if (self->profdb) {
		talloc_free((char *)self->profdb);
	}
	self->profdb = talloc_strdup(self->mem_ctx, profdb);

	return PyInt_FromLong(retval);
}

static PyObject *py_profile_get_default(PyObject *_self)
{
	PyMAPIProfileObject	*self = (PyMAPIProfileObject *) _self;
	enum MAPISTATUS		retval;
	char			*profname;
	
	/* Sanity checks */
	if (!self->mapi_ctx) {
		return NULL;
	}

	retval = GetDefaultProfile(self->mapi_ctx, &profname);
	if (retval != MAPI_E_SUCCESS) {
		return NULL;
	}

	return PyString_FromString(profname);
}

static int py_profile_set_default(PyObject *_self, PyObject *value)
{
	PyMAPIProfileObject	*self = (PyMAPIProfileObject *) _self;
	enum MAPISTATUS		retval;
	const char		*profname;

	/* Sanity checks */
	if (!self->mapi_ctx) {
		return -1;
	}

	if (!PyArg_Parse(value, "s", &profname)) {
		return -1;
	}

	retval = SetDefaultProfile(self->mapi_ctx, profname);
	return retval;
}

static PyMethodDef profile_methods[] = {
	{ "newdb", (PyCFunction) py_profile_newdb, METH_VARARGS },
	{ NULL },
};

static PyGetSetDef profile_getsetters[] = {
	{ "username", NULL, NULL },
	{ "password", NULL, NULL },
	{ "default_profile", (getter)py_profile_get_default, 
	  (setter)py_profile_set_default },
	{ "profile", NULL, NULL },
	{ "address", NULL, NULL },
	{ "domain", NULL, NULL },
	{ "realm", NULL, NULL },
	{ "exchange_version", NULL, NULL },
	{ "encrypt", NULL, NULL },
	{ "language", NULL, NULL },
	{ "dump_data", NULL, NULL },
	{ "debuglevel", NULL, NULL },
	{ NULL }
};

PyTypeObject PyMAPIProfileType = {
	PyObject_HEAD_INIT(NULL) 0,
	.tp_name = "Profile",
	.tp_basicsize = sizeof(PyMAPIProfileObject),
	.tp_methods = profile_methods,
	.tp_getset = profile_getsetters,
	.tp_doc = "MAPI Profile",
	.tp_new = py_profile_new,
	.tp_flags = Py_TPFLAGS_DEFAULT,
};
