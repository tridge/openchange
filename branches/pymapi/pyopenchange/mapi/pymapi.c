/*
   OpenChange MAPI implementation.

   Python interface to openchange

   Copyright (C) Julien Kerihuel 2010.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Python.h>
#include <libmapi/libmapi.h>
#include "pyopenchange/mapi/pymapi.h"

void initmapi(void);

static PyObject *py_MAPI_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
	TALLOC_CTX		*mem_ctx;
	struct mapi_context	*mapi_ctx;
	PyMAPIObject		*mapi_obj;
	char			*kwnames[] = { "profdb", NULL };
	const char		*profdb = NULL;
	enum MAPISTATUS		retval;

	mem_ctx = talloc_new(NULL);
	if (mem_ctx == NULL) {
		PyErr_NoMemory();
		return NULL;
	}

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "|s", kwnames, &profdb)) {
		profdb = talloc_asprintf(mem_ctx, DEFAULT_PROFDB, getenv("HOME"));
		retval = MAPIInitialize(&mapi_ctx, profdb);
		talloc_free((char *)profdb);
	} else {
		retval = MAPIInitialize(&mapi_ctx, profdb);
	}
	
	if (retval != MAPI_E_SUCCESS) {
		return NULL;
	}

	mapi_obj = PyObject_New(PyMAPIObject, &PyMAPI);
	mapi_obj->mem_ctx = mem_ctx;
	mapi_obj->mapi_ctx = mapi_ctx;

	return (PyObject *) mapi_obj;
}

static void py_MAPI_dealloc(PyObject *_self)
{
	PyMAPIObject	*self = (PyMAPIObject *)_self;

	MAPIUninitialize(self->mapi_ctx);
	talloc_free(self->mem_ctx);
	PyObject_Del(_self);
}

static PyMethodDef mapi_methods[] = {
	{ NULL },
};

static PyGetSetDef mapi_getsetters[] = {
	{ NULL },
};

PyTypeObject PyMAPI = {
	PyObject_HEAD_INIT(NULL) 0,
	.tp_name = "mapi",
	.tp_basicsize = sizeof (PyMAPIObject),
	.tp_methods = mapi_methods,
	.tp_getset = mapi_getsetters,
	.tp_doc = "MAPI API",
	.tp_new = py_MAPI_new,
	.tp_dealloc = (destructor) py_MAPI_dealloc,
	.tp_flags = Py_TPFLAGS_DEFAULT,
};

static PyMethodDef py_mapi_global_methods[] = {
	{ NULL }
};

void initmapi(void)
{
	PyObject	*m;

	if (PyType_Ready(&PyMAPI) < 0) {
		return;
	}

	if (PyType_Ready(&PyMAPIProfileType) < 0)
		return;

	m = Py_InitModule3("mapi", py_mapi_global_methods,
			   "Python interface to OpenChange MAPI");

	if (m == NULL) {
		return;
	}

	Py_INCREF(&PyMAPI);
	PyModule_AddObject(m, "mapi", (PyObject *)&PyMAPI);

	Py_INCREF((PyObject *)&PyMAPIProfileType);
	PyModule_AddObject(m, "Profile", (PyObject *)&PyMAPIProfileType);
}
