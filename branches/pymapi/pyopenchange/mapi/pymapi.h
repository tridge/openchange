/*
   OpenChange MAPI implementation.

   Python interface to openchange

   Copyright (C) Julien Kerihuel 2010.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef	__PYMAPI_H_
#define	__PYMAPI_H_

#include <Python.h>
#include <libmapi/libmapi.h>

/* mapi.Profile */
PyAPI_DATA(PyTypeObject) PyMAPIProfileType;
PyAPI_DATA(PyTypeObject) PyProfileType;

typedef struct {
	PyObject_HEAD
	TALLOC_CTX		*mem_ctx;
	struct mapi_context	*mapi_ctx;
} PyMAPIObject;

PyAPI_DATA(PyTypeObject)	PyMAPI;

#define DEFAULT_PROFDB	"%s/.openchange/profiles.ldb"

#endif /* ! __PYMAPI_H_ */
