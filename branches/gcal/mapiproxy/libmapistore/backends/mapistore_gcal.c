/*
   OpenChange Storage Abstraction Layer library
   MAPIStore SQLite backend

   OpenChange Project

   Copyright (C) Julien Kerihuel 2009

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mapistore_gcal.h"


/**
   \details Initialize gcall mapistore backend

   \return MAPISTORE_SUCCESS on success
 */


static int gcal_init(void)
{
	DEBUG(0, ("gcal backend initialized\n"));

	return MAPISTORE_SUCCESS;
}

static int gcal_create_context(TALLOC_CTX *mem_ctx, const char *uri, void **private_data)
{
	return MAPISTORE_SUCCESS;
}


/**
   \details Delete a connection context from the fsocpf backend

   \param private_data pointer to the current fsocpf context

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE_ERROR
 */
static int gcal_delete_context(void *private_data)
{
	return MAPISTORE_SUCCESS;
}

/**
   \details Create a folder in the fsocpf backend
   
   \param private_data pointer to the current fsocpf context

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE_ERROR
 */
static int gcal_op_mkdir(void *private_data, uint64_t parent_fid, uint64_t fid,
			   struct SRow *aRow)
{
	return MAPISTORE_SUCCESS;
}

static int gcal_op_rmdir(void *private_data, uint64_t parent_fid, uint64_t fid)
{
	return MAPISTORE_SUCCESS;
}

/**
   \details Open a folder from the fsocpf backend

   \param private_data pointer to the current fsocpf context
   \param parent_fid the parent folder identifier
   \param fid the identifier of the colder to open

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE_ERROR
 */
static int gcal_op_opendir(void *private_data, uint64_t parent_fid, uint64_t fid)
{
	return MAPISTORE_SUCCESS;
}


/**
   \details Close a folder from the fsocpf backend

   \param private_data pointer to the current fsocpf context

   \return MAPISTORE_SUCCESS on success, otherwise MAPISTORE_ERROR
 */
static int gcal_op_closedir(void *private_data)
{
	return MAPISTORE_SUCCESS;
}

/**
   \details Entry point for mapistore SQLite backend

   \return MAPISTORE_SUCCESS on success, otherwise -1
 */
int mapistore_init_backend(void)
{
	struct mapistore_backend	backend;
	int				ret;

	/* Fill in our name */
	backend.name = "gcal";
	backend.description = "gcal backend";
	backend.namespace = "gcal://";

	/* Fill in all the operations */
	backend.init = gcal_init;
	backend.create_context = gcal_create_context;
	backend.delete_context = gcal_delete_context;
	backend.op_mkdir = gcal_op_mkdir;
	backend.op_rmdir = gcal_op_rmdir;
	backend.op_opendir = gcal_op_opendir;
	backend.op_closedir = gcal_op_closedir;

	/* Register ourselves with the MAPISTORE subsystem */
	ret = mapistore_backend_register(&backend);
	if (ret != MAPISTORE_SUCCESS) {
		DEBUG(0, ("Failed to register the '%s' mapistore backend!\n", backend.name));
		return ret;
	}

	return MAPISTORE_SUCCESS;
}
